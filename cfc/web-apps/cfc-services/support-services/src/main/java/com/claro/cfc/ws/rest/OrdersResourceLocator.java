package com.claro.cfc.ws.rest;

import com.claro.cfc.db.dao.UsersAccess;
import com.claro.cfc.db.dto.LogAction;
import com.claro.cfc.db.dto.enums.UserLogDiscriminator;
import com.claro.cfc.db.dto.enums.UserStatusEnum;
import com.claro.cfc.enviroment.Environment;
import com.claro.cfc.enviroment.Environments;
import com.claro.cfc.utils.alert.SystemAlerts;
import com.claro.cfc.utils.net.RequestUtil;
import com.claro.cfc.ws.rest.auth.SecuredScope;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * Creaed by Jansel R. Abreu (Vanwolf) on 4/30/14.
 * <p/>
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad. Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad.Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * <p/>
 * <p/>
 */

@SecuredScope("order")
public class OrdersResourceLocator {
    private static final Logger log = Logger.getLogger(OrdersResourceLocator.class.getName());
    private static Environment env = Environments.getDefault();

    private String card;

    public OrdersResourceLocator(String card) {
        this.card = card;
    }

    @GET
    @Produces({"application/json", "application/xml"})
    public Response getIncomingDatasByCard(@QueryParam("imei") final String imei, @Context HttpHeaders headers) {
        log.info("imei: " + imei + ", card: " + card);
        String response;

        if(UserStatusEnum.ACTIVE.ordinal() != UsersAccess.getUserStatus(card) )
            return Response.status(Response.Status.UNAUTHORIZED).build();

        try {
            response = new RequestUtil().retrieveGetContent(env.saydotRetrieveOrders() + card);
            UsersAccess.logUser(card, LogAction.GET_INCOMING_DATA, response, imei, UserLogDiscriminator.DEVICE.toString());

            lightweightErrorCheck(response);

        } catch (Exception ie) {
            throw new WebApplicationException(ie);
        }
        log.info("Trying to return returned-orders: " + response);
        return Response.ok(response).build();
    }

    /**
     * PAWapi was found too greedy inF memory consumption, for the moment this functionality is disabled.
     */
    // DON'T do this unless you want to save memory and performance footprint
//    @Deprecated
    private void lightweightErrorCheck(String response){
        try {
            int index = response.indexOf("hasError");
            String hasError = response.substring(index, index + 15);
            if (null != hasError && hasError.contains("true")) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            SystemAlerts.emit("email.SAYDOT_DOWN", Environments.getDefault().toString());
                        }catch( Exception ex ){
                            log.error("",ex);
                        }
                    }
                }).start();
            }
        }catch( Exception e){
            log.error("",e);
        }
    }
}
