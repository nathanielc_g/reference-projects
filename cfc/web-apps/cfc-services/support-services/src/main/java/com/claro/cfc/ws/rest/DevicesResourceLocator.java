package com.claro.cfc.ws.rest;

import com.claro.cfc.db.dao.ApplicationAccess;
import com.claro.cfc.db.dao.TerminalAccess;
import com.claro.cfc.db.dao.UsersAccess;
import com.claro.cfc.db.dto.enums.UserStatusEnum;
import com.claro.cfc.ws.rest.auth.SecuredScope;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * Created by Nathaniel Calderon on 6/15/2016.
 */
@SecuredScope("device")
public class DevicesResourceLocator {

    private static final Logger log = Logger.getLogger(OrdersResourceLocator.class.getName());
    private static Gson gson = new Gson();

    private String card;

    public DevicesResourceLocator(String card) {
        this.card = card;
    }


    @POST
    @Path("/{imei}/updateVersion")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response updateVersion(@PathParam("imei") String imei, String jsonRequest, @Context HttpHeaders headers) {
        log.info("IMEI: " + imei + ", JsonRequest: " + jsonRequest);
        //final long active = 1l;
        //if (active != UsersAccess.getUserStatus(card))
        if (UserStatusEnum.ACTIVE.ordinal() != UsersAccess.getUserStatus(card))
            return Response.status(Response.Status.UNAUTHORIZED).build();

        boolean canContinue = true;
        String okMessage = "";
        long saved = 0;
        double versionCode = 0;
        try{
            JsonObject requestData = gson.fromJson(jsonRequest, JsonObject.class);
            versionCode = Double.parseDouble(requestData.get("appVersion").getAsString());
            double currentVersion = Double.parseDouble(ApplicationAccess.getVersion());
            //currentVersion < versionCode ||
            if (versionCode < 0)
                canContinue = false;
        } catch (Exception ex) {
            log.info("Error trying to parse Version App.", ex);
            canContinue = false;
        }

        if (!canContinue) {
            saved = -1;
            okMessage = "Version invalida.";
        }

        if(canContinue) {
            log.info("Try to updateVersion to Termianl: "+ imei);
            TerminalAccess.updateAppVersion(imei, versionCode);
            saved = 1;
            okMessage = "Updated ok.";
        }

        StringBuilder builder = new StringBuilder();
        builder.append("{\"ok\":")
                .append(0 < saved)
                .append(",\"okMessage\":\"")
                .append(okMessage)
                .append("\",\"okCode\":")
                .append(saved)
                .append("}");

        //UsersAccess.logUser(card, LogAction.DEVICE_APP_UPDATE, "Intento de actualizar version de app del dispositivo: " + builder.toString(), imei, UserLogDiscriminator.DEVICE.toString());

        return Response.ok(builder.toString()).build();
    }
}
