package com.claro.cfc.ws.rest;

import com.claro.cfc.enviroment.Environment;
import com.claro.cfc.enviroment.Environments;
import com.claro.cfc.utils.net.RequestUtil;
import com.claro.cfc.ws.rest.auth.SecuredScope;
import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.*;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 3/16/2015.
 */

@Path("/ext")
@SecuredScope("ext")
public class Extension {

    private static final Logger log = Logger.getLogger(OrdersResourceLocator.class.getName());
    private static Environment env = Environments.getDefault();

    private static final String GEOCODE_ENDPOINT = "http://maps.googleapis.com/maps/api/geocode/json";

    private Geocoder geocoder;

    @GET
    @Path("/domain/frames")
    @Produces({"application/json"})
    public Response getDomainFrames() {
        String response;
        try {

            String url = env.getDomainFrames();
            log.info( "Accessing Frame from endpoint: "+url );
            response = new RequestUtil().retrieveGetContent(url);
        } catch (Exception ie) {
            throw new WebApplicationException(ie);
        }
        log.info("Trying to return returned-locations: " + response);
        return Response.ok(response).build();
    }

    @GET
    @Path("/domain/v2/frames")
    @Produces({"application/json"})
    public Response getDomainV2Frames() {
        String response;
        try {

            String url = env.getDomainV2Frames();
            log.info( "Accessing Frame from endpoint: "+url );
            response = new RequestUtil().retrieveGetContent(url);
        } catch (Exception ie) {
            throw new WebApplicationException(ie);
        }
        log.info("Trying to return returned-locations: " + response);
        return Response.ok(response).build();
    }

    @GET
    @Path("/domain/v3/frames")
    @Produces({"application/json"})
    public Response getDomainV3Frames() {
        String response;
        try {

            String url = env.getDomainV3Frames();
            log.info( "Accessing Frame from endpoint: "+url );
            response = new RequestUtil().retrieveGetContent(url);
        } catch (Exception ie) {
            throw new WebApplicationException(ie);
        }
        log.info("Trying to return returned-locations: " + response);
        return Response.ok(response).build();
    }

    @GET
    @Path("/domain/v4/frames")
    @Produces({"application/json"})
    public Response getDomainV4Frames() {
        String response;
        try {

            String url = env.getDomainV4Frames();
            log.info( "Accessing Frame from endpoint: "+url );
            response = new RequestUtil().retrieveGetContent(url);
        } catch (Exception ie) {
            throw new WebApplicationException(ie);
        }
        log.info("Trying to return returned-locations: " + response);
        return Response.ok(response).build();
    }

    @GET
    @Path("/location/{lat},{lng}/address")
    @Produces( {"application/json"} )
    public Response getLocationAddress( @PathParam("lat") String lat, @PathParam("lng") String lng ){
        if( null == geocoder )
            geocoder = new Geocoder();
        LatLng location = new LatLng(lat,lng);
        GeocoderRequest request = new GeocoderRequestBuilder().setLocation(location).getGeocoderRequest();
        try{
            GeocodeResponse response = geocoder.geocode(request);
            List<GeocoderResult> results = response.getResults();
            if( null != results && 0<results.size()){
                GeocoderResult result = results.get(0);
                StringBuilder builder = new StringBuilder();

                for(GeocoderAddressComponent comp :  result.getAddressComponents() ){
                    builder.append( comp.getShortName() ).append(", ");
                }
                return Response.ok( "{ \"address\":\""+builder.toString()+"\"}" ).build();
            }
        }catch( Exception ex ){
            log.error( ex );
        }
        return Response.ok( "{ }" ).build();
    }


    @GET
    @Path("/location/geocode")
    @Produces( {"application/json"} )
    public Response getGeocodeAddress(@Context UriInfo uriInfo ){
        final String query = uriInfo.getRequestUri().getQuery();
        String response;
        final StringBuilder out = new StringBuilder(GEOCODE_ENDPOINT);
        try{
            if( null != query )
                out.append( "?" ).append(query);
            final String url = out.toString();
            log.info( "Getting geocode location as endpoint "+url );
            response = new RequestUtil().retrieveGetContent(url);
        }catch(Exception ex){
            log.error(ex);
            response = "{ \"error_message\":\"Invalid request.\" }";
        }
        log.info("Trying to return geocode-result: " + response);
        return Response.ok(response).build();
    }

}
