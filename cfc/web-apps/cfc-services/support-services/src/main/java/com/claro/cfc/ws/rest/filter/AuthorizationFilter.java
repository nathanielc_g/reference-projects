package com.claro.cfc.ws.rest.filter;

import com.claro.cfc.ws.rest.auth.AuthType;
import com.claro.cfc.ws.rest.auth.AuthorizationHandler;
import com.claro.cfc.ws.rest.auth.Secured;
import com.claro.cfc.ws.rest.auth.handlers.OAuthHandler;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import org.apache.log4j.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 * Created by Jansel Valentin on 11/8/2016.
 **/

@Provider
public class AuthorizationFilter implements ContainerRequestFilter{
    private static final Logger log = Logger.getLogger(AuthorizationFilter.class);

    private AbstractMethod method;
    private AuthorizationHandler handler;

    public AuthorizationFilter() {
    }

    public AuthorizationFilter(AbstractMethod method) {
        this.method = method;
    }

    /**
     *
     * @param request expected contain "Authorization" header with the access_token returned
     * @return
     */
    @Override
    public ContainerRequest filter(ContainerRequest request) {
        if( null != method ){
            Secured secured;
            // based on filter factory only protected method can have this filters installed.
            if(null != (secured = getSecuredAnnotation(method))){
                log.info( "Validating protected resource at "+request.getMethod()+" "+request.getAbsolutePath() );

                if( null != (handler = getHandler(secured.authType()))) {
                    log.info( "Using "+handler.getType()+" handler for authorization" );
                    if (!handler.authorize(request, secured)) {
                        Response response = Response
                                .status(Response.Status.UNAUTHORIZED)
                                .type(MediaType.APPLICATION_JSON)
                                .entity("{\"error\":\"invalid access_token\"}")
                                .build();
                        throw new WebApplicationException(response);
                    }
                }
            }
        }
        return request;
    }

    private Secured getSecuredAnnotation(AbstractMethod method){
        if( null != method ){
            return method.getMethod().getAnnotation(Secured.class);
        }
        return null;
    }

    /**
     * Only supported oauth handler
     */
    private AuthorizationHandler getHandler(AuthType type){
        if( null == handler ){
            synchronized (this){
                if( null == handler ){
                    if( AuthType.OAUTH == type )
                        handler = new OAuthHandler();
                }
            }
        }
        return handler;
    }
}
