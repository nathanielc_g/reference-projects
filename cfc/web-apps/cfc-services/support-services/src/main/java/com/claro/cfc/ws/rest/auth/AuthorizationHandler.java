package com.claro.cfc.ws.rest.auth;

import com.sun.jersey.spi.container.ContainerRequest;

/**
 * Created by Jansel Valentin on 11/8/2016.
 **/
public interface AuthorizationHandler {
    boolean authorize(ContainerRequest request, Secured requires);
    AuthType getType();
}
