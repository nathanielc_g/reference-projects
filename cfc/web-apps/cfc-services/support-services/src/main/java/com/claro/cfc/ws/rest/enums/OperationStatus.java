package com.claro.cfc.ws.rest.enums;

/**
 * Created by Nathaniel Calderon on 7/12/2017.
 */
public enum OperationStatus {
    DATA_FORMAT_ERROR(-1),
    TRANSACTION_ERROR(-2),
    OPERATION_DISABLED(-3),
    VERSION_OUT_OF_DATE_ERROR(-4),
    UNATHORIZED(-5),
    ENABLE_TO_SAVE_ACTION(-6);

    private int code;
    OperationStatus(int code) {
        this.code = code;
    }

    public int code() {
        return code;
    }
}
