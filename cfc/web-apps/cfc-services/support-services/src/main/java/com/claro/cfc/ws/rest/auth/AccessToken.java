package com.claro.cfc.ws.rest.auth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jansel Valentin on 11/8/2016.
 **/
public class AccessToken {

    /**
     * The lifetime of this token expressed in seconds.
     */
    @SerializedName("expires_in")
    public int expiresIn;

    /**
     * Type of the token the client will receive
     */
    @SerializedName("token_type")
    public String type = Type.BEARER.toString();

    /**
     * The access token the client is gonna use to access to services
     */
    @SerializedName("access_token")
    public String accessToken;

    public enum Type{
        BEARER{
            @Override
            public String toString() {
                return "Bearer";
            }
        }
    }
}
