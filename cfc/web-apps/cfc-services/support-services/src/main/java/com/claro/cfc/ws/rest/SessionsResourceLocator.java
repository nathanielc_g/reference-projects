package com.claro.cfc.ws.rest;

import com.claro.cfc.db.dao.DeviceSessions;
import com.claro.cfc.db.dto.DeviceSession;
import com.claro.cfc.ws.rest.auth.SecuredScope;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Jansel Valentin on 2/15/2016.
 **/
@SecuredScope("session")
public class SessionsResourceLocator {

    private static final Logger log = Logger.getLogger(OrdersResourceLocator.class.getName());
    private static Gson gson = new Gson();

    private String card;

    public SessionsResourceLocator(String card) {
        this.card = card;
    }


    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response get(@QueryParam("imei") String imei) {
        return Response.ok(gson.toJson(DeviceSessions.getUserSessions(card))).build();
    }


    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response save(String jsonSession, @QueryParam("imei") String imei) {
        log.info("Trying to save device session user=" + card + ", device=" + imei + ",sessions=" + jsonSession);

        DeviceSession session = new DeviceSession();
        session.setCreated(new Timestamp(new Date().getTime()));
        session.setDeviceImei(imei);
        session.setUserCard(card);
        session.setSessionJson(jsonSession.trim());
        try {
            DeviceSessions.saveSession(session);

        } catch (Exception ex) {
            log.error("", ex);
            throw new WebApplicationException(ex);
        }
        return Response.ok().build();
    }
}
