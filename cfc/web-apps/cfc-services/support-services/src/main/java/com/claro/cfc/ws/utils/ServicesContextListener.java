package com.claro.cfc.ws.utils;

import com.claro.api.client.paw.PAWAPIClient;
import com.claro.cfc.enviroment.Environment;
import com.claro.cfc.enviroment.Environments;
import com.claro.cfc.utils.alert.AlertEmitters;
import com.claro.cfc.utils.alert.paw.Emailer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by Jansel Valentin on 2/15/2016.
 **/
public class ServicesContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        Environment env = Environments.getDefault();
        PAWAPIClient.ENV pawEnv = Environment.DEVELOPMENT == env ? PAWAPIClient.ENV.DEVELOPMENT : PAWAPIClient.ENV.PRODUCTION;

        AlertEmitters.registerEmitter("e-mailer", new Emailer(PAWAPIClient.getInstance(pawEnv)));
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        AlertEmitters.unregister("e-mailer");
    }
}
