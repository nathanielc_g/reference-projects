package com.claro.cfc.ws.rest.auth.handlers;

import com.claro.cfc.db.dao.OAuthAccess;
import com.claro.cfc.ws.rest.auth.AuthType;
import com.claro.cfc.ws.rest.auth.AuthorizationHandler;
import com.claro.cfc.ws.rest.auth.Secured;
import com.sun.jersey.spi.container.ContainerRequest;

import javax.ws.rs.core.HttpHeaders;

/**
 * Created by Jansel Valentin on 11/8/2016.
 **/
public class OAuthHandler implements AuthorizationHandler{

    public OAuthHandler() {
    }

    @Override
    public boolean authorize(ContainerRequest request, Secured requires) {
        if( null == request || null == requires )
            return false;
        String methodScope = requires.scope();
        String accessToken = request.getHeaderValue(HttpHeaders.AUTHORIZATION);

        if(!OAuthAccess.isTokenExpired(accessToken)){
            String clientScopes = OAuthAccess.getClientScopesByAccessToken(accessToken);
            if(null != clientScopes){
                String[] scopes = clientScopes.split(",");
                for( String scope : scopes ) {
                    scope = scope.trim();
                    if (scope.equalsIgnoreCase(methodScope) || scope.equalsIgnoreCase("all"))
                        return true;
                }
            }
        }
        return false;
    }

    @Override
    public AuthType getType() {
        return AuthType.OAUTH;
    }
}
