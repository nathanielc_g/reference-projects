package com.claro.cfc.ws.rest;

import com.claro.cfc.db.dao.UsersAccess;
import com.claro.cfc.db.dto.LogAction;
import com.claro.cfc.db.dto.enums.UserLogDiscriminator;
import com.claro.cfc.db.dto.enums.UserStatusEnum;
import com.claro.cfc.db.dto.UsersEntity;
import com.claro.cfc.ws.rest.auth.SecuredScope;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;


/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/1/14.
 * <p/>
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad. Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad.Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 */
@SecuredScope("login")
public class LoginResourceLocator {
    private static final Logger log = Logger.getLogger(LoginResourceLocator.class);

    private String card;

    public LoginResourceLocator(String card) {
        this.card = card;
    }


    @GET
    @Produces({"application/json", "application/xml"})
    public Response loginGet(@QueryParam("imei") String imei, @Context HttpHeaders headers) {
        return login(card, imei);
    }


    private Response login(String card, String imei) {

        String jsonResponse = "{\"ok\": false}";
        log.info("trying to login user");

        String msg = "User Logged can't be logged with card: " + card + " and imei: " + imei;

        UsersEntity user = UsersAccess.getUserByCardAndImei(card, imei);
        //final long active = 1L;

        //if (null != user && active == user.getStatus()) {
        if (null != user && UserStatusEnum.ACTIVE.ordinal() == user.getStatus()) {
            log.info("login OK");
            jsonResponse = "{\"ok\": true}";
            msg = "User Logged correctly with card: " + card + "; and imei: " + imei;
        }

        log.info( msg );
        UsersAccess.logUser(card, LogAction.LOGIN, msg, imei, UserLogDiscriminator.DEVICE.toString());
        return Response.ok(jsonResponse).build();
    }
}
