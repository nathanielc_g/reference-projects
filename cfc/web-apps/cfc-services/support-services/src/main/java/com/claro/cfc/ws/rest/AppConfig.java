package com.claro.cfc.ws.rest;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.db.dao.ApplicationAccess;
import com.claro.cfc.db.dto.enums.AppConfigEnum;
import com.claro.cfc.ws.rest.auth.SecuredScope;
import com.claro.cfc.ws.rest.auth.Secured;
import com.claro.cfc.ws.utils.AppUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.Work;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;

/**
 * Created by Nathaniel Calderon on 9/2/2016.
 */

@Path("/app_config")
@SecuredScope("app")
public class AppConfig {

    private static final Logger log = Logger.getLogger(AppConfig.class);
    private static Gson gson = new Gson();

    @GET
    @Path("/db_connection")
    @Produces({"application/json","application/xml"})
    @Secured( scope = "app.db_connection" )
    public Response getDbConnection() {
        String dbUrlConnection = ApplicationAccess.getUrlConnection();
        StringBuilder builder = new StringBuilder();
        builder.append("{\"ok\":")
                .append(!dbUrlConnection.isEmpty())
                .append(",\"data\":\"")
                .append(dbUrlConnection)
                .append("\"}");
        return Response.ok(builder.toString()).build();
    }

    @GET
    @Path("/server_time")
    @Produces({"application/json","application/xml"})
    @Secured( scope = "app.server_time" )
    public Response getServerTime (){

        String timeup = Calendar.getInstance().getTime().toString();
        StringBuilder builder = new StringBuilder();
        builder.append("{\"ok\":")
                .append(!timeup.isEmpty())
                .append(",\"data\":\"")
                .append(timeup)
                .append("\"}");
        return Response.ok(builder.toString()).build();
    }

    @GET
    @Path("/wakeup")
    @Produces({"application/json","application/xml"})
    @Secured( scope = "app.server_wakeup" )
    public Response wakeup (){

        String timeup = ApplicationAccess.getAvailableTime();
        StringBuilder builder = new StringBuilder();
        builder.append("{\"ok\":")
                .append(!timeup.isEmpty())
                .append(",\"data\":\"")
                .append(timeup)
                .append("\"}");
        return Response.ok(builder.toString()).build();
    }

    @POST
    @Path("/wakeup")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({"application/json","application/xml"})
    @Secured( scope = "app.server_wakeup" )
    public Response updateWakeup (String jsonRequest){
        log.info("Attempt to update ConfigValue to: " + jsonRequest);

        boolean canUpdate = true;
        String errorMessage = "";
        String timeup = ApplicationAccess.getAvailableTime();
        StringBuilder newTimeup = new StringBuilder();
        if (timeup.isEmpty()) {
            return writeResponse(false, "", "Available Time config wasn't be found.");
        }

        String split[] = timeup.split(",");

        try {
            JsonObject requestData = gson.fromJson(jsonRequest, JsonObject.class);
            if (!requestData.has("ENABLE")) {
                throw new  JsonSyntaxException("Unexpected or wrong JsonObject.");
            }
            split[0]=requestData.get("ENABLE").getAsString();
            if (requestData.has("BEGIN") && requestData.has("END"))  {
                split[1]=requestData.get("BEGIN").getAsString();
                split[2]=requestData.get("END").getAsString();
            }
            canUpdate = true;
            newTimeup.append(split[0])
                    .append(",")
                    .append(split[1])
                    .append(",")
                    .append(split[2]);
        } catch (JsonSyntaxException ex) {
            canUpdate = false;
            errorMessage += ex.getMessage();
        } catch (NullPointerException ex) {
            canUpdate = false;
            errorMessage = "Null required member. \n";
            errorMessage += ex.getMessage();
        } catch (Exception ex) {
            canUpdate = false;
            errorMessage += ex.getMessage();
        }

        if (canUpdate)
            ApplicationAccess.updateAppConfig(AppConfigEnum.AVAILABLE_TIME.toString(), newTimeup.toString());


        return writeResponse(canUpdate, newTimeup.toString(), errorMessage);
        //return Response.ok(ApplicationAccess.getAvailableTime()).build();
    }

    @POST
    @Path("/reload_cache")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({"application/json","application/xml"})
    @Secured( scope = "app.reload_cache" )
    public Response reloadCache(String jsonRequest){
        log.info("Attempt to reload cache");
        AppUtils.reloadCache();
        return writeResponse(true, "", "");
        //return Response.ok(ApplicationAccess.getAvailableTime()).build();
    }


    @POST
    @Path("/execute_query")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({"application/json","application/xml"})
    @Secured( scope = "app.execute_query" )
    public Response executeQuery (String jsonRequest){
        log.info("Attempt to execute_query: " + jsonRequest);
        String query = "";
        try {
            JsonObject requestData = gson.fromJson(jsonRequest, JsonObject.class);
            query = requestData.get("query").getAsString();
        } catch (Exception ex){
            log.info("Error trying to parse json.", ex);
        }
        int updated = 0;
        if (!query.isEmpty())
            updated = ApplicationAccess.executeQuery(query);
        return writeResponse(updated > 0, "Rows updated: " + updated, "");
        //return Response.ok(ApplicationAccess.getAvailableTime()).build();
    }

    private Response writeResponse (boolean ok, String data, String errorMessage) {
        StringBuilder builder = new StringBuilder();
        builder.append("{\"ok\":")
                .append(ok)
                .append(",\"data\":\"")
                .append(data)
                .append("\",\"errorMessage\":\"")
                .append(errorMessage)
                .append("\"}");
        return Response.ok(builder.toString()).build();
    }

}
