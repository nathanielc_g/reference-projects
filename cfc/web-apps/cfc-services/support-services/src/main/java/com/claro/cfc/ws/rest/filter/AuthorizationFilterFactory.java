package com.claro.cfc.ws.rest.filter;

import com.claro.cfc.ws.rest.auth.Secured;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;

import javax.ws.rs.ext.Provider;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jansel Valentin on 11/8/2016.
 **/
@Provider
public class AuthorizationFilterFactory implements ResourceFilterFactory {
    @Override
    public List<ResourceFilter> create(AbstractMethod abstractMethod) {
        return injectFilters(abstractMethod);
    }

    private ResourceFilter createAuthFilter(final AbstractMethod method){
        return new ResourceFilter() {
            @Override
            public ContainerRequestFilter getRequestFilter() {
                return new AuthorizationFilter(method);
            }

            @Override
            public ContainerResponseFilter getResponseFilter() {
                return null;
            }
        };
    }

    private Secured getRequiresAuth(AbstractMethod method){
        if( null != method ){
            return method.getMethod().getAnnotation(Secured.class);
        }
        return null;
    }

    private List<ResourceFilter> injectFilters(AbstractMethod method){
        List<ResourceFilter> filters = new LinkedList<ResourceFilter>();
        if( null != getRequiresAuth(method))    // inject auth filter
            filters.add(createAuthFilter(method));

        return filters;
    }
}
