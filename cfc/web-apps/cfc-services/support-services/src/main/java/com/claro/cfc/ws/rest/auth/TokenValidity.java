package com.claro.cfc.ws.rest.auth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jansel Valentin on 11/8/2016.
 **/
public class TokenValidity {

    /**
     * The number of second left in the lifetime of the token
     */
    @SerializedName("expires_in")
    public Integer expiresIn;

    /**
     * The system target of the token, the one that requested the token
     */
    public String audience;

    /**
     * The list of scopes teh "audience" system consented to, this may be different from the
     * set of scopes configured in the client oauth detail.
     */
    public String scope;

    /**
     * Client id that to which the access_token was issued
     */
    @SerializedName("client_id")
    public String clientId;

}
