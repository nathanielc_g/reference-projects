package com.claro.cfc.ws.rest.auth;

/**
 * Created by Jansel Valentin on 11/8/2016.
 **/
public enum AuthType {
    OAUTH,
    BASIC
}
