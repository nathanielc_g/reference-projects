package com.claro.cfc.ws.rest;

import com.claro.cfc.db.dao.OAuthAccess;
import com.claro.cfc.db.dao.UsersAccess;
import com.claro.cfc.ws.rest.auth.Secured;
import com.claro.cfc.ws.rest.payload.BulkPayload;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/1/14.
 * <p/>
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad. Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad.Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 */

@Path("/technicians")
public class Technicians {
    private static final Logger log = Logger.getLogger(Technicians.class);
    private ExecutorService executor = Executors.newCachedThreadPool();

    @Path("/{card}/login")
    public LoginResourceLocator login(@PathParam("card") String card) {
        return new LoginResourceLocator(card);
    }

    @Path("/{card}/orders")
    public OrdersResourceLocator orders(@PathParam("card") String card) {
        return new OrdersResourceLocator(card);
    }

    @Path("/{card}/actions")
    public ActionsResourceLocator actions(@PathParam("card") String card) {
        return new ActionsResourceLocator(card);
    }

    @Path("/{card}/sessions")
    public SessionsResourceLocator sessions(@PathParam("card") String card) {
        return new SessionsResourceLocator(card);
    }

    @Path("/{card}/devices")
    public DevicesResourceLocator devices(@PathParam("card") String card) {
        return new DevicesResourceLocator(card);
    }


    /**
     * Note: by the nature of this method, it fits here, but when some operation is required on specific
     * user, must be redirected to a resource locator that work on specific users(technician)
     *
     * WARNING:
     * This is a critical operation for delete operation on users with a large data-set in user_log
     * table, it tend to consume so much time, take care who you give access to this method. and when give it
     * the operation must be executed less as possible, preferably 1 shut at night.
     */
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @Secured(scope = "user.bulk")
    public Response bulk(final BulkPayload bulkPayload, @HeaderParam("Authorization") final String accessToken) {
        log.info("bulk: " + bulkPayload + ", issuer token " + accessToken);
        final String bulkCards = bulkPayload.getCards();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                String issuerClient = OAuthAccess.getClientIdByToken(accessToken);
                if( null != accessToken ) {
                    log.info( "scheduling bulk.user for "+issuerClient+", token: "+accessToken );
                    UsersAccess.bulkUsers(issuerClient, bulkCards);
                }else
                    log.warn( "No client system found for token "+accessToken );
            }
        });
        return Response.ok("{\"status\":\"enqueued\"}").build();
    }
}