package com.claro.cfc.ws.rest;

import com.claro.cfc.db.dao.OAuthAccess;
import com.claro.cfc.db.dto.OAuthAccessToken;
import com.claro.cfc.ws.rest.auth.AccessToken;
import com.claro.cfc.ws.rest.auth.TokenValidity;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;

/**
 * Created by Jansel Valentin on 11/8/2016.
 **/

@Path("/oauth")
public class Oauth {

    private static final Logger log = Logger.getLogger(Oauth.class.getName());
    private static Gson gson = new Gson();

    /**
     * request an access_token to access services
     *
     * @param clientId     client_id is given to a new system in order to access resources in CFC
     * @param scope        commas separated operations the client request to perform on CFC
     * @param responseType response authType desired, now supported "token"
     * @return AccessTokenResponse serialized object
     */
    @GET
    @Path("/access_token")
    @Produces({MediaType.APPLICATION_JSON})
    public Response accessToken(@QueryParam("client_id") String clientId,
                                @QueryParam("scope") String scope,
                                @QueryParam("response_type") String responseType) {

        log.info("Getting access token for client_id:" + clientId + ", scope:" + scope + ", responseType:" + responseType);
        if (OAuthAccess.containsClient(clientId)) {
            OAuthAccessToken oauthToken = OAuthAccess.getOrCreateAccessToken(clientId, scope);

            AccessToken accessToken = new AccessToken();
            accessToken.expiresIn   = calculateLifetime(oauthToken.getValidUntil().getTime());
            accessToken.accessToken = oauthToken.getToken();

            return Response.ok(gson.toJson(accessToken)).build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    /**
     * exchange access_token by and authorization token, not supported in simple model
     */
    public Response exchangeTokenByCode() {
        return null;
    }

    /**
     * validate the access_token validity
     *
     * @param accessToken accessToken returned to the client
     * @return
     */
    @GET
    @Path("/validate")
    @Produces({MediaType.APPLICATION_JSON})
    public Response validate(@QueryParam("access_token") String accessToken) {
        log.info("Validating access token :" + accessToken);

        OAuthAccessToken token = OAuthAccess.getAccessToken(accessToken);

        TokenValidity validity = new TokenValidity();
        validity.audience = "null";
        validity.clientId = "null";
        if (null == token) {
            validity.scope = "null";
            validity.expiresIn = 0;
        } else {
            validity.audience = token.getClientId();
            validity.clientId = token.getClientId();
            validity.scope = token.getConsentedScopes();
            validity.expiresIn = calculateLifetime(token.getValidUntil().getTime());
        }
        return Response.ok(gson.toJson(validity)).build();
    }

    private int calculateLifetime(long untilTime){
        Date now = new Date();
        int lifetime = (int) ((untilTime - now.getTime()) / 1E3);
        return lifetime <0 ? 0 : lifetime;
    }

}
