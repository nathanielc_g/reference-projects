package com.claro.cfc.ws.rest.auth;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Jansel Valentin on 11/8/2016.
 **/

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Secured {
    AuthType authType() default  AuthType.OAUTH;
    String scope() default "all";
}
