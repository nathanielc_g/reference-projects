package com.claro.cfc.ws.rest;

import com.claro.cfc.db.dao.ApplicationAccess;
import com.claro.cfc.updater.Updater;
import com.claro.cfc.utils.cache.RefreshableProperty;
import com.claro.cfc.ws.rest.auth.SecuredScope;
import com.claro.cfc.ws.utils.AppUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.Date;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 7/25/2014.
 */

@Path("/versions")
@SecuredScope("version")
public class Versions {

    private static final Logger logger = Logger.getLogger(Versions.class);

    /*private static final RefreshableProperty<String> version = new RefreshableProperty<String>(true,new VersionValueAdapter());*/

    @GET
    @Path("/mobile")
    @Produces({"application/json", "application/xml"})
    public Response active() {
        String updateLink = Updater.getUpdateLink();
        /*String appVersion = version.getValue();*/
        String appVersion = AppUtils.getVersion();

        logger.info("Versions:active( updateLink: " + updateLink + " )");
        logger.info("Versions:active( appVersion: " + appVersion + " )");

        StringBuilder builder = new StringBuilder("{\"version\":");
        builder.append(appVersion)
                .append(",\"updateLink\":\"")
                .append(updateLink).append("\"}");

        return Response.ok(builder.toString()).build();
    }

    /*private static final class VersionValueAdapter implements RefreshableProperty.ValueAdapter<String>{
        @Override
        public String get() {
            final String value = ApplicationAccess.getVersion();
            logger.info( "Refreshing app version value to "+value+", at "+new Date() );
            return value;
        }
    }*/
}
