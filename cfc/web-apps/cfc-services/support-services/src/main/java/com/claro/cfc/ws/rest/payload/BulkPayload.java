package com.claro.cfc.ws.rest.payload;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Jansel Valentin on 11/8/2016.
 **/

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BulkPayload {

    @XmlElement
    private String cards;

    public BulkPayload() {
    }

    public BulkPayload(String cards) {
        this.cards = cards;
    }

    public String getCards() {
        return cards;
    }

    public void setCards(String cards) {
        this.cards = cards;
    }

    @Override
    public String toString() {
        return "BulkPayload{" +
                "cards='" + cards + '\'' +
                '}';
    }
}
