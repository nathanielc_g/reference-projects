package com.claro.cfc.ws.rest;

import com.claro.cfc.db.dao.OrdersActionsAccess;
import com.claro.cfc.db.dao.TaskAccess;
import com.claro.cfc.db.dao.UsersAccess;
import com.claro.cfc.db.dto.*;
import com.claro.cfc.db.dto.enums.TaskStatusEnum;
import com.claro.cfc.db.dto.enums.UserLogDiscriminator;
import com.claro.cfc.db.dto.enums.UserStatusEnum;
import com.claro.cfc.transport.Attribute;
import com.claro.cfc.transport.Attributes;
import com.claro.cfc.ws.rest.auth.SecuredScope;
import com.claro.cfc.ws.rest.enums.OperationStatus;
import com.claro.cfc.ws.utils.AppUtils;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.ref.Reference;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/9/14.
 * <p/>
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad. Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad.Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 */
@SecuredScope("action")
public class ActionsResourceLocator {
    private static final Logger log = Logger.getLogger(ActionsResourceLocator.class.getName());

    private String card;

    public ActionsResourceLocator(String card) {
        this.card = card;
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response saveAction(List<Attribute> attributes, @Context HttpHeaders headers) {
        long start = System.currentTimeMillis();

        attributes.add(Attribute.createAttribute("TARJETA", card));


        OperationStatus operationStatus = canSaveAction();
        if (operationStatus != OperationStatus.ENABLE_TO_SAVE_ACTION)
            return buildResponse(false, operationStatus);

        long saved = OrdersActionsAccess.saveAction(attributes, AppUtils.canPostFormOrderMultipleTimesValue());
        log.info("Atempt to save " + card + ", " + attributes.toString() + ", response " + saved);

        long actionId = saved;
        if (0 < saved) {
            log.info("Preparing to create tasks for actionId: " + actionId);
            Attribute formType = Attributes.find(attributes, "FORM_TYPE");
            if (null == formType) {
                log.info("******* Could not be possible to create task for null form authType on action id " + actionId);
            } else {
                createTasks(actionId, formType.getValue());
                /*createNotification(actionId, formType.getValue());*/
            }
        } else
            log.info("Skipping to create task for " + saved);

        String responseMessage = buildResponseMessage(0 < saved, saved);
        Attribute imei = Attributes.findOrCreate(attributes, "imei", card);
        UsersAccess.logUser(card, LogAction.SAVE_ACTION, "Intento de salvar las facilidades: " + responseMessage, imei.getValue(), UserLogDiscriminator.DEVICE.toString());

        // not a good practice due on data owner, but clean received data helps memory footprint, before return
        attributes.clear();

        long end = System.currentTimeMillis();
        log.info("Save Action Transaction spent " + (TimeUnit.MILLISECONDS.toSeconds(end - start)) + " secs to complete");

        return Response.ok(responseMessage).build();
    }

    /*private void createNotification(long actionId, String formType) {
        log.info("Preparing to create notification for actionId: " + actionId);
        NotificationAccess.saveSMSNotification(card,
                new NotificationBuilder()
                .setMessage(actionId + "-" + formType + ": PROCESADO")
                .setNotifyVia(NotifyViaEnum.SMS)
                .setActionId(actionId)
                .setNotifType(NotificationTypeEnum.TASK_RESULT)
                .build());
    }*/

 /*   private List<Attribute> prepareAttributes(List<Attribute> attributes){
        List<Attribute> preparedAttrs = new ArrayList<Attribute>(attributes);
        *//**
     * For Tasks Scheduler Compatibility
     * {
     *//*
        preparedAttrs.add(Attribute.createAttribute("TARJETA", card));
        *//**
     * }
     * For Tasks Scheduler Compatibility
     *//*
        return preparedAttrs;
    }
    */

    /**
     * Added for scheduler compatibility
     */

    private void createTasks(long actionId, String formType) {
        TaskAccess.createTasks(formType, actionId);
    }

    private OperationStatus canSaveAction() {
        if (UserStatusEnum.ACTIVE.ordinal() != UsersAccess.getUserStatus(card))
            return OperationStatus.UNATHORIZED;
//         Verify if Application is enable to post forms
        if (!AppUtils.isPostFormAvailable())
            return OperationStatus.OPERATION_DISABLED;

        // Verify if User has App updated
        if (!AppUtils.isUpToDate(card))
            return OperationStatus.VERSION_OUT_OF_DATE_ERROR;


        return OperationStatus.ENABLE_TO_SAVE_ACTION;
    }

    private Response buildResponse(boolean ok, OperationStatus code) {
        if (OperationStatus.UNATHORIZED == code)
            return Response.status(Response.Status.UNAUTHORIZED).build();

        return Response.ok(buildResponseMessage(ok, code.code())).build();
    }

    private String buildResponseMessage(boolean ok, long code) {
        StringBuilder builder = new StringBuilder();
        builder.append("{\"ok\":")
                .append(ok)
                .append(",\"okCode\":")
                .append(code)
                .append("}");
        return builder.toString();
    }


    @GET
    @Path("/{code}/wasSubmitted")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response wasSubmitted(@PathParam("code") String code, @QueryParam("q") String type, @Context HttpHeaders headers) {
        log.info("card: " + card + ", code: " + code + ", authType: " + type);

        boolean response;

        UsersEntity user = UsersAccess.getUserByCard(card);
        if (null == user)
            response = true;
        else {
            if (user.getStatus() == UserStatusEnum.ACTIVE.ordinal()) {
                ActionsEntity action = OrdersActionsAccess.getLastAction(card, code, type);
                response = null != action && action.getProcessed() != 0;
                if(response){
                    if(!action.getHandOperatedBy().equalsIgnoreCase("0")
                            || !action.getProcessedBy().equalsIgnoreCase("0"))
                        response = true;
                    else
                        response = false;

                    if (!response)
                        response = OrdersActionsAccess.wasActionProcessed(action);

                }
            } else {
                response = true;
            }
        }

        StringBuilder builder = new StringBuilder();
        builder.append("{\"submitted\":")
                .append(response)
                .append("}");

        return Response.ok(builder.toString()).build();
    }


    @GET
    @Path("/{code}/lastAction")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response lastAction(@PathParam("code") String code, @QueryParam("orderType") String orderType, @Context HttpHeaders headers) {
        log.info("card: " + card + ", code: " + code + ", orderType: " + orderType);
        final Map<String, String> map = new TreeMap<String, String>();
        Reference<List<ActionValuesEntity>> actionValues = OrdersActionsAccess.getActionValuesByCode(Long.parseLong(code), orderType);
        final List<ActionValuesEntity> values;

        if (null != actionValues && null != actionValues.get())
            values = actionValues.get();
        else
            values = Collections.EMPTY_LIST;

        if (null != values) {
            for (ActionValuesEntity value : values) {
                map.put(value.getAkey(), value.getAvalue());
            }
            values.clear();
        }

        Gson gson = new Gson();

        String jsonResponse = gson.toJson(map);
        map.clear();
        /*StringBuilder builder = new StringBuilder();
        builder.append("{\"response_data\":")
                .append(map)
                .append("}");*/
        return Response.ok(jsonResponse).build();
    }
}
