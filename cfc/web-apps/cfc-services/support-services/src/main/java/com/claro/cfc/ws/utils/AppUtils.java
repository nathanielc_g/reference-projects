package com.claro.cfc.ws.utils;

import com.claro.cfc.db.dao.ApplicationAccess;
import com.claro.cfc.db.dao.TerminalAccess;
import com.claro.cfc.db.dao.UsersAccess;
import com.claro.cfc.db.dto.TerminalEntity;
import com.claro.cfc.db.dto.UsersEntity;
import com.claro.cfc.updater.Updater;
import com.claro.cfc.utils.cache.RefreshableProperty;
import org.apache.log4j.Logger;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Nathaniel Calderon on 9/28/2016.
 */
public final class AppUtils {
    private static final Logger log = Logger.getLogger(AppUtils.class.getName());
    private static boolean canPostFormOrderMultipleTimesValue;
    private static String uptime;
    private static String version;
    /*private static final RefreshableProperty<String> time = new RefreshableProperty<String>(false, new AvailableTimeValue());
    private static final RefreshableProperty<String> version = new RefreshableProperty<String>(false, new Version());*/

    static {
        reloadCache();
    }

    private AppUtils () { }

    public final static boolean canPostFormOrderMultipleTimesValue() {
        return canPostFormOrderMultipleTimesValue;
    }

    public static boolean isUpToDate(String card) {
        /*String phoneNumber = UsersAccess.getUsersOutOfDate(card);*/
        UsersEntity user = UsersAccess.getUserByCard(card);
        double userVersion = user.getTerminal().getAppVersion();
        double appCurrentVersion = Double.valueOf(getVersion());
        if(userVersion==appCurrentVersion)
            return true;
        Updater.update(user.getPhoneNumber());
        /*if (phoneNumber.isEmpty())
            return true;
            */

        /*Updater.update(phoneNumber);*/
        return false;
    }

    public static boolean isPostFormAvailable () {
        boolean canPostForms = true;
        /*String availableTime = time.getValue();*/
        String availableTime = getUptime();
        if (!availableTime.isEmpty()) {
            String [] split = availableTime.split(",");
            try {
                boolean isActive = Boolean.parseBoolean(split[0]);
                byte activeFrom = Byte.parseByte(split[1]);
                byte activeTo = Byte.parseByte(split[2]);
                byte currentHour = (byte)Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                // activeFrom inclusive and activeTo too(include).
                if (isActive && (currentHour < activeFrom || currentHour > activeTo)) {
                    canPostForms = false;
                }
            }catch (NumberFormatException ex) {
                log.info("Error trying to parse AVAILABLE_TIME from APP_CONFIG. Corrupted value: " + availableTime);
            } catch (Exception ex) {
                log.info("Error trying to evaluate is on AVAILABLE_TIME. Value: " + availableTime);
            }
        }
        return canPostForms;
    }

    public static String getVersion() {
        return version;
    }

    private static String getUptime(){
        return uptime;
    }

    public static void reloadCache () {
        log.info("********* Reloading App Config Values *********");
        /*time.refresh();
        version.refresh();*/
        version = ApplicationAccess.getVersion();
        log.info("Refreshing version value to " + version +", at "+new Date() );
        uptime = ApplicationAccess.getAvailableTime();
        log.info("Refreshing uptime value to " + uptime +", at "+new Date() );
        canPostFormOrderMultipleTimesValue = ApplicationAccess.canPostOrderMultipletimes();
        log.info("Refreshing 'canPostFormMultiplesTime' value to " + canPostFormOrderMultipleTimesValue +", at "+new Date() );
    }



    /*private static final class AvailableTimeValue implements RefreshableProperty.ValueAdapter<String>{
        @Override
        public String get() {
            final String time = ApplicationAccess.getAvailableTime();
            log.info("Refreshing available time value to " + time+", at "+new Date() );
            return time;
        }
    }

    private static final class Version implements RefreshableProperty.ValueAdapter<String>{
        @Override
        public String get() {
            final String version = ApplicationAccess.getVersion();
            log.info("Refreshing version value to " + version +", at "+new Date() );
            return version;
        }
    }*/


}
