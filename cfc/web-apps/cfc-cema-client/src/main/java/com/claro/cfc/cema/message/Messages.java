package com.claro.cfc.cema.message;
/**
 * This class consists exclusively of static methods that operate on or return
 * SMSMessage.
 *
 * @author Nathaniel Calderon
 * @since 3.5
 * @see SMSMessage
 *
 * Created by Nathaniel Calderon on 7/20/2017.
 */
public class Messages {
    // Suppresses default constructor, ensuring non-instantiability.
    private Messages(){

    }
    public final static void normalize (SMSMessage message){
        String phoneNormalized = message.getPhone().replaceAll("\\(", "").replaceAll("\\)", "").replaceAll(" ", "").replaceAll("-", "");
        message.setPhone(phoneNormalized);
    }
}
