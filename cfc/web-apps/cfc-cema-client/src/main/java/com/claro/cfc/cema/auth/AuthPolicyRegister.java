package com.claro.cfc.cema.auth;

import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.http.impl.client.WinHttpClients;

/**
 * Created by Nathaniel Calderon on 7/20/2017.
 */
public class AuthPolicyRegister {
    static {
        AuthPolicy.registerAuthScheme(AuthPolicy.NTLM, JCIFS_NTLMScheme.class);
        if (!WinHttpClients.isWinAuthAvailable()) {
            System.out.println("Integrated Win auth is not supported!!!");
        }
    }
}
