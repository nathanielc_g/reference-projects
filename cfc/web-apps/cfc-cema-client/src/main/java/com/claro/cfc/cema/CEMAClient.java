package com.claro.cfc.cema;

import com.claro.cfc.cema.message.Messages;
import com.claro.cfc.cema.message.SMSMessage;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.WinHttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Nathaniel Calderon on 7/11/2017.
 */
public class CEMAClient implements SMSClient {

    //private static final String CLIENT_PASSWORD = "C$cM@vil";
    //private static final String SERVICE_URL = "http://nttappsweb0006/SmsMonitor/MsgCaptor.asmx";
    //private static final String SOAP_ACTION = "http://Claro.com.do/CapturarMsg";
    //private static final String SERVICE_URL = "http://nttappsweb0006/wscema/cema.webservice.asmx";  // AMBIENTE DE PRUEBAS
    /*private static final String SERVICE_URL = "http://172.27.12.38:81/Cema.WebService.asmx?WSDL";  // AMBIENTE DE PRUEBAS*/
    /*2016-GESTI-3339-8*/
    // TODO: Set production url service
    private static final String SERVICE_URL = "http://ntpappsweb0007/cemawebservice/cema.webservice.asmx";  // AMBIENTE PRODUCCION
    private static final String SOAP_ACTION = "http://Claro.com.do/SendSMS";
    /*private static final String UPDATE_DOMAIN = "http://otau.claro.com:00/update/";*/

    private static final Logger log = Logger.getLogger(CEMAClient.class);
    private String expirationDate;

    public CEMAClient() {
        init();
    }

    private void init (){
        expirationDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    /*public  void sendSMS (final SMSMessage...messages) {
        sendSMS(Arrays.asList(messages));
    }*/

    @Override
    public  void sendSMSBulk(final List<SMSMessage> messages) {
        final ExecutorService exec = Executors.newFixedThreadPool(messages.size()>4?4:messages.size());
        exec.execute(new Runnable() {
            @Override
            public void run() {
                for(final SMSMessage message: messages)
                    exec.execute(new Runnable() {
                        @Override
                        public void run() {
                            sendSMS(message);
                        }
                    });
                exec.shutdown();
            }
        });
    }

    @Override
    public void sendSMS(SMSMessage message) {
        CloseableHttpClient httpclient = WinHttpClients.createDefault();

        try {

            HttpPost httpPost = new HttpPost(SERVICE_URL);
            httpPost.addHeader("Content-type", "text/xml; charset=utf-8");
            httpPost.addHeader("SOAPAction", SOAP_ACTION);

            Messages.normalize(message);
            String httpMessage = parseXMLMessage(message);

            log.info(String.format("Message to be send: %s; To ENDPOINT: %s", httpMessage, SERVICE_URL));
            HttpEntity entity = new ByteArrayEntity(httpMessage.getBytes("UTF-8"));
            //HttpEntity entity = new StringEntity(getMessage(phone, msg), HTTP.UTF_8);
            httpPost.setEntity(entity);

            CloseableHttpResponse response = httpclient.execute(httpPost);
            log.info("Response for number " + message.getPhone() + ", " + EntityUtils.toString(response.getEntity()) );
            response.close();
        } catch (Exception ex) {
            log.info("Error Message: " + ex.getMessage());
            log.info(ex);
            log.info("Error While trying to send a message to phone " + message.getPhone());
        } finally {
            try {
                httpclient.close();
            } catch (IOException ex) {
                log.info("Error while trying to close httpclient: " + ex.getMessage());
            }
        }
    }

    private String parseXMLMessage(SMSMessage message) {
        String raw = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "  <soap:Body>\n" +
                "    <SendSMS xmlns=\"http://Claro.com.do/\">\n" +
                "      <Texto>" + message.getMessage() +"</Texto>\n" +
                "      <Telefono>" + message.getPhone() + "</Telefono>\n" +
                "      <Aplicacion>AP21</Aplicacion>\n" +
                "      <Prioridad>Normal</Prioridad>\n" +
                "      <FechaExpira>" + expirationDate + "</FechaExpira>\n" +
                "    </SendSMS>\n" +
                "  </soap:Body>\n" +
                "</soap:Envelope>";
        return raw;
    }
}
