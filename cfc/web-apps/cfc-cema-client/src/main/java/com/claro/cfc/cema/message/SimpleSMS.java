package com.claro.cfc.cema.message;

/**
 * Created by Nathaniel Calderon on 7/20/2017.
 */
public class SimpleSMS implements SMSMessage {
    private String phone;
    private String message;

    public SimpleSMS(String phone, String message) {
        if (phone == null || message == null)
            throw new IllegalArgumentException("Phone and Message cannot be null.");

        this.phone = phone;
        this.message = message;
    }


    @Override
    public String getPhone() {
        return phone;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleSMS simpleSMS = (SimpleSMS) o;

        if (!phone.equals(simpleSMS.phone)) return false;
        return message.equals(simpleSMS.message);
    }

    @Override
    public int hashCode() {
        int result = phone.hashCode();
        result = 31 * result + message.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SimpleSMS{" +
                "phone='" + phone + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
