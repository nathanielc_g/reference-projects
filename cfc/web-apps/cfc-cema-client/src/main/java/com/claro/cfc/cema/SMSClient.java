package com.claro.cfc.cema;

import com.claro.cfc.cema.message.SMSMessage;

import java.util.List;

/**
 * Created by Nathaniel Calderon on 7/20/2017.
 */
public interface SMSClient {
    void sendSMSBulk (List<SMSMessage> messages);
    void sendSMS(SMSMessage message);
}
