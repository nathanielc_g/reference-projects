package com.claro.cfc.cema.message;

/**
 * Created by Nathaniel Calderon on 7/20/2017.
 */
public interface SMSMessage {
    String getPhone();
    String getMessage();
    void setPhone(String phone);
    void setMessage(String message);
}
