package com.claro.cfc.cema;

import com.claro.cfc.cema.message.SMSMessage;
import com.claro.cfc.cema.message.SimpleSMS;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;

public class CEMAClientTest {
    List<SMSMessage> smsMessages;
    CEMAClient sender;
    @BeforeMethod
    public void setUp() throws Exception {
        sender = new CEMAClient();
        smsMessages = new ArrayList<SMSMessage>();
        for(int i = 0; i < 1500; i++)
            smsMessages.add(new SimpleSMS("8092201111", "Prueba "+i));
    }

    @AfterMethod
    public void tearDown() throws Exception {
    }

    @Test
    public void testSendSMSBulk() throws Exception {
        sender.sendSMSBulk(smsMessages);
        Thread.sleep(300000);
        assertTrue(1 == 1);

    }

}