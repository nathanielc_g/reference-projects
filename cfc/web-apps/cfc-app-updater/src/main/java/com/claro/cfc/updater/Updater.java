package com.claro.cfc.updater;

import com.claro.cfc.cema.CEMAClient;
import com.claro.cfc.cema.SMSClient;
import com.claro.cfc.cema.message.SimpleSMS;
import com.claro.cfc.enviroment.Environments;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/31/2014.
 */
public final class Updater {
    private static final String UPDATE_DOMAIN = "http://otau.claro.com:00/update/";


    public static void update(final Object... phones) {
        if (null == phones)
            return;

        final SMSClient cema = new CEMAClient();
        final ExecutorService exec = Executors.newFixedThreadPool(10);
        exec.execute(new Runnable() {
            @Override
            public void run() {
                final String firstMessage = "Existe un actualizacion  de CFC Movil, recibira un mensaje con el link de instalacion.";
                final String sendMessage = getUpdateLink();
                for (int i = 0; i < phones.length; ++i) {
                    final String phone = phones[i].toString();
                    exec.execute(new Runnable() {
                        @Override
                        public void run() {
                            cema.sendSMS(new SimpleSMS(phone, firstMessage));
                            cema.sendSMS(new SimpleSMS(phone, sendMessage));
                        }
                    });
                }
                exec.shutdown();
            }
        });
    }

    public static String getUpdateLink() {
        return UPDATE_DOMAIN + TargetCypher.encrypt(Environments.getDefault().getMobileUpdate());
    }



}
