package com.claro.cfc.updater;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * Created by Nathaniel Calderon on 6/12/2017.
 */
public class UpdaterTest {

    @Test()
    public void testUpdate () {
        //Updater.testUpdate("(809) 756-7249");
        System.out.println(String.format("Message to be send: %s ; To ENDPOINT: %s", "1", "2"));
        assertTrue(!Updater.getUpdateLink().isEmpty());
    }
}
