package com.claro.cfc.enviroment;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/31/2014.
 */
public final class Environments {

    private static Environment mDefault = null;

    public static Environment getDefault() {
        if (null == mDefault)
            mDefault = loadDefault();
        return mDefault;
    }


    private static Environment loadDefault() {
        Logger logger = Logger.getLogger( Environments.class );

        Properties env = new Properties();
        InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("env.properties");
        if( null != in ) {
            try {
                env.load(in);
            }catch ( IOException ex ){
                logger.error( "Error trying to load default environment file" );
                logger.error( ex );
            }
        }

        logger.info( "Environmnet object "+env );
        final String value = (String) env.get( "invariant.environment" );
        logger.info( "Environment configured as "+value );
        if( null != value )
            return Environment.valueOf( value );

       throw new IllegalStateException( "Could not possible load environments" );
    }
}
