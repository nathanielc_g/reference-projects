package com.claro.cfc.enviroment;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/19/14.
 * <p/>
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad. Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad.Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 */
public enum Environment {
    PRODUCTION{
        @Override
        public String saydotRetrieveOrders() {
            return "http://ntpappsweb0005/AsignacionesTecnicos/API/AsignacionesTecnicos/ObtenerAsignacionesTecnico?tarjeta=";
        }

        @Override
        public String saydotDeclineOrders() {
            return "http://ntpappsweb0005/AsignacionesTecnicos/API/AsignacionesTecnicos/EnviarAsignacionesRecibidas";
        }

        @Override
        public String getMobileUpdate() {
            return "http://172.27.4.185:80/captura-facilidades/OTAUpdated/apps/CFCMobile.apk";
        }

        @Override
        public String getDomainFrames() {
            return "http://172.27.5.135:8001/DomainServices/frames";
        }

        @Override
        public String getDomainV2Frames() {
            return "http://172.27.5.135:8001/DomainServices/frames2";
        }

        @Override
        public String getDomainV3Frames() {
                return "http://172.27.5.135:8001/DomainServices/frames3";
        }

        @Override
        public String getDomainV4Frames() {
            return "http://172.27.5.135:8001/DomainServices/frames4";
        }

    },
    DEVELOPMENT{
        @Override
        public String saydotRetrieveOrders() {
            //return "http://nttappsweb0003/cfcordave/API/AsignacionesTecnicos/ObtenerAsignacionesTecnico?tarjeta="; // ORIGINAL
            //return "http://nttsaydotwb0001/AsignacionesTecnicos_01/API/AsignacionesTecnicos/ObtenerAsignacionesTecnico?tarjeta="; // QA1
            return "http://nttsaydotwb0001/AsignacionesTecnicos_02/API/AsignacionesTecnicos/ObtenerAsignacionesTecnico?tarjeta="; // QA2
        }

        @Override
        public String saydotDeclineOrders() {
            return "http://nttappsweb0003/CFCORDAVE/API/AsignacionesTecnicos/EnviarAsignacionesRecibidas";
        }

        @Override
        public String getMobileUpdate() {
            /**
             * staging
             */
            return "http://172.27.17.166:8020/captura-facilidades/OTAUpdated/apps/CFCMobile.apk";
        }

        @Override
        public String getDomainFrames() {
            return "http://172.27.5.88:7004/DomainServices/frames";
        }

        @Override
        public String getDomainV2Frames() {
            return "http://172.27.5.88:7004/DomainServices/frames2";
        }

        @Override
        public String getDomainV3Frames() {
            return "http://172.27.5.88:7004/DomainServices/frames3";
        }

        @Override
        public String getDomainV4Frames() {
            return "http://172.27.5.88:7004/DomainServices/frames4";
        }
    };


    public abstract String saydotRetrieveOrders();

    @Deprecated
    public abstract String saydotDeclineOrders();

    public abstract String getMobileUpdate();

    public abstract String getDomainFrames();

    public abstract String getDomainV2Frames();

    public abstract String getDomainV3Frames();

    public abstract String getDomainV4Frames();
}
