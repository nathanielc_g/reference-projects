package com.claro.cfc.web.servlet.service.model;

import com.claro.cfc.web.servlet.service.model.dto.OrderAction;
import com.google.gson.Gson;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.*;

/**
 * Created by Nathaniel Calderon on 6/19/2017.
 */
public class SupportOrderActionServiceModelTest {
    private SupportOrderActionServiceModel model;
    private Map<String, Object[]> dummy;
    private Gson gson;
    @Test(enabled = false)
    public void testGetNonHandOperatedActions() throws Exception {
        List<OrderAction> actions = model.getNonHandOperatedActions(dummy);
        System.out.println(gson.toJson(actions));
        assertTrue(actions != null);
    }

    @Test(enabled = false)
    public void testSaveHandOperated() throws Exception {
        Object result = model.saveHandOperated(dummy);
        System.out.println(result);
        assertTrue(result != null);
    }

    @Test(enabled = false)
    public void testSaveProcessedBy() throws Exception {
        Object result = model.saveProcessedBy(dummy);
        System.out.println(result);
        assertTrue(result != null);
    }

    @Test(enabled = false)
    public void testFindLastAction() throws Exception {
        List<OrderAction> action = model.findLastAction(dummy);
        System.out.println(action);
        assertTrue(action != null);
    }

    @BeforeMethod
    public void setUp() throws Exception {
        model = new SupportOrderActionServiceModel();
        dummy = new HashMap<String, Object[]>();
        dummy.put("_ip", new Object[]{"10.0.0.1"});
        dummy.put("_userLoggedID", new Object[]{"54180"});
        dummy.put("card", new Object[]{"16471"});
        dummy.put("actionId", new Object[]{"37844508"});
        dummy.put("code", new Object[]{"12050471"});
        dummy.put("type", new Object[]{"ORDEN"});
        dummy.put("handOperatedBy", new Object[]{"16471"});
        dummy.put("processedBy", new Object[]{"16471"});
        gson = new Gson();
    }

    @AfterMethod
    public void tearDown() throws Exception {
    }



}
