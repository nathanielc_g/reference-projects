package com.claro.cfc.web.servlet.service.model;

import com.claro.cfc.web.servlet.service.model.DispositivoServiceModel;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nathaniel Calderon on 5/25/2017.
 */
public class DispositivoServiceModelTest {
    @Test(enabled = false)
    public void testAddNewUser () {
        Map<String, Object[]> inputMap = buildDummyMapToAdd();
        DispositivoServiceModel dsm = new DispositivoServiceModel();
        Map<String,String> outputMap = (Map<String,String>)dsm.addNewUser(inputMap);
        System.out.println(outputMap);
        assertTrue(outputMap.get("ok").equalsIgnoreCase("true"), "***** User Updated ******");
    }

    @Test(enabled = false)
    public void testUpdateUserAndDevice () {
        Map<String, Object[]> inputMap = buildDummyMapToUpdate();
        DispositivoServiceModel dsm = new DispositivoServiceModel();
        Map<String,Object> outputMap = (Map<String,Object>)dsm.updateUserAndDevice(inputMap);
        System.out.println(outputMap);
        assertTrue((Boolean)outputMap.get("ok") == true, "***** User Added ******");
    }

    private Map<String, Object[]> buildDummyMapToAdd(){
        Map<String, Object[]> dummyMap = new HashMap<String, Object[]>();
        dummyMap.put("IMEI", new Object[]{"000000000000000"});
        dummyMap.put("brand", new Object[]{"ALCATEL"});
        dummyMap.put("os", new Object[]{"4.1"});
        dummyMap.put("model", new Object[]{"ONE TOUCH S'Pop  4033A"});
        dummyMap.put("phone", new Object[]{"(809) 245-6688"});
        dummyMap.put("tarjeta", new Object[]{"54180"});
        //dummyMap.put("tarjeta", new Object[]{"56491"});
        dummyMap.put("status", new Object[]{"0"});
        dummyMap.put("expiration[]", new Object[]{"2017", "26", "05"});
        dummyMap.put("usuario", new Object[]{"1"});
        dummyMap.put("ip", new Object[]{"172.27.40.124"});
        dummyMap.put("mod", new Object[]{"1"});
        dummyMap.put("userLoggedCard", new Object[]{"11111"});
        return dummyMap;
    }

    private Map<String, Object[]> buildDummyMapToUpdate(){
        Map<String, Object[]> dummyMap = new HashMap<String, Object[]>();
        dummyMap.put("imei", new Object[]{"000000000000000"});
        dummyMap.put("tarjeta", new Object[]{"54180"});
        dummyMap.put("phone", new Object[]{"(809) 245-6688"});
        dummyMap.put("status", new Object[]{"2"});
        dummyMap.put("userLoggedCard", new Object[]{"11111"});
        dummyMap.put("ip", new Object[]{"172.27.40.124"});
        return dummyMap;
    }
}
