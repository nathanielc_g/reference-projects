<%--
  Created by Christopher Herrera
  Date: 1/10/14
  Time: 8:42 PM
  Email: Christopher_Herrera@claro.com.do
  Office: 809-220-8148
  Cellphone: 829-755-1677
  CLARO RD
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <!-- HEAD INCLUDES-->
    <%@include file="../head_pages.jsp" %>
</head>
<body class="page-body page-fade">

<div class="page-container">

    <!-- Sidebar Menu -->
    <%@include file="../../assets/fragments/sidebar_menu.jsp" %>
    <div class="main-content">
        <!-- Notifications and Raw Links -->
        <div class="row" id="top-bar">
            <%@include file="../../assets/fragments/profile_and_notifications.jsp" %>
            <%@include file="../../assets/fragments/raw_links.jsp" %>
        </div>
        <hr/>
        <h2>Reportes</h2>
        <ol id="breadCrumb"></ol>
        <hr/>
        <br/>

        <%--FILTERS HERE--%>
        <div class="row">
            <div class="col-md-3">

            </div>
            <div class="col-md-3">

            </div>
            <div class="col-md-3">

            </div>
            <div class="col-md-3">

            </div>
        </div>


        <!-- CONTENT HERE -->
        <div class="row">
            <div class="col-md-6" style="text-align: center" id="device-report">
                <form action="${pageContext.request.contextPath}/reports/device" method="POST">
                    <input type="hidden" name="queryFilters" data-bind="value: queryFilters">
                    <input type="submit" value="Generar reporte de logs de dispositivos" class="btn btn-info"/>
                </form>
            </div>
            <div class="col-md-6" id="web-report">
                <form action="${pageContext.request.contextPath}/reports/web" method="POST">
                    <input type="hidden" name="queryFilters" data-bind="value: queryFilters">
                    <input type="submit" value="Generar reporte de logs de la app web" class="btn btn-success"/>
                </form>

            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-6" style="text-align: center" id="actions-report">
                <form action="${pageContext.request.contextPath}/reports/actions" method="POST">
                    <input type="hidden" name="queryFilters" data-bind="value: queryFilters">
                    <input type="submit" value="Generar reporte de logs de formularios" class="btn btn-success"/>
                </form>
            </div>
            <div class="col-md-6" id="tasks-report">
               <%-- <form action="${pageContext.request.contextPath}/tasks-report/gen" method="POST">--%>
                <form action="${pageContext.request.contextPath}/tasks-report/gen" method="POST">
                <%--172.27.17.166:8020/--%>
                    <input type="hidden" name="queryFilters" data-bind="value: queryFilters">
                    <input type="submit" value="Generar reporte de logs de actualizacion" class="btn btn-info"/>
                </form>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-6" style="text-align: center" id="terminal-report">
                <form action="${pageContext.request.contextPath}/terminals-report/gen" method="POST">
                    <input type="hidden" name="queryFilters" data-bind="value: queryFilters ">
                    <input type="submit" value="Generar reporte de listado de terminales" class="btn btn-info"/>
                </form>
            </div>
            <div class="col-md-6" id="inactive-users">
                <form action="${pageContext.request.contextPath}/inactive-users-report/gen" method="POST">
                    <input type="hidden" name="queryFilters" data-bind="value: queryFilters ">
                    <input type="submit" value="Reporte usuarios inactivos" class="btn btn-success"/>
                </form>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-6" style="text-align: center" id="action-logs-report">
                <form action="${pageContext.request.contextPath}/action-logs-report/gen" method="POST">
                    <input type="hidden" name="queryFilters" data-bind="value: queryFilters ">
                    <input type="submit" value="Reporte de actualizaciones de Formularios" class="btn btn-success"/>
                </form>
            </div>
        </div>
        <br />

        <!-- FILTERS ARE HERE -->
        <div class="row">
            <div class="col-md-3">
                <div class="form-group ">
                  <label class="control-label " for="separators">
                   Separador
                  </label>
                  <select id="separators" name="separators" class="select form-control" data-bind="options: separators,
                       optionsText: 'label',
                       value: selectedSeparator,
                       optionsCaption: 'Seleccionar separador'"></select>
                </div>
                
            </div>
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="control-label " for="fields">
                       Campo
                    </label>
                    <select name="fields" class="select form-control" data-bind="options: fields,
                       optionsText: 'label',
                       value: selectedField,
                       optionsCaption: 'Seleccionar campo',
                       event: { change: onChangeField }
                       ">
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="control-label " for="operators">
                       Operador
                    </label>
                    <select name="operators" class="select form-control" data-bind="options: operators,
                       optionsText: 'label',
                       value: selectedOperator,
                       optionsCaption: 'Seleccionar operador'"></select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group" data-bind="visible: showValue">
                    <label class="control-label " for="value">
                       Valor
                    </label>
                    <input class="select form-control" data-bind='value: value, valueUpdate: "afterkeydown"' />
                </div>

                <div class="form-group" data-bind="visible: showValueDate">
                    <label class="control-label " for="dateValue">
                       Fecha
                    </label>
                    <input name="dateValue" data-provide="datepicker" class="datepicker form-control" data-bind='value: valueDate'>
                </div>

                
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-md-offset-6">
                <button type="button" class="btn btn-default" data-bind="click: addFilter, enable: isFormComplete()">Agregar</button>
            </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-danger"  data-bind="click: removeRowSelected">Eliminar</button>
            </div>
            <!-- <div class="col-md-3">
                <button type="button" class="btn"  data-bind="click: getFilterQuery">Generar</button>
            </div> -->
        </div>
        <div class="row">
            <div class="col-med-12"> 
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>
                                Separador
                            </th>
                            <th>
                                Campo
                            </th>
                            <th>
                                Operador
                            </th>
                            <th>
                                Valor
                            </th>
                            
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: filters">
                        <tr data-bind="click: $parent.onRowSelected.bind($data, $index()), css: { warning: $parent.indexSelected() == $index() }">
                            <td data-bind="text: separator.label">                          
                            </td>
                            <td data-bind="text: field.label">                              
                            </td>
                            <td data-bind="text: operator.label">
                            </td>                               
                            <td data-bind="text: value">                                
                            </td>                           
                        </tr>
                    </tbody>
                    
                </table>
            </div>
        </div>

        <br />

        <!-- FOOTER -->
        <%@include file="../../assets/fragments/footer.jsp" %>
    </div>

</div>

<!-- BOTTOM INCLUDES-->
<%@include file="../bottom_includes_pages.jsp" %>

<script type="text/javascript">
    var _QUERY = "";

    //var filterModel = function (separators, fields, operators) {
    function filterModel (separators, fields, operators) {
            $('.datepicker').datepicker({
                format: "dd-M-yyyy",
                todayBtn: "linked",
                autoclose: true,
                maxViewMode: 2,
                todayHighlight: true,
                endDate: "0d",
                setDate: new Date()                  
            });
            var self = this;
            self.separators = separators;
            self.fields = fields;
            self.operators = operators;
            self.queryFilters = ko.observable("");
            self.filters = ko.observableArray([
                
            ]);

            self.showValue = ko.observable(false);
            self.showValueDate = ko.observable(false);

            self.value = ko.observable(""); 
            self.valueDate = ko.observable("");

            self.selectedField = ko.observable();
            self.selectedOperator = ko.observable();
            self.selectedSeparator = ko.observable();
            self.filterSelected = ko.observable(null);
            self.indexSelected = ko.observable(null);

            self.isFormComplete = function () {
                return (self.value().length > 0 || self.valueDate().length > 0) && self.selectedSeparator() && self.selectedField() && self.selectedOperator();
            }


            self.addFilter = function () {
                if (self[self.selectedField().fieldValue]() == "") 
                //&& (this.allItems.indexOf(this.itemToAdd()) < 0)
                 // Prevent blanks and duplicates                       
                {
                    return;
                }

                self.filters.push(
                        {
                            separator: self.selectedSeparator()
                            ,field: self.selectedField()
                            ,operator: self.selectedOperator()
                            ,value: self[self.selectedField().fieldValue]()
                        }
                    );

                    self.clearForm();
                    self.prepareFilterQuery();

            }

            self.onChangeField = function () {      
                if(self.selectedField() != null && self.selectedField() != undefined) {
                    self.showVal(self.selectedField().showField);
                }       
            }

            self.showVal = function (field) {
                self.showValue(false);
                self.showValueDate(false);
                if (field != null) {
                    self[field](true);
                    self.value(""); // Clear the text box
                    self.valueDate(""); // Clear the text box   
                }

            }

            self.isRowSelected = function (index) {
                return self.indexSelected() == index? true: false;
                
            }

            self.onRowSelected = function (index) {
                self.indexSelected(index);
            }

            self.removeRowSelected = function () {
                self.filters.splice(self.indexSelected(), 1);
                self.indexSelected(null);
                self.prepareFilterQuery();
                //self.filters.removeAll(self.filters()[self.indexSelected()]);
            }

            self.clearForm = function () {
                self.selectedOperator(null);
                self.selectedSeparator(null);
                self.selectedField(null);    
                self.value(""); // Clear the text box
                self.valueDate(""); // Clear the text box
            }

            self.prepareFilterQuery = function () {
                var query = "";
                var subquery = "";
                self.filters().forEach(function (currentValue,index,arr) {
                    if(index > 0){
                        subquery = " " + currentValue.separator.value ; 
                    } 
                    subquery += " " + currentValue.field.subquery
                    
                    if (subquery.indexOf("@operator") != -1) {
                        subquery = subquery.replace(/@operator/g, currentValue.operator.value);
                    }

                    if (subquery.indexOf("@value") != -1) {
                        subquery = subquery.replace(/@value/g, currentValue.value);
                    }

                    query += subquery;

                });
                self.queryFilters(query);          
                //var query = "";
                /*self.filters().forEach(function (currentValue,index,arr) {

                }); */      
                //return query;

            }

    }

    (function (ko, $, window, document, undefined) {
        setActiveMenuOption(3);

        var session = <% out.print(session.getAttribute("SessionJson"));%>;
        deniedAccessIfNotAllowed(session, 'SecurityReport', 'device-report', 'web-report','actions-report','tasks-report');
    
        ko.applyBindings(new filterModel(
        [
            {label: "And", value: "and"}
            ,{label: "Or", value: "or"}
        ],
        /*[
            { "label": "(", "value": "("}
            ,{ "label": ")", "value": ")"}
            ,{label: "and", value: "and"}
            ,{label: "or", value: "or"}
        ],*/
        [
            {"label": "Fecha", "value": "fecha", "showField":"showValueDate", "fieldValue":"valueDate", "subquery":"@created @operator TO_TIMESTAMP('@value')"}
        ],
        [
             {"label": "=", "value": "="}
            ,{"label": "!=", "value": "!="}
            ,{"label": ">=", "value": ">="}
            ,{"label": "<=", "value": "<="}
            ,{"label": ">", "value": ">"}
            ,{"label": "<", "value": "<"}
        ]
        )); // This makes Knockout get to work

    }(ko, jQuery, window, document));
</script>
</body>
</html>