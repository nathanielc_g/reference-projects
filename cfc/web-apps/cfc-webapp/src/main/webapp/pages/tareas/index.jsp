<%--
  Created by IntelliJ IDEA.
  User: Nathaniel Calderon
  Date: 4/11/2017
  Time: 2:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <!-- HEAD INCLUDES-->
    <%@include file="../head_pages.jsp" %>
</head>
<body class="page-body page-fade">

<div class="page-container">

    <!-- Sidebar Menu -->
    <%@include file="../../assets/fragments/sidebar_menu.jsp" %>
    <div class="main-content">
        <!-- Notifications and Raw Links -->
        <div class="row" id="top-bar">
            <%@include file="../../assets/fragments/profile_and_notifications.jsp" %>
            <%@include file="../../assets/fragments/raw_links.jsp" %>
        </div>
        <hr/>
        <h2>Rebotes</h2>
        <ol id="breadCrumb"></ol>
        <hr/>
        <br/>

        <!-- CONTENT HERE -->
        <div id="main-container">

            <%--<div class="row">
                <button type="button"
                        data-bind="click: unitTest"
                        class="btn btn-info" data-loading-text="Cargando..."
                        class="btn-toggle-loading">unit test
                </button>
            </div>--%>

            <%--<div class="row">--%>

                <!-- OPERATIONS -->
                <div id="operations-container" class="row">
                    <div class="col-md-1 col-md-offset-10">
                        <button type="button" id="retrieveFailedForms"
                                data-bind="click: retrieveFailedForms, disable: isLoading"
                                class="btn btn-info"
                                >Consultar
                        </button>

                    </div>
                    <div class="col-md-1">
                        <button type="button" id="processFailedForms"
                                data-bind="click: processFailedForms, disable: isLoading"
                                class="btn btn-danger"
                                >Submit
                        </button>
                    </div>
                </div>
                <div class="sk-circle" data-bind="visible: isLoading">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
                <br>

                <!-- FILTERS -->
                <%--<div id="filters-container">--%>
                    <div id="required-filters" class="row">
                        <div class="col-md-2">
                            <select class="select form-control" data-bind="options: systems,
                              optionsText: 'label',
                              value: selectedSystem,
                              optionsCaption: 'Seleccionar OSS',
                              event: { change: onChangeSystem }
                              ">
                            </select>
                        </div>
                        <div class="col-md-2">
                            <input id="beginDate" placeholder="Fecha desde" data-provide="datepicker"
                                   class="datepicker form-control" data-bind='value: beginDate'>
                        </div>
                        <div class="col-md-2">
                            <input id="endDate" placeholder="Fecha hasta" data-provide="datepicker"
                                   class="datepicker form-control" data-bind='value: endDate'>
                        </div>
                        <div class="col-md-2">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" data-bind="checked: includeSuccessForm">Procesados
                                </label>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" data-bind="checked: isHandOperated"> Manual
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <select class="select form-control" data-bind="options: adjustActions,
                              optionsText: 'label',
                              value: selectedAdjustAction,
                              optionsCaption: 'Seleccionar acci&oacute;n de ajuste',
                              event: { change: onChangeAdjustAction }
                              ">
                            </select>
                            <!-- <div class="checkbox">
                              <label>
                                <input type="checkbox" data-bind="checked: isAdjustAction"> Ajustado
                              </label>
                            </div> -->
                        </div>
                    </div>

                    <div id="optional-form-filters" class="row">
                    </div>

                    <div id="optional-form-value-filters" class="row">
                        <div class="col-md-4">
                            <select class="select form-control" data-bind="options: fields,
                              optionsText: 'label',
                              value: selectedField,
                              optionsCaption: 'Seleccionar campo',
                              event: { change: onChangeField }
                              ">
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="operators" class="select form-control" data-bind="options: operators,
                              optionsText: 'label',
                              value: selectedOperator,
                              optionsCaption: 'Seleccionar operador'"></select>
                        </div>
                        <div class="col-md-4">
                            <input placeholder="Valor" class="form-control"
                                   data-bind='value: value, valueUpdate: "afterkeydown", visible: showValueInput'/>
                            <%--data-mask="phone"--%>
                            <input type="text" class="form-control" name="telefono"
                                   id="telefono" placeholder="Telefono"
                                   name="telefono"
                                   data-bind='value: value, valueUpdate: "afterkeydown", visible: showPhoneInput'/>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-1 col-md-offset-10">
                            <button type="button" id="addFilterRow" class="btn btn-info"
                                    data-bind="click: addRowField, enable: isAbleToAddRow">Agregar
                            </button>
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-danger" data-bind="click: removeRowFieldSelected">
                                Eliminar
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>
                                        Campo
                                    </th>
                                    <th>
                                        Operador
                                    </th>
                                    <th>
                                        Valor
                                    </th>
                                </tr>
                                </thead>
                                <tbody data-bind="template: { name: 'filterTemplate', foreach: fieldFilters }">
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- id="result-container"  -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- id="tbl-results" -->
                            <table id="tbl-results" class="table table-striped">
                                <thead>
                                <th><!-- CHECKBOX HEADER -->
                                    <input type="checkbox" id="checkAll" data-bind="checked: isAllChecked"/>
                                </th>
                                <th>FORM_ID</th>
                                <th>CODE</th>
                                <th>CREADO</th>
                                <th>TARJ</th>
                                <th>FTYPE</th>
                                <th>RTYPE</th>
                                <th>MESSAGE</th>
                                </thead>
                                <tbody data-bind="template: { name: 'taskResultsTemplate', foreach: taskResults }">

                                </tbody>
                            </table>
                        </div>

                    </div>
                <%--</div>--%>
            <%--</div>--%>

            <!-- CONTENT END HERE -->
            <br/>

            <script id="filterTemplate" type="html/text">
                <tr data-bind="click: $parent.onRowSelected.bind($data, $index()), css: { warning: $parent.indexSelected() == $index() }">
                  <td data-bind="text: field.label">
                  </td>
                  <td data-bind="text: operator.label">
                  </td>
                  <td data-bind="text: value">
                  </td>
                </tr>
            </script>

            <script id="taskResultsTemplate" type="html/text">
                <tr class="resultRow">
                  <td>
                    <input type="checkbox" class="checkboxRow" checked: isChecked />
                  </td>
                  <%--<td data-bind="text: actionId">
                  </td>--%>
                  <td>
                      <a href="javascript: void(0)" style="text-decoration: underline; color: blue;" data-bind="text: actionId, click: $parent.onActionClick.bind($data, $index())">
                      </a>
                  </td>
                  <td data-bind="text: code">
                  </td>
                  <td data-bind="text: created">
                  </td>
                  <td data-bind="text: userId">
                  </td>
                  <td data-bind="text: formType">
                  </td>
                  <td data-bind="text: taskResponseType">
                  </td>
                  <td data-bind="text: taskLogMessage">
                  </td>
                </tr>
              </script>
            <!-- FOOTER -->
            <%@include file="../../assets/fragments/footer.jsp" %>
        </div>

    </div>
    </div>

    <!-- BOTTOM INCLUDES-->
    <%@include file="../bottom_includes_pages.jsp" %>
    <script src="${pageContext.request.contextPath}/assets/js-custom/tareas/config.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js-custom/tareas/controller.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js-custom/tareas/model.js"></script>

    <%--FORMS--%>
    <%@include file="../fragments/forms/templates.jsp" %>
    <%@include file="../fragments/forms/pstn.jsp" %>
    <%@include file="../fragments/forms/pstn_data.jsp" %>
    <%@include file="../fragments/forms/pstn_fibra.jsp" %>
    <%@include file="../fragments/forms/frame.jsp" %>
    <%@include file="../fragments/forms/fibra.jsp" %>

    <%--FORMS SCRIPTS--%>
    <script src="${pageContext.request.contextPath}/assets/js-custom/tareas/forms.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js-custom/tareas/forms/form_utils.js"></script>

    <script src="${pageContext.request.contextPath}/assets/js-custom/tareas/forms/pstn.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js-custom/tareas/forms/pstn_data.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js-custom/tareas/forms/pstn_fibra.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js-custom/tareas/forms/frame.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js-custom/tareas/forms/fibra.js"></script>

    <script type="text/javascript">

        (function (ko, $, window, document, undefined) {
            setActiveMenuOption(4);

            var session = <% out.print(session.getAttribute("SessionJson"));%>;
            var _userLoggedCard = '<% out.print(session.getAttribute("userCard"));%>';
            var _ip = '<% out.print(session.getAttribute("ip"));%>';

            if (!checkAccess(session, 'SubmitTasks', null)) {
                location.href = '${pageContext.request.contextPath}/index.jsp';
            }
            //deniedAccessIfNotAllowed(session, 'SecurityReport', 'device-report', 'web-report','actions-report','tasks-report');

            ko.cleanNode(document.getElementById("main-container"));
            var config = {
                _ip: _ip
                , _userLoggedID: _userLoggedCard
                , genReportPath: "/genReportPath"
                , processTaskPath: "/processTaskPath"
                ,fetchTasksPath: '${pageContext.request.contextPath}/operations/TaskService/getTasksByCustomFilter'
                ,submitFormsPath: '${pageContext.request.contextPath}/operations/TaskService/submitAbortedForms'
                ,getActionValuesURL: '${pageContext.request.contextPath}/operations/OrdersActionsService/getActionValuesByActionId'
                ,baseURL: '${pageContext.request.contextPath}'
                ,_url: '${pageContext.request.contextPath}'
            };

            ko.applyBindings(new TasksModel(config), document.getElementById("main-container")); // This makes Knockout get to work

        }(ko, jQuery, window, document));
    </script>
</body>
</html>
