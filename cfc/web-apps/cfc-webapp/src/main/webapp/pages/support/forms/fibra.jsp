<%--
  Created by IntelliJ IDEA.
  User: Nathaniel Calderon
  Date: 4/17/2017
  Time: 10:44 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="modal fade" id="modal-fibra" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                data-bind="click: onClose"
                >x</button>
        <h4 class="modal-title">Formulario FIBRA</h4>
      </div>
      <div class="modal-body">
        <form role="form" id="form-fibra" method="post" class="validate" data-bind="submit: saveForm"
              novalidate="novalidate">

        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="telefono" class="control-label">Telefono</label>
              <input type="text" class="form-control" name="telefono"
                     placeholder="Telefono"
              <%--data-validate="required"--%>
                     name="telefono"
                     disabled
                     data-bind="value: telefono"/>
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-md-6" id="section_localidad_fibra">
            <div class="form-group">
              <label class="control-label" for="localidad">Localidad</label>
              <input id="localidad" class="form-control"
                     name="localidad"
                     disabled
                     data-bind="value: localidad">
              </input>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="terminal_fo" class="control-label">Terminal Fo</label>
              <input type="text" class="form-control"
              <%--data-validate="required"--%>
                     name="terminal_fo"
                     id="terminal_fo"
                     disabled
              <%--placeholder="FC[A-Z].{1,5}-[A-Z]\\d{1,2}"--%>
                     placeholder="Terminal Fo"
                     data-bind="value: terminalFo"/>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="splitterPort" class="control-label">Splitter Port</label>
              <input type="text" class="form-control" id="splitterPort"
                     placeholder="Splitter Port"
                     name="splitterPort"
                     disabled
              <%--data-validate="required"--%>
                     data-bind="value: splitterPort"/>

            </div>
          </div>


        </div>

        <div class="row">
          <%--<div class="col-md-12">--%>
          <%--<div class="form-group no-margin">--%>
          <%--<label for="direccion_2" class="control-label">Direcci&oacute;n</label>--%>
          <%--<textarea class="form-control autogrow" id="direccion_2"--%>
          <%--data-bind="value: direccion"--%>
          <%--placeholder="Direccion"--%>
          <%--name="direccion"--%>
          <%--disabled--%>
          <%--style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 31px;"></textarea>--%>
          <%--</div>--%>
          <%--</div>--%>
        </div>

        <div class="row">
          <div class="col-md-1">
            <input type="checkbox" class="checkbox"
                   data-bind="disable: handOperatedDisabled, checked: handOperated"/>
          </div>
          <div class="col-md-5">
            <span>Operacion manual Por:</span>
          </div>
          <div class="col-md-6">
            <span class="control-label" data-bind="text: handOperatedBy"></span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-1">
            <input type="checkbox" class="checkbox"
                   data-bind="disable: processedDisabled, checked: processed"/>
          </div>
          <div class="col-md-5">
            <span>Reprocessado Por:</span>
          </div>
          <div class="col-md-6">
            <span class="control-label" data-bind="text: processedBy"></span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <table class="table table-condensed">
              <thead>
              <tr>
                <th>
                  Sistema
                </th>
                <th>
                  Resultado de Procesamiento
                </th>
                <th>
                  Facilidades
                </th>
              </tr>
              </thead>
              <tbody data-bind="template: { name: 'ActionResultTemplate', foreach: actionResults }">

              </tbody>

            </table>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button"
                  data-bind="click: updateAction, disable: isUpdateDisabled"
                  class="btn btn-info" data-loading-text="Guardando..."
                  class="btn-toggle-loading">Guardar
          </button>
          <button type="button" class="btn btn-default close-modal" data-dismiss="modal" data-bind="click: onClose">Cerrar
          </button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>