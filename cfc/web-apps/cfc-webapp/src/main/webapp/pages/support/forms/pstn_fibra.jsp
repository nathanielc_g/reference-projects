<%--
  Created by IntelliJ IDEA.
  User: Nathaniel Calderon
  Date: 4/17/2017
  Time: 10:44 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="modal fade" id="modal-pstn_fibra" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: onClose">x</button>
        <h4 class="modal-title">Formulario PSTN+FIBRA</h4>
      </div>
      <div class="modal-body">
        <form role="form" id="form-pstn_fibra" method="post" class="validate" data-bind="submit: saveForm"
              novalidate="novalidate">

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="telefono_3" class="control-label">Telefono</label>
              <input type="text" class="form-control" name="telefono_3"
                     id="telefono_3" placeholder="Telefono"
              <%--data-validate="required"--%>
                     name="telefono"
                     disabled
                     data-bind="value: telefono"/>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label" for="cuenta_fija_3">Cuenta Fija</label>
              <input type="checkbox" class="checkbox" id="cuenta_fija_3"
                     name="cuenta_fija_3"
                     disabled
                     data-bind="checked: cuentaFija"/>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6" id="section_localidad_pstn_fibra">
            <div class="form-group">
              <label class="control-label" for="localidad_3">Frame</label>
              <input id="localidad_3" class="form-control"
                     name="localidad_3"
                     disabled
                     data-bind="value: localidad">
              </input>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="terminal_3" class="control-label">Terminal</label>
              <input type="text" class="form-control" id="terminal_3"
              <%--data-validate="required"--%>
                     name="terminal_3"
                     disabled
                     placeholder="Terminal" data-bind="value: terminal"/>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6" id="section_cabina_pstn_fibra">
            <div class="form-group">
              <label for="cabina_3" class="control-label">Cabina</label>
              <input type="text" class="form-control"
              <%--data-validate="required"--%>
                     data-bind="value: cabina"
                     name="cabina_3"
                     id="cabina_3"
                     disabled
                     placeholder="Cabina"/>
            </div>
          </div>

        </div>


        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="par_feeder_3" class="control-label">Par Feeder</label>
              <input type="text" class="form-control" id="par_feeder_3"
                     placeholder="Par Feeder"
              <%--placeholder=".*-####"--%>
                     name="par_feeder_3"
                     disabled
              <%--data-validate="required"--%>
                     data-bind="value: parFeeder"/>

            </div>
          </div>


          <div class="col-md-6" id="section_par_local_pstn_fibra">
            <div class="form-group">
              <label for="par_local_3" class="control-label">Par Local</label>
              <input type="text" class="form-control" id="par_local_3"
                     placeholder="Par Local"
              <%--data-validate="required"--%>
                     name="par_local_3"
                     disabled
                     data-bind="value: parLocal"/>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="terminal_fo_3" class="control-label">Terminal Fo</label>
              <input type="text" class="form-control"
              <%--data-validate="required"--%>
                     name="terminal_fo_3"
                     id="terminal_fo_3"
                     disabled
              <%--placeholder="FC[A-Z].{1,5}-[A-Z]\\d{1,2}" --%>
                     placeholder="Terminal Fo"
                     data-bind="value: terminalFo"/>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="splitterPort_3" class="control-label">Splitter Port</label>
              <input type="text" class="form-control" id="splitterPort_3"
                     placeholder="Splitter Port"
                     name="splitterPort_3"
                     disabled
              <%--data-validate="required"--%>
                     data-bind="value: splitterPort"/>

            </div>
          </div>
        </div>


        <div class="row">
          <%--<div class="col-md-12">--%>
          <%--<div class="form-group no-margin">--%>
          <%--<label for="direccion_3" class="control-label">Direcci&oacute;n</label>--%>
          <%--<textarea class="form-control autogrow" id="direccion_3"--%>
          <%--data-bind="value: direccion"--%>
          <%--placeholder="Direccion"--%>
          <%--name="direccion_3"--%>
          <%--disabled--%>
          <%--style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 31px;"></textarea>--%>
          <%--</div>--%>
          <%--</div>--%>
        </div>

        <div class="row">
          <div class="col-md-1">
            <input type="checkbox" class="checkbox"
                   data-bind="disable: handOperatedDisabled, checked: handOperated"/>
          </div>
          <div class="col-md-5">
            <span>Operacion manual Por:</span>
          </div>
          <div class="col-md-6">
            <span class="control-label" data-bind="text: handOperatedBy"></span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-1">
            <input type="checkbox" class="checkbox"
                   data-bind="disable: processedDisabled, checked: processed"/>
          </div>
          <div class="col-md-5">
            <span>Reprocessado Por:</span>
          </div>
          <div class="col-md-6">
            <span class="control-label" data-bind="text: processedBy"></span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <table class="table table-condensed">
              <thead>
              <tr>
                <th>
                  Sistema
                </th>
                <th>
                  Resultado de Procesamiento
                </th>
                <th>
                  Facilidades
                </th>
              </tr>
              </thead>
              <tbody data-bind="template: { name: 'ActionResultTemplate', foreach: actionResults }">

              </tbody>

            </table>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button"
                  data-bind="click: updateAction, disable: isUpdateDisabled"
                  class="btn btn-info" data-loading-text="Guardando..."
                  class="btn-toggle-loading">Guardar
          </button>
          <button type="button" class="btn btn-default close-modal" data-dismiss="modal" data-bind="click: onClose">Cerrar
          </button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>