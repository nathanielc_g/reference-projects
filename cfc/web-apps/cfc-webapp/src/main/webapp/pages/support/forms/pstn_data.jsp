<%--
  Created by IntelliJ IDEA.
  User: Nathaniel Calderon
  Date: 4/17/2017
  Time: 10:43 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="modal fade" id="modal-pstn_data" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: onClose">x</button>
        <h4 class="modal-title">Formulario PSTN+DATA</h4>
      </div>
      <div class="modal-body">
        <form role="form" id="form-pstn_data" method="post" class="validate" data-bind="submit: saveForm"
              novalidate="novalidate">

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="telefono_4" class="control-label">Telefono</label>
              <input type="text" class="form-control" name="telefono"
                     id="telefono_4" placeholder="Telefono"
              <%--data-validate="required"--%>
                     name="telefono_4"
                     disabled
                     data-bind="value: telefono"/>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label" for="cuenta_fija_4">Cuenta Fija</label>
              <input type="checkbox" class="checkbox" id="cuenta_fija_4"
                     name="cuenta_fija_4"
                     disabled
                     data-bind="checked: cuentaFija"/>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6" id="section_localidad_pstn_data">
            <div class="form-group">
              <label class="control-label" for="localidad_4">Frame</label>
              <input id="localidad_4" class="form-control"
                     name="localidad_4"
                     disabled
                     data-bind="value: localidad">
              </input>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="terminal_4" class="control-label">Terminal</label>
              <input type="text" class="form-control" id="terminal_4"
              <%--data-validate="required"--%>
                     name="terminal_4"
                     disabled
                     placeholder="Terminal" data-bind="value: terminal"/>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6" id="section_cabina_pstn_data">
            <div class="form-group">
              <label for="cabina_4" class="control-label">Cabina</label>
              <input type="text" class="form-control"
              <%--data-validate="required"--%>
                     data-bind="value: cabina"
                     name="cabina_4"
                     id="cabina_4"
                     disabled
                     placeholder="Cabina"/>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="par_feeder_4" class="control-label">Par Feeder</label>
              <input type="text" class="form-control" id="par_feeder_4"
                     placeholder="Par Feeder"
              <%--placeholder=".*-####"--%>
                     name="par_feeder_4"
                     disabled
              <%--data-validate="required"--%>
                     data-bind="value: parFeeder"/>

            </div>
          </div>

          <div class="col-md-6" id="section_par_local_pstn_data">
            <div class="form-group">
              <label for="par_local_4" class="control-label">Par Local</label>
              <input type="text" class="form-control" id="par_local_4"
                     placeholder="Par Local"
              <%--data-validate="required"--%>
                     name="par_local_4"
                     disabled
                     data-bind="value: parLocal"/>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="tipo_puerto">Tipo de Puerto</label>
              <!-- <select id="tipo_puerto" class="form-control"
                      name="tipo_puerto"
                      disabled
                      data-bind="options: puertos, value: tipoPuerto ">
              </select> -->
              <input type="text" class="form-control" id="tipo_puerto"
                     placeholder="Tipo Puerto"
                     name="tipo_puerto"
                     disabled
                     data-bind="value: tipoPuerto"/>
            </div>
          </div>

          <div class="col-md-8">
            <div class="form-group">
              <label for="dslam" class="control-label">DSLAM</label>
              <input type="text" class="form-control"
              <%--data-validate="required"--%>
                     data-bind="value: dslam"
                     name="dslam"
                     disabled
                     id="dslam"
                      />
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="puerto" class="control-label">Puerto</label>
              <input type="text" class="form-control"
              <%--data-validate="required"--%>
                     data-bind="value: puerto"
                     name="puerto"
                     id="puerto"
                     disabled
                      />
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-md-1">
            <input type="checkbox" class="checkbox"
                   data-bind="disable: handOperatedDisabled, checked: handOperated"/>
          </div>
          <div class="col-md-5">
            <span>Operacion manual Por:</span>
          </div>
          <div class="col-md-6">
            <span class="control-label" data-bind="text: handOperatedBy"></span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-1">
            <input type="checkbox" class="checkbox"
                   data-bind="disable: processedDisabled, checked: processed"/>
          </div>
          <div class="col-md-5">
            <span>Reprocessado Por:</span>
          </div>
          <div class="col-md-6">
            <span class="control-label" data-bind="text: processedBy"></span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <table class="table table-condensed">
              <thead>
              <tr>
                <th>
                  Sistema
                </th>
                <th>
                  Resultado de Procesamiento
                </th>
                <th>
                  Facilidades
                </th>
              </tr>
              </thead>
              <tbody data-bind="template: { name: 'ActionResultTemplate', foreach: actionResults }">

              </tbody>

            </table>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button"
                  data-bind="click: updateAction, disable: isUpdateDisabled"
                  class="btn btn-info" data-loading-text="Guardando..."
                  class="btn-toggle-loading">Guardar
          </button>
          <button type="button" class="btn btn-default close-modal" data-dismiss="modal" data-bind="click: onClose">Cerrar
          </button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>