<%--
  Created by IntelliJ IDEA.
  User: Nathaniel Calderon
  Date: 4/11/2017
  Time: 2:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <!-- HEAD INCLUDES-->
    <%@include file="../head_pages.jsp" %>
</head>
<body class="page-body page-fade">

<div class="page-container">

    <!-- Sidebar Menu -->
    <%@include file="../../assets/fragments/sidebar_menu.jsp" %>
    <div class="main-content">
        <!-- Notifications and Raw Links -->
        <div class="row" id="top-bar">
            <%@include file="../../assets/fragments/profile_and_notifications.jsp" %>
            <%@include file="../../assets/fragments/raw_links.jsp" %>
        </div>
        <hr/>
        <h2>Centro de Soporte</h2>
        <ol id="breadCrumb"></ol>

        <hr/>
        <br/>

        <!-- CONTENT HERE -->
        <div id="main-container">




            <!-- OPERATIONS -->
            <div id="operations-container" class="row">
                <div class="col-md-2 col-md-offset-10">
                    <button type="button" id="refresh"
                            class="form-control btn-default"
                            data-bind="click: refresh"
                            class="btn btn-info btn-toggle-loading" data-loading-text="Cargando..."
                    >Refrescar
                    </button>
                </div>
            </div>

            <hr>
            <!--FILTERS-->
            <div id="filters-container" class="row">
                <div class="col-md-3">
                    <input class="form-control" placeholder="Tecnico" type="text"
                           data-bind="value: card" />
                </div>
                <div class="col-md-3">
                    <select class="select form-control" data-bind="options: orderTypes,
                              optionsText: 'label',
                              value: selectedOrderType,
                              optionsCaption: 'Tipo de Orden',
                              event: { change: onChangeOrderType }
                              "></select>
                </div>
                <div class="col-md-3">
                    <input class="form-control" name="orderCode" placeholder="Codigo de Orden" type="text"
                           data-bind="value: orderCode" />
                </div>
                <div class="col-md-3">
                    <button
                            type="button"
                            id="searchAction"
                            class="btn btn-info"
                            data-loading-text="Cargando..."
                            class="btn-toggle-loading form-control"
                            data-bind="click: searchAction"
                    >Buscar</button>
                </div>
            </div>

            <hr>
            <!-- ORDER ACTIONS -->
            <div class="row">
                <div class="col-md-12">
                    <!-- id="tbl-results" -->
                    <table id="tbl-results" class="table table-striped">
                        <thead>
                        <th>FORM_ID</th>
                        <th>FORM_TYPE</th>
                        <th>CODE</th>
                        <th>CREADO</th>
                        <th>TECNICO</th>
                        <th>TIPO</th>
                        </thead>
                        <tbody data-bind="template: { name: 'orderActionTemplate', foreach: orderActions }">

                        </tbody>
                    </table>
                </div>

            </div>

            <script id="orderActionTemplate" type="html/text">
                <tr class="orderActionRow">
                  <td>
                      <a href="javascript: void(0)" style="text-decoration: underline; color: blue;" data-bind="text: actionId, click: $parent.onActionClick.bind($data, $index())">
                      </a>
                  </td>
                      <td data-bind="text: formType">
                  </td>
                  <td data-bind="text: code">
                  </td>
                  <td data-bind="text: created">
                  </td>
                  <td data-bind="text: userId">
                  </td>
                  <td data-bind="text: orderType">
                  </td>
                </tr>
            </script>
            <!-- CONTENT END HERE -->
            <br/>
            <!-- FOOTER -->
            <%@include file="../../assets/fragments/footer.jsp" %>
        </div>

    </div>
</div>

<!-- BOTTOM INCLUDES-->
<%@include file="../bottom_includes_pages.jsp" %>
<script src="${pageContext.request.contextPath}/assets/js-custom/support/support-helper.js"></script>
<script src="${pageContext.request.contextPath}/assets/js-custom/support/support-config.js"></script>
<script src="${pageContext.request.contextPath}/assets/js-custom/support/support-controller.js"></script>


<%--FORMS--%>
<%@include file="../support/forms/templates.jsp" %>
<%@include file="../support/forms/pstn.jsp" %>
<%@include file="../support/forms/pstn_data.jsp" %>
<%@include file="../support/forms/pstn_fibra.jsp" %>
<%@include file="../support/forms/frame.jsp" %>
<%@include file="../support/forms/fibra.jsp" %>

<%--FORMS SCRIPTS--%>
<script src="${pageContext.request.contextPath}/assets/js-custom/support/forms/form_support.js"></script>
<script src="${pageContext.request.contextPath}/assets/js-custom/support/forms/form_utils.js"></script>

<script src="${pageContext.request.contextPath}/assets/js-custom/support/forms/pstn.js"></script>
<script src="${pageContext.request.contextPath}/assets/js-custom/support/forms/pstn_data.js"></script>
<script src="${pageContext.request.contextPath}/assets/js-custom/support/forms/pstn_fibra.js"></script>
<script src="${pageContext.request.contextPath}/assets/js-custom/support/forms/frame.js"></script>
<script src="${pageContext.request.contextPath}/assets/js-custom/support/forms/fibra.js"></script>

<script type="text/javascript">

    var main_model;
    var persistHandOperated = false;
    var persistProcessedBy = false;
    (function (ko, $, window, document, undefined) {
        setActiveMenuOption(4);

        var session = <% out.print(session.getAttribute("SessionJson"));%>;
        var _userLoggedCard = '<% out.print(session.getAttribute("userCard"));%>';
        var _ip = '<% out.print(session.getAttribute("ip"));%>';

        if (!checkAccess(session, 'CheckHandOperated', null)) {
            location.href = '${pageContext.request.contextPath}/index.jsp';
        }
        //deniedAccessIfNotAllowed(session, 'SecurityReport', 'device-report', 'web-report','actions-report','tasks-report');

        ko.cleanNode(document.getElementById("main-container"));
        var config = {
            _ip: _ip
            , _userLoggedID: _userLoggedCard
            , genReportPath: "/genReportPath"
            , processTaskPath: "/processTaskPath"
            ,baseURL: '${pageContext.request.contextPath}'
            ,_url: '${pageContext.request.contextPath}'
            ,findLastActionPath: '${pageContext.request.contextPath}/operations/SupportOrderActionService/findLastAction'
            ,getNonHandOperatedActionsPath: '${pageContext.request.contextPath}/operations/SupportOrderActionService/getNonHandOperatedActions'
            ,saveHandOperatedPath: '${pageContext.request.contextPath}/operations/SupportOrderActionService/saveHandOperated'
            ,saveProcessedByPath: '${pageContext.request.contextPath}/operations/SupportOrderActionService/saveProcessedBy'
            ,findActionPath: '${pageContext.request.contextPath}/operations/OrdersActionsService/getActionValuesByActionId'
        };

        main_model = new SupportModel(config);
        ko.applyBindings(main_model, document.getElementById("main-container")); // This makes Knockout get to work
        main_model.refresh();

    }(ko, jQuery, window, document));
</script>
</body>
</html>
