<%--
  Created by Christopher Herrera
  Date: 3/25/14
  Time: 1:04 PM
  Email: Christopher_Herrera@claro.com.do
  Office: 809-220-8148
  Cellphone: 829-755-1677
  CLARO RD
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <!-- HEAD INCLUDES-->
    <%@include file="../head_pages.jsp" %>
</head>
<body class="page-body page-fade">

<div class="page-container">

    <!-- Sidebar Menu -->
    <%@include file="../../assets/fragments/sidebar_menu.jsp" %>
    <div class="main-content">
        <!-- Notifications and Raw Links -->
        <div class="row" id="top-bar">
            <%@include file="../../assets/fragments/profile_and_notifications.jsp" %>
            <%@include file="../../assets/fragments/raw_links.jsp" %>
        </div>
        <hr/>
        <!-- CONTENT HERE -->
        <h2>Lista Dispositivos</h2>
        <ol id="breadCrumb">

        </ol>
        <hr/>
        <br/>


        <div>
            <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered datatable table-hover"
                   id="table"></table>
        </div>

        <!-- FOOTER -->
        <%@include file="../../assets/fragments/footer.jsp" %>
    </div>

</div>
<!-- BOTTOM INCLUDES-->
<%@include file="../bottom_includes_pages.jsp" %>
<script type="text/javascript">
    ;
    (function ($, window, document, undefined) {
        //page config
        setActiveMenuOption(22);
        var localData;
        $.ajax({
            url: "${pageContext.request.contextPath}/operations/DispositivoService/getAllDispositivos",
            type: "POST",
            async: true,
            data: {
                data: "asd"
            },
            success: function (data) {
                console.log(data);
            }
        });


        var table = $("#table").dataTable({
            "sPaginationType": "bootstrap",
            "bAutoWidth": false,
            "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
            "bProcessing": true,
            "sAjaxSource": "${pageContext.request.contextPath}/operations/DispositivoService/getAllDispositivos",
            "aoColumns": [
                { "sTitle": "Tarjeta" },
                { "sTitle": "Status" },
                { "sTitle": "Fecha Creacion" },
                { "sTitle": "Fecha Expiracion"},
                { "sTitle": "Marca"},
                { "sTitle": "Modelo"},
                { "sTitle": "Version Android"},
                { "sTitle": "Version App"},
                {"sTitle": "Options"}
            ],
            "aoColumnDefs": [
                {
                    "aTargets": [1],
                    "mRender": function (data, type, full) {
                        if (data == true || data == "true") {
                            return 'ACTIVO';
                        } else {
                            return 'INACTIVO';
                        }
                    }
                },
                {
                    "aTargets": [8],
                    "sWidth": "25px",
                    "sDefaultContent": '<div class="btn-group">' +
                            '<button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">' +
                            '<span class="entypo-menu"></span></button>' +
                            '<ul class="dropdown-menu dropdown-primary pull-right" role="menu">' +
                            '<li><a href="#"><i class="entypo-mail"></i>Mensaje de Prueba</a></li>' +
                            '<li><a href="#"><i class="entypo-ccw"></i>Actualizar Ordenes</a></li>' +
                            '<li><a href="#"><i class="entypo-trash"></i>Eliminar Aplicacion</a></li>' +
                            '<li><a href="#"><i class="entypo-pencil"></i>Modificar</a></li>' +
                            '</ul></div>'
                }

            ]
        });


    }(jQuery, window, document));
</script>
</body>
</html>