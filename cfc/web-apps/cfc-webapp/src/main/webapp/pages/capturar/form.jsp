<%--
  Created by Christopher Herrera
  Date: 3/25/14
  Time: 1:04 PM
  Email: Christopher_Herrera@claro.com.do
  Office: 809-220-8148
  Cellphone: 829-755-1677
  CLARO RD
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <!-- HEAD INCLUDES-->
    <%@include file="../head_pages.jsp" %>
</head>
<body class="page-body page-fade">

<div class="page-container">

    <!-- Sidebar Menu -->
    <%@include file="../../assets/fragments/sidebar_menu.jsp" %>
    <div class="main-content">
        <!-- Notifications and Raw Links -->
        <div class="row" id="top-bar">
            <%@include file="../../assets/fragments/profile_and_notifications.jsp" %>
            <%@include file="../../assets/fragments/raw_links.jsp" %>
        </div>
        <hr/>
        <!-- CONTENT HERE -->
        <h2>Lista Dispositivos</h2>
        <ol id="breadCrumb">

        </ol>
        <hr/>
        <br/>


        <div>
            <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered datatable table-hover"
                   id="table"></table>
        </div>

        <!-- FOOTER -->
        <%@include file="../../assets/fragments/footer.jsp" %>
    </div>

</div>
<!-- BOTTOM INCLUDES-->
<%@include file="../bottom_includes_pages.jsp" %>
</body>
</html>
