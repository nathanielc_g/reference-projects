<%--
  Created by Christopher Herrera
  Date: 1/10/14
  Time: 11:05 AM
  Email: Christopher_Herrera@claro.com.do
  Office: 809-220-8148
  Cellphone: 829-755-1677
  CLARO RD
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <!-- HEAD INCLUDES-->
    <%@include file="../head_pages.jsp" %>
</head>
<body class="page-body page-left-in">

<div class="page-container">

    <!-- Sidebar Menu -->
    <%@include file="../../assets/fragments/sidebar_menu.jsp" %>
    <div class="main-content">
        <!-- Notifications and Raw Links -->
        <div class="row" id="top-bar">
            <%@include file="../../assets/fragments/profile_and_notifications.jsp" %>
            <%@include file="../../assets/fragments/raw_links.jsp" %>
        </div>
        <hr/>
        <!-- CONTENT HERE -->
        <div class="panel-heading">
            <div class="panel-title">
                <h2>Lista Dispositivos</h2>
            </div>
            <div class="panel-options" id="div-send-update">
                <button id="send-update" type="button"  data-loading-text="Actualizando..." class="btn btn-primary center-block btn-icon" style="text-align: right">
                    Actualizar Aplicacion Movil
                    <i class="entypo-ccw"></i>
                </button>
            </div>
        </div>

        <ol id="breadCrumb">

        </ol>
        <hr/>
        <br/>


        <div>
            <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered datatable table-hover"
                   id="table"></table>
        </div>

        <!-- FOOTER -->
        <%@include file="../../assets/fragments/footer.jsp" %>
    </div>

</div>
<!-- BOTTOM INCLUDES-->
<%@include file="../bottom_includes_pages.jsp" %>
<script type="text/javascript">



    (function ($, window, document, undefined) {
        //page config

        setActiveMenuOption(22);
        var localData;
        var session = <% out.print(session.getAttribute("SessionJson"));%>;

        deniedAccessIfNotAllowed(session,'UpdateMobileApp','div-send-update');
        deniedAccessIfNotAllowed(session, 'SearchDevice','menu-option-2-1','menu-option-2-2');
        deniedAccessIfNotAllowed(session, 'CreateDevice','menu-option-2-3');

        $("#send-update").on( 'click',function(event){

            if( !checkAccess(session,'UpdateMobileApp',null ) )
                return;

            var _userLoggedCard = '<% out.print(session.getAttribute("userCard"));%>';
            var _ip = '<% out.print(session.getAttribute("ip"));%>';

            $.ajax({
                url: "${pageContext.request.contextPath}/operations/DispositivoService/sendUpdateMessageToAll",
                type: "POST",
                async: true,
                data: {
                    data: "asd",
                    userLoggedCard:_userLoggedCard,
                    ip: _ip
                },
                beforeSend:toggleLoadingButton('#send-update'),
                success: function (data) {
                },
                complete: function () {
                    refreshTable('#table',"${pageContext.request.contextPath}/operations/DispositivoService/getAllDispositivos");
                    toggleLoadingButton('#send-update');
                    $.growl({ title: "Actualizacion Movil", message: "Las aplicaciones moviles han sido actualizadas" });
                }
            });
        });



        var table = $("#table").dataTable({
            "sPaginationType": "bootstrap",
            "bAutoWidth": false,
            "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
            "bProcessing": true,
            "sAjaxSource": "${pageContext.request.contextPath}/operations/DispositivoService/getAllDispositivos",
            "aoColumns": [
                { "sTitle": "Tarjeta" },
                { "sTitle": "Status" },
                { "sTitle": "Fecha Reactivacion"},
                { "sTitle": "Fecha Creacion" },
                { "sTitle": "Fecha Expiracion"},
                { "sTitle": "Telefono"},
                { "sTitle": "Marca"},
                { "sTitle": "Modelo"},
                { "sTitle": "Version Android"},
                { "sTitle": "Version App"},
                { "sTitle": "IMEI"}
//                { "sTitle": "Options"}
            ],
            "aoColumnDefs": [
                {
                    "aTargets": [1],
                    "mRender": function (data, type, full) {
                        return getUserStatus()[data].name;
                        /*if (data == true || data == "true") {
                            return 'ACTIVO';
                        } else {
                            return 'INACTIVO';
                        }*/
                    }
                }
//                {
//                    "aTargets": [8],
//                    "sWidth": "25px",
//                    "sDefaultContent": '<div class="btn-group">' +
//                            '<button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">' +
//                            '<span class="entypo-menu"></span></button>' +
//                            '<ul class="dropdown-menu dropdown-primary pull-right" role="menu">' +
////                            '<li><a href="#" id="user-delete"><i class="entypo-trash"></i>Eliminar Usuario</a></li>' +
//                            '<li><a href="#" onclick=""><i class="entypo-pencil"></i>Ver Ordenes/Averias</a></li>' +
//                            '<li><a href="#" onclick=""><i class="entypo-pencil"></i>Ver Ordenes/Averias Capturadas</a></li>' +
//                            '</ul></div>'
//                }

            ]
        });



    }(jQuery, window, document));

</script>
</body>
</html>