<%--
  Created by Christopher Herrera
  Date: 1/9/14
  Time: 12:00 PM
  Email: Christopher_Herrera@claro.com.do
  Office: 809-220-8148
  Cellphone: 829-755-1677
  CLARO RD
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <!-- HEAD INCLUDES-->
    <%@include file="../head_pages.jsp" %>
</head>
<body class="page-body page-left-in">

<div class="page-container">

<!-- Sidebar Menu -->
<%@include file="../../assets/fragments/sidebar_menu.jsp" %>
<div class="main-content">
    <!-- Notifications and Raw Links -->
    <div class="row" id="top-bar">
        <%@include file="../../assets/fragments/profile_and_notifications.jsp" %>
        <%@include file="../../assets/fragments/raw_links.jsp" %>
    </div>
    <hr/>
    <!-- CONTENT HERE -->

    <h2>Busqueda Dispositivos</h2>
    <ol id="breadCrumb">

    </ol>
    <hr/>
    <br/>

    <!-- Search -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel minimal minimal-gray">
                <div class="panel-heading">
                    Buscar
                </div>
                <div class="panel-body">
                    <form role="form" class="form-horizontal form-groups-bordered" data-bind="submit: fillContents">

                        <!-- Fila 1 -->
                        <div class="form-group">
                            <label for="search-criteria" class="col-sm-1 control-label">
                                Por:
                            </label>

                            <div class="col-sm-3">
                                <select id="search-criteria" class="form-control"
                                        data-bind="options: criteriaOptions, value: criteriaSelected "></select>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                        <span class="input-group-addon">
                                             <i id="search-input-icon" class="entypo-user"></i>
                                        </span>
                                    <input type="text" id="value-input" placeholder="Value" class="form-control"
                                           data-bind="value:inputValue"/>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-red" data-bind="disable: isLoading">
                                <i class="entypo-search">
                                </i>
                            </button>
                        </div>
                        <div class="sk-circle" data-bind="visible: isLoading">
                            <div class="sk-circle1 sk-child"></div>
                            <div class="sk-circle2 sk-child"></div>
                            <div class="sk-circle3 sk-child"></div>
                            <div class="sk-circle4 sk-child"></div>
                            <div class="sk-circle5 sk-child"></div>
                            <div class="sk-circle6 sk-child"></div>
                            <div class="sk-circle7 sk-child"></div>
                            <div class="sk-circle8 sk-child"></div>
                            <div class="sk-circle9 sk-child"></div>
                            <div class="sk-circle10 sk-child"></div>
                            <div class="sk-circle11 sk-child"></div>
                            <div class="sk-circle12 sk-child"></div>
                        </div>

                        <div class="form-group form-horizontal" data-bind="visible: botonesActivos">
                            <div class="col-sm-3" id="div-send-update">
                                <button type="button" id="send-update" data-loading-text="Actualizando..."
                                        class="btn btn-primary btn-icon" data-bind="click: updateApplication">
                                    Actualizar Aplicaci&oacute;n
                                    <i class="entypo-ccw"></i>
                                </button>
                            </div>
                            <div class="col-sm-3" id="div-seePendingDispatchItems">
                                <button type="button" class="btn btn-primary btn-icon"
                                        data-bind="click: seePendingDispatchItems">
                                    Ver Ordenes/Averias
                                    <i class="entypo-ticket"></i>
                                </button>
                            </div>
                            <div class="col-sm-3" id="div-seeSubmittedDispatchItems">
                                <button type="button" class="btn btn-primary btn-icon"
                                        data-bind="click: seeSubmittedDispatchItems">
                                    Ver Facilidades Digitadas
                                    <i class="entypo-doc-text-inv"></i>
                                </button>
                            </div>
                            <div class="col-sm-3" id="div-update-user">
                                <button type="button" class="btn btn-primary btn-icon update-btn" 
                                        data-bind="click: updateUser.bind($data)">
                                    Modificar
                                    <i class="entypo-pencil"></i>
                                </button>
                            </div>
                            <%--<div class="col-sm-3" id="div-update-status">
                                <button type="button" class="btn btn-primary btn-icon update-btn" 
                                        data-bind="click: updateUser.bind($data, 'UpdateDeviceTempStatus')">
                                    Modificar
                                    <i class="entypo-pencil"></i>
                                </button>
                            </div>--%>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- dataForm Tecnico-->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Data Tecnico</div>
                <div class="panel-body">
                    <form role="form" class="form-horizontal form-groups-bordered">
                        <!-- Fila 1-->
                        <div class="form-group">
                            <label for="data-imei" class="col-sm-1 control-label">IMEI: </label>

                            <div class="col-sm-5">
                                <input type="text" data-bind="value: imei" class="form-control
                                InputUpdateDevice
                                InputUpdateDeviceImei" id="data-imei"
                                       placeholder="IMEI"
                                       disabled
                                        />
                            </div>
                            <label for="data-tarjeta" class="col-sm-1 control-label">Tarjeta: </label>

                            <div class="col-sm-5">
                                <input type="text" data-bind="value: tarjeta" class="form-control" id="data-tarjeta"
                                       placeholder="Tarjeta"
                                       disabled/>
                            </div>
                        </div>
                        <!-- Fila 2-->
                        <div class="form-group">
                            <!-- 2016-GESTI-5048-1 - Logic for status property. -->
                            <label class="col-sm-1 control-label" for="data-status" >Status</label>
                            <div class="col-sm-5" data-bind="visible: showStatusEdit">
                                <select id="data-status" class="form-control
                                InputUpdateDevice
                                InputUpdateDeviceTempStatus
                                "
                                    name="status"
                                    data-bind="options: statusOptions, optionsText: 'name', value: statusSelected">
                                </select>        
                            </div>
                            <div class="col-sm-5" data-bind="visible: !showStatusEdit()">
                                <select id="view-status" class="form-control"
                                    name="view-status"
                                    data-bind="options: statusOptions, optionsText: 'name', value: statusSelected"
                                        disabled
                                        >
                                </select>        
                            </div>
                            <label for="data-reactivation" class="col-sm-1 control-label">Fecha Reactivacion:</label>
                            <div class="col-sm-5">
                                <input type="text" data-bind="value: reactiveDate" class="form-control"
                                id="data-reactivation" placeholder="Fecha Reactivacion" disabled/>
                            </div>
                            <!-- <label for="data-status" class="col-sm-1 control-label">Status: </label>
                            
                            <div class="col-sm-5">
                                <div class="make-switch has-switch" id="data-status">
                                    <div class="switch-on" style="left: 0%;">
                                        <span class="switch-left switch-success" data-bind="data-status">ON</span>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <!-- Fila 3 -->
                        <div class="form-group">
                            <label for="data-creado" class="col-sm-1 control-label">Fecha Creacion:</label>

                            <div class="col-sm-5">
                                <input type="text" data-bind="value: creado" class="form-control" id="data-creado"
                                       placeholder="Fecha Creacion" disabled/>
                            </div>
                            <label for="data-expiracion" class="col-sm-1 control-label">Fecha Expiracion:</label>

                            <div class="col-sm-5">
                                <input type="text" data-bind="value: expiracion" class="form-control"
                                       id="data-expiracion" placeholder="Fecha Expiracion" disabled/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- DataForm Dispositivo -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Data Dispositivo</div>
                <div class="panel-body">
                    <form role="form" class="form-horizontal form-groups-bordered">
                        <!-- Fila 1-->
                        <div class="form-group">
                            <label for="data-marca" class="col-sm-1 control-label">Marca: </label>

                            <div class="col-sm-5">
                                <input type="text" data-bind="value: marca" class="form-control" id="data-marca"
                                       placeholder="Marca" disabled/>
                            </div>
                            <label for="data-modelo" class="col-sm-1 control-label">Modelo: </label>

                            <div class="col-sm-5">
                                <input type="text" data-bind="value: modelo" class="form-control" id="data-modelo"
                                       placeholder="Modelo" disabled/>
                            </div>
                        </div>
                        <!-- Fila 2-->
                        <div class="form-group">
                            <label for="data-versionAndroid" class="col-sm-1 control-label">Version
                                Android: </label>

                            <div class="col-sm-5">
                                <input type="text" data-bind="value: versionAndroid" class="form-control"
                                       id="data-versionAndroid" placeholder="Version Android" disabled/>
                            </div>
                            <label for="data-versionApp" class="col-sm-1 control-label">Version App: </label>

                            <div class="col-sm-5">
                                <input type="text" data-bind="value: versionApp" class="form-control"
                                       id="data-versionApp" placeholder="Version App" disabled/>
                            </div>
                        </div>
                        <!-- Fila 3-->
                        <div class="form-group">
                            <label for="data-telefono" class="col-sm-1 control-label">Telefono: </label>

                            <div class="col-sm-5">
                                <input type="text" data-bind="value: telefono" class="form-control
                                    InputUpdateDevice
                                    InputUpdateDevicePhone
                                "
                                       id="data-telefono" placeholder="Telefono"
                                       disabled
                                        />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- FOOTER -->
    <%@include file="../../assets/fragments/footer.jsp" %>
</div>

</div>
<!-- BOTTOM INCLUDES-->

<%@include file="../bottom_includes_pages.jsp" %>
<script type="text/javascript">

// 2016-GESTI-5048-1 - New logic for status property.
function disableUpdateComponents (disable) {
    // Since 2017-GESTI-0290 - any profile with role 'UpdateDeviceImei' is able to update IMEI
    $('#data-status option:eq(0)').addClass("InputUpdateDevice");
    $('#data-status option:eq(0)').attr("disabled", true);
    disableUpdateDevice(disable);
    /*if ("UpdateDevice" === rol) {
        disableForUpdateDevice(disable);
    } else if ("UpdateDeviceTempStatus" === rol) {
        disableForUpdateDeviceTempStatus(disable);
    }*/
    if (disable) {
        setButtonDisabled('.update-btn', false);
        $('.update-btn').html('Modificar <i class="entypo-pencil"></i>');
    }

}

function renderViewAfterUpdate (rol) {

    setButtonDisabled('.update-btn', false);
    // 2016-GESTI-5048-1 - Logic for status property. 
    //disableUpdateComponents(true);    
    //disableUpdateComponents(rol, true);
    // Since 2017-GESTI-0290 - any profile with role 'UpdateDeviceImei' is able to update IMEI
    $('#data-status option:eq(0)').addClass("InputUpdateDevice");
    disableUpdateDevice(disable);

    $('.update-btn').html('Modificar <i class="entypo-pencil"></i>');
}
function disableForUpdateDevice(disable) {

    // 2016-GESTI-5048-1 - New logic for status property.
    /*if (disable) {
        console.log('trying to set empty click handle');

        $('#data-status').find('span').unbind('click');

    } else {
        console.log('trying to set click handle');

        $('#data-status').find('span').unbind('click');
        $('#data-status').find('span').on('click', function (e) {
            if(e.target !== this )
                return;

            console.log('witch is trying to handle click? '+$(this).data('disabled') );
            console.log($(this));

            text = 'ON';
            classRemove = 'switch-danger';
            classAdd = 'switch-success';

            if ($(this).text() == 'ON') {
                text = 'OFF';
                classRemove = 'switch-success';
                classAdd = 'switch-danger';
            }
            console.log( 'switch click handler set with, text: '+text+", classRemove: "+classRemove+", clasAdd: "+classAdd );
            $(this).text(text);
            $(this).removeClass(classRemove);
            $(this).addClass(classAdd);
        });
    }*/
    $('#data-status').attr('disabled', disable);
    $('#data-imei').attr('disabled', disable);
    $('#data-telefono').attr('disabled', disable);
}

function disableForUpdateDeviceTempStatus(disable) {
    // For permission UpdateDeviceTempStatus status INACTIVE is not allowed
    $('#data-status option:eq(0)').attr("disabled", true);
    $('#data-status').attr('disabled', disable);
}

// 2017-GESTI-0290 - any profile with role 'UpdateDeviceImei' is able to update IMEI
function disableUpdateDevice (disable) {
    // Each rol that make readonly any field must start with "UpdateDevice"
    var actions = getSession().accesos.filter(function (value) { console.log(value); return value.indexOf("UpdateDevice")>-1;})
    for(var i in actions) {
        console.log("Setting readonly action#"+ actions[i]);
        //$('#data-status option:eq(0)').prop("disabled", false);
        $(".Input" + actions[i]).prop("disabled", disable);
    }
}

//Setting the page conf
var dataTypeahead = {imeis: [], tarjetas: [], telefonos:[]};
var session = <% out.print(session.getAttribute("SessionJson"));%>;

(function (ko, $, window, document, undefined) {

    deniedAccessIfNotAllowed(session, 'UpdateMobileApp', 'div-send-update');
    deniedAccessIfNotAllowed(session, 'ViewDI', 'div-seePendingDispatchItems', 'div-seeSubmittedDispatchItems');
    deniedAccessIfNotAllowed(session, 'SearchDevice','menu-option-2-1','menu-option-2-2');
    deniedAccessIfNotAllowed(session, 'CreateDevice','menu-option-2-3');

    // Since 2017-GESTI-0290 - any profile with role 'UpdateDeviceImei' is able to update IMEI
    $("#div-update-user").hide();
    allowAccess(session, 'UpdateDevice', 'div-update-user');
    // 2016-GESTI-5048-1 - New logic for status property.
    allowAccess(session, 'UpdateDeviceTempStatus', 'div-update-user');
    // Since 2017-GESTI-0290 - any profile with role 'UpdateDeviceImei' is able to update IMEI
    allowAccess(session, 'UpdateDeviceImei', 'div-update-user');
    allowAccess(session, 'UpdateDevicePhone', 'div-update-user');

    /*$.ajax({
        async: true,
        type: "POST",
        url: "${pageContext.request.contextPath}/operations/DispositivoService/getDataSearchTypeAhead",
        success: function (data) {
            dataTypeahead = data;
        }
    });*/

    console.log(dataTypeahead);
    setActiveMenuOption(21);

    $('#submit-btn').on('click', function () {
        $('#value-input').trigger('change');
        console.log($('#value-input').val());
    });
    $('#value-input').blur(function () {
        $(this).change();
        console.log($(this).val());
    });

    $('#value-input').keyup(function(event) {
        if (event.keyCode === 13) {
            $('#submit-btn').click();
        }
    });

    //Modelo Principal
    function AppViewModel() {

        var _userLoggedCard = '<% out.print(session.getAttribute("userCard"));%>';
        var _ip = '<% out.print(session.getAttribute("ip"));%>';

        console.log( 'User That made the change '+_userLoggedCard+' under ip '+_ip );

        var self = this;
        self.criteriaOptions = ko.observableArray(["Seleccionar", "Tarjeta", "Telefono", "IMEI"]);
        self.criteriaSelected = ko.observable("Seleccionar");
        self.inputValue = ko.observable();

        //variables contenido
        self.imei = ko.observable();
        self.tarjeta = ko.observable();

        // 2016-GESTI-5048-1 - New logic for status property.
        //self.status = ko.observable(false);
        self.statusOptions = ko.observableArray(getUserStatus());    
        self.statusSelected = ko.observable(getUserStatus()[0]);
        self.reactiveDate = ko.observable();
        self.showStatusEdit = ko.observable(false);
        self.canInactiveTemp = ko.observable(false);

        self.creado = ko.observable();
        self.expiracion = ko.observable();
        self.marca = ko.observable();
        self.modelo = ko.observable();
        self.versionAndroid = ko.observable();
        self.versionApp = ko.observable();
        self.telefono = ko.observable();
        self.updating = false;

        //variables botones
        self.botonesActivos = ko.observable(false);

        //Variable for previuos state
        self.previousImei = null;

        self.imei.subscribe(function (oldValue, nexValue) {
            console.log('Preparing to update data, from ' + oldValue + " to " + nexValue);
            self.previousImei = oldValue;
        }, this, 'beforeChange');

        self.isLoading = ko.observable(false);


        // 2016-GESTI-5048-1 - New logic for status property.
        //status subscriptor
        /*self.status.extend({notify: 'always'});
        self.status.subscribe(function (newValue) {
            $status = $('#data-status');
            $status.empty();
            if (newValue == 'true' || newValue === true) {
                console.log('to be supposed change onload to ' + newValue + ' to on');
                $status.html(
                        $('<div/>')
                                .addClass('switch-on')
                                .css({left: '0%'})
                                .html(
                                $('<span/>').addClass('switch-left switch-success').html('ON')
                        )
                );
            } else {
                console.log('to be supposed change onload to ' + newValue + ' to off');
                $status.html(
                        $('<div/>')
                                .addClass('switch-off')
                                .css({left: '0%'})
                                .html(
                                $('<span/>').addClass('switch-left switch-danger').html('OFF')
                        )
                );
            }
        });*/

        //criteria Selected subscriptor
        self.criteriaSelected.subscribe(function (newValue) {
            $inputIcon = $("#search-input-icon");
            $valueInput = $("#value-input");
            switch (newValue) {
                case "Tarjeta":
                    $inputIcon.removeClass().addClass("entypo-vcard");
                    console.log(dataTypeahead);
                    $valueInput.val('').typeahead('destroy').typeahead({
                        name: 'Tarjetas',
                        local: dataTypeahead.tarjetas
                    });

                    break;
                case "Telefono":
                    $inputIcon.removeClass().addClass("entypo-mobile");
                    console.log(dataTypeahead);
                    $valueInput.val('').typeahead('destroy').typeahead({
                        name: 'Telefonos',
                        local: dataTypeahead.telefonos
                    });

                    break;
                case "IMEI":
                    $inputIcon.removeClass().addClass("entypo-ticket");
                    console.log(dataTypeahead);
                    $valueInput.val('').typeahead('destroy').typeahead({
                        name: 'IMEI',
                        local: dataTypeahead.imeis
                    });

                    break;
                default :
                    $valueInput.val('').typeahead('destroy');
                    break;
            }
        });
        /*
         self.inputValue.subscribe(function(){

         });*/

        self.fillContents = function () {

            self.isLoading(true);
            var criteria = self.criteriaSelected();
           // self.criteriaSelected('');
            var value = self.inputValue();

            // Check is the input value is not empty text
            if (!$.trim(value))
                return;

            //value = $('#value-input').val();
            //self.inputValue('');
            $.ajax({
                async: true,
                type: "POST",
                url: "${pageContext.request.contextPath}/operations/DispositivoService/searchByCriteriaFormData",
                data: {
                    criteria: criteria,
                    valor: value,
                    userLoggedCard: _userLoggedCard,
                    ip : _ip
                },
                success: function (data) {
                    console.dir(data);

                    if (data == null) {
                        $.growl({title: "Error de Busquedad", message: "Usuario no pudo ser encontrado."});
                        self.isLoading(false);
                        return;
                    }

                    self.isLoading(false);
                    self.recentLoadedUser = data;

                    console.log('recentLoadedUser');
                    console.dir(self.recentLoadedUser);
                    window.history.replaceState(null, "Captura Facilidades Celular", window.location.pathname + "?" + criteria + "=" + value);
                    console.log(window.location.pathname + "?" + criteria + "=" + value);
                    self.previousImei = null;
                    self.botonesActivos(true);
                    self.imei(data.imei);
                    self.tarjeta(data.tarjeta);

                    // 2016-GESTI-5048-1 - Logic for status property.
                    //self.status(data.status);
                    self.statusSelected(self.statusOptions()[data.status]);
                    self.reactiveDate(data.reactiveDate);
                    self.canInactiveTemp(data.canInactiveTemp === "true");

                    self.creado(data.creacion);
                    self.expiracion(data.expiracion);
                    self.marca(data.marca);
                    self.modelo(data.modelo);
                    self.versionAndroid(data.os);
                    self.versionApp(data.app);
                    self.telefono(data.telefono);
                },
                error: function () {
                    self.isLoading(false);
                }
            });

        };

        self.cleanContents = function () {
            self.imei('IMEI');
            self.tarjeta('Tarjeta');

            // 2016-GESTI-5048-1 - Logic for status property.
            //self.status(false);
            self.status(getUserStatus()[0]);
            self.reactiveDate('Fecha Reactivacion');
            
            self.creado('Fecha Creacion');
            self.expiracion('Fecha Expiracion');
            self.marca('Marca');
            self.modelo('Modelo');
            self.versionAndroid('Version Android');
            self.versionApp('Version App');
            self.telefono('Telefono')
        };


        self.updateApplication = function () {
            if (!checkAccess(session, 'UpdateMobileApp', null))
                return;

            $.ajax({
                url: "${pageContext.request.contextPath}/operations/DispositivoService/sendUpdateMessageToPhone",
                type: "POST",
                async: true,
                data: {
                    card: self.recentLoadedUser.tarjeta,
                    userLoggedCard: _userLoggedCard,
                    ip : _ip
                },
                beforeSend: toggleLoadingButton('#send-update'),
                success: function (data) {
                },
                complete: function () {
//                    toggleLoadingButton('#send-update');
                    $.growl({ title: "Actualizacion Movil", message: "Las aplicacion movil fue actualizada" });
                }, error: function (data) {
                    self.updating = false;
                }
            });

            console.log('preparint to update user app')
            console.dir(self.recentLoadedUser);
        };

        self.seePendingDispatchItems = function () {
            if (!checkAccess(session, 'ViewDI', null))
                return;

            console.log('preparing to see dispatch items');
            window.location.href = "${pageContext.request.contextPath}/pages/items/ordenes.jsp?q=" + self.recentLoadedUser.tarjeta;
        };

        self.seeSubmittedDispatchItems = function () {
            if (!checkAccess(session, 'ViewDI', null))
                return;
            console.log('preparing to actions');
            window.location.href = "${pageContext.request.contextPath}/pages/items/facilidades.jsp?q=" + self.recentLoadedUser.tarjeta;
        }

        self.updateUser = function (data) {


            //if (!checkAccess(session, rol, null))
            if (!checkAccess(session, 'UpdateDevice', null, true) && !checkAccess(session, 'UpdateDeviceTempStatus', null, true) && !checkAccess(session, 'UpdateDeviceImei', null, true)) {
                $.growl({ title: "Error de Acceso", message: "A su perfil no le esta permitido ejecutar esta accion" });
                return;
            }


            if (!self.updating) {
                self.updating = true;
                $('.update-btn').text('Guardar Cambios');
    

                self.showStatusEdit(true);

                // 2016-GESTI-5048-1 - Logic for status property. 
                //disableUpdateComponents(false);    
                disableUpdateComponents(false);

                return;
            }            

            // 2016-GESTI-5048-1 - Logic for status property.
            //var _status = $('#data-status').find('span').text() == 'ON' ? 1 : 0;
            var _status =  self.statusSelected().value;
            self.showStatusEdit(false);
            // IF STATUS == 2 AND canInactiveTemp is false, User won't be able to update technician
            //"UpdateDeviceTempStatus" == rol &&
            if (!checkAccess(session, 'UpdateDevice', null, true) &&
                    checkAccess(session, 'UpdateDeviceTempStatus', null, true) && _status == 2 && !self.canInactiveTemp()) {

                // 2016-GESTI-5048-1 - Logic for status property.
                //renderViewAfterUpdate(rol);
                disableUpdateComponents(true);
                self.updating = false;
                console.log("Technician cannot be updated. canInactiveTemp: " + self.canInactiveTemp());
                $.growl({'title': 'Error Actualizando Usuario', 'message': 'Usuario no puede ser desactivado temporalmente hasta despues de un mes de su fecha de inactivacion'});
                self.statusSelected(self.statusOptions()[self.recentLoadedUser.status]);
                return;
            }
            
            console.log('status to be sent: ' + _status);

            $.ajax({
                url: '${pageContext.request.contextPath}/operations/DispositivoService/updateUserAndDevice',
                type: 'POST',
                data: {
                    oldImei: self.previousImei,
                    imei: self.imei(),
                    tarjeta: self.tarjeta(),
                    phone: self.telefono(),
                    status: _status,
                    userLoggedCard: _userLoggedCard,
                    ip: _ip
                },
                beforeSend: function () {
                    console.log('Data to be sent ' + self.previousImei + ' : ' + self.imei() + ':' + self.tarjeta() + ' : ' + self.telefono() + ':' + _status);
                    //toggleLoadingButton('.update-btn');
                    setButtonDisabled('.update-btn', true);
                    $('.update-btn').text('Actualizando Usuario...');
                },
                success: function (data) {
                    if (data.ok == true || data.ok == 'true') {
                        $.growl({'title': 'Usuario Actualizado', 'message': ''});
                    } else {
                        console.log("Data retrieved from server on updateUser");
                        console.dir(data);
                        $.growl({'title': 'Error Actualizando Usuario', 'message': data.msg});
                        self.statusSelected(self.statusOptions()[self.recentLoadedUser.status]);
                        self.imei(self.recentLoadedUser.imei);
                        self.telefono(self.recentLoadedUser.telefono);
                    }
                    // 2016-GESTI-5048-1 - Logic for status property.                    
                    //renderViewAfterUpdate(rol);
                    disableUpdateComponents(true);
                    self.updating = false;
                }, error: function (data) {
                    self.updating = false;
                }
            });

        }

    }//AppViewModel

    ko.bindingHandlers.visible = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = ko.utils.unwrapObservable(valueAccessor());

            var $element = $(element);

            if (value)
                $element.show();
            else
                $element.hide();
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = ko.utils.unwrapObservable(valueAccessor());

            var $element = $(element);

            var allBindings = allBindingsAccessor();

            // Grab data from binding property
            var duration = allBindings.duration || 500;
            var isCurrentlyVisible = !(element.style.display == "none");

            if (value && !isCurrentlyVisible)
                $element.show(duration);
            else if ((!value) && isCurrentlyVisible)
                $element.hide(duration);
        }
    };
    var appViewModel = new AppViewModel();
    ko.applyBindings(appViewModel);
    loadDeviceByParam(appViewModel);
}(ko, jQuery, window, document));

function loadDeviceByParam(viewModel) {
    var paramName = "Tarjeta";
    var paramValue = getParameterByName(paramName);


    if(!paramValue) {
        paramName = "Telefono";
        paramValue = getParameterByName(paramName);
    }
    if(!paramValue){
        paramName = "IMEI";
        paramValue = getParameterByName(paramName);
    }

    if(!paramValue)
        return;

    viewModel.criteriaSelected(paramName);
    viewModel.inputValue(paramValue);
    viewModel.fillContents();
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
</script>
</body>
</html>