    <%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%--
  Created by Christopher Herrera
  Date: 1/8/14
  Time: 2:50 PM
  Email: Christopher_Herrera@claro.com.do
  Office: 809-220-8148
  Cellphone: 829-755-1677
  CLARO RD
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <!-- HEAD INCLUDES-->
    <%@include file="../head_pages.jsp" %>
    <% java.text.DateFormat df = new SimpleDateFormat("dd/MM/yyyy"); %>
</head>
<body class="page-body page-left-in">

<div class="page-container">

<!-- Sidebar Menu -->
<%@include file="../../assets/fragments/sidebar_menu.jsp" %>
<div class="main-content">
<!-- Notifications and Raw Links -->
<div class="row" id="top-bar">
    <%@include file="../../assets/fragments/profile_and_notifications.jsp" %>
    <%@include file="../../assets/fragments/raw_links.jsp" %>
</div>
<hr/>
<h2>Crear Dispositivo/Usuario</h2>
<ol id="breadCrumb"></ol>
<hr/>
<br/>
<!-- CONTENT HERE -->

<div class="col-md-12">
<form id="rootwizard-2" method="post" action="" class="form-wizard validate">


<div class="steps-progress">
    <div class="progress-indicator"></div>
</div>


<ul>
    <li class="active">
        <a href="#tab2-1" data-toggle="tab"><span>1</span>Tecnico Info</a>
    </li>
    <li>
        <a href="#tab2-2" data-toggle="tab"><span>2</span>Dispositivo</a>
    </li>
    <li>
        <a href="#tab2-3" data-toggle="tab"><span>3</span>Confirmar</a>
    </li>
</ul>


<div class="tab-content">
<div class="tab-pane active" id="tab2-1">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Informacion Tecnico
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="IMEI">IMEI Disp:</label>
                            <input class="form-control" name="IMEI" id="IMEI"
                                   data-validate="required,number,minlength[15]" data-mask="999999999999999"
                                   data-bind="value: IMEI" placeholder="IMEI Dispositivo"/>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="tarjeta">Tarjeta Emp.</label>
                            <input class="form-control" name="tarjeta" id="tarjeta"
                                   data-validate="required,number,minlength[1]" data-bind="value: tarjeta"
                                   placeholder="Numero Tarjeta"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="expiracion">Expiracion</label>
                            <input class="form-control datepicker" name="expiracion" id="expiracion"
                                   data-validate="required" data-bind="value: expiracion"
                                   placeholder="<%= df.format(new Date()) %>"/>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="status">Status</label>
                            <input type="checkbox" class="checkbox" id="status" data-bind="checked: status"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="telefono">Telefono</label>
                            <input class="form-control" name="telefono" data-mask="phone" id="telefono"
                                   data-validate="required" data-bind="value: telefono" placeholder="Telefono"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="tab-pane" id="tab2-2">

    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Detalle
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="marca">Marca Disp.</label>
                            <input class="form-control" name="marca" id="marca" data-bind="value: marca"
                                   data-validation="required" placeholder="Marca" disabled/>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="modelo">Modelo Disp.</label>
                            <input class="form-control" name="modelo" id="modelo" data-validate="required"
                                   data-bind="value: modelo" placeholder="Modelo" disabled/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="highlight" data-bind="foreach: dispositivos">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <img data-src="holder.js/300x200" data-bind="attr:{ src: img, alt: marca}"
                         style=" height: 180px;" src="">

                    <div class="caption">
                        <h3><span data-bind="text: marca"></span> <span data-bind="text: version"></span></h3>

                        <p>Marca: <span data-bind="text: marca"></span></p>

                        <p>Modelo: <span data-bind="text: modelo"></span></p>

                        <p>Version: <span data-bind="text: version"></span></p>

                        <p>
                            <button type="button" data-bind="click: function(){$root.setDisp($data)}"
                                    class="btn btn-primary center-block" role="button">Seleccionar
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="tab-pane" id="tab2-3">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Informacion Tecnico
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="IMEI-conf">IMEI</label>
                            <input class="form-control" name="IMEI" id="IMEI-conf" data-bind="value: IMEI"
                                   disabled/>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="tarjeta-conf">Tarjeta Empleado</label>
                            <input class="form-control" name="tarjeta" id="tarjeta-conf" data-bind="value: tarjeta"
                                   disabled/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="expiracion-conf">Expiracion</label>
                            <input class="form-control datepicker" name="expiracion" id="expiracion-conf"
                                   data-bind="value: expiracion" disabled/>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="status-conf">Status</label>
                            <input type="checkbox" class="checkbox" id="status-conf" data-bind="checked: status"
                                   disabled/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="marca">Marca</label>
                            <input class="form-control" name="marca" id="marca-conf" data-bind="value: marca"
                                   disabled/>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="modelo">Modelo</label>
                            <input class="form-control" name="modelo" id="modelo-conf" data-bind="value: modelo"
                                   disabled/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label" for="telefono-conf">Telefono</label>
                        <input class="form-control" name="telefono-conf" id="telefono-conf"
                               data-bind="value:telefono" disabled/>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label" for="os-conf">Version OS</label>
                        <input class="form-control" name="os-conf" id="os-conf" data-bind="value:os" disabled/>
                    </div>
                </div>
                <div class="row">
                    <br/>
                    <br/>

                    <div class="col-sm-4"></div>
                    <div class="col-sm-4 center-block">
                        <button type="button" data-loading-text="Creando Usuario..." class=" form-control btn btn-primary center-block" id="finBtn"
                                data-bind="click: saveForm()">Finalizar
                        </button>
                    </div>
                    <div class="col-sm-3"></div>
                </div>

            </div>
        </div>
    </div>
</div>

<ul class="pager wizard">
    <li class="previous">
        <a href="#"><i class="entypo-left-open"></i> Previous</a>
    </li>

    <li class="next">
        <a href="#">Next <i class="entypo-right-open"></i></a>
    </li>
</ul>
</div>

</form>
</div>


<!-- Footer -->


<!-- FOOTER -->
<%@include file="../../assets/fragments/footer.jsp" %>
</div>

</div>
<!-- BOTTOM INCLUDES-->
<%@include file="../../assets/fragments/bottom_includes.jsp" %>
<script type="text/javascript">

    (function (ko, $, window, document, undefined) {
        setActiveMenuOption(23);
        var session = <% out.print(session.getAttribute("SessionJson"));%>;
        var _userLoggedCard = <% out.print(session.getAttribute("userCard"));%>;
        var validateFlag = false;


        // Javascript code copyright 2009 by Fiach Reid : www.webtropy.com
        // This code may be used freely, as long as this copyright notice is intact.
        function CalculateIMEI(Luhn) {
            var sum = 0;
            for (i = 0; i < Luhn.length; i++) {
                sum += parseInt(Luhn.substring(i, i + 1));
            }
            var delta = new Array(0, 1, 2, 3, 4, -4, -3, -2, -1, 0);
            for (i = Luhn.length - 1; i >= 0; i -= 2) {
                var deltaIndex = parseInt(Luhn.substring(i, i + 1));
                var deltaValue = delta[deltaIndex];
                sum += deltaValue;
            }
            var mod10 = sum % 10;
            mod10 = 10 - mod10;
            if (mod10 == 10) {
                mod10 = 0;
            }
            return mod10;
        }

        function ValidateIMEI(Luhn) {
            var LuhnDigit = parseInt(Luhn.substring(Luhn.length - 1, Luhn.length));
            var LuhnLess = Luhn.substring(0, Luhn.length - 1);
            if (CalculateIMEI(LuhnLess) == parseInt(LuhnDigit)) {
                return true;
            }
            return false;
        }

        $('#IMEI').change(function () {
            if (!ValidateIMEI($(this).val().trim())) {
                $(this).focus();
                $.growl.error({message: 'Este IMEI no es Valido'});
                validateFlag = false;
            } else {
                validateFlag = true;
            }
        });

        function createModel() {
            var self = this;
            self.marca = ko.observable();
            self.modelo = ko.observable();
            self.status = ko.observable(false);
            self.IMEI = ko.observable();
            self.os = ko.observable();
            self.expiracion = ko.observable();
            self.tarjeta = ko.observable();
            self.telefono = ko.observable();
            self.dispositivos = ko.observableArray([
                {marca: 'ALCATEL', modelo: 'ONE TOUCH S\'Pop ', version: '4033A', os: '4.1', img: '${pageContext.request.contextPath}/assets/images/phones/Alcatel%20One%20Touch%20SPop%204033A.jpg'}, //Alcatel%20One%20Touch%20SPop%204030A.jpg
                {marca: 'ALCATEL', modelo: 'ONE TOUCH X\'Pop ', version: '5035A', os: '4.1', img: '${pageContext.request.contextPath}/assets/images/phones/Alcatel%20One%20Touch%20XPop%205035A.jpg'},
                {marca: 'ALCATEL', modelo: 'ONE TOUCH ', version: '910A', os: '2.4', img: '${pageContext.request.contextPath}/assets/images/phones/Alcatel%20One%20Touch%20910A.jpg'}
            ]);

            self.setDisp = function (dispositivo) {
                if (self.IMEI() == null || self.IMEI() == "") {
                    return;
                }
                self.marca(dispositivo.marca);
                self.modelo(dispositivo.modelo + " " + dispositivo.version);
                self.os(dispositivo.os);
                console.log(dispositivo);
                console.log(self.modelo);
                $.growl({'title': 'Dispositivo Seleccionado', 'message': dispositivo.marca + ' ' + dispositivo.modelo + ' ' + dispositivo.version});
            };

            self.saveForm = function () {
                if (self.IMEI() == null || self.IMEI() == "") {
                    return;
                }
                self.status() ? self.status(1) : self.status(0);
                expiration = self.expiracion().split("/");
                $.ajax({
                    url: '${pageContext.request.contextPath}/operations/DispositivoService/addNewUser',
                    type: 'POST',
                    data: {
                        IMEI: self.IMEI(),
                        brand: self.marca(),
                        os: self.os(),
                        model: self.modelo(),
                        phone: self.telefono(),
                        tarjeta: self.tarjeta(),
                        status: self.status(),
                        expiration: [expiration[2], expiration[1], expiration[0]],
                        usuario: session.roleId,
                        ip: session.ip,
                        mod: 1,
                        userLoggedCard : _userLoggedCard
                    },
                    beforeSend: function(){
                      toggleLoadingButton( '#finBtn' );
                    },
                    success: function (data) {
                        if (data.ok == true || data.ok == 'true') {
                            $.growl({'title': 'Usuario Guardado', 'message': ''});
                            window.location.href = "${pageContext.request.contextPath}/pages/dispositivos/lista.jsp";
                        }else{
                            console.log( "Data retrieved from server" );
                            console.dir( data );
                            $.growl({'title': 'Error Guardando Usuario','message':data.msg});
                            toggleLoadingButton( '#finBtn' );
                        }
                    }
                });
            }
        }
        ko.applyBindings(new createModel());
    }(ko, jQuery, window, document));
</script>
</body>
</html>