<%--
  Created by Christopher Herrera
  Date: 1/10/14
  Time: 11:05 AM
  Email: Christopher_Herrera@claro.com.do
  Office: 809-220-8148
  Cellphone: 829-755-1677
  CLARO RD
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <!-- HEAD INCLUDES-->
    <%@include file="../head_pages.jsp" %>
</head>
<body class="page-body page-left-in">


<!-- MODAL PSTN-->

<div class="modal fade" id="modal-pstn" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <!--   -->
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Formulario PSTN</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="form-pstn" method="post" class="validate" data-bind="submit: saveForm"
                      novalidate="novalidate">


                    <%@include file="../../assets/fragments/form_pstn.jsp" %>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="direccion" class="control-label">La Direcci&oacute;n es correcta ?</label>
                                <%--<input type="checkbox" class="checkbox" id="direccion"--%>
                                <%--name="direccion"--%>
                                <%--data-bind="checked: direccion"/>--%>

                                <input type="radio" class="radio-inline" name="direccion" id="direccion" value="SI"
                                       data-bind="checked: direccion"/> Si
                                <input type="radio" class="radio-inline" name="direccion" id="direccion" value="NO"
                                       data-bind="checked: direccion"/> No

                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Cerrar
                        </button>
                        <button type="submit" class="btn btn-info" data-loading-text="Guardando..."
                                class="btn-toggle-loading">Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- MODAL FIBRA-->

<div class="modal fade" id="modal-fibra" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Formulario FIBRA</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="form-fibra" method="post" class="validate" data-bind="submit: saveForm"
                      novalidate="novalidate">

                    <div class="row">
                        <div class="col-med-6">
                            <div class="form-group">
                                <button id="refresh_1" class="btn btn-primary" data-bind="click: refreshClick, enable: enableRefresh"><span class="glyphicon glyphicon-refresh">Refrescar</button>
                            </div>
                        </div>
                    </div>      

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="telefono_1" class="control-label">Telefono</label>
                                <input type="text" class="form-control" name="telefono" id="telefono_1"
                                       placeholder="Telefono" data-validate="required"
                                       name="telefono" data-mask="phone"
                                       data-bind="value: telefono, attr : {'disabled' : phoneIsDisabled}"/>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6" id="section_localidad">
                            <div class="form-group">
                                <label class="control-label" for="localidad">Localidad</label>
                                <select id="localidad" class="form-control"
                                        name="localidad"
                                        data-bind="options: localidades, optionsText: 'name', value: localidad, optionsCaption: 'Seleccione FRAME'">
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="terminal_fo" class="control-label">Terminal Fo</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="FCGAC3C-A1, FCBVA1A-K30" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control"
                                       data-validate="required"
                                       name="terminal_fo"
                                       id="terminal_fo"
                                       placeholder="Terminal Fo"
                                <%--placeholder="FC[A-Z].{1,5}-[A-Z]\\d{1,2}" data-bind="value: terminalFo"/>--%>
                                       data-bind="value: terminalFo"/>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="splitterPort" class="control-label">Splitter Port</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="SPT-FCGAC3C-A1-SP05, SPT-FCBVA1A-K30-SP01" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" id="splitterPort"
                                <%--placeholder="SPT-{Terminal Fo}-SP.{2,3}"--%>
                                       placeholder="Splitter-Port"
                                       name="splitterPort"
                                       data-validate="required"
                                       data-bind="value: splitterPort"/>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="direccion_2" class="control-label">La Direcci&oacute;n es correcta ?</label>
                                <%--                                <input type="checkbox" class="checkbox" id="direccion_2"
                                                                       name="direccion_2"
                                                                       data-bind="checked: direccion"/>--%>

                                <input type="radio" class="radio-inline" name="direccion_2" id="direccion_2" value="SI"
                                       data-bind="checked: direccion"/> Si
                                <input type="radio" class="radio-inline" name="direccion_2" id="direccion_2" value="NO"
                                       data-bind="checked: direccion"/> No
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Cerrar
                        </button>
                        <button type="submit" class="btn btn-info" data-loading-text="Guardando..."
                                class="btn-toggle-loading">Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- MODAL PSTN_FIBRA-->

<div class="modal fade" id="modal-pstn_fibra" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Formulario PSTN+FIBRA</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="form-pstn_fibra" method="post" class="validate" data-bind="submit: saveForm"
                      novalidate="novalidate">

                      <div class="row">
                          <div class="col-med-6">
                              <div class="form-group">
                                  <button id="refresh_fibra" class="btn btn-primary" data-bind="click: refreshClick, enable: enableRefresh"><span class="glyphicon glyphicon-refresh">Refrescar</button>
                              </div>
                          </div>
                      </div>   

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="telefono_3" class="control-label">Telefono</label>
                                <input type="text" class="form-control" name="telefono_3"
                                       id="telefono_3" placeholder="Telefono" data-validate="required"
                                       name="telefono" data-mask="phone"
                                       data-bind="value: telefono, attr : {'disabled' : phoneIsDisabled}"/>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="cuenta_fija_3">Cuenta Fija</label>
                                <input type="checkbox" class="checkbox" id="cuenta_fija_3"
                                       name="cuenta_fija_3"
                                       data-bind="checked: cuentaFija"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" id="section_localidad_fibra">
                            <div class="form-group">
                                <label class="control-label" for="localidad_3">Frame</label>
                                <select id="localidad_3" class="form-control"
                                        name="localidad_3"
                                        data-bind="options: localidades, optionsText: 'name', value: localidad, optionsCaption: 'Seleccione FRAME'">
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6" data-bind="visible: showCabina">
                            <div class="form-group">
                                <label for="cabina_fibra" class="control-label">Cabina</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="CVM6D, CBH7" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" data-validate="required"
                                       data-bind="value: cabina"
                                       name="cabina__fibra"
                                       id="cabina_fibra"
                                       placeholder="Cabina"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="terminal_fibra" class="control-label">Terminal</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="CVM6D-4N, CBH7-T, P16-4A(Cuenta Fija)" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" id="terminal_fibra"
                                       data-validate="required"
                                       name="terminal_fibra"
                                       placeholder="Terminal" data-bind="value: terminal"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="par_feeder_fibra" class="control-label">Par Feeder</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" id="par_feeder_fibra"
                                <%--placeholder=".*-####"--%>
                                       data-inputmask-regex=".*-[0-9]{4}"
                                       placeholder="Par Feeder"
                                       name="par_feeder_fibra"
                                       data-validate="required"
                                       data-bind="value: parFeeder"/>

                            </div>
                        </div>
                        <div class="col-md-4" data-bind="visible: showNumParFeeder">
                            <div class="form-group">
                                <label for="num_par_feeder_fibra" class="control-label">Rango</label>
                                <%--<span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>--%>
                                <select id="num_par_feeder_fibra"
                                        class="form-control"
                                        name="numParFeeder_fibra"
                                        data-bind="value: numParFeeder, options: rangeFeeder"  >
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" data-bind="visible: showOtrosParFeeder">
                            <div class="form-group">
                                <label for="otros_par_feeder_fibra" class="control-label">Otros</label>
                                <input type="text" class="form-control" id="otros_par_feeder_fibra"
                                <%--placeholder=".*-####"--%>
                                       placeholder="Otros"
                                       name="otrosParFeeder_fibra"
                                <%--data-validate="required"--%>
                                       data-bind="value: otrosParFeeder"
                                        />
                            </div>
                        </div>
                    </div>

                    <div class="row" data-bind="visible: showParLocal">
                        <div class="col-md-4" >
                            <div class="form-group">
                                <label for="par_local_fibra" class="control-label">Par Local</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="CVM6D-0169, CBH7-0096" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" id="par_local_fibra"
                                       placeholder="Par Local"
                                       data-validate="required"
                                       name="par_local_fibra"
                                       data-inputmask-regex=".*-[0-9]{4}"
                                       data-bind="value: parLocal"/>
                            </div>
                        </div>
                        <div class="col-md-4" data-bind="visible: showNumParLocal">
                            <div class="form-group">
                                <label for="num_par_local_fibra" class="control-label">Rango</label>
                                <%--<span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>--%>
                                <select id="num_par_local_fibra"
                                        class="form-control"
                                        name="numParLocal_fibra"
                                        data-bind="value: numParLocal, options: rangeLocal"  >
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" data-bind="visible: showOtrosParLocal">
                            <div class="form-group">
                                <label for="otros_par_local_fibra" class="control-label">Otros</label>
                                <input type="text" class="form-control" id="otros_par_local_fibra"
                                <%--placeholder=".*-####"--%>
                                       placeholder="Otros"
                                       name="otrosParLocal_fibra"
                                <%--data-validate="required"--%>
                                       data-bind="value: otrosParLocal"
                                        />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="terminal_fo_3" class="control-label">Terminal Fo</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="FCGAC3C-A1, FCBVA1A-K30" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control"
                                       data-validate="required"
                                       name="terminal_fo_3"
                                       id="terminal_fo_3"
                                <%--placeholder="FC[A-Z].{1,5}-[A-Z]\\d{1,2}" data-bind="value: terminalFo"/>--%>
                                       placeholder="Terminal Fo" data-bind="value: terminalFo"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="splitterPort_3" class="control-label">Splitter Port</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="SPT-FCGAC3C-A1-SP05, SPT-FCBVA1A-K30-SP01" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" id="splitterPort_3"
                                <%--placeholder="SPT-{Terminal Fo}-SP.{2,3}"--%>
                                       placeholder="Splitter-Port"
                                       name="splitterPort_3"
                                       data-validate="required"
                                       data-bind="value: splitterPort"/>

                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="direccion_3" class="control-label">La Direcci&oacute;n es correcta ?</label>
                                <%--<input type="checkbox" class="checkbox" id="direccion_3"--%>
                                <%--name="direccion_3"--%>
                                <%--data-bind="checked: direccion"/>--%>

                                <input type="radio" class="radio-inline" name="direccion_3" id="direccion_3" value="SI"
                                       data-bind="checked: direccion"/> Si
                                <input type="radio" class="radio-inline" name="direccion_3" id="direccion_3" value="NO"
                                       data-bind="checked: direccion"/> No

                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Cerrar
                        </button>
                        <button type="submit" class="btn btn-info" data-loading-text="Guardando..."
                                class="btn-toggle-loading">Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- MODAL PSTN_DATA-->

<div class="modal fade" id="modal-pstn_data" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Formulario PSTN+DATA</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="form-pstn_data" method="post" class="validate" data-bind="submit: saveForm"
                      novalidate="novalidate">

                      <div class="row">
                          <div class="col-med-6">
                              <div class="form-group">
                                  <button id="refresh_data" class="btn btn-primary" data-bind="click: refreshClick, enable: enableRefresh"><span class="glyphicon glyphicon-refresh"></span>Refrescar</button>
                              </div>
                          </div>
                      </div>   

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="telefono_4" class="control-label">Telefono</label>
                                <input type="text" class="form-control" name="telefono"
                                       id="telefono_4" placeholder="Telefono" data-validate="required"
                                       name="telefono_4" data-mask="phone"
                                       data-bind="value: telefono, attr : {'disabled' : phoneIsDisabled}"/>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="cuenta_fija_4">Cuenta Fija</label>
                                <input type="checkbox" class="checkbox" id="cuenta_fija_4"
                                       name="cuenta_fija_4"
                                       data-bind="checked: cuentaFija"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" id="section_localidad_data">
                            <div class="form-group">
                                <label class="control-label" for="localidad_4">Frame</label>
                                <select id="localidad_4" class="form-control"
                                        name="localidad_4"
                                        data-bind="options: localidades, optionsText: 'name', value: localidad, optionsCaption: 'Seleccione FRAME'">
                                </select>
                            </div>
                        </div>


                        <div class="col-md-6" data-bind="visible: showCabina">
                            <div class="form-group">
                                <label for="cabina_4" class="control-label">Cabina</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="CVM6D, CBH7" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" data-validate="required"
                                       data-bind="value: cabina"
                                       name="cabina_4"
                                       id="cabina_4"
                                       placeholder="Cabina"/>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="terminal_data" class="control-label">Terminal</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="CVM6D-4N, CBH7-T, P16-4A(Cuenta Fija)" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" id="terminal_data"
                                       data-validate="required"
                                       name="terminal_data"
                                       placeholder="Terminal" data-bind="value: terminal"/>
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="par_feeder_data" class="control-label">Par Feeder</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" id="par_feeder_data"
                                       placeholder="Par Feeder"
                                <%--placeholder=".*-####"--%>
                                       name="par_feeder_data"
                                       data-inputmask-regex="[a-zA-Z0-9]+-[0-9]{4}"
                                       data-validate="required"
                                       data-bind="value: parFeeder"/>

                            </div>
                        </div>

                        <div class="col-md-4" data-bind="visible: showNumParFeeder">
                            <div class="form-group">
                                <label for="num_par_feeder_data" class="control-label">Rango</label>
                                <%--<span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>--%>
                                <select id="num_par_feeder_data"
                                        class="form-control"
                                        name="numParFeeder_data"
                                        data-bind="value: numParFeeder, options: rangeFeeder"  >
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" data-bind="visible: showOtrosParFeeder">
                            <div class="form-group">
                                <label for="otros_par_feeder_data" class="control-label">Otros</label>
                                <input type="text" class="form-control" id="otros_par_feeder_data"
                                <%--placeholder=".*-####"--%>
                                       placeholder="Otros"
                                       name="otrosParFeeder_data"
                                <%--data-validate="required"--%>
                                       data-bind="value: otrosParFeeder"
                                        />
                            </div>
                        </div>
                    </div>

                    <div class="row" data-bind="visible: showParLocal">
                        <div class="col-md-4" >
                            <div class="form-group">
                                <label for="par_local_data" class="control-label">Par Local</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="CVM6D-0169, CBH7-0096" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" id="par_local_data"
                                       placeholder="Par Local"
                                       data-validate="required"
                                       name="par_local_data"
                                       data-bind="value: parLocal"/>
                            </div>
                        </div>

                        <div class="col-md-4" data-bind="visible: showNumParLocal">
                            <div class="form-group">
                                <label for="num_par_local_data" class="control-label">Rango</label>
                                <%--<span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>--%>
                                <select id="num_par_local_data"
                                        class="form-control"
                                        name="numParLocal_data"
                                        data-bind="value: numParLocal, options: rangeLocal"  >
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" data-bind="visible: showOtrosParLocal">
                            <div class="form-group">
                                <label for="otros_par_local_data" class="control-label">Otros</label>
                                <input type="text" class="form-control" id="otros_par_local_data"
                                <%--placeholder=".*-####"--%>
                                       placeholder="Otros"
                                       name="otrosParLocal_data"
                                <%--data-validate="required"--%>
                                       data-bind="value: otrosParLocal"
                                        />
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="tipo_puerto">Tipo de puerto</label>
                                <select id="tipo_puerto" class="form-control"
                                        name="tipo_puerto"
                                        data-bind="options: puertos, value: tipoPuerto ">
                                </select>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group no-margin">
                                <label for="tipo_dslam" class="control-label">Tipo de DSLAM</label>

                                <div class="form-control">
                                    <input type="radio" class="radio-inline" name="tipo_dslam" id="tipo_dslam" value="INDOOR"
                                           data-bind="checked: dslamType"/> Indoor
                                    <input type="radio" class="radio-inline" name="tipo_dslam" id="tipo_dslam_2" value="OUTDOOR"
                                           data-bind="checked: dslamType"/> Outdoor
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="dslam" class="control-label">DSLAM</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Alcatel: A27FEBRERO8, ACA10A1 ; Cisco: BARAHONA2, MONTECRISTI2 ; Stinger: SBOCACHICA3, SSANFRANCISCO4 ; Zixel: ZAZUA10, ZCONSTANZA8 ; ZTE: TCA2E1, TCVM6D2" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" data-validate="required"
                                       data-bind="value: dslam"
                                       name="dslam"
                                       id="dslam"
                                        />
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="puerto" class="control-label">Puerto</label>
                                <span class="entypo-help-circled"  data-toggle="popover" data-trigger="hover" data-placement="left" style="left: -100px" data-content="Alcatel:1-1-1-1, 1-1-1-11 ; Cisco: TARJ 1-PTO 1, TARJ 11-PTO 1 ; Stinger: SHELF 1-TARJ 1-PTO 1, SHELF 1-TARJ 1-PTO 11, ; Zixel ADSL: SLOT 1-PUERTO-1 ADSL, SLOT 1-PUERTO-11 ADSL ; Zixel SHDSL: SLOT 1-SHDSL PORT 1, SLOT 1-SHDSL PORT 11 ; ZTE: 1-1, 1-11" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" data-validate="required"
                                       data-bind="value: puerto"
                                       name="puerto"
                                       id="puerto"
                                        />
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="direccion_4" class="control-label">La Direcci&oacute;n es correcta ?</label>
                                <%--<input type="checkbox" class="checkbox" id="direccion_4"--%>
                                <%--name="direccion_4"--%>
                                <%--data-bind="checked: direccion"/>--%>

                                <input type="radio" class="radio-inline" name="direccion_4" id="direccion_4" value="SI"
                                       data-bind="checked: direccion"/> Si
                                <input type="radio" class="radio-inline" name="direccion_4" id="direccion_4" value="NO"
                                       data-bind="checked: direccion"/> No

                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Cerrar
                        </button>
                        <button type="submit" class="btn btn-info" data-loading-text="Guardando..."
                                class="btn-toggle-loading">Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- MODAL FRAME-->

<div class="modal fade" id="modal-frame" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Formulario FRAME</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="form-frame" method="post" class="validate" data-bind="submit: saveForm"
                      novalidate="novalidate">

                      <div class="row">
                          <div class="col-med-6">
                              <div class="form-group">
                                  <button id="refresh_frame" class="btn btn-primary" data-bind="click: refreshClick, enable: enableRefresh"><span class="glyphicon glyphicon-refresh"></span>Refrescar</button>
                              </div>
                          </div>
                      </div>   

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="localidad_6">Frame</label>
                                <select id="localidad_6" class="form-control"
                                        name="localidad_6"
                                        data-bind="options: localidades, optionsText: 'name', value: localidad, optionsCaption: 'Seleccione FRAME'">
                                </select>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="telefono_6" class="control-label">Telefono</label>
                                <input type="text" class="form-control" name="telefono"
                                       id="telefono_6" placeholder="Telefono" data-validate="required"
                                       name="telefono_6" data-mask="phone"
                                       data-bind="value: telefono, attr : {'disabled' : phoneIsDisabled}"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="par_feeder_6" class="control-label">Par Feeder</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" id="par_feeder_6"
                                       placeholder="Par Feeder"
                                <%--placeholder=".*-####"--%>
                                       name="par_feeder_6"
                                       data-inputmask-regex="[a-zA-Z0-9]+-[0-9]{4}"
                                       data-validate="required"
                                       data-bind="value: parFeeder"/>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="servicio_dato">Servicio Datos</label>
                                <input type="checkbox" class="checkbox" id="servicio_dato"
                                       name="servicio_dato"
                                       checked="true"
                                       data-bind="checked: servioDato"/>
                            </div>
                        </div>
                    </div>

                    <div class="row" data-bind="visible: isServicioDatos">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="tipo_puerto">Tipo de puerto</label>
                                <select id="tipo_puerto_6" class="form-control"
                                        name="tipo_puerto"
                                        data-bind="options: puertos, value: tipoPuerto ">
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group no-margin">
                                <label for="tipo_dslam" class="control-label">Tipo de DSLAM</label>

                                <div class="form-control">
                                    <input type="radio" class="radio-inline" name="tipo_dslam" id="tipo_dslam_6_1" value="INDOOR"
                                           data-bind="checked: dslamType"/> Indoor
                                    <input type="radio" class="radio-inline" name="tipo_dslam" id="tipo_dslam_6_2" value="OUTDOOR"
                                           data-bind="checked: dslamType"/> Outdoor
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" data-bind="visible: isServicioDatos">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="dslam" class="control-label">DSLAM</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Alcatel: A27FEBRERO8, ACA10A1 ; Cisco: BARAHONA2, MONTECRISTI2 ; Stinger: SBOCACHICA3, SSANFRANCISCO4 ; Zixel: ZAZUA10, ZCONSTANZA8 ; ZTE: TCA2E1, TCVM6D2" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" data-validate="required"
                                       data-bind="value: dslam"
                                       name="dslam"
                                       id="dslam_6"
                                        />
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="puerto" class="control-label">Puerto</label>
                                <span class="entypo-help-circled"  data-toggle="popover" data-trigger="hover" data-placement="left" style="left: -100px" data-content="Alcatel:1-1-1-1, 1-1-1-11 ; Cisco: TARJ 1-PTO 1, TARJ 11-PTO 1 ; Stinger: SHELF 1-TARJ 1-PTO 1, SHELF 1-TARJ 1-PTO 11, ; Zixel ADSL: SLOT 1-PUERTO-1 ADSL, SLOT 1-PUERTO-11 ADSL ; Zixel SHDSL: SLOT 1-SHDSL PORT 1, SLOT 1-SHDSL PORT 11 ; ZTE: 1-1, 1-11" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" data-validate="required"
                                       data-bind="value: puerto"
                                       name="puerto"
                                       id="puerto_6"
                                        />
                            </div>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Cerrar
                        </button>
                        <button type="submit" class="btn btn-info" data-loading-text="Guardando..."
                                class="btn-toggle-loading">Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="page-container">


    <!-- Sidebar Menu -->
    <%@include file="../../assets/fragments/sidebar_menu.jsp" %>
    <div class="main-content">
        <!-- Notifications and Raw Links -->
        <div class="row" id="top-bar">
            <%@include file="../../assets/fragments/profile_and_notifications.jsp" %>
            <%@include file="../../assets/fragments/raw_links.jsp" %>
        </div>
        <hr/>
        <!-- CONTENT HERE -->
        <div class="panel-heading">
            <div class="panel-title">
                <h2 id="orders-title"></h2>
            </div>
        </div>

        <ol id="breadCrumb">

        </ol>
        <hr/>
        <br/>


        <div>
            <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered datatable table-hover"
                   id="table"></table>
        </div>

        <div>
            <!-- FOOTER -->
            <%@include file="../../assets/fragments/footer.jsp" %>
        </div>

    </div>
    <!-- BOTTOM INCLUDES-->
    <%--<%@include file="../bottom_includes_pages.jsp" %>--%>
    <%@include file="../../assets/fragments/bottom_includes.jsp" %>

    <script type="text/javascript">

        //    $("#par_feeder").inputmask("Regex",{regex: '[a-zA-Z0-9]+-[0-9]{4}'});
        //    $("#terminal_fo").inputmask("Regex",{regex: 'FC[A-Z].{1,5}-[A-Z][0-9]{1,2}'});
        //    $("#par_feeder_3").inputmask("Regex",{regex: '[a-zA-Z0-9]+-[0-9]{4}'});
        //    $("#terminal_fo_3").inputmask("Regex",{regex: 'FC[A-Z].{1,5}-[A-Z][0-9]{1,2}'});
        //    $("#par_feeder_4").inputmask("Regex",{regex: '[a-zA-Z0-9]+-[0-9]{4}'});


        var session = <% out.print(session.getAttribute("SessionJson"));%>;
        if (!checkAccess(session, 'ViewDI', null)) {
            location.href = '${pageContext.request.contextPath}/pages/dispositivos/buscar.jsp';
        }

        var buttonsVisible = !!~$.inArray('SubmitDI', session['accesos']);
        var formButtonStyle = buttonsVisible ? '' : "style='display:none;'";
        var _userLoggedCard = '<% out.print(session.getAttribute("userCard"));%>';
        var _ip = '<% out.print(session.getAttribute("ip"));%>';

        var models = {};

        function launchForm($this) {
            if (!checkAccess(session, 'SubmitDI', null)) {
                return;
            }

            var q = getUrlQueryString().q;
            q = ('' == q || null == q ) ? '' : q;

            var data = {};
            data.url = '${pageContext.request.contextPath}/operations/OrdersActionsService/saveDispatchItem';
            data.lastFormUrl = '${pageContext.request.contextPath}/operations/OrdersActionsService/getLastActionValuesByCode';
            data.tableRefreshUrl = "${pageContext.request.contextPath}/operations/OrdersActionsService/getPendingOrdersByCard?card=" + q + '&ulc=' + _userLoggedCard + '&ip=' + _ip;
            data._userLoggedCard = _userLoggedCard;
            data._ip = _ip;

            data.orderCode = $($this).data('code');
            data.orderType = $($this).data('type');
            data.orderPhone = $($this).data('phone');
            var locationType = $($this).data('location-type');

            console.log( 'lcoation type: '+locationType );
            data.tarjeta = q;

            var formType = $($this).data('form');

            var form = $("#form-" + formType);

            console.log('Showing modal #modal-' + formType + ", data and form #form-" + formType);
            console.dir(data);

            $('#form-' + formType).trigger('reset');

            if (form.data('cfc-attached') == undefined) {
                var model = null;
                if (formType == 'pstn')
                    model = new PSTNModel(data);
                else if (formType == 'fibra')
                    model = new FIBRAModel(data);
                else if (formType == 'pstn_fibra')
                    model = new PSTN_FIBRAModel(data);
                else if (formType == 'pstn_data')
                    model = new PSTN_DATAModel(data);
                else if (formType == 'frame') {
                    // @Nathaniel Since 2016-GESTI-7145 any order is able to work upon FrameFormFragment
                    model = new FRAMEModel(data);
                    /*
                     Old implementation - Before 2016-GESTI-7145 was implemented.
                    if( null == locationType || undefined == locationType || "FRAME" !== locationType ){
                        $.growl({ title: 'No Disponible', message: 'Esta orden no admite formulario FRAME'});
                        return;
                    }else {
                        model = new FRAMEModel(data);
                    }*/
                }else
                    return;
                //  ko.cleanNode(document.getElementById("modal-" + form));
                ko.applyBindings(model, document.getElementById("modal-" + formType));
                console.log('Aplying model to form ' + formType);
                console.dir(model);

                models[formType] = model;
                form.data('cfc-attached', true);
            }else{

                // Old implementation - Before 2016-GESTI-7145 was implemented.
                /*if (formType == 'frame') {
                    if (null == locationType || undefined == locationType || "FRAME" !== locationType) {
                        $.growl({title: 'No Disponible', message: 'Esta orden no admite formulario FRAME'});
                        return;
                    }
                }*/

                var model = models[formType];
                console.log( 'Setting params over retrieved model' );
                console.dir( model );
                model.setParams(data);
            }

            console.log('Showing modal #modal-' + formType);
            jQuery('#modal-' + formType).modal('show', {backdrop: 'static'});

            $('#servicio_dato').prop('checked',true)
        }


        (function (ko, $, window, document, undefined) {

            setActiveMenuOption(121);
            var localData;
            var q = getUrlQueryString().q;

            q = ('' == q || null == q ) ? '' : q;

            $('#orders-title').text('Listado de Ordenes de la Tarjeta ' + q);
                        
            

//        Parte del breadcromb para la opcion 12
            $('#q-card').text('Ordenes de la Tarjeta ' + q);

            var table = $("#table").dataTable({
                "sPaginationType": "bootstrap",
                "bAutoWidth": false,
                "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
                "bProcessing": true,
                "sAjaxSource": "${pageContext.request.contextPath}/operations/OrdersActionsService/getPendingOrdersByCard?card=" + q + '&ulc=' + _userLoggedCard + '&ip=' + _ip,
                "aoColumns": [
                    { "sTitle": "Tipo" },
                    { "sTitle": "Codigo" },
                    { "sTitle": "Fecha Creacion" },
                    { "sTitle": ""},
                    { "sTitle": ""},
                    { "sTitle": ""},
                    { "sTitle": ""},
                    { "sTitle": ""}
                ],
                "aoColumnDefs": [
                    {
                        "aTargets": [0],
                        "mRender": function (data, type, full) {
                            if (data == 'ORDEN') {
                                return '<span class="label label-success">' + data + '</span>';
                            } else {
                                return '<span class="label label-secondary">' + data + '</span>';
                            }
                        }
                    },
                    {
                        "aTargets": [3],
                        "mRender": function (data, type, full) {
                            return '<button onclick="launchForm(this); return;" type="button" ' + formButtonStyle + ' class="btn btn-secondary" data-type="' + full[0] + '" data-code="' + full[1] +'" data-phone="' + full[8] + '" data-form="pstn">Digitar como PSTN</button>';
                        }
                    },
                    {
                        "aTargets": [4],
                        "mRender": function (data, type, full) {
                            return '<button onclick="launchForm(this); return;" type="button"  ' + formButtonStyle + ' class="btn btn-primary" data-type="' + full[0] + '" data-code="' + full[1] + '" data-phone="' + full[8] + '" data-form="fibra">Digitar como FIBRA</button>';
                        }
                    },
                    {
                        "aTargets": [5],
                        "mRender": function (data, type, full) {
                            return '<button onclick="launchForm(this); return;" type="button"  ' + formButtonStyle + ' class="btn btn-orange" data-type="' + full[0] + '" data-code="' + full[1] + '" data-phone="' + full[8] + '" data-form="pstn_fibra">Digitar como PSTN+FIBRA</button>';
                        }
                    },
                    {
                        "aTargets": [6],
                        "mRender": function (data, type, full) {
                            return '<button onclick="launchForm(this); return;" type="button"  ' + formButtonStyle + ' class="btn btn-gold" data-type="' + full[0] + '" data-code="' + full[1] +  '" data-phone="' + full[8] +'" data-form="pstn_data">Digitar como PSTN+DATA</button>';
                        }
                    },
                    {
                        "aTargets": [7],
                        "mRender": function (data, type, full) {
                            return '<button onclick="launchForm(this); return;" type="button"  ' + formButtonStyle + ' class="btn btn-success" data-type="' + full[0] + '" data-code="' + full[1] + '" data-phone="' + full[8] +'" data-location-type="' + full[9] +'" data-form="frame">Digitar como FRAME</button>';
                        }
                    }
                ]
            });

            /**
             *  añadiendos bindings models
             */
        }(ko, jQuery, window, document));


    </script>
</body>
</html>