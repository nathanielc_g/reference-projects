<%--
  Created by Christopher Herrera
  Date: 1/10/14
  Time: 11:05 AM
  Email: Christopher_Herrera@claro.com.do
  Office: 809-220-8148
  Cellphone: 829-755-1677
  CLARO RD
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <!-- HEAD INCLUDES-->
    <%@include file="../head_pages.jsp" %>
</head>
<body class="page-body page-left-in">


<!-- MODAL PSTN-->

<div class="modal fade" id="modal-pstn" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                data-bind="click: onClose"
                >×
                </button>
                <h4 class="modal-title">Formulario PSTN</h4>
            </div>
            <div class="modal-body">
                <%--<form role="form" id="form-pstn" method="post" class="validate" data-bind="submit: saveForm"
                      novalidate="novalidate">--%>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="telefono" class="control-label">Telefono</label>
                                <input type="text" class="form-control" name="telefono"
                                       id="telefono" placeholder="Telefono"
                                <%--data-validate="required"--%>
                                       name="telefono"
                                       disabled
                                       data-bind="value: telefono"/>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="cuenta_fija">Cuenta Fija</label>
                                <input type="checkbox" class="checkbox" id="cuenta_fija"
                                       name="cuenta_fija"
                                       disabled
                                       data-bind="checked: cuentaFija"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" id="section_localidad_pstn">
                            <div class="form-group">
                                <label class="control-label" for="localidad">Frame</label>
                                <input id="localidad" class="form-control"
                                        name="localidad"
                                        disabled
                                        data-bind="value: localidad">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="terminal" class="control-label">Terminal</label>
                                <input type="text" class="form-control" id="terminal"
                                <%--data-validate="required"--%>
                                       name="terminal"
                                       disabled
                                       placeholder="Terminal" data-bind="value: terminal"/>
                            </div>
                        </div>
                        <div class="col-md-6" id="section_cabina_pstn">
                            <div class="form-group">
                                <label for="cabina" class="control-label">Cabina</label>
                                <input type="text" class="form-control"
                                <%--data-validate="required"--%>
                                       data-bind="value: cabina"
                                       name="cabina"
                                       disabled
                                       id="cabina"
                                       placeholder="Cabina"/>
                            </div>
                        </div>                                                
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="par_feeder" class="control-label">Par Feeder</label>
                                <input type="text" class="form-control" id="par_feeder"
                                <%--placeholder=".*-####"--%>
                                       placeholder="Par Feeder"
                                       name="par_feeder"
                                       disabled
                                <%--data-validate="required"--%>
                                       data-bind="value: parFeeder"/>

                            </div>
                        </div>

                        

                        <div class="col-md-6" id="section_par_local_pstn">
                            <div class="form-group">
                                <label for="par_local" class="control-label">Par Local</label>
                                <input type="text" class="form-control" id="par_local"
                                       placeholder="Par Local"
                                <%--data-validate="required"--%>
                                       name="par_local"
                                       disabled
                                       data-bind="value: parLocal"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <%--<div class="col-md-12">--%>
                        <%--<div class="form-group no-margin">--%>
                        <%--<label for="direccion" class="control-label">Direcci&oacute;n</label>--%>
                        <%--<textarea class="form-control autogrow" id="direccion"--%>
                        <%--data-bind="value: direccion"--%>
                        <%--placeholder="Direccion"--%>
                        <%--name="direccion"--%>
                        <%--disabled--%>
                        <%--style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 31px;"></textarea>--%>
                        <%--</div>--%>
                        <%--</div>--%>
                    </div>

                    <div class="row">
                        <div class="col-md-1">
                            <input type="checkbox" class="checkbox"
                                         data-bind="disable: handOperatedDisabled, checked: handOperated"/>
                          </div>
                          <div class="col-md-5">
                            <span>Operacion manual Por:</span>
                          </div>
                          <div class="col-md-6">                                  
                                  <span class="control-label" data-bind="text: handOperatedBy"></span>
                          </div>
                    </div>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>
                                        Sistema
                                    </th>
                                    <th>
                                        Resultado de Procesamiento
                                    </th>
                                    <th>
                                        Facilidades
                                    </th>
                                </tr>
                                </thead>
                                <tbody data-bind="template: { name: 'ActionResultTemplate', foreach: actionResults }">

                                </tbody>

                            </table>
                        </div>
                    </div>                    

                    <div class="modal-footer">
                        <button type="button"
                                data-bind="click: updateAction, disable: handOperatedDisabled"
                                class="btn btn-info" data-loading-text="Guardando..."
                                class="btn-toggle-loading">Guardar
                        </button>
                        <button type="button" class="btn btn-default close-modal" data-dismiss="modal"
                        data-bind="click: onClose"
                        >Cerrar
                        </button>
                    </div>
                <%--</form>--%>
            </div>
        </div>
    </div>
</div>

  <script id="ActionResultTemplate" type="html/text">
      <tr class="resultRow">
          <td data-bind="text: TYPE_NAME">
          </td>
          <td data-bind="text: MESSAGE_LOG">
          </td>
          <td data-bind="text: UPDATED_ATTRS_LOG">
          </td>
      </tr>
  </script>

<!-- MODAL FIBRA-->

<div class="modal fade" id="modal-fibra" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                  data-bind="click: onClose"
                >×</button>
                <h4 class="modal-title">Formulario FIBRA</h4>
            </div>
            <div class="modal-body">
                <%--<form role="form" id="form-fibra" method="post" class="validate" data-bind="submit: saveForm"
                      novalidate="novalidate">--%>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="telefono" class="control-label">Telefono</label>
                                <input type="text" class="form-control" name="telefono"
                                       placeholder="Telefono"
                                <%--data-validate="required"--%>
                                       name="telefono"
                                       disabled
                                       data-bind="value: telefono"/>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6" id="section_localidad_fibra">
                            <div class="form-group">
                                <label class="control-label" for="localidad">Localidad</label>
                                <input id="localidad_2" class="form-control"
                                       name="localidad"
                                       disabled
                                       data-bind="value: localidad">
                                </input>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="terminal_fo" class="control-label">Terminal Fo</label>
                                <input type="text" class="form-control"
                                <%--data-validate="required"--%>
                                       name="terminal_fo"
                                       id="terminal_fo"
                                       disabled
                                <%--placeholder="FC[A-Z].{1,5}-[A-Z]\\d{1,2}"--%>
                                       placeholder="Terminal Fo"
                                       data-bind="value: terminalFo"/>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="splitterPort" class="control-label">Splitter Port</label>
                                <input type="text" class="form-control" id="splitterPort"
                                       placeholder="Splitter Port"
                                       name="splitterPort"
                                       disabled
                                <%--data-validate="required"--%>
                                       data-bind="value: splitterPort"/>

                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <%--<div class="col-md-12">--%>
                        <%--<div class="form-group no-margin">--%>
                        <%--<label for="direccion_2" class="control-label">Direcci&oacute;n</label>--%>
                        <%--<textarea class="form-control autogrow" id="direccion_2"--%>
                        <%--data-bind="value: direccion"--%>
                        <%--placeholder="Direccion"--%>
                        <%--name="direccion"--%>
                        <%--disabled--%>
                        <%--style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 31px;"></textarea>--%>
                        <%--</div>--%>
                        <%--</div>--%>
                    </div>

                    <div class="row">
                        <div class="col-md-1">
                            <input type="checkbox" class="checkbox"                                        
                                         data-bind="disable: handOperatedDisabled, checked: handOperated"/>
                          </div>
                          <div class="col-md-5">
                            <span>Operacion manual Por:</span>
                          </div>
                          <div class="col-md-6">                                  
                                  <span class="control-label" data-bind="text: handOperatedBy"></span>
                          </div>
                    </div>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>
                                        Sistema
                                    </th>
                                    <th>
                                        Resultado de Procesamiento
                                    </th>
                                    <th>
                                        Facilidades
                                    </th>
                                </tr>
                                </thead>
                                <tbody data-bind="template: { name: 'ActionResultTemplate', foreach: actionResults }">

                                </tbody>

                            </table>
                        </div>
                    </div>                    

                    <div class="modal-footer">
                        <button type="button"
                                data-bind="click: updateAction, disable: handOperatedDisabled"
                                class="btn btn-info" data-loading-text="Guardando..."
                                class="btn-toggle-loading">Guardar
                        </button>
                        <button type="button" class="btn btn-default close-modal" data-dismiss="modal" data-bind="click: onClose">Cerrar
                        </button>
                    </div>
                <%--</form>--%>
            </div>
        </div>
    </div>
</div>


<!-- MODAL PSTN_FIBRA-->

<div class="modal fade" id="modal-pstn_fibra" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: onClose">×</button>
                <h4 class="modal-title">Formulario PSTN+FIBRA</h4>
            </div>
            <div class="modal-body">
                <%--<form role="form" id="form-pstn_fibra" method="post" class="validate" data-bind="submit: saveForm"
                      novalidate="novalidate">--%>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="telefono_3" class="control-label">Telefono</label>
                                <input type="text" class="form-control" name="telefono_3"
                                       id="telefono_3" placeholder="Telefono"
                                <%--data-validate="required"--%>
                                       name="telefono"
                                       disabled
                                       data-bind="value: telefono"/>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="cuenta_fija_3">Cuenta Fija</label>
                                <input type="checkbox" class="checkbox" id="cuenta_fija_3"
                                       name="cuenta_fija_3"
                                       disabled
                                       data-bind="checked: cuentaFija"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6" id="section_localidad_pstn_fibra">
                            <div class="form-group">
                                <label class="control-label" for="localidad_3">Frame</label>
                                <input id="localidad_3" class="form-control"
                                        name="localidad_3"
                                        disabled
                                        data-bind="value: localidad">
                                </input>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="terminal_3" class="control-label">Terminal</label>
                                <input type="text" class="form-control" id="terminal_3"
                                <%--data-validate="required"--%>
                                       name="terminal_3"
                                       disabled
                                       placeholder="Terminal" data-bind="value: terminal"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6" id="section_cabina_pstn_fibra">
                            <div class="form-group">
                                <label for="cabina_3" class="control-label">Cabina</label>
                                <input type="text" class="form-control"
                                <%--data-validate="required"--%>
                                       data-bind="value: cabina"
                                       name="cabina_3"
                                       id="cabina_3"
                                       disabled
                                       placeholder="Cabina"/>
                            </div>
                        </div>
                        
                    </div>
                    

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="par_feeder_3" class="control-label">Par Feeder</label>
                                <input type="text" class="form-control" id="par_feeder_3"
                                       placeholder="Par Feeder"
                                <%--placeholder=".*-####"--%>
                                       name="par_feeder_3"
                                       disabled
                                <%--data-validate="required"--%>
                                       data-bind="value: parFeeder"/>

                            </div>
                        </div>
                        

                        <div class="col-md-6" id="section_par_local_pstn_fibra">
                            <div class="form-group">
                                <label for="par_local_3" class="control-label">Par Local</label>
                                <input type="text" class="form-control" id="par_local_3"
                                       placeholder="Par Local"
                                <%--data-validate="required"--%>
                                       name="par_local_3"
                                       disabled
                                       data-bind="value: parLocal"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="terminal_fo_3" class="control-label">Terminal Fo</label>
                                <input type="text" class="form-control"
                                <%--data-validate="required"--%>
                                       name="terminal_fo_3"
                                       id="terminal_fo_3"
                                       disabled
                                <%--placeholder="FC[A-Z].{1,5}-[A-Z]\\d{1,2}" --%>
                                       placeholder="Terminal Fo"
                                       data-bind="value: terminalFo"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="splitterPort_3" class="control-label">Splitter Port</label>
                                <input type="text" class="form-control" id="splitterPort_3"
                                       placeholder="Splitter Port"
                                       name="splitterPort_3"
                                       disabled
                                <%--data-validate="required"--%>
                                       data-bind="value: splitterPort"/>

                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <%--<div class="col-md-12">--%>
                        <%--<div class="form-group no-margin">--%>
                        <%--<label for="direccion_3" class="control-label">Direcci&oacute;n</label>--%>
                        <%--<textarea class="form-control autogrow" id="direccion_3"--%>
                        <%--data-bind="value: direccion"--%>
                        <%--placeholder="Direccion"--%>
                        <%--name="direccion_3"--%>
                        <%--disabled--%>
                        <%--style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 31px;"></textarea>--%>
                        <%--</div>--%>
                        <%--</div>--%>
                    </div>

                    <div class="row">
                        <div class="col-md-1">
                            <input type="checkbox" class="checkbox"                                        
                                         data-bind="disable: handOperatedDisabled, checked: handOperated"/>
                          </div>
                          <div class="col-md-5">
                            <span>Operacion manual Por:</span>
                          </div>
                          <div class="col-md-6">                                  
                                  <span class="control-label" data-bind="text: handOperatedBy"></span>
                          </div>
                    </div>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>
                                        Sistema
                                    </th>
                                    <th>
                                        Resultado de Procesamiento
                                    </th>
                                    <th>
                                        Facilidades
                                    </th>
                                </tr>
                                </thead>
                                <tbody data-bind="template: { name: 'ActionResultTemplate', foreach: actionResults }">

                                </tbody>

                            </table>
                        </div>
                    </div>                    

                    <div class="modal-footer">
                        <button type="button"
                                data-bind="click: updateAction, disable: handOperatedDisabled"
                                class="btn btn-info" data-loading-text="Guardando..."
                                class="btn-toggle-loading">Guardar
                        </button>
                        <button type="button" class="btn btn-default close-modal" data-dismiss="modal" data-bind="click: onClose">Cerrar
                        </button>
                    </div>
                <%--</form>--%>
            </div>
        </div>
    </div>
</div>


<!-- MODAL PSTN_DATA-->

<div class="modal fade" id="modal-pstn_data" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: onClose">×</button>
                <h4 class="modal-title">Formulario PSTN+DATA</h4>
            </div>
            <div class="modal-body">
                <%--<form role="form" id="form-pstn_data" method="post" class="validate" data-bind="submit: saveForm"
                      novalidate="novalidate">--%>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="telefono_4" class="control-label">Telefono</label>
                                <input type="text" class="form-control" name="telefono"
                                       id="telefono_4" placeholder="Telefono"
                                <%--data-validate="required"--%>
                                       name="telefono_4"
                                       disabled
                                       data-bind="value: telefono"/>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="cuenta_fija_4">Cuenta Fija</label>
                                <input type="checkbox" class="checkbox" id="cuenta_fija_4"
                                       name="cuenta_fija_4"
                                       disabled
                                       data-bind="checked: cuentaFija"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6" id="section_localidad_pstn_data">
                            <div class="form-group">
                                <label class="control-label" for="localidad_4">Frame</label>
                                <input id="localidad_4" class="form-control"
                                        name="localidad_4"
                                          disabled
                                        data-bind="value: localidad">
                                </input>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="terminal_4" class="control-label">Terminal</label>
                                <input type="text" class="form-control" id="terminal_4"
                                <%--data-validate="required"--%>
                                       name="terminal_4"
                                       disabled
                                       placeholder="Terminal" data-bind="value: terminal"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6" id="section_cabina_pstn_data">
                            <div class="form-group">
                                <label for="cabina_4" class="control-label">Cabina</label>
                                <input type="text" class="form-control"
                                <%--data-validate="required"--%>
                                       data-bind="value: cabina"
                                       name="cabina_4"
                                       id="cabina_4"
                                       disabled
                                       placeholder="Cabina"/>
                            </div>
                        </div>                        
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="par_feeder_4" class="control-label">Par Feeder</label>
                                <input type="text" class="form-control" id="par_feeder_4"
                                       placeholder="Par Feeder"
                                <%--placeholder=".*-####"--%>
                                       name="par_feeder_4"
                                       disabled
                                <%--data-validate="required"--%>
                                       data-bind="value: parFeeder"/>

                            </div>
                        </div>                        

                        <div class="col-md-6" id="section_par_local_pstn_data">
                            <div class="form-group">
                                <label for="par_local_4" class="control-label">Par Local</label>
                                <input type="text" class="form-control" id="par_local_4"
                                       placeholder="Par Local"
                                <%--data-validate="required"--%>
                                       name="par_local_4"
                                       disabled
                                       data-bind="value: parLocal"/>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="tipo_puerto">Tipo de Puerto</label>
                                <!-- <select id="tipo_puerto" class="form-control"
                                        name="tipo_puerto"
                                        disabled
                                        data-bind="options: puertos, value: tipoPuerto ">
                                </select> -->
                                <input type="text" class="form-control" id="tipo_puerto"
                                       placeholder="Tipo Puerto"
                                       name="tipo_puerto"
                                       disabled
                                       data-bind="value: tipoPuerto"/>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="dslam" class="control-label">DSLAM</label>
                                <input type="text" class="form-control"
                                <%--data-validate="required"--%>
                                       data-bind="value: dslam"
                                       name="dslam"
                                       disabled
                                       id="dslam"
                                        />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="puerto" class="control-label">Puerto</label>
                                <input type="text" class="form-control"
                                <%--data-validate="required"--%>
                                       data-bind="value: puerto"
                                       name="puerto"
                                       id="puerto"
                                       disabled
                                        />
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-1">
                            <input type="checkbox" class="checkbox"                                        
                                         data-bind="disable: handOperatedDisabled, checked: handOperated"/>
                          </div>
                          <div class="col-md-5">
                            <span>Operacion manual Por:</span>
                          </div>
                          <div class="col-md-6">                                  
                                  <span class="control-label" data-bind="text: handOperatedBy"></span>
                          </div>
                    </div>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>
                                        Sistema
                                    </th>
                                    <th>
                                        Resultado de Procesamiento
                                    </th>
                                    <th>
                                        Facilidades
                                    </th>
                                </tr>
                                </thead>
                                <tbody data-bind="template: { name: 'ActionResultTemplate', foreach: actionResults }">

                                </tbody>

                            </table>
                        </div>
                    </div>                    

                    <div class="modal-footer">
                        <button type="button"
                                data-bind="click: updateAction, disable: handOperatedDisabled"
                                class="btn btn-info" data-loading-text="Guardando..."
                                class="btn-toggle-loading">Guardar
                        </button>
                        <button type="button" class="btn btn-default close-modal" data-dismiss="modal" data-bind="click: onClose">Cerrar
                        </button>
                    </div>
                <%--</form>--%>
            </div>
        </div>
    </div>
</div>


<!-- MODAL FRAME-->

<div class="modal fade" id="modal-frame" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                        data-bind="click: onClose"
                >x</button>
                <h4 class="modal-title">Formulario FRAME</h4>
            </div>
            <div class="modal-body">
                <%--<form role="form" id="form-frame" method="post" class="validate" data-bind="submit: saveForm"
                      novalidate="novalidate">--%>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="telefono_6" class="control-label">Telefono</label>
                                <input type="text" class="form-control" name="telefono"
                                       id="telefono_6" placeholder="Telefono"
                                <%--data-validate="required"--%>
                                       name="telefono_6"
                                       disabled
                                       data-bind="value: telefono"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="localidad_6">Frame</label>
                                <input id="localidad_6" class="form-control"
                                        name="localidad_6"
										disabled
                                        data-bind="value: localidad">
                                </input>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="par_feeder_4" class="control-label">Par Feeder</label>
                                <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>
                                <input type="text" class="form-control" id="par_feeder_6"
                                       placeholder="Par Feeder"
                                <%--placeholder=".*-####"--%>
                                       disabled
                                       name="par_feeder_6"
                                       data-inputmask-regex="[a-zA-Z0-9]+-[0-9]{4}"
                                       data-validate="required"
                                       data-bind="value: parFeeder"/>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="servicio_dato">Servicio Datos</label>
                                <input type="checkbox" class="checkbox" id="servicio_dato"
                                       name="servicio_dato"
                                       checked="true"
                                       disabled
                                       data-bind="checked: servioDato"/>
                            </div>
                        </div>
                    </div>

                    <div class="row" data-bind="visible: isServicioDatos">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="dslam_6" class="control-label">DSLAM</label>
                                <input type="text" class="form-control" data-validate="required"
                                       data-bind="value: dslam"
                                       name="dslam_6"
                                       disabled
                                       id="dslam_6"
                                        />
                            </div>
                        </div>
                    </div>

                    <div class="row" data-bind="visible: isServicioDatos">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="tipo_puerto_6">Tipo de Puerto</label>
                                <!-- <select id="tipo_puerto_6" class="form-control"
                                        name="tipo_puerto_6"
                                        disabled
                                        data-bind="options: puertos, value: tipoPuerto ">
                                </select> -->
                                <input type="text" class="form-control" id="tipo_puerto_6"
                                       placeholder="Tipo Puerto"
                                       name="tipo_puerto_6"
                                       disabled
                                       data-bind="value: tipoPuerto"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="puerto_6" class="control-label">Puerto</label>
                                <input type="text" class="form-control"
                                <%--data-validate="required"--%>
                                       data-bind="value: puerto"
                                       name="puerto_6"
                                       id="puerto_6"
                                       disabled
                                        />
                            </div>
                        </div>
                    </div>


                    <%--<div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="dslam_1" class="control-label">DSLAM</label>
                                <input type="text" class="form-control"
                                &lt;%&ndash;data-validate="required"&ndash;%&gt;
                                       data-bind="value: dslam"
                                       name="dslam_1"
                                       disabled
                                       id="dslam_1"
                                        />
                            </div>
                        </div>
                    </div>--%>

                    <div class="row">
                        <div class="col-md-1">
                            <input type="checkbox" class="checkbox"                                        
                                         data-bind="disable: handOperatedDisabled, checked: handOperated"/>
                          </div>
                          <div class="col-md-5">
                            <span>Operacion manual Por:</span>
                          </div>
                          <div class="col-md-6">                                  
                                  <span class="control-label" data-bind="text: handOperatedBy"></span>
                          </div>
                    </div>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>
                                        Sistema
                                    </th>
                                    <th>
                                        Resultado de Procesamiento
                                    </th>
                                    <th>
                                        Facilidades
                                    </th>
                                </tr>
                                </thead>
                                <tbody data-bind="template: { name: 'ActionResultTemplate', foreach: actionResults }">

                                </tbody>

                            </table>
                        </div>
                    </div>                    

                    <div class="modal-footer">
                        <button type="button"
                                data-bind="click: updateAction, disable: handOperatedDisabled"
                                class="btn btn-info" data-loading-text="Guardando..."
                                class="btn-toggle-loading">Guardar
                        </button>
                        <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Cerrar
                        </button>
                    </div>
                <%--</form>--%>
            </div>
        </div>
    </div>
</div>


<div class="page-container">


    <!-- Sidebar Menu -->
    <%@include file="../../assets/fragments/sidebar_menu.jsp" %>
    <div class="main-content">
        <!-- Notifications and Raw Links -->
        <div class="row" id="top-bar">
            <%@include file="../../assets/fragments/profile_and_notifications.jsp" %>
            <%@include file="../../assets/fragments/raw_links.jsp" %>
        </div>
        <hr/>
        <!-- CONTENT HERE -->
        <div class="panel-heading">
            <div class="panel-title">
                <h2 id="orders-title"></h2>
            </div>
        </div>

        <ol id="breadCrumb">

        </ol>
        <hr/>
        <br/>


        <div>
            <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered datatable table-hover"
                   id="table"></table>
        </div>

        <div>
            <!-- FOOTER -->
            <%@include file="../../assets/fragments/footer.jsp" %>
        </div>

    </div>
    <!-- BOTTOM INCLUDES-->
    <%--<%@include file="../bottom_includes_pages.jsp" %>--%>
    <%@include file="../../assets/fragments/bottom_includes.jsp" %>

    <script type="text/javascript">

        var session = <% out.print(session.getAttribute("SessionJson"));%>;
        var _userLoggedCard = '<% out.print(session.getAttribute("userCard"));%>';
        var _ip = '<% out.print(session.getAttribute("ip"));%>';

        if (!checkAccess(session, 'ViewDI', null)) {
            location.href = '${pageContext.request.contextPath}/pages/dispositivos/buscar.jsp';
        }

        var buttonsVisible = !!~$.inArray('ViewDI', session['accesos']);
        var formButtonStyle = buttonsVisible ? '' : "style='display:none;'";


        var _f = function (formType, data) {

            console.log('calling _f ' + formType);
            data._url = '${pageContext.request.contextPath}/';
            data._userLoggedCard = _userLoggedCard;
            data._ip = _ip;
            data.isViewMode = true;


            var model;
            if (formType == 'pstn')
                model = new PSTNModel(data);
            else if (formType == 'fibra')
                model = new FIBRAModel(data);
            else if (formType == 'pstn_fibra')
                model = new PSTN_FIBRAModel(data);
            else if (formType == 'pstn_data')
                model = new PSTN_DATAModel(data);
            else if (formType == 'frame') {
                model = new FRAMEModel(data);
            } else
                return;

            console.log('binding view');
            ko.cleanNode(document.getElementById("modal-" + formType));
            //ko.cleanNode(document.getElementById("modal-" + formType));
            ko.applyBindings(model, document.getElementById("modal-" + formType));
            console.dir(model);
            model.fillContent(data);
            model.fillActionResults(data);

        };


        function launchFilledForm($this) {
            var q = getUrlQueryString().q;
            q = ('' == q || null == q ) ? '' : q;


            var $actionId = $($this).data('action');
            var formType = $($this).data('form').toLowerCase().replace('+', '_');
            /*if (formType == 'frame_data') {
                formType = "frame";
            }*/
            /*var form = $("#form-" + formType);*/


            var data = {};

            data.orderCode = 'empty';
            data.orderType = 'empty';
            data.tarjeta = q;
            data.actionId = $actionId;
            data.url = '${pageContext.request.contextPath}/';
            console.log('Preparing to show form ' + formType);


            $.ajax({
                url: '${pageContext.request.contextPath}/operations/OrdersActionsService/getActionValuesByActionId',
                async: true,
                type: 'POST',
                data: {
                    actionId: $actionId,
                    userLoggedCard: _userLoggedCard,
                    ip: _ip,
                    card: q
                },
                beforeSend: function () {
                    //$($this).toggleClass('disabled');
                    toggleLoadingButton('button.btn-toggle-loading');
                    /*$($this).attr('disabled', 'disabled');*/
                },
                success: function (data) {
                    console.log('incoming data ' + data);
                    console.dir(data);
                    data.actionId = $actionId;
                    _f(formType, data);
                },
                complete: function (data) {
                    console.log('data on complete' + data);
                    console.dir(data);
                    //$($this).toggleClass('disabled');
                    //$($this).attr('disabled', false);
                    /*$($this).removeAttr("disabled");*/
                    toggleLoadingButton('button.btn-toggle-loading');
                    console.log('Showing modal #modal-' + formType);
                    jQuery('#modal-' + formType).modal('show', {backdrop: 'static'});
                }
            });
        }


        (function (ko, $, window, document, undefined) {

            setActiveMenuOption(121);
            var localData;
            var q = getUrlQueryString().q;

            q = ('' == q || null == q ) ? '' : q;

            $('#orders-title').text('Listado de Facilidades Digitadas de la Tarjeta ' + q);

//        Parte del breadcromb para la opcion 12
            $('#q-card').text('Facilidades digitadas por la Tarjeta ' + q);

            var table = $("#table").dataTable({
                "sPaginationType": "bootstrap",
                "bAutoWidth": false,
                "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
                "bProcessing": true,
                "sAjaxSource": "${pageContext.request.contextPath}/operations/OrdersActionsService/getActionsByCard?card=" + q,
                "aoColumns": [
                    { "sTitle": "Digitado como" },
                    { "sTitle": "Codigo" },
                    { "sTitle": "Fecha Creacion" },
                    { "sTitle": ""}, //Codigo de la accion
                    { "sTitle": ""}
                ],
                "aoColumnDefs": [
                    {
                        "aTargets": [0],
                        "mRender": function (data, type, full) {
                            if (data == 'PSTN') {
                                return '<span class="label label-default">' + data + '</span>';
                            } else if (data == 'FIBRA') {
                                return '<span class="label label-secondary">' + data + '</span>';
                            } else if (data == 'PSTN+FIBRA') {
                                return '<span class="label label-primary">' + data + '</span>';
                            } else if (data == 'PSTN+DATA') {
                                return '<span class="label label-info">' + data + '</span>';
                            } else {
                                return '<span class="label label-success">' + data + '</span>';
                            }
                        }
                    },
                    {
                        "aTargets": [3],
                        "mRender": function (data, type, full) {
                            return '<button onclick="launchFilledForm(this); return;" type="button" ' + formButtonStyle + '  class="btn btn-secondary btn-toggle-loading"  data-loading-text="Cargando facilidades..." data-action="' + full[3] + '" data-form="' + (full[0] == "FRAME+DATA"? "FRAME":full[0]) + '">Ver facilidades</button>';
                        }
                    }
                ]
            });

        }(ko, jQuery, window, document));


    </script>
</body>
</html>