<%--
  Created by IntelliJ IDEA.
  User: Nathaniel Calderon
  Date: 4/17/2017
  Time: 10:44 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="modal fade" id="modal-frame" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                data-bind="click: onClose"
        >x</button>
        <h4 class="modal-title">Formulario FRAME</h4>
      </div>
      <div class="modal-body">
        <%--<form role="form" id="form-frame" method="post" class="validate" data-bind="submit: saveForm"
              novalidate="novalidate">--%>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="telefono_6" class="control-label">Telefono</label>
              <input type="text" class="form-control" name="telefono"
                     id="telefono_6" placeholder="Telefono"
              <%--data-validate="required"--%>
                     name="telefono_6"
                     disabled
                     data-bind="value: telefono"/>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label" for="localidad_6">Frame</label>
              <input id="localidad_6" class="form-control"
                     name="localidad_6"
                     disabled
                     data-bind="value: localidad">
              </input>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="par_feeder_6" class="control-label">Par Feeder</label>
              <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>
              <input type="text" class="form-control" id="par_feeder_6"
                     placeholder="Par Feeder"
              <%--placeholder=".*-####"--%>
                     disabled
                     name="par_feeder_6"
                     data-inputmask-regex="[a-zA-Z0-9]+-[0-9]{4}"
                     data-validate="required"
                     data-bind="value: parFeeder"/>

            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label" for="servicio_dato">Servicio Datos</label>
              <input type="checkbox" class="checkbox" id="servicio_dato"
                     name="servicio_dato"
                     checked="true"
                     disabled
                     data-bind="checked: servioDato"/>
            </div>
          </div>
        </div>

        <div class="row" data-bind="visible: isServicioDatos">
          <div class="col-md-12">
            <div class="form-group">
              <label for="dslam_6" class="control-label">DSLAM</label>
              <input type="text" class="form-control" data-validate="required"
                     data-bind="value: dslam"
                     name="dslam_6"
                     disabled
                     id="dslam_6"
                      />
            </div>
          </div>
        </div>

        <div class="row" data-bind="visible: isServicioDatos">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label" for="tipo_puerto_6">Tipo de Puerto</label>
              <!-- <select id="tipo_puerto_6" class="form-control"
                      name="tipo_puerto_6"
                      disabled
                      data-bind="options: puertos, value: tipoPuerto ">
              </select> -->
              <input type="text" class="form-control" id="tipo_puerto_6"
                     placeholder="Tipo Puerto"
                     name="tipo_puerto_6"
                     disabled
                     data-bind="value: tipoPuerto"/>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="puerto_6" class="control-label">Puerto</label>
              <input type="text" class="form-control"
              <%--data-validate="required"--%>
                     data-bind="value: puerto"
                     name="puerto_6"
                     id="puerto_6"
                     disabled
                      />
            </div>
          </div>
        </div>


        <%--<div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="dslam_1" class="control-label">DSLAM</label>
                    <input type="text" class="form-control"
                    &lt;%&ndash;data-validate="required"&ndash;%&gt;
                           data-bind="value: dslam"
                           name="dslam_1"
                           disabled
                           id="dslam_1"
                            />
                </div>
            </div>
        </div>--%>

        <div class="row">
          <div class="col-md-1">
            <input type="checkbox" class="checkbox"
                   data-bind="disable: handOperatedDisabled, checked: handOperated"/>
          </div>
          <div class="col-md-5">
            <span>Operacion manual Por:</span>
          </div>
          <div class="col-md-6">
            <span class="control-label" data-bind="text: handOperatedBy"></span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <table class="table table-condensed">
              <thead>
              <tr>
                <th>
                  Sistema
                </th>
                <th>
                  Resultado de Procesamiento
                </th>
                <th>
                  Facilidades
                </th>
              </tr>
              </thead>
              <tbody data-bind="template: { name: 'ActionResultTemplate', foreach: actionResults }">

              </tbody>

            </table>
          </div>
        </div>

        <div class="modal-footer">
          <%--<button type="button"
                  data-bind="click: updateAction, disable: handOperatedDisabled"
                  class="btn btn-info" data-loading-text="Guardando..."
                  class="btn-toggle-loading">Guardar
          </button>--%>
            <button type="button" class="btn btn-default close-modal" data-dismiss="modal"
                    data-bind="click: onClose"
            >Cerrar
            </button>
        </div>
        <%--</form>--%>
      </div>
    </div>
  </div>
</div>
