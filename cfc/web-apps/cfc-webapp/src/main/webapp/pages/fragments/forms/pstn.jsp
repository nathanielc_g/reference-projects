<%--
  Created by IntelliJ IDEA.
  User: Nathaniel Calderon
  Date: 4/17/2017
  Time: 10:42 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="modal fade" id="modal-pstn" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                data-bind="click: onClose"
                >x
        </button>
        <h4 class="modal-title">Formulario PSTN</h4>
      </div>
      <div class="modal-body">
        <%--<form role="form" id="form-pstn" method="post" class="validate" data-bind="submit: saveForm"
              novalidate="novalidate">--%>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="telefono" class="control-label">Telefono</label>
              <input type="text" class="form-control" name="telefono"
                     id="telefono" placeholder="Telefono"
              <%--data-validate="required"--%>
                     name="telefono"
                     disabled
                     data-bind="value: telefono"/>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label" for="cuenta_fija">Cuenta Fija</label>
              <input type="checkbox" class="checkbox" id="cuenta_fija"
                     name="cuenta_fija"
                     disabled
                     data-bind="checked: cuentaFija"/>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6" id="section_localidad_pstn">
            <div class="form-group">
              <label class="control-label" for="localidad">Frame</label>
              <input id="localidad" class="form-control"
                     name="localidad"
                     disabled
                     data-bind="value: localidad">
              </input>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="terminal" class="control-label">Terminal</label>
              <input type="text" class="form-control" id="terminal"
              <%--data-validate="required"--%>
                     name="terminal"
                     disabled
                     placeholder="Terminal" data-bind="value: terminal"/>
            </div>
          </div>
          <div class="col-md-6" id="section_cabina_pstn">
            <div class="form-group">
              <label for="cabina" class="control-label">Cabina</label>
              <input type="text" class="form-control"
              <%--data-validate="required"--%>
                     data-bind="value: cabina"
                     name="cabina"
                     disabled
                     id="cabina"
                     placeholder="Cabina"/>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="par_feeder" class="control-label">Par Feeder</label>
              <input type="text" class="form-control" id="par_feeder"
              <%--placeholder=".*-####"--%>
                     placeholder="Par Feeder"
                     name="par_feeder"
                     disabled
              <%--data-validate="required"--%>
                     data-bind="value: parFeeder"/>

            </div>
          </div>



          <div class="col-md-6" id="section_par_local_pstn">
            <div class="form-group">
              <label for="par_local" class="control-label">Par Local</label>
              <input type="text" class="form-control" id="par_local"
                     placeholder="Par Local"
              <%--data-validate="required"--%>
                     name="par_local"
                     disabled
                     data-bind="value: parLocal"/>
            </div>
          </div>
        </div>
        <div class="row">
          <%--<div class="col-md-12">--%>
          <%--<div class="form-group no-margin">--%>
          <%--<label for="direccion" class="control-label">Direcci&oacute;n</label>--%>
          <%--<textarea class="form-control autogrow" id="direccion"--%>
          <%--data-bind="value: direccion"--%>
          <%--placeholder="Direccion"--%>
          <%--name="direccion"--%>
          <%--disabled--%>
          <%--style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 31px;"></textarea>--%>
          <%--</div>--%>
          <%--</div>--%>
        </div>

        <div class="row">
          <div class="col-md-1">
            <input type="checkbox" class="checkbox"
                   data-bind="disable: handOperatedDisabled, checked: handOperated"/>
          </div>
          <div class="col-md-5">
            <span>Operacion manual Por:</span>
          </div>
          <div class="col-md-6">
            <span class="control-label" data-bind="text: handOperatedBy"></span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <table class="table table-condensed">
              <thead>
              <tr>
                <th>
                  Sistema
                </th>
                <th>
                  Resultado de Procesamiento
                </th>
                <th>
                  Facilidades
                </th>
              </tr>
              </thead>
              <tbody data-bind="template: { name: 'ActionResultTemplate', foreach: actionResults }">

              </tbody>

            </table>
          </div>
        </div>

        <div class="modal-footer">
          <%--<button type="button"
                  data-bind="click: updateAction, disable: handOperatedDisabled"
                  class="btn btn-info" data-loading-text="Guardando..."
                  class="btn-toggle-loading">Guardar
          </button>--%>
          <button type="button" class="btn btn-default close-modal" data-dismiss="modal"
                  data-bind="click: onClose"
                  >Cerrar
          </button>
        </div>
        <%--</form>--%>
      </div>
    </div>
  </div>
</div>
