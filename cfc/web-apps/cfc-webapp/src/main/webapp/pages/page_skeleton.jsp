<%--
  Created by Christopher Herrera
  Date: 1/8/14
  Time: 2:50 PM
  Email: Christopher_Herrera@claro.com.do
  Office: 809-220-8148
  Cellphone: 829-755-1677
  CLARO RD
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <!-- HEAD INCLUDES-->
    <%@include file="../head_pages.jsp" %>
</head>
<body class="page-body page-fade">

<div class="page-container">

    <!-- Sidebar Menu -->
    <%@include file="../../assets/fragments/sidebar_menu.jsp" %>
    <div class="main-content">
        <!-- Notifications and Raw Links -->
        <div class="row" id="top-bar">
            <%@include file="../../assets/fragments/profile_and_notifications.jsp" %>
            <%@include file="../../assets/fragments/raw_links.jsp" %>
        </div>
        <hr/>
        <h2>Page Skeleton (change)</h2>
        <ol id="breadCrumb"></ol>
        <hr/>
        <br/>
        <!-- CONTENT HERE -->



        <!-- FOOTER -->
        <%@include file="../../assets/fragments/footer.jsp" %>
    </div>

</div>
<!-- BOTTOM INCLUDES-->
<%@include file="../assets/fragments/bottom_includes.jsp" %>
</body>
</html>