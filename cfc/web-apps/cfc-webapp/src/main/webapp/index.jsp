<%-- Created by IntelliJ IDEA. --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- HEAD INCLUDES-->
    <%@include file="assets/fragments/head.jsp" %>

    <%
        if (request.getParameter("usr") != null) {
            response.sendRedirect(request.getContextPath() + "/index.jsp");
        }
    %>

</head>
<body class="page-body page-left-in fill" style="min-height: 100%">

<div class="page-container">

    <!-- Sidebar Menu -->
    <%@include file="assets/fragments/sidebar_menu.jsp" %>

    <div class="main-content">
        <!-- Notifications and Raw Links -->
        <div class="row" id="top-bar">
            <%@include file="assets/fragments/profile_and_notifications.jsp" %>
            <%@include file="assets/fragments/raw_links.jsp" %>
        </div>

        <hr/>

        <h2>Captura Facilidades Celular</h2>
        <ol id="breadCrumb">

        </ol>
        <hr/>
        <br/>

        <!-- Page Stats (Only Index) -->
        <div class="row">
            <%@include file="assets/fragments/page_stats.jsp" %>
        </div>

        <!-- CONTENT HERE -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-invert">
                    <div class="panel-heading">
                        <div class="panel-title"> Bienvenido</div>
                    </div>
                    <div class="panel-body" id="welcome-text">
                        Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
                        autorizado por esta entidad.
                        Su uso indebido puede conllevar acciones disciplinarias y/o legales.
                        Toda actividad realizada en este sistema está siendo registrada y monitoreada.
                    </div>
                </div>
            </div>

        </div>

        <!-- FOOTER -->
        <%@include file="assets/fragments/footer.jsp" %>
    </div>

</div>
<!-- BOTTOM INCLUDES-->
<script type="text/javascript">
    $(document).ready(function () {
        setActiveMenuOption(1);
        var session = <% out.print(session.getAttribute("SessionJson"));%>;
        $.growl({ title: "Bienvenido ", message: "Usuario: " + session.usr });

        $("#ucount").data('end', session.userCount);
//        $("#diCount").data('end', session.diCount);

        /**
         * Disabling sidebar menu options based on rol of logged user.
         */
        deniedAccessIfNotAllowed(session,'SearchDevice','menu-option-2-1','menu-option-2-2');
        deniedAccessIfNotAllowed(session,'CreateDevice','menu-option-2-3');

        console.dir(session);
    });
</script>
<%@include file="assets/fragments/bottom_includes.jsp" %>
</body>