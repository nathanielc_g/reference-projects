<%--
  Created by IntelliJ IDEA.
  User: nathaniel calderon
  Date: 10/27/2015
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="row">
    <div class="col-med-6">
        <div class="form-group">
            <button id="refresh" class="btn btn-primary" data-bind="click: refreshClick, enable: enableRefresh"><span class="glyphicon glyphicon-refresh"></span>Refrescar</button>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="telefono" class="control-label">Telefono</label>
      <input type="text" class="form-control" name="telefono"
             id="telefono" placeholder="Telefono" data-validate="required"
             name="telefono" data-mask="phone"
             data-bind="value: telefono, attr : {'disabled' : phoneIsDisabled}"/>
    </div>
  </div>

  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label" for="cuenta_fija">Cuenta Fija</label>
      <input type="checkbox" class="checkbox" id="cuenta_fija"
             name="cuenta_fija"
             data-bind="checked: cuentaFija"/>
    </div>
  </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label" for="localidad">Frame</label>
            <select id="localidad" class="form-control"
                    name="localidad"
                    data-bind="options: localidades, optionsText: 'name', value: localidad, optionsCaption: 'Seleccione FRAME'">
            </select>
        </div>
    </div>

  <div class="col-md-6" data-bind="visible: showCabina">
    <div class="form-group">
      <label for="cabina" class="control-label">Cabina</label>
      <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="CVM6D, CBH7" data-original-title="Ejemplo"></span>
      <input type="text" class="form-control" data-validate="required"
             data-bind="value: cabina"
             name="cabina"
             id="cabina"
             placeholder="Cabina"/>
    </div>
  </div>

</div>

<div class="row">
  <div class="col-md-4">
      <div class="form-group">
          <label for="terminal" class="control-label">Terminal</label>
          <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="CVM6D-4N, CBH7-T, P16-4A(Cuenta Fija)" data-original-title="Ejemplo"></span>
          <input type="text" class="form-control" id="terminal"
                 data-validate="required"
                 name="terminal"
                 placeholder="Terminal" data-bind="value: terminal"/>
      </div>

  </div>

</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="par_feeder" class="control-label">Par Feeder</label>
            <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>
            <input type="text" class="form-control" id="par_feeder"
            <%--placeholder=".*-####"--%>
                   placeholder="Par Feeder"
                   name="par_feeder"
                   data-validate="required"
                   data-bind="value: parFeeder"
                   data-inputmask-regex=".*-[0-9]{4}"/>
        </div>
    </div>
    <div class="col-md-4" data-bind="visible: showNumParFeeder">
        <div class="form-group">
            <label for="num_par_feeder" class="control-label">Rango</label>
            <%--<span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>--%>
            <select id="num_par_feeder"
                    class="form-control"
                    name="numParFeeder"
                    data-bind="value: numParFeeder, options: rangeFeeder"  >
            </select>
        </div>
    </div>
    <div class="col-md-4" data-bind="visible: showOtrosParFeeder">
        <div class="form-group">
            <label for="otros_par_feeder" class="control-label">Otros</label>
            <input type="text" class="form-control" id="otros_par_feeder"
            <%--placeholder=".*-####"--%>
                   placeholder="Otros"
                   name="otrosParFeeder"
                   <%--data-validate="required"--%>
                   data-bind="value: otrosParFeeder"
                    />
        </div>
    </div>

</div>

<div class="row" data-bind="visible: showParLocal" >
    <div class="col-md-4">
        <div class="form-group">
            <label for="par_local" class="control-label">Par Local</label>
            <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="CVM6D-0169, CBH7-0096" data-original-title="Ejemplo"></span>
            <input type="text" class="form-control" id="par_local"
                   placeholder="Par Local"
                   data-validate="required"
                   name="par_local"
                   data-inputmask-regex=".*-[0-9]{4}"
                   data-bind="value: parLocal"/>
        </div>
    </div>
    <div class="col-md-4" data-bind="visible: showNumParLocal">
        <div class="form-group">
            <label for="num_par_local" class="control-label">Rango</label>
            <%--<span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>--%>
            <select id="num_par_local"
                    class="form-control"
                    name="numParLocal"
                    data-bind="value: numParLocal, options: rangeLocal"  >
            </select>
        </div>
    </div>
    <div class="col-md-4" data-bind="visible: showOtrosParLocal">
        <div class="form-group">
            <label for="otros_par_local" class="control-label">Otros</label>
            <input type="text" class="form-control" id="otros_par_local"
            <%--placeholder=".*-####"--%>
                   placeholder="Otros"
                   name="otrosParLocal"
            <%--data-validate="required"--%>
                   data-bind="value: otrosParLocal"
                    />
        </div>
    </div>
</div>
