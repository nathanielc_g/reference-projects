<!-- ARCHIVO PARA IMPORTAR DENTRO DE EL HEAD DE TODOS LOS JSP-->

<meta charset="utf-8">
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->

<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="description" content="CFC Admin Panel"/>
<meta name="author" content="Christopher Herrera (Christopher_Herrera@claro.com.do)"/>

<title>Captura Facilidades Celular</title>

<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/font-icons/entypo/css/animation.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/neon.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/custom.css">
<link href="${pageContext.request.contextPath}/assets/js/growl/stylesheets/jquery.growl.css" rel="stylesheet"
      type="text/css"/>

<script src="assets/js/jquery-1.10.2.min.js"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->