<%--
  Created by IntelliJ IDEA.
  User: nathaniel calderon
  Date: 10/27/2015
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="telefono" class="control-label">Telefono</label>
      <input type="text" class="form-control" name="telefono"
             id="telefono" placeholder="Telefono" 
              disabled="disabled" 
             name="telefono" 

             data-bind="value: telefono"/>
    </div>
  </div>

  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label" for="cuenta_fija">Cuenta Fija</label>
      <input type="checkbox" class="checkbox" id="cuenta_fija"
             name="cuenta_fija"
             disabled="disabled" 
             data-bind="checked: cuentaFija"/>
    </div>
  </div>
</div>
<div class="row">

    <div class="col-md-6" id="section_localidad_pstn">
        <div class="form-group">
            <label class="control-label" for="localidad">Frame</label>
            <select id="localidad" class="form-control"
                    name="localidad"
                    disabled="disabled" 
                    data-bind="options: localidades, optionsText: 'n', value: localidad, optionsCaption: 'Seleccione FRAME'">
            </select>
        </div>
    </div>

  <div class="col-md-6" id="section_cabina_pstn">
    <div class="form-group">
      <label for="cabina" class="control-label">Cabina</label>
      <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="CVM6D, CBH7" data-original-title="Ejemplo"></span>
      <input type="text" class="form-control" 

             data-bind="value: cabina"
             name="cabina"
             disabled="disabled" 
             id="cabina"
             placeholder="Cabina"/>
    </div>
  </div>

</div>

<div class="row">
  <div class="col-md-4">
      <div class="form-group">
          <label for="terminal" class="control-label">Terminal</label>
          <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="CVM6D-4N, CBH7-T, P16-4A(Cuenta Fija)" data-original-title="Ejemplo"></span>
          <input type="text" class="form-control" id="terminal"
                 
                 disabled="disabled" 
                 name="terminal"
                 placeholder="Terminal" data-bind="value: terminal"/>
      </div>

  </div>

</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="par_feeder" class="control-label">Par Feeder</label>
            <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>
            <input type="text" class="form-control" id="par_feeder"
            <%--placeholder=".*-####"--%>
                   placeholder="Par Feeder"

                   disabled="disabled" 
                   name="par_feeder"                   
                   data-bind="value: parFeeder"
                   data-inputmask-regex=".*-[0-9]{4}"/>
        </div>
    </div>
    <div class="col-md-4 section_num_par_feeder">
        <div class="form-group">
            <label for="num_par_feeder" class="control-label">Rango</label>
            <%--<span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>--%>
            <select id="num_par_feeder"
                    class="form-control"
                    disabled="disabled" 
                    name="numParFeeder"
                    data-bind="value: numParFeeder, options: range"  >
            </select>
        </div>
    </div>
    <div class="col-md-4 section_otros_par_feeder">
        <div class="form-group">
            <label for="otros_par_feeder" class="control-label">Otros</label>
            <input type="text" class="form-control" id="otros_par_feeder"
            <%--placeholder=".*-####"--%>
                   placeholder="Otros"

                   disabled="disabled" 
                   name="otrosParFeeder"
                   
                   data-bind="value: otrosParFeeder"
                    />
        </div>
    </div>

</div>

<div class="row" id="section_par_local_pstn">
    <div class="col-md-4">
        <div class="form-group">
            <label for="par_local" class="control-label">Par Local</label>
            <span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="CVM6D-0169, CBH7-0096" data-original-title="Ejemplo"></span>
            <input type="text" class="form-control" id="par_local"
                   placeholder="Par Local"
                   
                   disabled="disabled" 
                   name="par_local"
                   data-inputmask-regex=".*-[0-9]{4}"
                   data-bind="value: parLocal"/>
        </div>
    </div>
    <div class="col-md-4 section_num_par_local">
        <div class="form-group">
            <label for="num_par_local" class="control-label">Rango</label>
            <%--<span class="entypo-help-circled" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="P16-0088, C04-0012" data-original-title="Ejemplo"></span>--%>
            <select id="num_par_local"
                    class="form-control"
                    name="numParLocal"
                    data-bind="value: numParLocal, options: range"  >
            </select>
        </div>
    </div>
    <div class="col-md-4 section_otros_par_local">
        <div class="form-group">
            <label for="otros_par_local" class="control-label">Otros</label>
            <input type="text" class="form-control" id="otros_par_local"
            
                   placeholder="Otros"
                   name="otrosParLocal"
            
                   data-bind="value: otrosParLocal"
                    />
        </div>
    </div>
</div>
