<%--
  Created by Christopher Herrera
  Date: 1/8/14
  Time: 10:55 AM
  Email: Christopher_Herrera@claro.com.do
  Office: 809-220-8148
  CellPhone: 829-755-1677
--%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/js/rickshaw/rickshaw.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/js/select2/select2.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/js/selectboxit/jquery.selectBoxIt.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/js/bootstrap-datepicker-1.6.1-dist/css/bootstrap-datepicker3.min.css" >

<!-- Bottom Scripts -->
<script src="${pageContext.request.contextPath}/assets/js/gsap/main-gsap.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/joinable.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/resizeable.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/neon-api.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jquery.sparkline.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/rickshaw/vendor/d3.v3.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/rickshaw/rickshaw.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/raphael-min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/morris.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/toastr.js"></script>

<script src="${pageContext.request.contextPath}/assets/js/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jquery.inputmask.bundle.min.js"></script>

<script src="${pageContext.request.contextPath}/assets/js/neon-chat.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/neon-custom.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/knockoutjs/knockout-3.0.0.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/typeahead.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/growl/javascripts/jquery.growl.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-datepicker.js"></script>

<script src="${pageContext.request.contextPath}/assets/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/dataTables.bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jquery.dataTables.columnFilter.js"></script>

<script src="${pageContext.request.contextPath}/assets/js/select2/select2.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/gsap/jquery.gsap.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jquery.bootstrap.wizard.min.js"></script>

<script src="${pageContext.request.contextPath}/assets/js/bootstrap-switch.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jquery.multi-select.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jsonpath/jsonpath.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/bootstrap-datepicker-1.6.1-dist/js/bootstrap-datepicker.min.js"></script>


<!-- Custom Scripts -->
<script src="${pageContext.request.contextPath}/assets/js-custom/page-fix.js"></script>
<script src="${pageContext.request.contextPath}/assets/js-custom/page-conf.js"></script>
<script src="${pageContext.request.contextPath}/assets/js-custom/localidades.js"></script>
<script src="${pageContext.request.contextPath}/assets/js-custom/utils.js"></script>
<script src="${pageContext.request.contextPath}/assets/js-custom/form-models.js"></script>
<script src="${pageContext.request.contextPath}/assets/js-custom/fields-validator.js"></script>


