<%--
  Created by Christopher Herrera
  Date: 1/8/14
  Time: 12:44 PM
  Email: Christopher_Herrera@claro.com.do
  Office: 809-220-8148
  Cellphone: 829-755-1677
  CLARO RD
--%>


<div class="col-md-6 col-sm-8 clearfix">

    <ul class="user-info pull-left pull-none-xsm">
        <!-- Profile Info -->
        <li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->

            <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="user-name">
                <img src="${pageContext.request.contextPath}/assets/images/thumb-1.png" alt="" class="img-circle"/>
                <% out.print( session.getAttribute("usr")  ); %>
            </a>
        </li>
    </ul>

    <ul class="user-info pull-left pull-right-xs pull-none-xsm">
        <!-- Task Notifications -->
        <li class="notifications dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <i class="entypo-list"></i>
            </a>
            <ul class="dropdown-menu">
                <li class="external">
                    <form action="${pageContext.request.contextPath}/logout" method="POST">
                        <input type="submit" value="Logout" class="btn btn-info" />
                    </form>
                </li>
            </ul>
        </li>
    </ul>
</div>
