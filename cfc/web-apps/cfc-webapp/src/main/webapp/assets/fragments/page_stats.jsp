<%--
  Created by Christopher Herrera
  Date: 1/8/14
  Time: 1:22 PM
  Email: Christopher_Herrera@claro.com.do
  Office: 809-220-8148
  Cellphone: 829-755-1677
  CLARO RD
--%>

<div class="col-sm-4" >
    <div class="tile-stats tile-aqua">
        <div class="icon"><i class="entypo-users"></i></div>
        <div id="ucount" class="num" data-start="0" data-end="" data-postfix="" data-duration="1500" data-delay="0">0
        </div>

        <h3>Usuarios</h3>

        <p>Usuarios de CFC Movil</p>
    </div>

</div>

<%--<div class="col-sm-3" style="display:none">--%>

    <%--<div class="tile-stats tile-gray">--%>
        <%--<div class="icon"><i class="entypo-chart-bar"></i></div>--%>
        <%--<div id="diCount" class="num" data-start="0" data-end="0" data-postfix="" data-duration="1500"--%>
             <%--data-delay="600">--%>
        <%--</div>--%>

        <%--<h3>Facilidades</h3>--%>

        <%--<p>Facilidades a Capturar</p>--%>
    <%--</div>--%>

<%--</div>--%>

<div class="col-sm-4">

    <div class="tile-stats tile-red">
        <div class="icon"><i class="entypo-list"></i></div>
        <div class="num" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="1200">0</div>

        <h3>Tareas</h3>

        <p>Tareas Pendientes.</p>
    </div>
</div>

<div class="col-sm-4">

    <div class="tile-stats tile-green">
        <div class="icon"><i class="entypo-sun"></i></div>
        <div class="num" data-start="0" data-end="0" data-postfix="" data-duration="1500" data-delay="1800">0</div>

        <h3>Facilidades Sync.</h3>

        <p>Facilidades Sincronizadas</p>
    </div>

</div>
