<%--
  Created by Christopher Herrera
  Date: 1/8/14
  Time: 11:17 AM
  Email: Christopher_Herrera@claro.com.do
  Office: 809-220-8148
  Cellphone: 829-755-1677
  CLARO RD
--%>

<script>
    function getSession(){
      var session = <% out.print(session.getAttribute("SessionJson"));%>;
      return session;
    }
</script>
<div class="sidebar-menu fill fixed">

    <header class="logo-env">
        <!-- logo -->
        <div class="logo">
            <a href="${pageContext.request.contextPath}/index.jsp">
                <img src="${pageContext.request.contextPath}/assets/images/logo_claro.png" width="30" height="30"
                     alt=""/>
                CFC
            </a>
        </div>
        <!-- logo collapse icon -->
        <div class="sidebar-collapse">
            <a href="#" class="sidebar-collapse-icon with-animation">
                <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                <i class="entypo-menu"></i>
            </a>
        </div>
        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>

    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
        <!-- Search Bar
        <li id="search">
            <form method="get" action="">
                <input type="text" name="q" class="search-input" placeholder="Search something..."/>
                <button type="submit">
                    <i class="entypo-search"></i>
                </button>
            </form>
        </li>-->

        <li id="menu-option-1" class="">
            <a href="${pageContext.request.contextPath}/index.jsp">
                <i class="entypo-home"></i>
                <span>Inicio</span>
            </a>
        </li>

        <li id="menu-option-2" class="">
            <a href="#">
                <i class="entypo-mobile"></i>
                <span>Dispositivos</span>
            </a>
            <ul>
                <li id="menu-option-2-1">
                    <a href="#" onclick="return checkAccess(getSession(),'SearchDevice','${pageContext.request.contextPath}/pages/dispositivos/buscar.jsp');">
                        <i class="entypo-search"></i>
                        <span>
                            Buscar
                        </span>
                    </a>
                </li>
                <li id="menu-option-2-2">
                    <a href="#" onclick="return checkAccess(getSession(),'SearchDevice','${pageContext.request.contextPath}/pages/dispositivos/lista.jsp');">
                        <i class="entypo-layout"></i>
                        <span>
                            Lista
                        </span>
                    </a>
                </li>
                <li id="menu-option-2-3">
                    <a href="#" onclick="return checkAccess(getSession(),'CreateDevice','${pageContext.request.contextPath}/pages/dispositivos/crear.jsp');">
                        <i class="entypo-user-add"></i>
                        <span>
                            Crear
                        </span>
                    </a>
                </li>
            </ul>
        </li>
        <li id="menu-option-3" class="">
            <a href="${pageContext.request.contextPath}/pages/reportes/index.jsp">
                <i class="entypo-chart-line"></i>
                <span>Reportes</span>
            </a>
        </li>
        <li id="menu-option-4" class="">
            <%--<a href="${pageContext.request.contextPath}/pages/tareas/index.jsp">--%>
                <%--onclick="return checkAccess(getSession(),'SubmitTasks','${pageContext.request.contextPath}/pages/tareas/index.jsp');"--%>
            <a href="#">
                <i class="entypo-list"></i>
                <span>Tareas</span>
            </a>
            <ul>
                <li id="menu-option-4-1">
                    <a href="#" onclick="return checkAccess(getSession(),'SubmitTasks','${pageContext.request.contextPath}/pages/tareas/index.jsp');">
                        <i class="entypo-back-in-time"></i>
                        <span>
                        Rebotes
                    </span>
                    </a>
                </li>
                <li id="menu-option-4-2">
                    <a href="#" onclick="return checkAccess(getSession(),'CheckHandOperated','${pageContext.request.contextPath}/pages/support/index.jsp');">
                        <i class="entypo-ticket"></i>
                        <span>
                        Centro de Soporte
                    </span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
