/**
 * Created by Nathaniel Calderon on 4/11/2017.
 */
var mainDataTable;

function getDataTable () {
    $('#tbl-results').DataTable();
}
function onResponseFunc (model) {
    var self = this;
    self.model = model;
    self.onSuccess = function (data) {
        self.model.onSuccess(data);
    }

    self.onError = function (data) {
        self.model.onError(data);
    }
}

function httpRequest (path, form_data, model) {
    $.ajax({
        url: path,
        async: true,
        type: 'POST',
        data: form_data,
        beforeSend: function () {
            toggleLoadingButton('button.btn-toggle-loading');
        },
        success: function (data) {
            console.dir(data);
            model.onSuccess(model, data);
        },
        complete: function (data) {
            toggleLoadingButton('button.btn-toggle-loading');
        }, error: function (request, status, error) {
            model.onError(model, data);
            console.dir(request);
            console.dir(status);
            console.dir(error);
        }
    });
}

function checkIfAbleToGenReport (self) {
    var isAbleToGenReport = true;
    isAbleToGenReport = checkIfNonNull(self.isHandOperated());
    if(!isAbleToGenReport)
        return isAbleToGenReport;

    isAbleToGenReport = checkIfNonNull(self.selectedSystem());
    if  (!isAbleToGenReport)
        return isAbleToGenReport;
    isAbleToGenReport = checkIfNonNull(self.beginDate());
    if  (!isAbleToGenReport)
        return isAbleToGenReport;
    isAbleToGenReport = checkIfNonNull(self.endDate());
    if  (!isAbleToGenReport)
        return isAbleToGenReport;

    return isAbleToGenReport;
}

function showViewIsNotAbleToGenReport () {
    $.growl({'title': 'Favor completar campos requeridos.', 'message': ''});
}

function getRequiredFilters (model) {
    var requiredFilters = [];
    requiredFilters.push({"key":"@taskTypeId","value": model.selectedSystem().value});
    requiredFilters.push({"key":"@beginDate","value": model.beginDate()});
    requiredFilters.push({"key":"@endDate","value": model.endDate()});
    var isHandOperated = model.isHandOperated();
    if (isHandOperated)
        requiredFilters.push({"key":"@handOperatedByFilter","value": " and a.HAND_OPERATED_BY != 0 "});
    else
        requiredFilters.push({"key":"@handOperatedByFilter","value": ""});

    var includeSuccessForm = model.includeSuccessForm();
    if(includeSuccessForm){
        requiredFilters.push({"key":"@includeSuccessForm","value": "or t.status = 2"});
        requiredFilters.push({"key":"@notIncludeSuccessForms","value": ""});
    }else {
        requiredFilters.push({"key":"@includeSuccessForm","value": ""});
        requiredFilters.push({"key":"@notIncludeSuccessForms","value": "or status = 2"});
    }
    return requiredFilters;
}

function getOptionalFilters (model) {
    var optionalFilters = [];
    //var subquery = "";
    model.fieldFilters().forEach(function (currentFilter,index,arr) {
        var subquery = " " + currentFilter.field.subquery

        if (subquery.indexOf("@operator") != -1) {
            subquery = subquery.replace(/@operator/g, currentFilter.operator.value);
        }

        if (subquery.indexOf("@value") != -1) {
            if (currentFilter.operator.value === "like") {
                subquery = subquery.replace(/@value/g, currentFilter.value + "%");
            }else {
                subquery = subquery.replace(/@value/g, currentFilter.value);
            }

        }
        optionalFilters.push({"key": currentFilter.field.key,"value": subquery});
    });
    return optionalFilters;
}

function getBasicFormData (model) {
    var basicFormData = {
        _ip: model._ip
        ,_userLoggedID: model._userLoggedID
    };
    return basicFormData;
}

function genReport (model) {
    var form_data = getBasicFormData(model);
    form_data.requiredFilters = JSON.stringify(model.requiredFilters);
    form_data.optionalFilters = JSON.stringify(model.optionalFilters);

    console.log("********filters******", form_data);

    model.onSuccess = function (model, data) {
        console.log("********taskResults******");
        console.log(data);
        renderTaskResults(model, data);
        model.isLoading(false);
    }

    model.onError = function (model, data) {
        model.isLoading(false);
    }

    httpRequest(model.fetchTasksPath, form_data, model);

}

function renderTaskResults (model, taskResults) {
    //var tbl = $('#tbl-results').DataTable();
    //tbl.destroy();
    if(checkIfNonNull(mainDataTable)){
        mainDataTable.fnDestroy();
        mainDataTable = null;
        $("#tbl-results>tbody>tr").remove();
    }

    model.taskResults([]);
    model.taskResults(taskResults);

    if(!checkIfNonNull(mainDataTable))
        mainDataTable = $('#tbl-results').DataTable();

}

function checkIfAbleToProcess (model) {
    return $(".checkboxRow:checked").length > 0;
}

function showViewIsNotAbleToProcess () {
    $.growl({'title': 'No existen formularios marcados para postear.', 'message': ''});
}

function getSelectedForms (model) {
    var forms = [];
    model.taskResults().forEach(function (currentValue,index,arr) {
        //currentValue.isChecked
        if ($(".checkboxRow:eq("+index+")").is(":checked")) {
            forms.push(
                {
                    actionId: currentValue.actionId
                    ,taskTypeName: model.selectedSystem().name

                }
            );

        }
    });
    return forms;
}

function submitForms(model) {
    var form_data = getBasicFormData(model);

    form_data.forms = JSON.stringify(model.forms);
    console.log("********forms data******");
    console.log(form_data);

    model.onSuccess = function (model, data) {
        model.isLoading(false);
        if(checkIfNonNull(mainDataTable)) {
            mainDataTable.fnDestroy();
            mainDataTable = null;
            $("#tbl-results>tbody>tr").remove();
        }
        model.isAllChecked(false);
        model.taskResults([]);
        model.forms = [];
        showViewOK("Rebotes posteados exitosamente");

    }

    model.onError = function (model, data) {
        model.isLoading(false);
    }

    httpRequest(model.submitFormsPath, form_data, model);


}

function showViewOK (message) {
    $.growl({'title': 'Operacion exitosa', 'message': message});
}

function checkIfNonNull (value) {
    return value !== null && value !== undefined && value !== "";
}