/**
 * Created by Nathaniel Calderon on 4/17/2017.
 */
function PSTN_FIBRAModel($_data) {
    console.log("Crearing PSTN_FIBRAModel with data ");
    console.dir($_data);
    var self = this;
    var flag = "_fibra";

    self.orderCode = '';
    self.orderType = '';
    self.orderPhone = '';
    self.isFillingForm = false;

    self.telefono = ko.observable(self.orderPhone);
    self.phoneIsDisabled = ko.observable(false);
    self.cuentaFija = ko.observable(false);
    self.cabina = ko.observable();
    self.parFeeder = ko.observable();
    self.terminal = ko.observable();
    self.parLocal = ko.observable();
    self.splitterPort = ko.observable();
    self.terminalFo = ko.observable("FC");
    self.localidad = ko.observable(); //Frame field  in form
    //self.localidades = ko.observable(getLocalidades());
    self.localidades = ko.observable(); //Frame list  in form
    self.direccion = ko.observable(true);

    var terminaLFoOld = '';

    self.terminalFo.extend({notify: 'always'})
    self.terminalFo.subscribe(function (old) {
        if (self.isFillingForm)
            return;
        terminaLFoOld = old;
    }, this, 'beforeChange');


    self.terminalFo.extend({notify: 'always'})
    self.terminalFo.subscribe(function (value) {
        if (self.isFillingForm)
            return;

        console.log('Reporting Change to terminalFo ' + terminaLFoOld + " - " + value);
        if (terminaLFoOld == value)
            return;
        self.splitterPort('SPT-' + value + '-SP');
    });

    // PSTN_FIBRA MODEL
    // begin 2015-GESTI-4791-1

    self = setupPSTNModel(self, flag);
    if ($_data.isViewMode === true)
        self = setupViewModel(self, $_data);

    self.numParFeeder.subscribe(function (value) {
        if (value == "Otros") {
            self.otrosParFeeder("");

            self.showOtrosParFeeder(true);
        } else {
            self.showOtrosParFeeder(false);
        }
    });

    self.numParLocal.subscribe(function (value) {
        if (value == "Otros") {
            self.otrosParFeeder("");
            self.showOtrosParLocal(true);
        } else {
            self.showOtrosParLocal(false);
        }
    });

    $("#terminal" + flag).blur(function () {
        if (!validateTerminal(self))
            return;

        var _range = getRange(getTerminalSecondSession(self.terminal()));
        self.rangeLocal(_range);
        self.rangeFeeder(_range);


    });

    self.cuentaFija.extend({notify: 'always'});
    self.cuentaFija.subscribe(function (value) {
        PSTNHideFields(self, value, flag);
    });

    self.cabina.extend({notify: 'always'});
    self.cabina.subscribe(function () {
        if (self.isFillingForm)
            return;
        self.terminal(getCabina(self) + "-");
    });


    self.localidad.extend({notify: 'always'});
    self.localidad.subscribe(function (value) {
        if (self.isFillingForm)
            return;
        console.log(value);
        if (!self.cuentaFija()) {
            self.cabina(value ? getJsonPathCabinaPrefix(value) : '');
        }

        var frame = self.localidad();

        if (!frame)
            return;

        console.log('setting terminalFoPrefix with frame..');
        console.log(frame);

        var terminalFoPrefix = getJsonPathFramePath(frame, '$..prefixes.terminal_fo');
        if (!terminalFoPrefix) {
            terminalFoPrefix = "";
        }
        self.terminalFo("FC" + terminalFoPrefix);
    });


    self.terminal.extend({notify: 'always'});
    self.terminal.subscribe(function () {
        if (self.isFillingForm)
            return;
        self.parLocal(getTerminalFistSession(self.terminal()) + '-');

        if (self.cuentaFija()) {
            self.parFeeder(getTerminalFistSession(self.terminal()) + '-');
        }
    });

    self.saveForm = function () {

        if (!validatePSTNForm(self, flag))
            return false;


        var validated = true;

        /*var input = (null != self.terminalFo() && undefined != self.terminalFo()) ? self.terminalFo().trim().toUpperCase() : '';
         if (!( REGEX_TERMINAL_FO.test(input) )) {
         $.growl({title: 'Terminal Fo no valido', message: ''});
         //return false;
         validated = false;
         }*/

        if(!validateTerminalFo(self)){
            return false;
        }


        if(!validateSplitterPort(self)){
            return false;
        }


        if (!validated)
            return false;

        var $data = getPSTNFormData(self, flag);

        $data['SPLITTER_PORT'] = toUpperCase(self.splitterPort()).trim();
        $data['SPLITTER'] = "SPT-" + toUpperCase(self.terminalFo()).trim();
        $data['TERMINAL_FO'] = toUpperCase(self.terminalFo()).trim();

        $data['FORM_TYPE'] = 'PSTN+FIBRA';
        $data['TARJETA'] = $_data.tarjeta;

        cabinaFO = '';
        if (self.terminalFo() != '' && undefined != self.terminalFo())
            cabinaFO = self.terminalFo().substring(0, Math.abs(self.terminalFo().indexOf('-'))).trim();

        $data['CABINA_FO'] = cabinaFO;

        console.log("Preparing to send...");
        console.dir(self);

        generalSendForm($_data._userLoggedCard, $_data._ip, $_data.url, $_data.tableRefreshUrl, $data);
    };

    self.fillContent = function (data) {

        self.isFillingForm = true;

        console.log('updating data');
        console.log(data);
        self = fillPSTNForm(self, data, flag);
        self.splitterPort(data['SPLITTER_PORT']);
        self.terminalFo(data['TERMINAL_FO']);
        self.isFillingForm = false;
    }

    self.setParams = function (xdata) {
        console.log('Setting params on PSTN');
        console.dir(xdata);
        PSTNHideFields(self, false, flag);
        hideSpinner(self, false, flag);

        self.telefono("");
        self.orderCode = xdata.orderCode;
        self.orderType = xdata.orderType;
        self.orderPhone = parsePhone(xdata.orderPhone);
        self = setPhone(self, flag, self.orderPhone);
        //self.telefono(self.orderPhone);
    };

    self.setParams($_data);

    // End 2015-GESTI-4791-1

    // BEGIN 2015-GESTI-3083-2
    self.enableRefresh = true;
    self.refreshClick =  function() {

        console.log('Preparing to refresh data');
        self.enableRefresh = false;
        $.ajax({
            url: $_data.lastFormUrl,
            async: true,
            type: 'POST',
            data: {
                code: self.orderCode,
                orderType: self.orderType,
                userLoggedCard: _userLoggedCard,
                ip: $_data._ip,
                card: $_data._userLoggedCard
            },
            beforeSend: function () {
                //$($this).attr('disabled', 'disabled');
            },
            success: function (data) {
                console.log('incoming data ' + data);
                console.dir(data);
                //$($this).attr('disabled', false);
                if (!$.isEmptyObject(data)) {
                    console.log('Filling form');
                    self.isFillingForm = true;
                    self = resetModel(self, ["telefono", "cuentaFija", "localidad", "cabina"
                        , "terminal", "parLocal", "numParLocal", "otrosParLocal"
                        , "parFeeder", "numParFeeder", "otrosParFeeder", "terminalFo", "splitterPort", "direccion"]);
                    self = loadLastAction(self, data, flag);
                    self.terminalFo(data['TERMINAL_FO']);
                    self.splitterPort(data['SPLITTER_PORT']);
                    self.isFillingForm = false;
                } else {
                    $.growl({title: getAppMessage("not_posted_form"), message: ''});

                }
                self.enableRefresh = true;

            }
            /*,
             complete: function (data) {
             //console.log('data on complete' + data);
             //console.dir(data);
             }*/
        });


    }

    // END 2015-GESTI-3083-2

}