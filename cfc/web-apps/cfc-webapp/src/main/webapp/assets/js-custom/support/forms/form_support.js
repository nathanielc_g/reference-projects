/**
 * Created by Nathaniel Calderon on 6/22/2017.
 */
function saveHandOperated(model) {
    var form_data = getBasicFormData();
    model.onSuccess = function (model, data) {
        console.log("********saveHandOperated******");
        console.log(data);
        renderResult(model, data);
    }

    model.onError = function (model, data) {

    }
    httpRequest(model.saveHandOperatedPath, form_data, model);
}

function saveProcessedBy (model) {
    var form_data = getBasicFormData();
    model.onSuccess = function (model, data) {
        console.log("********saveProcessedBy******");
        console.log(data);
        renderResult(model, data);
    }

    model.onError = function (model, data) {

    }
    httpRequest(model.saveProcessedByPath, form_data, model);
}

function renderResult(model, data){

}

/**
 * Created by Nathaniel Calderon on 4/17/2017.
 */
function loadViewForm (_self, formType, formdata) {

    console.log('calling _f' + formType);
    var data = formdata;
    data._url = _self.baseURL + "/";
    data._userLoggedCard = _self._userLoggedID;
    data._ip = _self._ip;
    data.actionId = _self.selectedAction.actionId;
    data.isViewMode = true;
    data.isSupportMode = true;


    var model;

    if (formType == 'pstn')
        model = new PSTNModel(data);
    else if (formType == 'fibra')
        model = new FIBRAModel(data);
    else if (formType == 'pstn_fibra')
        model = new PSTN_FIBRAModel(data);
    else if (formType == 'pstn_data')
        model = new PSTN_DATAModel(data);
    else if (formType == 'frame') {
        model = new FRAMEModel(data);
    }else
        return;

    console.log('binding view');
    ko.cleanNode(document.getElementById("modal-" + formType));
    //ko.cleanNode(document.getElementById("modal-" + formType));
    ko.applyBindings(model, document.getElementById("modal-" + formType));
    console.dir(model);
    model.fillContent(data);
    model.fillActionResults(data);

}

function launchFilledForm(_self) {

    var actionId = _self.selectedAction.actionId;
    var formType = _self.selectedAction.formType.toLowerCase().replace('+', '_');
    if (formType == 'frame_data') {
        formType = "frame";
    }
    /*var form = $("#form-" + formType);*/

    var data = {};
    data.orderCode = 'empty';
    data.orderType = 'empty';
    data.tarjeta = _self.selectedAction.userId;
    data.actionId = actionId;
    data.url = _self.baseURL;
    console.log('Preparing to show form ' + formType);


    $.ajax({
        url: _self.findActionPath,
        async: true,
        type: 'POST',
        data: {
            actionId: actionId,
            userLoggedCard: _self._userLoggedID,
            ip: _self._ip,
            card: _self.selectedAction.userId
        },
        beforeSend: function () {
            //$($this).toggleClass('disabled');
            toggleLoadingButton('button.btn-toggle-loading');
            /*$($this).attr('disabled', 'disabled');*/
        },
        success: function (data) {
            console.log('incoming data ' + data);
            console.dir(data);
            data.actionId = actionId;
            loadViewForm(_self, formType, data);
        },
        complete: function (data) {
            console.log('data on complete' + data);
            console.dir(data);
            //$($this).toggleClass('disabled');
            //$($this).attr('disabled', false);
            /*$($this).removeAttr("disabled");*/
            toggleLoadingButton('button.btn-toggle-loading');
            console.log('Showing modal #modal-' + formType);
            jQuery('#modal-' + formType).modal('show', {backdrop: 'static'});
        }
    });
}