/**
 * Created by Nathaniel Calderon on 4/17/2017.
 */
function loadViewForm (_self, formType, formdata) {

    console.log('calling _f ' + formType);
    var data = formdata;
    data._url = _self.baseURL + "/";
    data._userLoggedCard = _self._userLoggedID;
    data._ip = _self._ip;
    data.isViewMode = true;


    var model;

    if (formType == 'pstn')
        model = new PSTNModel(data);
    else if (formType == 'fibra')
        model = new FIBRAModel(data);
    else if (formType == 'pstn_fibra')
        model = new PSTN_FIBRAModel(data);
    else if (formType == 'pstn_data')
        model = new PSTN_DATAModel(data);
    else if (formType == 'frame') {
        model = new FRAMEModel(data);
    } else
        return;

    console.log('binding view');
    ko.cleanNode(document.getElementById("modal-" + formType));
    //ko.cleanNode(document.getElementById("modal-" + formType));
    ko.applyBindings(model, document.getElementById("modal-" + formType));
    console.dir(model);
    model.fillContent(data);
    model.fillActionResults(data);

}

function launchFilledForm(_self) {

    var $taskId = _self.actionSelected.taskId;
    var $actionId = _self.actionSelected.actionId;
    var formType = _self.actionSelected.formType.toLowerCase().replace('+', '_');
    if (formType == 'frame_data') {
        formType = "frame";
    }
    /*var form = $("#form-" + formType);*/

    var data = {};

    data.orderCode = 'empty';
    data.orderType = 'empty';
    data.tarjeta = _self.userId;
    data.actionId = $actionId;
    data.taskId = $taskId;
    data.url = _self.baseURL;
    console.log('Preparing to show form ' + formType);


    $.ajax({
        url: _self.getActionValuesURL,
        async: true,
        type: 'POST',
        data: {
            actionId: $actionId,
            userLoggedCard: _self._userLoggedID,
            ip: _self._ip,
            card: _self.actionSelected.userId
        },
        beforeSend: function () {
            //$($this).toggleClass('disabled');
            toggleLoadingButton('button.btn-toggle-loading');
            /*$($this).attr('disabled', 'disabled');*/
        },
        success: function (data) {
            console.log('incoming data ' + data);
            console.dir(data);
            data.actionId = $actionId;
            data.taskId = $taskId;
            loadViewForm(_self, formType, data);
        },
        complete: function (data) {
            console.log('data on complete' + data);
            console.dir(data);
            //$($this).toggleClass('disabled');
            //$($this).attr('disabled', false);
            /*$($this).removeAttr("disabled");*/
            toggleLoadingButton('button.btn-toggle-loading');
            console.log('Showing modal #modal-' + formType);
            jQuery('#modal-' + formType).modal('show', {backdrop: 'static'});
        }
    });
}