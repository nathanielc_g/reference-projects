/**
 * Created by Nathaniel Calderon on 4/11/2017.
 */
function TasksModel (config) {
    var self = this;
    self._ip = config._ip;
    self._userLoggedID = config._userLoggedID;

    self.fetchTasksPath = config.fetchTasksPath;
    self.submitFormsPath = config.submitFormsPath;

    self.getActionValuesURL = config.getActionValuesURL;
    self.baseURL = config.baseURL;

    self.unitTest = function () {
        self.isUpdating = true;
        var system = self.systems[0];
        self.fields(system.fields);
        var field = system.fields[0];
        self.operators(field.operators);
        var operator = field.operators[0];
        self.selectedSystem(system);
        self.beginDate("27-Mar-2017");
        self.endDate("03-Apr-2017");
        self.selectedField(field);
        self.selectedOperator(operator);
        self.value("prueba");
        $("#addFilterRow").click();
        self.isUpdating = false;

    }

    self = configComponent(self);

    self.isLoading = ko.observable(false);
    self.retrieveFailedForms = function () {
        var isAbleToGenReport = checkIfAbleToGenReport(self);
        if (!isAbleToGenReport) {
            showViewIsNotAbleToGenReport();
            return;
        }
        self.isLoading(true);
        self.requiredFilters = getRequiredFilters(self);
        self.optionalFilters = getOptionalFilters(self);
        genReport(self);
    };

    // functions
    self.processFailedForms = function () {
        var isAbleToProcessTask = checkIfAbleToProcess(self);
        if (!isAbleToProcessTask) {
            showViewIsNotAbleToProcess();
            return;
        }
        self.isLoading(true);
        self.forms = getSelectedForms(self);
        var result = submitForms(self);
    };

    self.onChangeSystem = function () {
        if (self.isUpdating)
            return;
        self.fieldFilters([]);
        if(self.selectedSystem() != null && self.selectedSystem() != undefined) {
            self.fields(self.selectedSystem().fields);
        } else {
            self.fields([]);
        }
    }

    self.onChangeAdjustAction = function () {
        if (self.isUpdating)
            return;

    }

    self.showPhoneInput = ko.observable(false);
    self.showValueInput = ko.observable(true);
    self.onChangeField = function () {
        if (self.isUpdating)
            return;

        if(self.selectedField() != null && self.selectedField() != undefined) {
            self.operators(self.selectedField().operators);
            if(self.selectedField().key == "TELEFONO"){
                self.showPhoneInput(true);
                self.showValueInput(false);
            }else{
                self.showPhoneInput(false);
                self.showValueInput(true);
            }
        } else {
            self.operators([]);

        }
        self.value("");
        self.isAbleToAddRow(false);

    }

    self.value.extend({ notify: 'always' });
    self.value.subscribe(function(newValue) {
        if (self.isUpdating)
            return;
        if(newValue != null && newValue != undefined && newValue != "") {
            self.isAbleToAddRow(true);
        } else {
            self.isAbleToAddRow(false);
        }
    });

    self.beginDate.extend({ notify: 'always' });
    self.beginDate.subscribe(function(newValue) {
        if (self.isUpdating)
            return;
        //self.isUpdating = true;
        if(newValue != null && newValue != undefined && newValue != "") {
            var date = new Date(newValue);

            var beginDate = new Date(newValue);
            var endDate = new Date(self.endDate());

            //var timeDiff = Math.abs(beginDate.getTime() - endDate.getTime());
            var timeDiff = endDate.getTime() - beginDate.getTime();
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            if (diffDays >= 0 && diffDays <= 30) {
                //self.isUpdating = false;
                return;
            }

            /*$("#endDate").datepicker("setStartDate",date);
            $("#endDate").datepicker("setEndDate",newValue);*/
            $("#endDate").datepicker("setDate",date);

        }
        //self.isUpdating = false;
    });

    self.endDate.extend({ notify: 'always' });
    self.endDate.subscribe(function(newValue) {
        if (self.isUpdating)
            return;
        self.isUpdating = true;
        if(newValue != null && newValue != undefined && newValue != "") {
            var date = new Date(newValue);

            var beginDate = new Date(self.beginDate());
            var endDate = new Date(newValue);
            //var timeDiff = Math.abs(beginDate.getTime() - endDate.getTime());
            var timeDiff = endDate.getTime() - beginDate.getTime();
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            if (diffDays >= 0 && diffDays <= 30) {
                self.isUpdating = false;
                return;
            }
            date.setDate(date.getDate()-7);
            /*$("#beginDate").datepicker("setStartDate",date);*/
            /*$("#beginDate").datepicker("setEndDate",newValue);*/
            $("#beginDate").datepicker("setDate",date);

        }
        self.isUpdating = false;
    });

    self.isAllChecked.extend({ notify: 'always' });
    self.isAllChecked.subscribe(function(newValue) {
        if (self.isUpdating)
            return;
        if(newValue !== null && newValue !== undefined && newValue !== "") {
            self.taskResults().forEach(function (currentValue,index,arr) {

                currentValue.isChecked = newValue;
            });
            $(".checkboxRow").prop("checked", newValue);

        }
    });

    self.addRowField = function () {
        self.fieldFilters.push(
            {
                field: self.selectedField()
                ,operator: self.selectedOperator()
                ,value: self.value().replace(/%/g, "")
            }
        );
        self.clearForm();

    }

    self.isFieldLineCompleted = function () {

    }

    self.removeRowFieldSelected = function () {
        self.fieldFilters.splice(self.indexSelected(), 1);
        self.indexSelected(null);
        //self.prepareFilterQuery();
    }

    self.isRowSelected = function (index) {
        return self.indexSelected() == index? true: false;

    }

    self.onRowSelected = function (index) {
        self.indexSelected(index);
    }

    self.clearForm = function () {
        self.selectedField(null);
        self.selectedOperator(null);
        self.value(""); // Clear the text box
    }

    self.onActionClick = function ($index, $data) {
        console.log($index, $data);
        self.actionSelected = $data;
        launchFilledForm(self);
    }


}