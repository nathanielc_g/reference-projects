/**
 * Created by Jansel Rodriguez on 6/1/2014.
 */



function refreshTable(tableId, urlData) {
    $.getJSON(urlData, null, function (json) {
        table = $(tableId).dataTable();
        oSettings = table.fnSettings();

        table.fnClearTable(this);

        for (var i = 0; i < json.aaData.length; i++) {
            table.oApi._fnAddData(oSettings, json.aaData[i]);
        }

        oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
        table.fnDraw();
    });
}


function toggleLoadingButton(buttonId) {
    var button = $(buttonId);
    button.toggleClass('disabled');
    if (button.is(':disabled') == 'true' || button.is(':disabled') == true ) {
        button.attr('disabled', false);
    } else {
        button.attr('disabled', 'disabled');
    }
}

function setButtonDisabled (selector, disabled) {
    var button = $(selector);
    if (disabled == true) {
        button.attr('disabled', 'disabled');
    } else {
        button.removeAttr("disabled");
    }
}


function getUrlQueryString() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}


function checkAccess(session,rol,redirect, noPrintMessage) {

    console.log( 'Checking rol '+rol+" on group " );
    console.dir( session['accesos'] );

    if ($.inArray(rol, session['accesos']) != -1) {
        if( null != redirect )
            location.href = redirect;
        return true;
    }else{
        if (noPrintMessage == true) {

        } else {
            $.growl({ title: "Error de Acceso", message: "A su perfil no le esta permitido ejecutar esta accion" });
        }
        return false;
    }
}

function deniedAccessIfNotAllowed(session,rol/*,arguments*/){
   var actions = Array.prototype.slice.call( arguments,2 );
    if( 0 == actions.length )
        return;
    console.log( 'checking access for '+actions );

    /**
     * $.inArray return comparable -1 for not exiting elements and index >=0 for exiting elements
     * ~ convert elements to 2-complement for -1, ~-1 = 0 , for >=0, ~any>=0 = -(any+1)
     * ! convert result in boolean value, 0,!0 = true, and !any>0 false
     * !! reverse !
     * !!! reverse !! that is equal to !
     *
     * so y this case, doing !~$.inArray is equal to say ( -1 == $.inArray )
     */
    if(!~$.inArray(rol,session['accesos'])){
        console.log('Trying to hide actions for rol '+rol );

        for( var r in actions ){
            console.log( 'Hiding action #'+actions[r] );
            $('#'+actions[r]).hide();
        }
    }
}

// Since 2017-GESTI-0290 - any profile with role 'UpdateDeviceImei' is able to update IMEI
function allowAccess (session,rol/*,arguments*/){
    var actions = Array.prototype.slice.call( arguments,2 );
    if( 0 == actions.length )
        return;
    console.log( 'checking access for '+actions );

    /**
     * $.inArray return comparable -1 for not exiting elements and index >=0 for exiting elements
     * ~ convert elements to 2-complement for -1, ~-1 = 0 , for >=0, ~any>=0 = -(any+1)
     * ! convert result in boolean value, 0,!0 = true, and !any>0 false
     * !! reverse !
     * !!! reverse !! that is equal to !
     *
     * so y this case, doing !~$.inArray is equal to say ( -1 == $.inArray )
     */
    if(!~$.inArray(rol,session['accesos'])){
        /*console.log('Trying to hide actions for rol '+rol );

        for( var r in actions ){
            console.log( 'Hiding action #'+actions[r] );
            $('.'+actions[r]).prop("disabled","true");
            $('.'+actions[r]).hide();
        }*/
    } else {
        console.log('Trying to allow actions for rol '+rol );
        for( var r in actions ){
            console.log( 'Hiding action #'+actions[r] );
           // $('.'+actions[r]).prop("disabled","false");
            $('#'+actions[r]).show();
        }
    }
}


function exFunctIfAllowed(session,rol, disable, func){   
    if(func == undefined || func == null)
        return;
    console.log( 'checking access for '+actions );

    /**
     * $.inArray return comparable -1 for not exiting elements and index >=0 for exiting elements
     * ~ convert elements to 2-complement for -1, ~-1 = 0 , for >=0, ~any>=0 = -(any+1)
     * ! convert result in boolean value, 0,!0 = true, and !any>0 false
     * !! reverse !
     * !!! reverse !! that is equal to !
     *
     * so y this case, doing !~$.inArray is equal to say ( -1 == $.inArray )
     */
    if(~$.inArray(rol,session['accesos'])){
        console.log('Refreshing components for rol '+rol );
        func(disable);
    }
}



function alerts( url ){
    alert( url );
}




function getPorts() {
    return [
        // PRIMER ELEMENTO ES UTILIZADO PARA VALIDAR FORMULARIO
        ["Seleccione Tipo de Puerto","ALCATEL","CISCO","STINGER","ZTE","ZYXEL ADSL","ZYXEL SHDSL"],
        [   "",
            "0-0-0-0",
            "TARJ 0-PTO 0",
            "SHELF 0-TARJ 0-PTO 0",
            "0-0",
            "SLOT 0-PUERTO-0 ADSL",
            "SLOT 0-SHDSL PORT 0"
        ],
        [
            / /,
            /\d-\d-\d{1,2}-\d{1,2}$/,
            /(TARJ )\d{1,2}(-PTO )\d$/,
            /(SHELF )\d(-TARJ )\d{1,2}(-PTO )\d{1,2}$/,
            /\d-\d{1,2}$/,
            /(SLOT )\d{1,2}(-PUERTO-)\d{1,2}( ADSL)$/,
            /(SLOT )\d(-SHDSL PORT )\d{1,2}$/
        ]
    ]
}

function getAppMessage (keyName) {
    if (keyName == null || keyName == undefined || keyName == ""){
        return "";
    }

    var messages = {
      "not_posted_form": "No exiten formularios para esta solicitud"
    };

    return messages[keyName];

}

// 2016-GESTI-5048-1 - New logic for status property.
function getUserStatus () {
    return [{"name":"INACTIVO", "value": 0}, {"name":"ACTIVO", "value": 1}, {"name":"TEMPORAL INACTIVO", "value": 2}];
}

function formatPhoneNumber(s) {
    var s2 = (""+s).replace(/\D/g, '');
    var m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
    return (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
}