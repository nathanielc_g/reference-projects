/**
 * Created by Nathaniel Calderon on 6/20/2017.
 */

var mainDataTable;

function SupportModel (config) {
    // default setup
    var model = this;

    model._ip = config._ip;
    model._userLoggedID = config._userLoggedID;
    model.baseURL = config.baseURL;
    /*paths*/
    model.findLastActionPath = config.findLastActionPath;
    model.getNonHandOperatedActionsPath = config.getNonHandOperatedActionsPath;
    model.saveHandOperatedPath = config.saveHandOperatedPath;
    model.saveProcessedByPath = config.saveProcessedByPath;
    model.findActionPath = config.findActionPath;

    applyViewModel(model);

    model.testModel = function () {
        var orderType = model.orderTypes[1];
        var card = "13026";
        var code = "12050212";

        model.selectedOrderType(orderType);
        model.card(card);
        model.orderCode(code);
        //model.orderActions([{"actionId":37844503,"code":12050212,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:31"},{"actionId":37844506,"code":12050584,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:31"},{"actionId":37844565,"code":12050948,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:31"},{"actionId":37844574,"code":12050020,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:31"},{"actionId":37844618,"code":12050401,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:33"},{"actionId":37844640,"code":12050240,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:33"},{"actionId":37844652,"code":12050485,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:33"},{"actionId":37844661,"code":12050216,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:33"},{"actionId":37844703,"code":12050777,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:34"},{"actionId":37844706,"code":12050554,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:34"},{"actionId":37844718,"code":12050842,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:34"},{"actionId":37844740,"code":12050994,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:34"},{"actionId":37844752,"code":12050611,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:34"},{"actionId":37844761,"code":12050920,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:34"},{"actionId":37844812,"code":12050148,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:34"},{"actionId":37844815,"code":12050819,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:34"},{"actionId":37844836,"code":12050468,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:34"},{"actionId":37844839,"code":12050680,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:34"},{"actionId":37844848,"code":12050415,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:34"},{"actionId":37844857,"code":12050125,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 11:34"},{"actionId":37844906,"code":12050018,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37844915,"code":12050604,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37844918,"code":12050697,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37844940,"code":12050983,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37844943,"code":12050361,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37844952,"code":12050101,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37844961,"code":12050471,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845018,"code":12050731,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845021,"code":12050404,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845041,"code":12050797,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845062,"code":12050895,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845064,"code":12050618,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845072,"code":12050319,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845096,"code":12050241,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845099,"code":12050450,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845130,"code":12050986,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845133,"code":12050177,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845142,"code":12050791,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845197,"code":12050967,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845200,"code":12050287,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845220,"code":12050885,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845229,"code":12050250,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"},{"actionId":37845236,"code":12050849,"orderType":"ORDEN","userId":"13026","created":"21-JUN-2017 12:36"}]);
        /*$("#refresh").click();*/
    };

    model.onChangeOrderType = function () {
        console.log("Updating Order Type...");
        if (model.isUpdating) {

        }
    };

    model.refresh = function () {
        refresh(model);
    }

    model.searchAction = function () {
        searchAction(model);
    };

    model.onActionClick = function ($index, $data) {
        console.log($index, $data);
        model.selectedAction = $data;
        launchFilledForm(model);
    };
}

function searchAction(model) {
    var form_data = getBasicFormData(model);
    var valid = validateFormData(model);
    if(!valid) {
        print("Form Error", "Favor completar formulario.");
        return;
    }


    form_data.card = model.card();
    form_data.type = model.selectedOrderType().value;
    form_data.code = model.orderCode();
    model.onSuccess = function (model, data) {
        console.log("********searchAction******");
        console.log(data);
        renderOrderActions(model, data);
    }

    model.onError = function (model, data) {

    }
    httpRequest(model.findLastActionPath, form_data, model);
}

function validateFormData(model) {
    if (model == null || model == undefined)
        return false;
    if (model.orderCode() == null || model.orderCode() == undefined)
        return false;
    if (model.card() == null || model.card() == undefined)
        return false;
    if (model.selectedOrderType() == null || model.selectedOrderType() == undefined)
        return false;
    if (model.orderCode() == null || model.orderCode() == undefined)
        return false;
    return true;
}

function refresh (model) {
    var form_data = getBasicFormData(model);
    model.onSuccess = function (model, data) {
        console.log("********refresh******");
        console.log(data);
        renderOrderActions(model, data);
    }

    model.onError = function (model, data) {

    }
    httpRequest(model.getNonHandOperatedActionsPath, form_data, model);
}

function findAction (model) {
    var form_data = getBasicFormData();
    model.onSuccess = function (model, data) {
        console.log("********findAction******");
        console.log(data);
        renderAction(model, data);
    }

    model.onError = function (model, data) {

    }
    httpRequest(model.findActionPath, form_data, model);
}

function renderOrderActions(model, orderActions) {
    //var tbl = $('#tbl-results').DataTable();
    //tbl.destroy();
    if(checkIfNonNull(mainDataTable)){
        mainDataTable.fnDestroy();
        mainDataTable = null;
        $("#tbl-results>tbody>tr").remove();
    }

    model.orderActions([]);
    model.orderActions(orderActions);

    if(!checkIfNonNull(mainDataTable))
        mainDataTable = $('#tbl-results').DataTable();
}

function renderAction (model, action) {

}
