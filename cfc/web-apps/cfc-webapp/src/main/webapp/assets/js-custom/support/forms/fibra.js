/**
 * Created by Nathaniel Calderon on 4/17/2017.
 */
function FIBRAModel($_data) {
    console.log("Crearing FIBRAModel with data ");
    console.dir($_data);
    var self = this;
    var flag = "fibra";

    self.orderCode = '';
    self.orderType = '';
    self.orderPhone = '';

    self.isFillingForm = false;


    self.telefono = ko.observable(self.orderPhone);
    self.phoneIsDisabled = ko.observable(false);
    self.localidad = ko.observable();
    self.localidades = ko.observable();
    self.splitterPort = ko.observable();
    self.terminalFo = ko.observable("FC");
    self.direccion = ko.observable(true);

    if ($_data.isViewMode === true)
        self = setupViewModel(self, $_data);

    attachFrames(self.localidades);


    var terminaLFoOld = '';

    self.terminalFo.extend({notify: 'always'})
    self.terminalFo.subscribe(function (old) {
        if (self.isFillingForm)
            return;
        terminaLFoOld = old;
    }, this, 'beforeChange');


    self.terminalFo.extend({notify: 'always'})
    self.terminalFo.subscribe(function (value) {
        if (self.isFillingForm)
            return;

        console.log('Reporting Change to terminalFo ' + terminaLFoOld + " - " + value);
        if (terminaLFoOld == value)
            return;
        self.splitterPort('SPT-' + value + '-SP');
    });


    self.setParams = function (xdata) {
        self.telefono("");
        self.orderCode = xdata.orderCode;
        self.orderType = xdata.orderType;
        self.orderPhone = parsePhone(xdata.orderPhone);
        self = setPhone(self, flag, self.orderPhone);
        //self.telefono(self.orderPhone);
    };


    /**
     * setup section
     */
    self.setParams($_data);


    self.saveForm = function () {
        var form = $("#form-fibra");

        if (form.hasClass("validate")) {
            var valid = form.valid();
            if (!valid) {
                form.data("validator").focusInvalid();
                return false;
            }
        }

        var REGEX_PHONE = /8(0|2|4)9-\d{3}-\d{4}/;

        var validated = true;

        var input = (null != self.telefono() && undefined != self.telefono()) ? formatPhoneNumber(self.telefono()) : '';

        if (null != input && undefined != input)
            input = input.replace('(', '').replace(')', '').replace(' ', '-');

        if (!( REGEX_PHONE.test(input) )) {
            $.growl({title: 'Telefono no valido', message: ''});
            //return false;
            validated = false;
        }

        // FRAME
        if (!self.localidad()) {
            $.growl({title: 'FRAME no valido', message: ''});
            return false;
        }

        /*input = (null != self.terminalFo() && undefined != self.terminalFo()) ? self.terminalFo().trim().toUpperCase() : '';

         if (!( REGEX_TERMINAL_FO.test(input) )) {
         $.growl({title: 'Terminal Fo no valido', message: ''});
         //return false;
         validated = false;
         }*/
        if(!validateTerminalFo(self)){
            return false;
        }

        /*input = (null != self.terminalFo() && undefined != self.terminalFo()) ? self.terminalFo().trim().toUpperCase() : '';
         var splitterPortRegex = new RegExp("SPT-" + input + "-SP0[1-8]");

         input = (null != self.splitterPort() && undefined != self.splitterPort()) ? self.splitterPort().trim().toUpperCase() : '';
         if (!(splitterPortRegex.test(input) )) {
         $.growl({title: 'Splitter Port no valido', message: ''});
         //return false;
         validated = false;
         }*/

        if(!validateSplitterPort(self)){
            return false;
        }

        //console( self.direccion() );

        if (!validated)
            return false;

        var $data = {};
        $data['TELEFONO'] = self.telefono();
        $data['LOCALIDAD'] = toUpperCase(getJsonPathFrameName(self.localidad())).trim();
        $data['DIRECCION_CORRECTA'] = self.direccion() ? "SI" : "NO";
        $data['SPLITTER_PORT'] = toUpperCase(self.splitterPort()).trim();
        $data['SPLITTER'] = "SPT-" + toUpperCase(self.terminalFo()).trim();
        $data['TERMINAL_FO'] = toUpperCase(self.terminalFo()).trim();
        $data['CODE'] = self.orderCode;
        $data['TYPE'] = self.orderType;
        $data['FORM_TYPE'] = 'FIBRA';

        cabinaFO = '';
        if (self.terminalFo() != '' && undefined != self.terminalFo())
            cabinaFO = self.terminalFo().substring(0, Math.abs(self.terminalFo().indexOf('-')))

        $data['CABINA_FO'] = cabinaFO;
        $data['TARJETA'] = $_data.tarjeta;


        console.log("Preparing to send...");
        console.dir(self);

        generalSendForm($_data._userLoggedCard, $_data._ip, $_data.url, $_data.tableRefreshUrl, $data);

    };

    self.fillContent = function (data) {
        self.isFillingForm = true;

        console.log('updating data');
        console.log(data);

        self.telefono(data['TELEFONO']);
        self.localidad(data['LOCALIDAD']);
        self.terminalFo(data['TERMINAL_FO']);
        self.splitterPort(data['SPLITTER_PORT']);
        if (data['DIRECCION_CORRECTA'] != undefined && data['DIRECCION_CORRECTA'] != null){
            self.direccion(data['DIRECCION_CORRECTA']);
        }

        self.isFillingForm = false;
    }

    // BEGIN 2015-GESTI-3083-2
    self.enableRefresh = true;
    self.refreshClick =  function() {

        console.log('Preparing to refresh data');
        self.enableRefresh = false;

        $.ajax({
            url: $_data.lastFormUrl,
            async: true,
            type: 'POST',
            data: {
                code: self.orderCode,
                orderType: self.orderType,
                userLoggedCard: _userLoggedCard,
                ip: $_data._ip,
                card: $_data._userLoggedCard
            },
            beforeSend: function () {
                //$($this).attr('disabled', 'disabled');
            },
            success: function (data) {
                console.log('incoming data ' + data);
                console.dir(data);
                //$($this).attr('disabled', false);
                if (!$.isEmptyObject(data)) {
                    console.log('Filling form');
                    self.isFillingForm = true;

                    self = resetModel(self, ["telefono", "terminalFo", "localidad", "splitterPort"
                        , "direccion"]);

                    self.telefono(data['TELEFONO']);
                    self.localidad(self.localidades()[getIndexFromFrames(self.localidades(), data['LOCALIDAD'])]);
                    self.terminalFo(data['TERMINAL_FO']);
                    self.splitterPort(data['SPLITTER_PORT']);
                    if (data['DIRECCION_CORRECTA'] != undefined && data['DIRECCION_CORRECTA'] != null){
                        self.direccion(data['DIRECCION_CORRECTA']);
                    }

                    self.isFillingForm = false;
                } else {
                    $.growl({title: getAppMessage("not_posted_form"), message: ''});


                }

                self.enableRefresh = true;
            }
            /*,
             complete: function (data) {
             //console.log('data on complete' + data);
             //console.dir(data);
             }*/
        });

    }

    // END 2015-GESTI-3083-2
}