/**
 * Created by Nathaniel Calderon on 6/23/2017.
 */
/**
 * Created by Nathaniel Calderon on 6/22/2017.
 */
function httpRequest (path, form_data, model) {
    $.ajax({
        url: path,
        async: true,
        type: 'POST',
        data: form_data,
        beforeSend: function () {
            toggleLoadingButton('button.btn-toggle-loading');
        },
        success: function (data) {
            console.dir(data);
            model.onSuccess(model, data);
        },
        complete: function (data) {
            toggleLoadingButton('button.btn-toggle-loading');
        }, error: function (request, status, error) {
            model.onError(model, data);
            console.dir(request);
            console.dir(status);
            console.dir(error);
        }
    });
}

function makeDataTable (tableId) {
    $(tableId).DataTable();
}

function checkIfNonNull (value) {
    return value !== null && value !== undefined && value !== "";
}

function showViewOK (message) {
    $.growl({'title': 'Operacion exitosa', 'message': message});
}

function print (tittle, message) {
    $.growl({'title': tittle, 'message': message, duration: 6000});
}

function getBasicFormData (model) {
    var basicFormData = {
        _ip: model._ip
        ,_userLoggedID: model._userLoggedID
    };
    return basicFormData;
}