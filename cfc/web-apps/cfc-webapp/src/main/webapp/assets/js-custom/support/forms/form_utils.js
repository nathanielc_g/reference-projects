/**
 * Created by Nathaniel Calderon on 4/17/2017.
 */
function fillActionResults (_url, _userLoggedCard, _ip, actionId, $selfForm) {

    $.ajax({
        url: _url + 'operations/OrdersActionsService/getActionResultsById/',
        async: true,
        type: 'POST',
        data: {
            actionId: actionId,
            userLoggedCard: _userLoggedCard,
            ip: _ip
        },
        beforeSend: function () {
            toggleLoadingButton('button.btn-toggle-loading');
        },
        success: function (data) {
            //console.log('incoming data ' + datself.terminal(a));
            console.dir(data);
            //$selfForm.actionResults(data.TASK_RESULTS);
            /*$selfForm.actionResults.splice(0, $selfForm.actionResults().length);
             for(task in $selfForm.actionResults()){
             $selfForm.actionResults.remove($selfForm.actionResults()[task]);
             }*/
            for(index in data.TASK_RESULTS)
                $selfForm.actionResults.push(data.TASK_RESULTS[index]);
            /*if (!checkAccess(getSession(), 'CheckHandOperated', null, true))
                $selfForm.handOperatedDisabled(true);
            else
                $selfForm.handOperatedDisabled(data.HAND_OPERATED_DISABLED);*/
            /*$selfForm.handOperatedDisabled(data.HAND_OPERATED_DISABLED);
            $selfForm.processedDisabled(data.PROCESSED_BY_DISABLED);*/

            $selfForm.handOperatedDisabled(true);
            $selfForm.processedDisabled(true);

            if(data.HAND_OPERATED_BY != "0") {
                $selfForm.handOperated(true);
                $selfForm.handOperatedBy(data.HAND_OPERATED_BY);
                persistHandOperated = true;
                print("Información","Formulario esta siendo o ha sido trabajado por: "+ data.HAND_OPERATED_BY);

                $selfForm.processed(true);
                if (data.PROCESSED_BY != "0")
                    $selfForm.processedBy(data.PROCESSED_BY);
                else {
                    $selfForm.isUpdateDisabled(false);
                    $selfForm.processedBy(_userLoggedCard);
                }

                persistProcessedBy = true;
            } else {
                $selfForm.handOperated(true);
                $selfForm.handOperatedBy(_userLoggedCard);
                $selfForm.isUpdateDisabled(false);

                $selfForm.processed(false);
                $selfForm.processedBy(_userLoggedCard);
            }
            $selfForm.afterFillActionResults($selfForm);
        },
        complete: function (data) {
            toggleLoadingButton('button.btn-toggle-loading');
        }, error: function (request, status, error) {
            console.dir(request);
            console.dir(status);
            console.dir(error);
        }
    });
}


function updateActionEntity (_url, _userLoggedCard, _ip, $data, type) {
    console.log('data to be sent');
    console.dir($data);

    $.ajax({
        url: _url + 'operations/OrdersActionsService/updateAction/',
        async: true,
        type: 'POST',
        data: $data,
        beforeSend: function () {
            toggleLoadingButton('button.btn-toggle-loading');
        },
        success: function (data) {
            //console.log('incoming data ' + datself.terminal(a));

            console.dir(data);
            if (0 < data.ok) {
                /*$.growl({'title': 'Formulario Actualizado', 'message': ''});*/
                /*if(closeModal != undefined && closeModal == true)
                $(".close-modal").click();*/
            } else {
                $.growl({title: 'Error Actualizando formulario', 'message': data.message});
            }
        },
        complete: function (data) {
            console.log('data on complete' + data);
            toggleLoadingButton("button.btn-toggle-loading");
        }, error: function (request, status, error) {
            console.dir(request);
            console.dir(status);
            console.dir(error);
        }
    });
}

function sendNotification (_url, $data) {
    console.log('data to be sent');
    console.dir($data);

    $.ajax({
        url: _url + 'operations/SupportOrderActionService/sendNotification/',
        async: true,
        type: 'POST',
        data: $data,
        beforeSend: function () {
            toggleLoadingButton('button.btn-toggle-loading');
        },
        success: function (data) {
            //console.log('incoming data ' + datself.terminal(a));

            console.dir(data);
            if (0 < data.ok) {
                /*$.growl({'title': 'Formulario Actualizado', 'message': ''});*/
                /*if(closeModal != undefined && closeModal == true)
                 $(".close-modal").click();*/
            } else {
                $.growl({title: 'Error enviando notification', 'message': data.message});
            }
        },
        complete: function (data) {
            console.log('data on complete' + data);
            toggleLoadingButton("button.btn-toggle-loading");
        }, error: function (request, status, error) {
            console.dir(request);
            console.dir(status);
            console.dir(error);
        }
    });
}

function generalSendForm(_userLoggedCard, _ip, url, tableRefreshUrl, $data) {
    console.log('data to be sent');
    console.dir($data);

    $.ajax({
        url: url,
        async: true,
        type: 'POST',
        data: {
            attr: $data,
            userLoggedCard: _userLoggedCard,
            ip: _ip
        },
        beforeSend: function () {
            toggleLoadingButton('button.btn-toggle-loading');
        },
        success: function (data) {
            //console.log('incoming data ' + datself.terminal(a));

            console.dir(data);

            if (0 < data.ok) {
                $.growl({'title': 'Formulario Guardado', 'message': ''});
                $(".close-modal").click();
            } else {
                $.growl({title: 'Error Enviando formulario', 'message': data.message});
            }
        },
        complete: function (data) {
            console.log('data on complete' + data);
            toggleLoadingButton("button.btn-toggle-loading");
            refreshTable('#table', tableRefreshUrl);
        }, error: function (request, status, error) {
            console.dir(request);
            console.dir(status);
            console.dir(error);
        }
    });
}

var loadedFrames = null;
function attachFrames(knSelectFrames) {
    if (null != loadedFrames && undefined != loadedFrames)
        knSelectFrames(loadedFrames)
    else {
        var fragments = location.href.split('/');
        var host = fragments[0] + "//" + fragments[2];
        var url = host + "/cfc/services/v2/support/ext/domain/v3/frames";
        console.log('Gettign frame list from ' + url);

        $.get(url, function (data) {
            console.dir(data)

            if (data.frames)
                knSelectFrames(loadedFrames = data.frames)
        }, "json");
    }
}


function toUpperCase(input) {
    return null != input && undefined != input ? input.toUpperCase() : '';
}

// NO SE UTILIZA FLAG
function setPhone (_self, flag, phone) {
    var orderPhoneCandidate = 1;
    var candidatePhone = _self.orderPhone;
    _self.phoneIsDisabled(false);
    if (_self.orderPhone == undefined || _self.orderPhone ==null || _self.orderPhone == "") {
        orderPhoneCandidate = 0;
        candidatePhone = phone;
    }
    if (candidatePhone == undefined || candidatePhone == null || candidatePhone == "") {
        console.log("No candidate phone got.");
    } else {
        var input = parsePhone(candidatePhone);
        _self.telefono(input);
        var validated = validatePhone(input);
        if (validated && orderPhoneCandidate == 1) {
            // Logica desechada a solicitud del usuario para permitir la modificacion del telefono para los casos de intraportabilidad
            //_self.phoneIsDisabled(true);
        }

    }
    return _self;
}

function validatePhone (_input) {
    var REGEX_PHONE = /8(0|2|4)9-\d{3}-\d{4}/;
    var validated = true;
    var input = (null != _input && undefined != _input) ? _input : '';

    if (null != input && undefined != input)
        input = input.replace('(', '').replace(')', '').replace(' ', '-');

    if (!( REGEX_PHONE.test(input) )) {
        //return false;
        validated = false;
    }

    return validated;
}



function parsePhone(input) {
    input = '' + input;
    if (null == input || 'undefined' == input || '' == input)
        return '';
    input = input.replace(/\D/g, '');
    if (6 > input.length)
        return '';
    var firstThree = input.substr(0, 3);
    var nextThree = input.substr(3, 3);
    var last = input.substr(6);
    return firstThree + '-' + nextThree + '-' + last;
};


function isValidSuggestedPort(value) {
    value = value.replace(/\D/g, ' ');
    value = value.trim();
    var numbers = value.split(" ");
    if (null == numbers)
        return false;

    var cero = "0";//new String("0").valueOf();
    var empty = " ";//new String(" ").valueOf();

    for (i = 0; i < numbers.length; ++i) {
        var number = numbers[i];//new String( numbers[i]).valueOf();
        if (empty != number) {
            if (number.startsWith(cero))
                return false;
        }
    }
    return true;
}

function fillPSTNForm(_self, data, flag) {

    _self.telefono(data['TELEFONO']);
    _self.cuentaFija(data['CUENTA_FIJA'] == 'SI' ? true : false);
    _self.cabina(data['CABINA']);

    _self.terminal(data['TERMINAL']);

    _self.parFeeder(data['PAR_FEEDER']);
    //setParFeeder(_self, data['PAR_FEEDER'], flag);

    _self.parLocal(data['PAR_LOCAL']);
    //setParLocal(_self, data['PAR_LOCAL'], flag);

    _self.localidad(data['LOCALIDAD']);

    _self.direccion(data['DIRECCION_CORRECTA'] == 'SI' ? true : false);

    PSTNHideFields(_self, _self.cuentaFija(), flag);
    hideSpinner(_self, _self.cuentaFija(), flag);

    return _self;


}

// begin 2015-GESTI-3083-2
function loadLastAction (_self, data, flag) {
    _self.telefono(data['TELEFONO']);
    _self.cuentaFija(data['CUENTA_FIJA'] == 'SI' ? true : false);

    if (_self.localidades() != null && _self.localidades() != undefined && _self.localidades().length > 0){
        var idx = getIndexFromFrames(_self.localidades(), data['LOCALIDAD']);
        _self.localidad(_self.localidades()[idx]);
    }

    _self.cabina(data['CABINA']);

    _self.terminal(data['TERMINAL']);
    if (data['DIRECCION_CORRECTA'] != undefined && data['DIRECCION_CORRECTA'] != null){
        _self.direccion(data['DIRECCION_CORRECTA']);
    }

    //_self.parFeeder(data['PAR_FEEDER']);
    var arr = getRange(getTerminalSecondSession(_self.terminal()));
    _self.rangeFeeder(arr);
    _self = setParFeeder(_self, data['PAR_FEEDER'], flag);

    //_self.parLocal(data['PAR_LOCAL']);
    _self.rangeLocal(arr);
    _self = setParLocal(_self, data['PAR_LOCAL'], flag);





    return _self;

}



// end 2015-GESTI-3083-2


function getRange(terminalSecondSession) {
    var strNumSecondSession = terminalSecondSession.replace(/[a-zA-Z]{1,2}/g, "")
    var numSecondSession = strNumSecondSession.length > 0 ? parseInt(strNumSecondSession) : 0;
    var charSecondSession = numSecondSession == 0 ? terminalSecondSession : terminalSecondSession.replace(numSecondSession + "", "");
    var firstCharacter = charSecondSession.length > 0 ? charSecondSession.substring(0, 1) : "";
    var min = (numSecondSession * 100) + 1;
    var max = (numSecondSession * 100) + 50;

    if (/[n-z]|[N-Z]/g.test(firstCharacter)) {
        min = (numSecondSession * 100) + 51;
        max = (numSecondSession * 100) + 100;
    }

    var items = [];
    items[items.length] = "Seleccionar";
    for (var i = min; i <= max; i++) {
        items[items.length] = ("0000" + i).slice(-4);
    }
    items[items.length] = "Otros";
    return items;
}

function getParFeeder(_self, isFormFrame) {
    var parFeederVal = _self.parFeeder();
    if (isFormFrame === true) {
        return parFeederVal;
    }

    if (_self.cuentaFija) {
        var sb = parFeederVal.toString();

        if (!(_self.numParFeeder().toString() == "Seleccionar")) {
            if (_self.numParFeeder().toString() == "Otros") {
                sb += _self.otrosParFeeder().toString();
            } else {
                sb += _self.numParFeeder().toString();
            }
        }
        parFeederVal = sb;
    }
    return parFeederVal;
}

function setParFeeder(_self, _parFeederVal, flag) {
    if(!_self.cuentaFija()){
        _self.parFeeder(_parFeederVal);
        return _self;
    }
    var parFeederFirstSession = getParFistSession(_parFeederVal) + "-";
    _self.parFeeder(parFeederFirstSession);
    var parFeederSecondSession = getParSecondSession(_parFeederVal);
    if ($.inArray(parFeederSecondSession, _self.rangeFeeder()) > -1) {
        _self.showNumParFeeder(true);
        _self.numParFeeder(parFeederSecondSession);
    } else {
        _self.numParFeeder("Otros");

        _self.showOtrosParFeeder(true);

        _self.otrosParFeeder(parFeederSecondSession);
    }
    return _self;
}

function setParLocal(_self, _parLocalVal, flag) {
    if(_self.cuentaFija()){
        _self.parLocal(_parLocalVal);
        return _self;
    }
    var parLocalFirstSession = getParFistSession(_parLocalVal) + "-";
    _self.parLocal(parLocalFirstSession);
    var parLocalSecondSession = getParSecondSession(_parLocalVal);
    if ($.inArray(parLocalSecondSession, _self.rangeLocal()) > -1) {
        _self.showNumParLocal(true);
        _self.numParLocal(parLocalSecondSession);
    } else {
        _self.numParLocal("Otros");

        _self.showOtrosParLocal(true);

        _self.otrosParLocal(parLocalSecondSession);
    }
    return _self;
}

function getParLocal(_self) {
    var sb = _self.parLocal().toString();
    if (!_self.cuentaFija()) {
        var numParLocalVal = _self.numParLocal() != null && !(_self.numParLocal().toString() == "Seleccionar") ? _self.numParLocal().toString() : "";
        if (numParLocalVal.toString() == "Otros") {
            sb += _self.otrosParLocal().toString();
        } else {
            sb += numParLocalVal;
        }
    }
    return sb;
}

function getTerminalSecondSession(terminal) {
    if (terminal == null || terminal == undefined || terminal == "") {
        return "";
    }
    var indexHyphen = terminal.indexOf("-");
    if (indexHyphen <= 0) {
        return "";
    }
    return terminal.substring(indexHyphen + 1);
}

function getTerminalFistSession(terminal) {
    if (terminal == null || terminal == undefined || terminal == "") {
        return "";
    }
    var indexHyphen = terminal.indexOf("-");
    if (indexHyphen <= 0) {
        return "";
    }
    return terminal.substring(0, indexHyphen);
}

function getParSecondSession(parVal) {
    if(parVal == undefined || parVal ==null){
        return "";
    }
    var indexHyphen = parVal.toString().indexOf("-");
    if (indexHyphen <= 0) {
        return "";
    }
    return parVal.toString().substring(indexHyphen + 1);
}

function getParFistSession(parVal) {
    if(parVal == undefined || parVal ==null){
        return "";
    }

    var indexHyphen = parVal.toString().indexOf("-");
    if (indexHyphen <= 0) {
        return "";
    }
    return parVal.toString().substring(0, indexHyphen);
}

function getCabina(_self) {
    return _self.cabina();
}

function validatePSTNForm(_self, flag) {
    //var REGEX_FEEDER = /.*?-\d{4}/;
    var REGEX_FEEDER = /^[a-zA-Z0-9]+-\d{4}/;
    var REGEX_END_DASH = /.*-$/;
    //var REGEX_TERMINAL = /^[a-zA-Z0-9]+-(?=[^0])[0-9]{0,2}[a-zA-Z](x|X)?/;
    //var REGEX_TERMINAL = /^[a-zA-Z0-9]+-(?=[^0])[0-9]{0,2}[a-zA-Z](x|X)?/;
    var REGEX_PHONE = /8(0|2|4)9-\d{3}-\d{4}/;
    var REGEX_PAR_LOCAL = /^[a-zA-Z0-9]+-\d{4}/;

    var form = $("#form-pstn" + flag);

    if (form.hasClass("validate")) {
        var valid = form.valid();
        if (!valid) {
            form.data("validator").focusInvalid();
            return false;
        }
    }


    var validated = true;
    //var input = (null != _self.telefono() && undefined != _self.telefono()) ? _self.telefono() : '';
    var input = (null != _self.telefono() && undefined != _self.telefono()) ? formatPhoneNumber(_self.telefono()) : '';

    if (null != input && undefined != input)
        input = input.replace('(', '').replace(')', '').replace(' ', '-');

    if (!( REGEX_PHONE.test(input) )) {
        $.growl({title: 'Telefono no valido', message: ''});
        return false;
    }


    if(!validateCabina(_self)){
        return false;
    }


    // VALIDATING TERMINAL
    if (!validateTerminal(_self)) {
        return false;
    }

    if (!_self.localidad()) {
        $.growl({title: 'FRAME no valido', message: ''});
        return false;
    }

    // VALIDATING PAR FEEDER
    if (!validateParFeeder(_self, false)) {
        return false;
    }



    /*var parFeeder = getParFeeder(_self);
     input = (null != parFeeder && undefined != parFeeder) ? parFeeder.trim().toUpperCase() : '';
     if (!( REGEX_FEEDER.test(input) )) {
     $.growl({title: 'Par feeder no valido', message: ''});
     return false;
     }*/

    if (!_self.cuentaFija()) {

        var parLocal = getParLocal(_self);
        input = (null != parLocal && undefined != parLocal) ? parLocal.trim().toUpperCase() : '';
        if (!( REGEX_PAR_LOCAL.test(input))) {
            $.growl({title: 'Par Local no valido', message: ''})
            return false;
        }

    }

    return validated;

}

function PSTNHideFields(_self, value, flag) {

    if (!_self.isFillingForm) {
        _self.cabina("");
        _self.terminal("");
        _self.parFeeder("");
        _self.parLocal("");
    }

    if (value) {
        _self.showCabina(false);
        _self.showParLocal(false);

        _self.showNumParFeeder(true);
    } else {
        _self.showCabina(true);
        _self.showParLocal(true);

        _self.showNumParFeeder(false);
    }

    _self.showOtrosParFeeder(false);
    _self.showOtrosParLocal(false);
    //  if (!self.isFillingForm)
    setReadOnlyEditPar(value, flag);
    return _self;

}

function hideSpinner(_self, value, flag) {
    if (value) {
        _self.showCabina(false);
        _self.showParLocal(false);

        _self.showNumParFeeder(true);
    } else {
        _self.showCabina(true);
        _self.showParLocal(true);

        _self.showNumParFeeder(false);
    }

    _self.showOtrosParFeeder(false);
    _self.showOtrosParLocal(false);
    return _self;

}

function setReadOnlyEditPar(readOnly, flag) {
    if (readOnly) {
        $("#par_local" + flag).prop('readonly', true);
        $("#par_feeder" + flag).prop('readonly', true);

    } else {
        $("#par_local" + flag).prop('readonly', true);
        $("#par_feeder" + flag).prop('readonly', false);
    }

}

function setupPSTNModel(_self, flag, _data) {
    _self.showCabina = ko.observable(true);
    _self.showParLocal = ko.observable(true);
    _self.showNumParLocal = ko.observable(true);
    _self.showOtrosParLocal = ko.observable(false);
    _self.showNumParFeeder = ko.observable(false);
    _self.showOtrosParFeeder = ko.observable(false);

    _self.numParFeeder = ko.observable();
    _self.otrosParFeeder = ko.observable();
    _self.rangeFeeder = ko.observableArray([]);
    _self.rangeFeeder(getRange(""));
    _self.numParLocal = ko.observable();
    _self.otrosParLocal = ko.observable();
    _self.rangeLocal = ko.observableArray([]);
    _self.rangeLocal(getRange(""));


    attachFrames(_self.localidades);

    _self.showNumParFeeder(false);

    setReadOnlyEditPar(false, flag);

    return _self;
}

function refreshViewModel (_self, _data) {
    var self = _self;
    _self.handOperatedDisabled(true);
    _self.handOperatedBy("");
    _self.handOperated(false);
    //_self.actionResults([]);
}

function setupViewModel (_self, _data) {
    persistHandOperated = false;
    var self = _self;
    _self.isUpdateDisabled = ko.observable(true);
    _self.handOperatedDisabled = ko.observable(true);
    _self.handOperatedBy = ko.observable();
    _self.handOperated = ko.observable(false);
    _self.handOperated.extend({notify: 'always'});
    _self.handOperated.subscribe(function (value) {
        if (self.isFillingForm)
            return;

        if (value){
            self.handOperatedBy(self._userLoggedCard);
        }
        else{
            self.handOperatedBy(self._userLoggedCard);
        }
    });

    _self.processedDisabled = ko.observable(true);
    _self.processedBy = ko.observable();
    _self.processed = ko.observable(false);
    _self.processed.extend({notify: 'always'});
    _self.processed.subscribe(function (value) {
        if (self.isFillingForm)
            return;

        if (value){
            self.processedBy(self._userLoggedCard);
        }
        else{
            self.processedBy(self._userLoggedCard);
        }
    });

    _self.actionResults = ko.observableArray([]);
    _self.actionId = "";
    _self._ip = _data._ip;
    _self._userLoggedCard = _data._userLoggedCard;
    _self._url = _data._url;

    _self.updateAction = function () {
        persistHandOperated = true;
        if(persistHandOperated == true && persistProcessedBy == true) {
            var $data = {
                actionId: _self.actionId,
                handOperatedBy: _self.handOperatedBy(),
                processedBy: _self._userLoggedCard,
                userLoggedCard: _self._userLoggedCard,
                ip: _self._ip
            };
            console.log("Preparing to send...", $data);
            updateActionEntity(_self._url,_self._userLoggedCard, _self._ip, $data);
        } else {
            main_model.refresh();
            var $data = {
                actionId: _self.actionId,
                userLoggedCard: _self._userLoggedCard,
                ip: _self._ip
            };
            // TODO: IT WAS TEMPORALLY DISABLED UNTIL REQ WERE COMPLETED
            //sendNotification(_self._url, $data);
        }
        $.growl({'title': 'Formulario Actualizado', 'message': ''})
        $(".close-modal").click()
    }

    _self.fillActionResults = function (data) {
        self.isFillingForm = true;
        self.actionId = data.actionId;
        fillActionResults(data._url, data._userLoggedCard, data._ip, data.actionId, self);
        self.isFillingForm = false;
    }

    _self.afterFillActionResults = function ($self) {
        if(persistHandOperated == false) {
            var $data = {
                actionId: $self.actionId,
                handOperatedBy: $self._userLoggedCard,
                userLoggedCard: $self._userLoggedCard,
                ip: $self._ip
            };
            console.log("Preparing to send...", $data);
            updateActionEntity($self._url,$self._userLoggedCard, $self._ip, $data);
        }
    }

    _self.onClose = function () {
        self.isFillingForm = true;
        self.actionResults.splice(0, self.actionResults().length);
        for(task in self.actionResults()){
            self.actionResults.remove(task);
        }

        if(persistHandOperated == false) {
            var $data = {
                actionId: self.actionId,
                handOperatedBy: 0,
                userLoggedCard: self._userLoggedCard,
                ip: self._ip
            };
            console.log("Preparing to send...");
            updateActionEntity(self._url,self._userLoggedCard, self._ip, $data);
        }
        self.isFillingForm = false;
    }
    return _self;
}

function getPSTNFormData(_self, flag) {
    var $data = {};
    $data['TELEFONO'] = _self.telefono();
    $data['CUENTA_FIJA'] = _self.cuentaFija() ? "SI" : "NO";

    if (!_self.cuentaFija()) {
        $data['CABINA'] = toUpperCase(_self.cabina()).trim();
        $data['PAR_LOCAL'] = toUpperCase(getParLocal(_self)).trim();
    }

    $data['LOCALIDAD'] = toUpperCase(getJsonPathFrameName(_self.localidad())).trim();
    //$data['PAR_FEEDER'] = toUpperCase(self.parFeeder());
    $data['PAR_FEEDER'] = toUpperCase(getParFeeder(_self)).trim();
    $data['TERMINAL'] = toUpperCase(_self.terminal()).trim();
    //$data['PAR_LOCAL'] = toUpperCase(self.parLocal());


    $data['DIRECCION_CORRECTA'] = _self.direccion();
    $data['CODE'] = _self.orderCode;
    $data['TYPE'] = _self.orderType;
    return $data;
}


function getJsonPathCabinaPrefix(loc) {
    value = JSONPath({
        json: loc,
        path: "$..prefixes.cabina"
    });
    if (value && 0 < value.length)
        return value.shift();
    return undefined;
}

function getJsonPathFrameName(loc) {
    value = JSONPath({
        json: loc,
        path: "$.name"
    });
    if (value && 0 < value.length)
        return value.shift();
    return undefined;
}

function getJsonArrayPathFrameName(loc) {
    value = JSONPath({
        json: loc,
        path: "$.[*].name"
    });
    if (value && 0 < value.length)
        return value;
    return undefined;
}

function getIndexFromFrames (Loc, value) {
    if(Loc == undefined || Loc == null || Loc.length == 0){
        return 0;
    }

    var idx = -1;
    var array = getJsonArrayPathFrameName(Loc);
    if (array != undefined){
        idx = array.indexOf(value);
    }
    if(idx >= 0) {
        return idx;
    } else {
        return 0;
    }
}

function getJsonPathFramePath(loc, _path) {
    value = JSONPath({
        json: loc,
        path: _path
    })
    if (value && 0 < value.length)
        return value.shift();
    return "";
}

function resetModel (_self, fieldsNames) {
    for(var i = 0; i < fieldsNames.length; i++) {
        var fieldName = fieldsNames[i];
        if(ko.isObservable(_self[fieldName])) {
            _self[fieldName]("");
        }
    }
    return _self;
}