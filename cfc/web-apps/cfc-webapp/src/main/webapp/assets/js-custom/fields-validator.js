/**
 * Created by Nathaniel Calderon on 2/25/2016.
 */

var REGEX_DSLAM = /^[a-zA-Z][a-zA-Z0-9]+[a-zA-Z][1-9][0-9]?$/;
var REGEX_TERMINAL = /^([1-9][a-zA-Z0-9]*|[a-zA-Z][a-zA-Z0-9]+)-(?=[^0])([1-9]|[1-3][0-9])?[a-zA-Z](x|X)?/;
var REGEX_PRE_HYPHEN_TERMINAL = /^^(M0|C0|C[Oo](\d|-)|\d+-|[a-zA-Z]-).*$/;


var REGEX_FEEDER = /^[a-zA-Z0-9]+-\d{4}/;

/*
 Invalid Values:

 CO-....
 Co-....
 CO#....
 Co#....
 C0�� (Cualquier car�cter despu�s)
 M0�..(Cualquier car�cter despu�s)
 ##-.... ning�n digito sin car�cter antes del guion
 [0123456789]-....
 [a-zA-Z]-.... no permita un solo car�cter antes del guion

 */
var REGEX_PRE_HYPHEN_FEEDER = /^(M0|C0|C[Oo](\d|-)|\d+-|[a-zA-Z]-).*$/;

var REGEX_CABINA = /^[a-zA-Z0-9]+$/;

var REGEX_TERMINAL_FO = /FC[a-zA-Z0-9]{1,10}-[a-zA-Z]\d{1,3}/;

function validateDslam (input) {
    var validated = true;
    input = (null != input && undefined != input ) ? input.trim().toUpperCase() : '';
    if (!REGEX_DSLAM.test('' + input) || $.isArray(input.toUpperCase().match(/(DSLAM|COPC|PRUEBA)/))) {
        $.growl({title: 'DSLAM invalido.', message: ''});
        //return false;
        validated = false;
    }
    return validated;
}

function validateParFeeder (_self, isFormFrame) {
    var validated = true;
    var parFeeder = getParFeeder(_self, isFormFrame);
    var input = (null != parFeeder && undefined != parFeeder) ? parFeeder.trim().toUpperCase() : '';
    //if (( REGEX_END_DASH.test(input))) {
    if (!REGEX_FEEDER.test(input) || REGEX_PRE_HYPHEN_FEEDER.test(input)) {
        $.growl({title: 'Par feeder no valido', message: ''})
        //return false;
        validated = false;
    }
    return validated;


}

function validateTerminal (_self) {
    var validated = true;

    var input = (null != _self.terminal() && undefined != _self.terminal()) ? _self.terminal().trim().toUpperCase() : '';
    var cabina = (null != _self.cabina() && undefined != _self.cabina()) ? _self.cabina().trim().toUpperCase() : '';

    var terminalRegex = new RegExp("^" + cabina + "-.*$");
    /*if (!REGEX_TERMINAL.test(input)
        ||
        !terminalRegex.test(input)
    ) {*/
    if (
            !REGEX_TERMINAL.test(input)
            || (!_self.cuentaFija() && !terminalRegex.test(input))
            || REGEX_PRE_HYPHEN_TERMINAL.test(input)
    ) {
    //if (!REGEX_TERMINAL.test(input) || REGEX_PRE_HYPHEN_TERMINAL.test(input)) {
        $.growl({title: 'Terminal no valido', message: ''})
        //return false;
        validated = false;
    }
    return validated;
}

function validateTerminalFo (self) {
    var validated = true;
    var input = (null != self.terminalFo() && undefined != self.terminalFo()) ? self.terminalFo().trim().toUpperCase() : '';

    if (!REGEX_TERMINAL_FO.test(input)) {
        $.growl({title: 'Terminal Fo no valido', message: ''});
        //return false;
        validated = false;
    }
    return validated;
}

function validateSplitterPort (self) {
    var validated = true;
    var input = (null != self.terminalFo() && undefined != self.terminalFo()) ? self.terminalFo().trim().toUpperCase() : '';
    var splitterPortRegex = new RegExp("SPT-" + input + "-SP0[1-8]");

    input = (null != self.splitterPort() && undefined != self.splitterPort()) ? self.splitterPort().trim().toUpperCase() : '';
    if (!(splitterPortRegex.test(input) )) {
        $.growl({title: 'Splitter Port no valido', message: ''});
        //return false;
        validated = false;
    }
    return validated;
}

function validateCabina (_self) {

    var validated = true;
    if (!_self.cuentaFija()) {
        var input = (null != _self.cabina() && undefined != _self.cabina()) ? _self.cabina().trim().toUpperCase() : '';
        if (!REGEX_CABINA.test(input)) {
            $.growl({title: 'Cabina invalida', message: ''});
            //return false;
            validated = false;
        }
    }
    return validated;

}

