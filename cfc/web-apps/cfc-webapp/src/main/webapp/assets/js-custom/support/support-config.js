/**
 * Created by Nathaniel Calderon on 6/23/2017.
 */

function getDefaultViewModel () {
    var viewModel =  {};
    viewModel.isAbleToRefresh = false;
    viewModel.isAbleToSearch = false;
    viewModel.orderTypes = [{
        label: "AVERIA"
        ,value: "AVERIA"
    }, {
        label: "ORDEN"
        ,value: "ORDEN"
    }];
    return viewModel;
}

function applyViewModel (model) {
    var defaultViewModel = getDefaultViewModel();
    model.onSuccess = function (model, data) {

    };

    model.onError = function (model, data) {

    };

    model.isUpdating = false;
    model.isAbleToSearch = ko.observable(false);
    model.isAbleToRefresh = ko.observable(false);
    model.orderTypesArray = defaultViewModel.orderTypes;
    model.orderTypes = defaultViewModel.orderTypes;

    model.selectedOrderType  = ko.observable();
    model.card = ko.observable();
    model.orderCode = ko.observable();
    model.orderActions = ko.observableArray([]);
    model.actionSelected = ko.observable();
}