/**
 * Created by Nathaniel Calderon on 4/17/2017.
 */
function PSTNModel($_data) {


    console.log("Creating PSTNModel with data ");
    console.dir($_data);
    var self = this;
    var flag = "";
    self.orderCode = '';
    self.orderType = '';
    self.orderPhone = '';

    self.isFillingForm = false;

    self.telefono = ko.observable(self.orderPhone);
    self.phoneIsDisabled = ko.observable(false);
    self.cuentaFija = ko.observable(false);
    self.cabina = ko.observable();
    self.parFeeder = ko.observable();
    self.terminal = ko.observable();
    self.parLocal = ko.observable();


    self.localidad = ko.observable(); //Frame field  in form
    //self.localidades = ko.observable(getLocalidades());
    self.localidades = ko.observable(); //Frame list  in form
    self.direccion = ko.observable();

    // begin 2015-GESTI-4791-1

    self = setupPSTNModel(self, flag);

    if ($_data.isViewMode === true)
        self = setupViewModel(self, $_data);

    self.numParFeeder.subscribe(function (value) {
        if (value == "Otros") {
            self.otrosParFeeder("");
            //$("#otros_par_feeder" + flag).show();
            self.showOtrosParFeeder(true);

        } else {
            //$("#otros_par_feeder" + flag).hide();
            self.showOtrosParFeeder(false);
        }
    });


    self.numParLocal.subscribe(function (value) {
        if (value == "Otros") {
            self.otrosParLocal("");
            //$("#otros_par_local" + flag).show();
            self.showOtrosParLocal(true);

        } else {
            //$("#otros_par_local" + flag).hide();
            self.showOtrosParLocal(false);

        }
    });

    $("#terminal" + flag).blur(function () {
        if (!validateTerminal(self))
            return;

        var _range = getRange(getTerminalSecondSession(self.terminal()));
        self.rangeLocal(_range);
        self.rangeFeeder(_range);


    });

    self.cuentaFija.extend({notify: 'always'});
    self.cuentaFija.subscribe(function (value) {
        PSTNHideFields(self, value, flag);
        hideSpinner(self, value, flag);

    });

    self.cabina.extend({notify: 'always'});
    self.cabina.subscribe(function () {
        if (self.isFillingForm)
            return;
        self.terminal(getCabina(self) + "-");
    });

    self.localidad.extend({notify: 'always'});
    self.localidad.subscribe(function (value) {
        if (self.isFillingForm)
            return;
        try {
            if (!self.cuentaFija()) {
                self.cabina(value ? getJsonPathCabinaPrefix(value) : '');
            }


        } catch (e) {
            console.log(e)
        }
    });

    self.terminal.extend({notify: 'always'});
    self.terminal.subscribe(function () {
        if (self.isFillingForm)
            return;
        self.parLocal(getTerminalFistSession(self.terminal()) + '-');

        if (self.cuentaFija()) {
            self.parFeeder(getTerminalFistSession(self.terminal()) + '-');
        }

    });

    self.saveForm = function () {

        if (!validatePSTNForm(self, flag))
            return false;

        var $data = getPSTNFormData(self, flag);
        $data['FORM_TYPE'] = 'PSTN';
        $data['TARJETA'] = $_data.tarjeta;


        console.log("Preparing to send...");
        console.dir(self);

        generalSendForm($_data._userLoggedCard, $_data._ip, $_data.url, $_data.tableRefreshUrl, $data);
    };

    self.fillContent = function (data) {

        self.isFillingForm = true;


        console.log('updating data');
        console.log(data);

        self = fillPSTNForm(self, data, flag);
        self.isFillingForm = false;
    }

    self.setParams = function (xdata) {
        console.log('Setting params on PSTN');
        console.dir(xdata);
        self = PSTNHideFields(self, false, flag);
        self = hideSpinner(self, false, flag);
        self.telefono("");
        self.orderCode = xdata.orderCode;
        self.orderType = xdata.orderType;
        self.orderPhone = parsePhone(xdata.orderPhone);
        self = setPhone(self, flag, self.orderPhone);
        //self.telefono(self.orderPhone);
    };

    self.setParams($_data);

    // End 2015-GESTI-4791-1


    // BEGIN 2015-GESTI-3083-2
    self.enableRefresh = true;
    self.refreshClick =  function() {

        self.enableRefresh = false;
        console.log('Preparing to refresh data');
        /*self.rangeLocal(["Seleccionar"]);
         self.rangeFeeder(["Seleccionar"]);
         self.numParLocal("Seleccionar");
         self.numParFeeder("Seleccionar");
         self.direccion(true);*/
        $.growl({title: 'Buscando ultimo formulario posteado', message: ''});

        $.ajax({
            url: $_data.lastFormUrl,
            async: true,
            type: 'POST',
            data: {
                code: self.orderCode,
                orderType: self.orderType,
                userLoggedCard: _userLoggedCard,
                ip: $_data._ip,
                card: $_data._userLoggedCard
            },
            beforeSend: function () {
                //$($this).attr('disabled', 'disabled');
            },
            success: function (data) {
                console.log('incoming data ' + data);
                console.dir(data);
                //$($this).attr('disabled', false);
                if (!$.isEmptyObject(data)) {
                    console.log('Filling form');
                    self.isFillingForm = true;
                    console.log("Reseting Form Values");
                    self = resetModel(self, ["telefono", "cuentaFija", "localidad", "cabina"
                        , "terminal", "parLocal", "numParLocal", "otrosParLocal"
                        , "parFeeder", "numParFeeder", "otrosParFeeder", "direccion"]);
                    self = loadLastAction(self, data, flag);
                    self.isFillingForm = false;
                } else {
                    $.growl({title: getAppMessage("not_posted_form"), message: ''});

                }
                self.enableRefresh = true;

            }
            /*,
             complete: function (data) {
             //console.log('data on complete' + data);
             //console.dir(data);
             }*/
        });
    }

    // END 2015-GESTI-3083-2


}