/**
 * Created by Jansel Rodriguez on 6/2/2014.
 */
function fillActionResults(_url, _userLoggedCard, _ip, actionId, $selfForm) {

    $.ajax({
        url: _url + 'operations/OrdersActionsService/getActionResultsById/',
        async: true,
        type: 'POST',
        data: {
            actionId: actionId,
            userLoggedCard: _userLoggedCard,
            ip: _ip
        },
        beforeSend: function () {
            toggleLoadingButton('button.btn-toggle-loading');
        },
        success: function (data) {
            //console.log('incoming data ' + datself.terminal(a));
            console.dir(data);
            //$selfForm.actionResults(data.TASK_RESULTS);
            /*$selfForm.actionResults.splice(0, $selfForm.actionResults().length);
            for(task in $selfForm.actionResults()){
                $selfForm.actionResults.remove($selfForm.actionResults()[task]);
            }*/
            for (index in data.TASK_RESULTS)
                $selfForm.actionResults.push(data.TASK_RESULTS[index]);
            if (!checkAccess(session, 'CheckHandOperated', null, true))
                $selfForm.handOperatedDisabled(true);
            else
                $selfForm.handOperatedDisabled(data.HAND_OPERATED_DISABLED);

            if (data.HAND_OPERATED_BY != "0") {
                $selfForm.handOperated(true);
                $selfForm.handOperatedBy(data.HAND_OPERATED_BY);
            } else {
                $selfForm.handOperated(false);
                $selfForm.handOperatedBy(_userLoggedCard);
            }
        },
        complete: function (data) {
            toggleLoadingButton('button.btn-toggle-loading');
        }, error: function (request, status, error) {
            console.dir(request);
            console.dir(status);
            console.dir(error);
        }
    });
}

function updateHandOperated(_url, _userLoggedCard, _ip, $data) {
    console.log('data to be sent');
    console.dir($data);

    $.ajax({
        url: _url + 'operations/OrdersActionsService/updateAction/',
        async: true,
        type: 'POST',
        data: {
            actionId: $data.actionId,
            handOperatedBy: $data.handOperatedBy,
            userLoggedCard: _userLoggedCard,
            ip: _ip
        },
        beforeSend: function () {
            toggleLoadingButton('button.btn-toggle-loading');
        },
        success: function (data) {
            //console.log('incoming data ' + datself.terminal(a));

            console.dir(data);

            if (0 < data.ok) {
                $.growl({'title': 'Formulario Actualizado', 'message': ''});
                $(".close-modal").click();
            } else {
                $.growl({title: 'Error Actualizando formulario', 'message': data.message});
            }
        },
        complete: function (data) {
            console.log('data on complete' + data);
            toggleLoadingButton("button.btn-toggle-loading");
        }, error: function (request, status, error) {
            console.dir(request);
            console.dir(status);
            console.dir(error);
        }
    });
}

function generalSendForm(_userLoggedCard, _ip, url, tableRefreshUrl, $data) {
    console.log('data to be sent');
    console.dir($data);

    $.ajax({
        url: url,
        async: true,
        type: 'POST',
        data: {
            attr: $data,
            userLoggedCard: _userLoggedCard,
            ip: _ip
        },
        beforeSend: function () {
            toggleLoadingButton('button.btn-toggle-loading');
        },
        success: function (data) {
            //console.log('incoming data ' + datself.terminal(a));

            console.dir(data);

            if (0 < data.ok) {
                $.growl({'title': 'Formulario Guardado', 'message': ''});
                $(".close-modal").click();
            } else {
                $.growl({title: 'Error Enviando formulario', 'message': data.message});
            }
        },
        complete: function (data) {
            console.log('data on complete' + data);
            toggleLoadingButton("button.btn-toggle-loading");
            refreshTable('#table', tableRefreshUrl);
        }, error: function (request, status, error) {
            console.dir(request);
            console.dir(status);
            console.dir(error);
        }
    });
}

var loadedFrames = null;

function attachFrames(knSelectFrames) {
    if (null != loadedFrames && undefined != loadedFrames)
        knSelectFrames(loadedFrames)
    else {
        var fragments = location.href.split('/');
        var host = fragments[0] + "//" + fragments[2];
        var url = host + "/cfc/services/v2/support/ext/domain/v3/frames";
        console.log('Gettign frame list from ' + url);

        $.get(url, function (data) {
            console.dir(data)

            if (data.frames)
                knSelectFrames(loadedFrames = data.frames)
        }, "json");
    }
}


function toUpperCase(input) {
    return null != input && undefined != input ? input.toUpperCase() : '';
}

// NO SE UTILIZA FLAG
function setPhone(_self, flag, phone) {
    var orderPhoneCandidate = 1;
    var candidatePhone = _self.orderPhone;
    _self.phoneIsDisabled(false);
    if (_self.orderPhone == undefined || _self.orderPhone == null || _self.orderPhone == "") {
        orderPhoneCandidate = 0;
        candidatePhone = phone;
    }
    if (candidatePhone == undefined || candidatePhone == null || candidatePhone == "") {
        console.log("No candidate phone got.");
    } else {
        var input = parsePhone(candidatePhone);
        _self.telefono(input);
        var validated = validatePhone(input);
        if (validated && orderPhoneCandidate == 1) {
            // Logica desechada a solicitud del usuario para permitir la modificacion del telefono para los casos de intraportabilidad
            //_self.phoneIsDisabled(true);
        }

    }
    return _self;
}

function validatePhone(_input) {
    var REGEX_PHONE = /8(0|2|4)9-\d{3}-\d{4}/;
    var validated = true;
    var input = (null != _input && undefined != _input) ? _input : '';

    if (null != input && undefined != input)
        input = input.replace('(', '').replace(')', '').replace(' ', '-');

    if (!( REGEX_PHONE.test(input) )) {
        //return false;
        validated = false;
    }

    return validated;
}


function parsePhone(input) {
    input = '' + input;
    if (null == input || 'undefined' == input || '' == input)
        return '';
    input = input.replace(/\D/g, '');
    if (6 > input.length)
        return '';
    var firstThree = input.substr(0, 3);
    var nextThree = input.substr(3, 3);
    var last = input.substr(6);
    return firstThree + '-' + nextThree + '-' + last;
};


function isValidSuggestedPort(value) {
    value = value.replace(/\D/g, ' ');
    value = value.trim();
    var numbers = value.split(" ");
    if (null == numbers)
        return false;

    var cero = "0";//new String("0").valueOf();
    var empty = " ";//new String(" ").valueOf();

    for (i = 0; i < numbers.length; ++i) {
        var number = numbers[i];//new String( numbers[i]).valueOf();
        if (empty != number) {
            if (number.startsWith(cero))
                return false;
        }
    }
    return true;
}

function fillPSTNForm(_self, data, flag) {

    _self.telefono(data['TELEFONO']);
    _self.cuentaFija(data['CUENTA_FIJA'] == 'SI' ? true : false);
    _self.cabina(data['CABINA']);

    _self.terminal(data['TERMINAL']);

    _self.parFeeder(data['PAR_FEEDER']);
    //setParFeeder(_self, data['PAR_FEEDER'], flag);

    _self.parLocal(data['PAR_LOCAL']);
    //setParLocal(_self, data['PAR_LOCAL'], flag);

    _self.localidad(data['LOCALIDAD']);

    _self.direccion(data['DIRECCION_CORRECTA'] == 'SI' ? true : false);

    PSTNHideFields(_self, _self.cuentaFija(), flag);
    hideSpinner(_self, _self.cuentaFija(), flag);

    return _self;


}

// begin 2015-GESTI-3083-2
function loadLastAction(_self, data, flag) {
    _self.telefono(data['TELEFONO']);
    _self.cuentaFija(data['CUENTA_FIJA'] == 'SI' ? true : false);

    if (_self.localidades() != null && _self.localidades() != undefined && _self.localidades().length > 0) {
        var idx = getIndexFromFrames(_self.localidades(), data['LOCALIDAD']);
        _self.localidad(_self.localidades()[idx]);
    }

    _self.cabina(data['CABINA']);

    _self.terminal(data['TERMINAL']);
    if (data['DIRECCION_CORRECTA'] != undefined && data['DIRECCION_CORRECTA'] != null) {
        _self.direccion(data['DIRECCION_CORRECTA']);
    }

    //_self.parFeeder(data['PAR_FEEDER']);
    var arr = getRange(getTerminalSecondSession(_self.terminal()));
    _self.rangeFeeder(arr);
    _self = setParFeeder(_self, data['PAR_FEEDER'], flag);

    //_self.parLocal(data['PAR_LOCAL']);
    _self.rangeLocal(arr);
    _self = setParLocal(_self, data['PAR_LOCAL'], flag);

    return _self;

}


// end 2015-GESTI-3083-2


function getRange(terminalSecondSession) {
    var strNumSecondSession = terminalSecondSession.replace(/[a-zA-Z]{1,2}/g, "")
    var numSecondSession = strNumSecondSession.length > 0 ? parseInt(strNumSecondSession) : 0;
    var charSecondSession = numSecondSession == 0 ? terminalSecondSession : terminalSecondSession.replace(numSecondSession + "", "");
    var firstCharacter = charSecondSession.length > 0 ? charSecondSession.substring(0, 1) : "";
    var min = (numSecondSession * 100) + 1;
    var max = (numSecondSession * 100) + 50;

    if (/[n-z]|[N-Z]/g.test(firstCharacter)) {
        min = (numSecondSession * 100) + 51;
        max = (numSecondSession * 100) + 100;
    }

    var items = [];
    items[items.length] = "Seleccionar";
    for (var i = min; i <= max; i++) {
        items[items.length] = ("0000" + i).slice(-4);
    }
    items[items.length] = "Otros";
    return items;
}

function getParFeeder(_self, isFormFrame) {
    var parFeederVal = _self.parFeeder();
    if (isFormFrame === true) {
        return parFeederVal;
    }

    if (_self.cuentaFija) {
        var sb = parFeederVal.toString();

        if (!(_self.numParFeeder().toString() == "Seleccionar")) {
            if (_self.numParFeeder().toString() == "Otros") {
                sb += _self.otrosParFeeder().toString();
            } else {
                sb += _self.numParFeeder().toString();
            }
        }
        parFeederVal = sb;
    }
    return parFeederVal;
}

function setParFeeder(_self, _parFeederVal, flag) {
    if (!_self.cuentaFija()) {
        _self.parFeeder(_parFeederVal);
        return _self;
    }
    var parFeederFirstSession = getParFistSession(_parFeederVal) + "-";
    _self.parFeeder(parFeederFirstSession);
    var parFeederSecondSession = getParSecondSession(_parFeederVal);
    if ($.inArray(parFeederSecondSession, _self.rangeFeeder()) > -1) {
        _self.showNumParFeeder(true);
        _self.numParFeeder(parFeederSecondSession);
    } else {
        _self.numParFeeder("Otros");

        _self.showOtrosParFeeder(true);

        _self.otrosParFeeder(parFeederSecondSession);
    }
    return _self;
}

function setParLocal(_self, _parLocalVal, flag) {
    if (_self.cuentaFija()) {
        _self.parLocal(_parLocalVal);
        return _self;
    }
    var parLocalFirstSession = getParFistSession(_parLocalVal) + "-";
    _self.parLocal(parLocalFirstSession);
    var parLocalSecondSession = getParSecondSession(_parLocalVal);
    if ($.inArray(parLocalSecondSession, _self.rangeLocal()) > -1) {
        _self.showNumParLocal(true);
        _self.numParLocal(parLocalSecondSession);
    } else {
        _self.numParLocal("Otros");

        _self.showOtrosParLocal(true);

        _self.otrosParLocal(parLocalSecondSession);
    }
    return _self;
}

function getParLocal(_self) {
    var sb = _self.parLocal().toString();
    if (!_self.cuentaFija()) {
        var numParLocalVal = _self.numParLocal() != null && !(_self.numParLocal().toString() == "Seleccionar") ? _self.numParLocal().toString() : "";
        if (numParLocalVal.toString() == "Otros") {
            sb += _self.otrosParLocal().toString();
        } else {
            sb += numParLocalVal;
        }
    }
    return sb;
}

function getTerminalSecondSession(terminal) {
    if (terminal == null || terminal == undefined || terminal == "") {
        return "";
    }
    var indexHyphen = terminal.indexOf("-");
    if (indexHyphen <= 0) {
        return "";
    }
    return terminal.substring(indexHyphen + 1);
}

function getTerminalFistSession(terminal) {
    if (terminal == null || terminal == undefined || terminal == "") {
        return "";
    }
    var indexHyphen = terminal.indexOf("-");
    if (indexHyphen <= 0) {
        return "";
    }
    return terminal.substring(0, indexHyphen);
}

function getParSecondSession(parVal) {
    if (parVal == undefined || parVal == null) {
        return "";
    }
    var indexHyphen = parVal.toString().indexOf("-");
    if (indexHyphen <= 0) {
        return "";
    }
    return parVal.toString().substring(indexHyphen + 1);
}

function getParFistSession(parVal) {
    if (parVal == undefined || parVal == null) {
        return "";
    }

    var indexHyphen = parVal.toString().indexOf("-");
    if (indexHyphen <= 0) {
        return "";
    }
    return parVal.toString().substring(0, indexHyphen);
}

function getCabina(_self) {
    return _self.cabina();
}

function validatePSTNForm(_self, flag) {
    //var REGEX_FEEDER = /.*?-\d{4}/;
    var REGEX_FEEDER = /^[a-zA-Z0-9]+-\d{4}/;
    var REGEX_END_DASH = /.*-$/;
    //var REGEX_TERMINAL = /^[a-zA-Z0-9]+-(?=[^0])[0-9]{0,2}[a-zA-Z](x|X)?/;
    //var REGEX_TERMINAL = /^[a-zA-Z0-9]+-(?=[^0])[0-9]{0,2}[a-zA-Z](x|X)?/;
    var REGEX_PHONE = /8(0|2|4)9-\d{3}-\d{4}/;
    var REGEX_PAR_LOCAL = /^[a-zA-Z0-9]+-\d{4}/;

    var form = $("#form-pstn" + flag);

    if (form.hasClass("validate")) {
        var valid = form.valid();
        if (!valid) {
            form.data("validator").focusInvalid();
            return false;
        }
    }


    var validated = true;
    var input = (null != _self.telefono() && undefined != _self.telefono()) ? formatPhoneNumber(_self.telefono()) : '';

    if (null != input && undefined != input)
        input = input.replace('(', '').replace(')', '').replace(' ', '-');

    if (!( REGEX_PHONE.test(input) )) {
        $.growl({title: 'Telefono no valido', message: ''});
        return false;
    }


    if (!validateCabina(_self)) {
        return false;
    }


    // VALIDATING TERMINAL
    if (!validateTerminal(_self)) {
        return false;
    }

    if (!_self.localidad()) {
        $.growl({title: 'FRAME no valido', message: ''});
        return false;
    }

    // VALIDATING PAR FEEDER
    if (!validateParFeeder(_self, false)) {
        return false;
    }


    /*var parFeeder = getParFeeder(_self);
    input = (null != parFeeder && undefined != parFeeder) ? parFeeder.trim().toUpperCase() : '';
    if (!( REGEX_FEEDER.test(input) )) {
        $.growl({title: 'Par feeder no valido', message: ''});
        return false;
    }*/

    if (!_self.cuentaFija()) {

        var parLocal = getParLocal(_self);
        input = (null != parLocal && undefined != parLocal) ? parLocal.trim().toUpperCase() : '';
        if (!( REGEX_PAR_LOCAL.test(input))) {
            $.growl({title: 'Par Local no valido', message: ''})
            return false;
        }

    }

    return validated;

}

function PSTNHideFields(_self, value, flag) {

    if (!_self.isFillingForm) {
        _self.cabina("");
        _self.terminal("");
        _self.parFeeder("");
        _self.parLocal("");
    }

    if (value) {
        _self.showCabina(false);
        _self.showParLocal(false);

        _self.showNumParFeeder(true);
    } else {
        _self.showCabina(true);
        _self.showParLocal(true);

        _self.showNumParFeeder(false);
    }

    _self.showOtrosParFeeder(false);
    _self.showOtrosParLocal(false);
    //  if (!self.isFillingForm)
    setReadOnlyEditPar(value, flag);
    return _self;

}

function hideSpinner(_self, value, flag) {
    if (value) {
        _self.showCabina(false);
        _self.showParLocal(false);

        _self.showNumParFeeder(true);
    } else {
        _self.showCabina(true);
        _self.showParLocal(true);

        _self.showNumParFeeder(false);
    }

    _self.showOtrosParFeeder(false);
    _self.showOtrosParLocal(false);
    return _self;

}

function setReadOnlyEditPar(readOnly, flag) {
    if (readOnly) {
        $("#par_local" + flag).prop('readonly', true);
        $("#par_feeder" + flag).prop('readonly', true);

    } else {
        $("#par_local" + flag).prop('readonly', true);
        $("#par_feeder" + flag).prop('readonly', false);
    }

}

function setupPSTNModel(_self, flag, _data) {
    _self.showCabina = ko.observable(true);
    _self.showParLocal = ko.observable(true);
    _self.showNumParLocal = ko.observable(true);
    _self.showOtrosParLocal = ko.observable(false);
    _self.showNumParFeeder = ko.observable(false);
    _self.showOtrosParFeeder = ko.observable(false);

    _self.numParFeeder = ko.observable();
    _self.otrosParFeeder = ko.observable();
    _self.rangeFeeder = ko.observableArray([]);
    _self.rangeFeeder(getRange(""));
    _self.numParLocal = ko.observable();
    _self.otrosParLocal = ko.observable();
    _self.rangeLocal = ko.observableArray([]);
    _self.rangeLocal(getRange(""));


    attachFrames(_self.localidades);

    _self.showNumParFeeder(false);

    setReadOnlyEditPar(false, flag);

    return _self;
}

function refreshViewModel(_self, _data) {
    var self = _self;
    _self.handOperatedDisabled(true);
    _self.handOperatedBy("");
    _self.handOperated(false);
    //_self.actionResults([]);
}

function setupViewModel(_self, _data) {
    var self = _self;
    _self.handOperatedDisabled = ko.observable(true);
    _self.handOperatedBy = ko.observable();
    _self.handOperated = ko.observable(false);
    _self.handOperated.extend({notify: 'always'});
    _self.handOperated.subscribe(function (value) {
        if (self.isFillingForm)
            return;

        if (value) {
            self.handOperatedBy(self._userLoggedCard);
        }
        else {
            self.handOperatedBy(self._userLoggedCard);
        }
    });
    _self.actionResults = ko.observableArray([]);
    _self.actionId = "";
    _self._ip = _data._ip;
    _self._userLoggedCard = _data._userLoggedCard;
    _self._url = _data._url;

    _self.updateAction = function () {
        var $data = {};
        $data["actionId"] = self.actionId;
        $data["handOperatedBy"] = self.handOperated();
        console.log("Preparing to send...");
        console.dir(self);
        updateHandOperated(self._url, self._userLoggedCard, self._ip, $data);
    }

    _self.fillActionResults = function (data) {
        self.isFillingForm = true;
        self.actionId = data.actionId;
        fillActionResults(data._url, data._userLoggedCard, data._ip, data.actionId, self);
        self.isFillingForm = false;
    }

    _self.onClose = function () {
        self.isFillingForm = true;
        self.actionResults.splice(0, self.actionResults().length);
        for (task in self.actionResults()) {
            self.actionResults.remove(task);
        }
        self.isFillingForm = false;
    }
    return _self;
}

function getPSTNFormData(_self, flag) {
    var $data = {};
    $data['TELEFONO'] = _self.telefono();
    $data['CUENTA_FIJA'] = _self.cuentaFija() ? "SI" : "NO";

    if (!_self.cuentaFija()) {
        $data['CABINA'] = toUpperCase(_self.cabina()).trim();
        $data['PAR_LOCAL'] = toUpperCase(getParLocal(_self)).trim();
    }

    $data['LOCALIDAD'] = toUpperCase(getJsonPathFrameName(_self.localidad())).trim();
    //$data['PAR_FEEDER'] = toUpperCase(self.parFeeder());
    $data['PAR_FEEDER'] = toUpperCase(getParFeeder(_self)).trim();
    $data['TERMINAL'] = toUpperCase(_self.terminal()).trim();
    //$data['PAR_LOCAL'] = toUpperCase(self.parLocal());


    $data['DIRECCION_CORRECTA'] = _self.direccion();
    $data['CODE'] = _self.orderCode;
    $data['TYPE'] = _self.orderType;
    return $data;
}


function getJsonPathCabinaPrefix(loc) {
    value = JSONPath({
        json: loc,
        path: "$..prefixes.cabina"
    });
    if (value && 0 < value.length)
        return value.shift();
    return undefined;
}

function getJsonPathFrameName(loc) {
    value = JSONPath({
        json: loc,
        path: "$.name"
    });
    if (value && 0 < value.length)
        return value.shift();
    return undefined;
}

function getJsonArrayPathFrameName(loc) {
    value = JSONPath({
        json: loc,
        path: "$.[*].name"
    });
    if (value && 0 < value.length)
        return value;
    return undefined;
}

function getIndexFromFrames(Loc, value) {
    if (Loc == undefined || Loc == null || Loc.length == 0) {
        return 0;
    }

    var idx = -1;
    var array = getJsonArrayPathFrameName(Loc);
    if (array != undefined) {
        idx = array.indexOf(value);
    }
    if (idx >= 0) {
        return idx;
    } else {
        return 0;
    }
}

function getJsonPathFramePath(loc, _path) {
    value = JSONPath({
        json: loc,
        path: _path
    })
    if (value && 0 < value.length)
        return value.shift();
    return "";
}

function resetModel(_self, fieldsNames) {
    for (var i = 0; i < fieldsNames.length; i++) {
        var fieldName = fieldsNames[i];
        if (ko.isObservable(_self[fieldName])) {
            _self[fieldName]("");
        }
    }
    return _self;
}


function PSTNModel($_data) {


    console.log("Creating PSTNModel with data ");
    console.dir($_data);
    var self = this;
    var flag = "";
    self.orderCode = '';
    self.orderType = '';
    self.orderPhone = '';

    self.isFillingForm = false;

    self.telefono = ko.observable(self.orderPhone);
    self.phoneIsDisabled = ko.observable(false);
    self.cuentaFija = ko.observable(false);
    self.cabina = ko.observable();
    self.parFeeder = ko.observable();
    self.terminal = ko.observable();
    self.parLocal = ko.observable();


    self.localidad = ko.observable(); //Frame field  in form
    //self.localidades = ko.observable(getLocalidades());
    self.localidades = ko.observable(); //Frame list  in form
    self.direccion = ko.observable();

    // begin 2015-GESTI-4791-1

    self = setupPSTNModel(self, flag);

    if ($_data.isViewMode === true)
        self = setupViewModel(self, $_data);

    self.numParFeeder.subscribe(function (value) {
        if (value == "Otros") {
            self.otrosParFeeder("");
            //$("#otros_par_feeder" + flag).show();
            self.showOtrosParFeeder(true);

        } else {
            //$("#otros_par_feeder" + flag).hide();
            self.showOtrosParFeeder(false);
        }
    });


    self.numParLocal.subscribe(function (value) {
        if (value == "Otros") {
            self.otrosParLocal("");
            //$("#otros_par_local" + flag).show();
            self.showOtrosParLocal(true);

        } else {
            //$("#otros_par_local" + flag).hide();
            self.showOtrosParLocal(false);

        }
    });

    $("#terminal" + flag).blur(function () {
        if (!validateTerminal(self))
            return;

        var _range = getRange(getTerminalSecondSession(self.terminal()));
        self.rangeLocal(_range);
        self.rangeFeeder(_range);


    });

    self.cuentaFija.extend({notify: 'always'});
    self.cuentaFija.subscribe(function (value) {
        PSTNHideFields(self, value, flag);
        hideSpinner(self, value, flag);

    });

    self.cabina.extend({notify: 'always'});
    self.cabina.subscribe(function () {
        if (self.isFillingForm)
            return;
        self.terminal(getCabina(self) + "-");
    });

    self.localidad.extend({notify: 'always'});
    self.localidad.subscribe(function (value) {
        if (self.isFillingForm)
            return;
        try {
            if (!self.cuentaFija()) {
                self.cabina(value ? getJsonPathCabinaPrefix(value) : '');
            }


        } catch (e) {
            console.log(e)
        }
    });

    self.terminal.extend({notify: 'always'});
    self.terminal.subscribe(function () {
        if (self.isFillingForm)
            return;
        self.parLocal(getTerminalFistSession(self.terminal()) + '-');

        if (self.cuentaFija()) {
            self.parFeeder(getTerminalFistSession(self.terminal()) + '-');
        }

    });

    self.saveForm = function () {

        if (!validatePSTNForm(self, flag))
            return false;

        var $data = getPSTNFormData(self, flag);
        $data['FORM_TYPE'] = 'PSTN';
        $data['TARJETA'] = $_data.tarjeta;


        console.log("Preparing to send...");
        console.dir(self);

        generalSendForm($_data._userLoggedCard, $_data._ip, $_data.url, $_data.tableRefreshUrl, $data);
    };

    self.fillContent = function (data) {

        self.isFillingForm = true;


        console.log('updating data');
        console.log(data);

        self = fillPSTNForm(self, data, flag);
        self.isFillingForm = false;
    }

    self.setParams = function (xdata) {
        console.log('Setting params on PSTN');
        console.dir(xdata);
        self = PSTNHideFields(self, false, flag);
        self = hideSpinner(self, false, flag);
        self.telefono("");
        self.orderCode = xdata.orderCode;
        self.orderType = xdata.orderType;
        self.orderPhone = parsePhone(xdata.orderPhone);
        self = setPhone(self, flag, self.orderPhone);
        //self.telefono(self.orderPhone);
    };

    self.setParams($_data);

    // End 2015-GESTI-4791-1


    // BEGIN 2015-GESTI-3083-2
    self.enableRefresh = true;
    self.refreshClick = function () {

        self.enableRefresh = false;
        console.log('Preparing to refresh data');
        /*self.rangeLocal(["Seleccionar"]);
        self.rangeFeeder(["Seleccionar"]);
        self.numParLocal("Seleccionar");
        self.numParFeeder("Seleccionar");
        self.direccion(true);*/
        $.growl({title: 'Buscando ultimo formulario posteado', message: ''});

        $.ajax({
            url: $_data.lastFormUrl,
            async: true,
            type: 'POST',
            data: {
                code: self.orderCode,
                orderType: self.orderType,
                userLoggedCard: _userLoggedCard,
                ip: $_data._ip,
                card: $_data._userLoggedCard
            },
            beforeSend: function () {
                //$($this).attr('disabled', 'disabled');
            },
            success: function (data) {
                console.log('incoming data ' + data);
                console.dir(data);
                //$($this).attr('disabled', false);
                if (!$.isEmptyObject(data)) {
                    console.log('Filling form');
                    self.isFillingForm = true;
                    console.log("Reseting Form Values");
                    self = resetModel(self, ["telefono", "cuentaFija", "localidad", "cabina"
                        , "terminal", "parLocal", "numParLocal", "otrosParLocal"
                        , "parFeeder", "numParFeeder", "otrosParFeeder", "direccion"]);
                    self = loadLastAction(self, data, flag);
                    self.isFillingForm = false;
                } else {
                    $.growl({title: getAppMessage("not_posted_form"), message: ''});

                }
                self.enableRefresh = true;

            }
            /*,
            complete: function (data) {
                //console.log('data on complete' + data);
                //console.dir(data);
            }*/
        });
    }

    // END 2015-GESTI-3083-2


}


function FIBRAModel($_data) {
    console.log("Crearing FIBRAModel with data ");
    console.dir($_data);
    var self = this;
    var flag = "fibra";

    self.orderCode = '';
    self.orderType = '';
    self.orderPhone = '';

    self.isFillingForm = false;


    self.telefono = ko.observable(self.orderPhone);
    self.phoneIsDisabled = ko.observable(false);
    self.localidad = ko.observable();
    self.localidades = ko.observable();
    self.splitterPort = ko.observable();
    self.terminalFo = ko.observable("FC");
    self.direccion = ko.observable(true);

    if ($_data.isViewMode === true)
        self = setupViewModel(self, $_data);

    attachFrames(self.localidades);


    var terminaLFoOld = '';

    self.terminalFo.extend({notify: 'always'})
    self.terminalFo.subscribe(function (old) {
        if (self.isFillingForm)
            return;
        terminaLFoOld = old;
    }, this, 'beforeChange');


    self.terminalFo.extend({notify: 'always'})
    self.terminalFo.subscribe(function (value) {
        if (self.isFillingForm)
            return;

        console.log('Reporting Change to terminalFo ' + terminaLFoOld + " - " + value);
        if (terminaLFoOld == value)
            return;
        self.splitterPort('SPT-' + value + '-SP');
    });


    self.setParams = function (xdata) {
        self.telefono("");
        self.orderCode = xdata.orderCode;
        self.orderType = xdata.orderType;
        self.orderPhone = parsePhone(xdata.orderPhone);
        self = setPhone(self, flag, self.orderPhone);
        //self.telefono(self.orderPhone);
    };


    /**
     * setup section
     */
    self.setParams($_data);


    self.saveForm = function () {
        var form = $("#form-fibra");

        if (form.hasClass("validate")) {
            var valid = form.valid();
            if (!valid) {
                form.data("validator").focusInvalid();
                return false;
            }
        }

        var REGEX_PHONE = /8(0|2|4)9-\d{3}-\d{4}/;

        var validated = true;

        var input = (null != self.telefono() && undefined != self.telefono()) ? formatPhoneNumber(self.telefono()) : '';

        if (null != input && undefined != input)
            input = input.replace('(', '').replace(')', '').replace(' ', '-');

        if (!( REGEX_PHONE.test(input) )) {
            $.growl({title: 'Telefono no valido', message: ''});
            //return false;
            validated = false;
        }

        // FRAME
        if (!self.localidad()) {
            $.growl({title: 'FRAME no valido', message: ''});
            return false;
        }

        /*input = (null != self.terminalFo() && undefined != self.terminalFo()) ? self.terminalFo().trim().toUpperCase() : '';

        if (!( REGEX_TERMINAL_FO.test(input) )) {
            $.growl({title: 'Terminal Fo no valido', message: ''});
            //return false;
            validated = false;
        }*/
        if (!validateTerminalFo(self)) {
            return false;
        }

        /*input = (null != self.terminalFo() && undefined != self.terminalFo()) ? self.terminalFo().trim().toUpperCase() : '';
        var splitterPortRegex = new RegExp("SPT-" + input + "-SP0[1-8]");

        input = (null != self.splitterPort() && undefined != self.splitterPort()) ? self.splitterPort().trim().toUpperCase() : '';
        if (!(splitterPortRegex.test(input) )) {
            $.growl({title: 'Splitter Port no valido', message: ''});
            //return false;
            validated = false;
        }*/

        if (!validateSplitterPort(self)) {
            return false;
        }

        //console( self.direccion() );

        if (!validated)
            return false;

        var $data = {};
        $data['TELEFONO'] = self.telefono();
        $data['LOCALIDAD'] = toUpperCase(getJsonPathFrameName(self.localidad())).trim();
        $data['DIRECCION_CORRECTA'] = self.direccion() ? "SI" : "NO";
        $data['SPLITTER_PORT'] = toUpperCase(self.splitterPort()).trim();
        $data['SPLITTER'] = "SPT-" + toUpperCase(self.terminalFo()).trim();
        $data['TERMINAL_FO'] = toUpperCase(self.terminalFo()).trim();
        $data['CODE'] = self.orderCode;
        $data['TYPE'] = self.orderType;
        $data['FORM_TYPE'] = 'FIBRA';

        cabinaFO = '';
        if (self.terminalFo() != '' && undefined != self.terminalFo())
            cabinaFO = self.terminalFo().substring(0, Math.abs(self.terminalFo().indexOf('-')))

        $data['CABINA_FO'] = cabinaFO;
        $data['TARJETA'] = $_data.tarjeta;


        console.log("Preparing to send...");
        console.dir(self);

        generalSendForm($_data._userLoggedCard, $_data._ip, $_data.url, $_data.tableRefreshUrl, $data);

    };

    self.fillContent = function (data) {
        self.isFillingForm = true;

        console.log('updating data');
        console.log(data);

        self.telefono(data['TELEFONO']);
        self.localidad(data['LOCALIDAD']);
        self.terminalFo(data['TERMINAL_FO']);
        self.splitterPort(data['SPLITTER_PORT']);
        if (data['DIRECCION_CORRECTA'] != undefined && data['DIRECCION_CORRECTA'] != null) {
            self.direccion(data['DIRECCION_CORRECTA']);
        }

        self.isFillingForm = false;
    }

    // BEGIN 2015-GESTI-3083-2
    self.enableRefresh = true;
    self.refreshClick = function () {

        console.log('Preparing to refresh data');
        self.enableRefresh = false;

        $.ajax({
            url: $_data.lastFormUrl,
            async: true,
            type: 'POST',
            data: {
                code: self.orderCode,
                orderType: self.orderType,
                userLoggedCard: _userLoggedCard,
                ip: $_data._ip,
                card: $_data._userLoggedCard
            },
            beforeSend: function () {
                //$($this).attr('disabled', 'disabled');
            },
            success: function (data) {
                console.log('incoming data ' + data);
                console.dir(data);
                //$($this).attr('disabled', false);
                if (!$.isEmptyObject(data)) {
                    console.log('Filling form');
                    self.isFillingForm = true;

                    self = resetModel(self, ["telefono", "terminalFo", "localidad", "splitterPort"
                        , "direccion"]);

                    self.telefono(data['TELEFONO']);
                    self.localidad(self.localidades()[getIndexFromFrames(self.localidades(), data['LOCALIDAD'])]);
                    self.terminalFo(data['TERMINAL_FO']);
                    self.splitterPort(data['SPLITTER_PORT']);
                    if (data['DIRECCION_CORRECTA'] != undefined && data['DIRECCION_CORRECTA'] != null) {
                        self.direccion(data['DIRECCION_CORRECTA']);
                    }

                    self.isFillingForm = false;
                } else {
                    $.growl({title: getAppMessage("not_posted_form"), message: ''});


                }

                self.enableRefresh = true;
            }
            /*,
            complete: function (data) {
                //console.log('data on complete' + data);
                //console.dir(data);
            }*/
        });

    }

    // END 2015-GESTI-3083-2
}


function PSTN_FIBRAModel($_data) {
    console.log("Crearing PSTN_FIBRAModel with data ");
    console.dir($_data);
    var self = this;
    var flag = "_fibra";

    self.orderCode = '';
    self.orderType = '';
    self.orderPhone = '';
    self.isFillingForm = false;

    self.telefono = ko.observable(self.orderPhone);
    self.phoneIsDisabled = ko.observable(false);
    self.cuentaFija = ko.observable(false);
    self.cabina = ko.observable();
    self.parFeeder = ko.observable();
    self.terminal = ko.observable();
    self.parLocal = ko.observable();
    self.splitterPort = ko.observable();
    self.terminalFo = ko.observable("FC");
    self.localidad = ko.observable(); //Frame field  in form
    //self.localidades = ko.observable(getLocalidades());
    self.localidades = ko.observable(); //Frame list  in form
    self.direccion = ko.observable(true);

    var terminaLFoOld = '';

    self.terminalFo.extend({notify: 'always'})
    self.terminalFo.subscribe(function (old) {
        if (self.isFillingForm)
            return;
        terminaLFoOld = old;
    }, this, 'beforeChange');


    self.terminalFo.extend({notify: 'always'})
    self.terminalFo.subscribe(function (value) {
        if (self.isFillingForm)
            return;

        console.log('Reporting Change to terminalFo ' + terminaLFoOld + " - " + value);
        if (terminaLFoOld == value)
            return;
        self.splitterPort('SPT-' + value + '-SP');
    });

    // PSTN_FIBRA MODEL
    // begin 2015-GESTI-4791-1

    self = setupPSTNModel(self, flag);
    if ($_data.isViewMode === true)
        self = setupViewModel(self, $_data);

    self.numParFeeder.subscribe(function (value) {
        if (value == "Otros") {
            self.otrosParFeeder("");

            self.showOtrosParFeeder(true);
        } else {
            self.showOtrosParFeeder(false);
        }
    });

    self.numParLocal.subscribe(function (value) {
        if (value == "Otros") {
            self.otrosParFeeder("");
            self.showOtrosParLocal(true);
        } else {
            self.showOtrosParLocal(false);
        }
    });

    $("#terminal" + flag).blur(function () {
        if (!validateTerminal(self))
            return;

        var _range = getRange(getTerminalSecondSession(self.terminal()));
        self.rangeLocal(_range);
        self.rangeFeeder(_range);


    });

    self.cuentaFija.extend({notify: 'always'});
    self.cuentaFija.subscribe(function (value) {
        PSTNHideFields(self, value, flag);
    });

    self.cabina.extend({notify: 'always'});
    self.cabina.subscribe(function () {
        if (self.isFillingForm)
            return;
        self.terminal(getCabina(self) + "-");
    });


    self.localidad.extend({notify: 'always'});
    self.localidad.subscribe(function (value) {
        if (self.isFillingForm)
            return;
        console.log(value);
        if (!self.cuentaFija()) {
            self.cabina(value ? getJsonPathCabinaPrefix(value) : '');
        }

        var frame = self.localidad();

        if (!frame)
            return;

        console.log('setting terminalFoPrefix with frame..');
        console.log(frame);

        var terminalFoPrefix = getJsonPathFramePath(frame, '$..prefixes.terminal_fo');
        if (!terminalFoPrefix) {
            terminalFoPrefix = "";
        }
        self.terminalFo("FC" + terminalFoPrefix);
    });


    self.terminal.extend({notify: 'always'});
    self.terminal.subscribe(function () {
        if (self.isFillingForm)
            return;
        self.parLocal(getTerminalFistSession(self.terminal()) + '-');

        if (self.cuentaFija()) {
            self.parFeeder(getTerminalFistSession(self.terminal()) + '-');
        }
    });

    self.saveForm = function () {

        if (!validatePSTNForm(self, flag))
            return false;


        var validated = true;

        /*var input = (null != self.terminalFo() && undefined != self.terminalFo()) ? self.terminalFo().trim().toUpperCase() : '';
        if (!( REGEX_TERMINAL_FO.test(input) )) {
            $.growl({title: 'Terminal Fo no valido', message: ''});
            //return false;
            validated = false;
        }*/

        if (!validateTerminalFo(self)) {
            return false;
        }


        if (!validateSplitterPort(self)) {
            return false;
        }


        if (!validated)
            return false;

        var $data = getPSTNFormData(self, flag);

        $data['SPLITTER_PORT'] = toUpperCase(self.splitterPort()).trim();
        $data['SPLITTER'] = "SPT-" + toUpperCase(self.terminalFo()).trim();
        $data['TERMINAL_FO'] = toUpperCase(self.terminalFo()).trim();

        $data['FORM_TYPE'] = 'PSTN+FIBRA';
        $data['TARJETA'] = $_data.tarjeta;

        cabinaFO = '';
        if (self.terminalFo() != '' && undefined != self.terminalFo())
            cabinaFO = self.terminalFo().substring(0, Math.abs(self.terminalFo().indexOf('-'))).trim();

        $data['CABINA_FO'] = cabinaFO;

        console.log("Preparing to send...");
        console.dir(self);

        generalSendForm($_data._userLoggedCard, $_data._ip, $_data.url, $_data.tableRefreshUrl, $data);
    };

    self.fillContent = function (data) {

        self.isFillingForm = true;

        console.log('updating data');
        console.log(data);
        self = fillPSTNForm(self, data, flag);
        self.splitterPort(data['SPLITTER_PORT']);
        self.terminalFo(data['TERMINAL_FO']);
        self.isFillingForm = false;
    }

    self.setParams = function (xdata) {
        console.log('Setting params on PSTN');
        console.dir(xdata);
        PSTNHideFields(self, false, flag);
        hideSpinner(self, false, flag);

        self.telefono("");
        self.orderCode = xdata.orderCode;
        self.orderType = xdata.orderType;
        self.orderPhone = parsePhone(xdata.orderPhone);
        self = setPhone(self, flag, self.orderPhone);
        //self.telefono(self.orderPhone);
    };

    self.setParams($_data);

    // End 2015-GESTI-4791-1

    // BEGIN 2015-GESTI-3083-2
    self.enableRefresh = true;
    self.refreshClick = function () {

        console.log('Preparing to refresh data');
        self.enableRefresh = false;
        $.ajax({
            url: $_data.lastFormUrl,
            async: true,
            type: 'POST',
            data: {
                code: self.orderCode,
                orderType: self.orderType,
                userLoggedCard: _userLoggedCard,
                ip: $_data._ip,
                card: $_data._userLoggedCard
            },
            beforeSend: function () {
                //$($this).attr('disabled', 'disabled');
            },
            success: function (data) {
                console.log('incoming data ' + data);
                console.dir(data);
                //$($this).attr('disabled', false);
                if (!$.isEmptyObject(data)) {
                    console.log('Filling form');
                    self.isFillingForm = true;
                    self = resetModel(self, ["telefono", "cuentaFija", "localidad", "cabina"
                        , "terminal", "parLocal", "numParLocal", "otrosParLocal"
                        , "parFeeder", "numParFeeder", "otrosParFeeder", "terminalFo", "splitterPort", "direccion"]);
                    self = loadLastAction(self, data, flag);
                    self.terminalFo(data['TERMINAL_FO']);
                    self.splitterPort(data['SPLITTER_PORT']);
                    self.isFillingForm = false;
                } else {
                    $.growl({title: getAppMessage("not_posted_form"), message: ''});

                }
                self.enableRefresh = true;

            }
            /*,
            complete: function (data) {
                //console.log('data on complete' + data);
                //console.dir(data);
            }*/
        });


    }

    // END 2015-GESTI-3083-2

}

function PSTN_DATAModel($_data) {

    console.log("Creating PSTN_DATAModel with data ");
    console.dir($_data);
    var self = this;
    var flag = "_data";

    var puertoRegex = getPorts()[2][0];

    self.orderCode = '';
    self.orderType = '';
    self.orderPhone = '';
    self.isFillingForm = false;

    self.telefono = ko.observable(self.orderPhone);
    self.phoneIsDisabled = ko.observable(false);
    self.cuentaFija = ko.observable(false);
    self.cabina = ko.observable();
    self.parFeeder = ko.observable();
    self.terminal = ko.observable();
    self.parLocal = ko.observable();
    self.localidad = ko.observable(); //Frame field  in form
    //self.localidades = ko.observable(getLocalidades());
    self.localidades = ko.observable(); //Frame list  in form
    self.direccion = ko.observable(true);
    self.puertos = ko.observable(getPorts()[0]);
    self.tipoPuerto = ko.observable();
    self.puerto = ko.observable();
    self.dslam = ko.observable();
    self.dslamType = ko.observable();

    self.tipoPuerto.extend({notify: 'always'});
    self.tipoPuerto.subscribe(function (value) {

        var ports = getPorts()[0];
        var index = ports.indexOf(value);
        puertoRegex = getPorts()[2][index];
        console.log('port.pstn_data regex selected ' + puertoRegex);
        if (self.isFillingForm)
            return;


        var portMask = getPorts()[1][index];
        console.log('preparing to set port: ' + portMask);
        self.puerto(portMask);

        console.log(self.puerto);
        console.dir(self.puerto);

        if (self.setDslamPrefix)
            self.setDslamPrefix();
    });

    // begin 2015-GESTI-4791-1

    self = setupPSTNModel(self, flag);

    if ($_data.isViewMode === true)
        self = setupViewModel(self, $_data);

    self.numParFeeder.subscribe(function (value) {
        if (value == "Otros") {
            self.otrosParFeeder("");
            self.showOtrosParFeeder(true);
        } else {
            self.showOtrosParFeeder(false);
        }
    });

    self.numParLocal.subscribe(function (value) {
        if (value == "Otros") {
            self.otrosParFeeder("");
            self.showOtrosParLocal(true);
        } else {
            self.showOtrosParLocal(false);
        }
    });

    $("#terminal" + flag).blur(function () {
        if (!validateTerminal(self))
            return;

        var _range = getRange(getTerminalSecondSession(self.terminal()));
        self.rangeLocal(_range);
        self.rangeFeeder(_range);


    });

    self.cuentaFija.extend({notify: 'always'});
    self.cuentaFija.subscribe(function (value) {
        PSTNHideFields(self, value, flag);

        if (self.isFillingForm)
            return;

        if ('true' == value || true == value)
            self.dslamType('indoor');

        if (self.setDslamPrefix)
            self.setDslamPrefix();
    });

    self.cabina.extend({notify: 'always'});
    self.cabina.subscribe(function () {
        if (self.isFillingForm)
            return;
        self.terminal(getCabina(self) + "-");

        if (self.setDslamPrefix)
            self.setDslamPrefix();
    });


    self.localidad.extend({notify: 'always'});
    self.localidad.subscribe(function (value) {
        if (self.isFillingForm)
            return;
        console.log(value);
        if (!self.cuentaFija()) {
            self.cabina(value ? getJsonPathCabinaPrefix(value) : '');
        }
    });


    self.terminal.extend({notify: 'always'});
    self.terminal.subscribe(function () {
        if (self.isFillingForm)
            return;
        self.parLocal(getTerminalFistSession(self.terminal()) + '-');

        if (self.cuentaFija()) {
            self.parFeeder(getTerminalFistSession(self.terminal()) + '-');
        }

    });

    self.dslamType.extend({notify: 'always'});
    self.dslamType.subscribe(function (value) {
        if (self.isFillingForm)
            return;

        console.log('changing dslam type');
        if (self.setDslamPrefix)
            self.setDslamPrefix();
    });

    self.setDslamPrefix = function () {
        if (self.dslamType()) {
            var type = self.dslamType();

            var portType = self.tipoPuerto().toLowerCase();

            console.log('setting dslam ' + type + ' porttype => ' + portType);

            if ('indoor' == type) {
                var frame = self.localidad();

                if (!frame)
                    return;

                console.log('setting dslam with frame..');
                console.log(frame);

                if (portType.indexOf('zyxel') != -1)
                    portType = 'zyxel';


                portPrefix = getJsonPathFramePath(frame, '$..prefixes.ports.' + portType);
                self.dslam(portPrefix);

            } else if ('outdoor' == type) {
                try {
                    if (portType.indexOf('alcatel') != -1)
                        if (self.cabina() != undefined && self.cabina() != null) {
                            self.dslam("A" + self.cabina());
                        } else {
                            self.dslam("A");
                        }
                    else if (portType.indexOf("zte") != -1)
                        if (self.cabina() != undefined && self.cabina() != null) {
                            self.dslam("T" + self.cabina());
                        } else {
                            self.dslam("T");
                        }
                } catch (e) {
                    console.log(e)
                }
            }
        }
    }

    self.saveForm = function () {

        console.log('Saving form');
        if (!validatePSTNForm(self, flag))
            return false;

        var validated = true;

        // VALIDANDO TIPO DE PUERTO
        if (self.tipoPuerto() == getPorts()[0][0]) {
            $.growl({title: 'Debe seleccionar un tipo de puerto', message: ''});
            return false;
        }

        var input = (null != self.puerto() && undefined != self.puerto()) ? self.puerto().trim().toUpperCase() : '';
        console.log("Preparing to validate puerto: " + input + " isValidSuggestedPort ? " + isValidSuggestedPort(input));

        if (!(puertoRegex.test('' + input)) || !isValidSuggestedPort(input)) {
            $.growl({title: 'Valor de puerto no valido para el tipo ' + self.tipoPuerto(), message: ''});
            //return false;
            validated = false;
        }

        if (!validateDslam(self.dslam())) {
            return false;
        }

        if (!validated)
            return false;

        var $data = getPSTNFormData(self, flag);
        $data['FORM_TYPE'] = 'PSTN+DATA';
        $data['TARJETA'] = $_data.tarjeta;

        $data['TIPO_PUERTO'] = toUpperCase(self.tipoPuerto()).trim();
        $data['PUERTO'] = toUpperCase(self.puerto()).trim();
        $data['DSLAM'] = toUpperCase(self.dslam()).trim();
        $data['DSLAM_TYPE'] = self.dslamType();


        console.log("Preparing to send...");
        console.dir(self);

        generalSendForm($_data._userLoggedCard, $_data._ip, $_data.url, $_data.tableRefreshUrl, $data);
    };

    self.fillContent = function (data) {

        self.isFillingForm = true;
        console.log('updating data');
        console.log(data);

        self = fillPSTNForm(self, data, flag);

        self.tipoPuerto(data['TIPO_PUERTO']);
        self.puerto(data['PUERTO']);
        self.dslam(data['DSLAM']);
        self.dslamType(data['DSLAM_TYPE']);
        self.isFillingForm = false;
    }

    self.setParams = function (xdata) {
        console.log('Setting params on PSTN DATA');
        console.dir(xdata);
        PSTNHideFields(self, false, flag);
        hideSpinner(self, false, flag);
        self.telefono("");
        self.orderCode = xdata.orderCode;
        self.orderType = xdata.orderType;
        self.orderPhone = parsePhone(xdata.orderPhone);
        //self.telefono(self.orderPhone);
        self = setPhone(self, flag, self.orderPhone);
    };

    self.setParams($_data);

    // End 2015-GESTI-4791-1

    // BEGIN 2015-GESTI-3083-2

    self.enableRefresh = true;
    self.refreshClick = function () {

        console.log('Preparing to refresh data');
        self.enableRefresh = false;
        $.ajax({
            url: $_data.lastFormUrl,
            async: true,
            type: 'POST',
            data: {
                code: self.orderCode,
                orderType: self.orderType,
                userLoggedCard: _userLoggedCard,
                ip: $_data._ip,
                card: $_data._userLoggedCard
            },
            beforeSend: function () {
                //$($this).attr('disabled', 'disabled');
            },
            success: function (data) {
                console.log('incoming data ' + data);
                console.dir(data);
                //$($this).attr('disabled', false);
                if (!$.isEmptyObject(data)) {
                    console.log('Filling form');
                    self.isFillingForm = true;
                    self = resetModel(self, ["telefono", "cuentaFija", "localidad", "cabina"
                        , "terminal", "parLocal", "numParLocal", "otrosParLocal"
                        , "parFeeder", "numParFeeder", "otrosParFeeder", "puerto", "dslam", "dslamType", "direccion"]);
                    self = loadLastAction(self, data, flag);
                    self.tipoPuerto(data['TIPO_PUERTO']);
                    self.puerto(data['PUERTO']);
                    self.dslam(data['DSLAM']);
                    self.dslamType(data['DSLAM_TYPE']);
                    self.isFillingForm = false;
                } else {
                    $.growl({title: getAppMessage("not_posted_form"), message: ''});

                }
                self.enableRefresh = true;


            }
            /*,
            complete: function (data) {
                //console.log('data on complete' + data);
                //console.dir(data);
            }*/
        });

    }

    // END 2015-GESTI-3083-2

}

function FRAMEModel($_data) {

    console.log("Crearing FRAMEModel with data ");
    console.dir($_data);
    var self = this;
    var flag = "frame";

    var puertoRegex = getPorts()[2][0];

    self.orderCode = '';
    self.orderType = '';
    self.orderPhone = '';
    self.isFillingForm = false;

    self.telefono = ko.observable(self.orderPhone);
    self.phoneIsDisabled = ko.observable(false);
    self.localidad = ko.observable();
    self.localidades = ko.observable();
    attachFrames(self.localidades);

    self.direccion = ko.observable();

    self.servioDato = ko.observable(true);
    self.isServicioDatos = ko.observable(true);

    self.parFeeder = ko.observable();

    self.cabina = ko.observable();
    self.cabina("");
    self.puertos = ko.observable(getPorts()[0]);
    self.dslamType = ko.observable();
    self.tipoPuerto = ko.observable();
    self.dslam = ko.observable();
    self.puerto = ko.observable();

    if ($_data.isViewMode === true)
        self = setupViewModel(self, $_data);

    self.tipoPuerto.extend({notify: 'always'});
    self.tipoPuerto.subscribe(function (value) {
        var ports = getPorts()[0];
        var index = ports.indexOf(value);
        puertoRegex = getPorts()[2][index];
        console.log('port.frame regex selected ' + puertoRegex);
        if (self.isFillingForm)
            return;

        var portMask = getPorts()[1][index];
        console.log('preparing to set port: ' + portMask);
        self.puerto(portMask);

        console.log(self.puerto);
        console.dir(self.puerto);

    });

    self.dslamType.extend({notify: 'always'});
    self.dslamType.subscribe(function (value) {
        if (self.isFillingForm)
            return;

        console.log('changing dslam type');
        if (self.setDslamPrefix)
            self.setDslamPrefix();
    });

    self.setDslamPrefix = function () {
        if (self.dslamType()) {
            var type = self.dslamType();

            var portType = self.tipoPuerto().toLowerCase();

            console.log('setting dslam ' + type + ' porttype => ' + portType);

            if ('indoor' == type) {
                var frame = self.localidad();

                if (!frame)
                    return;

                console.log('setting dslam with frame..');
                console.log(frame);

                if (portType.indexOf('zyxel') != -1)
                    portType = 'zyxel';


                portPrefix = getJsonPathFramePath(frame, '$..prefixes.ports.' + portType);
                self.dslam(portPrefix);

            } else if ('outdoor' == type) {
                try {
                    if (portType.indexOf('alcatel') != -1)
                        if (self.cabina() != undefined && self.cabina() != null) {
                            self.dslam("A" + self.cabina());
                        } else {
                            self.dslam("A");
                        }
                    else if (portType.indexOf("zte") != -1)
                        if (self.cabina() != undefined && self.cabina() != null) {
                            self.dslam("T" + self.cabina());
                        } else {
                            self.dslam("T");
                        }
                } catch (e) {
                    console.log(e)
                }
            }
        }
    }

    self.servioDato.extend({notify: 'always'});
    self.servioDato.subscribe(function (value) {
        self.isServicioDatos(value);
    });


    self.setParams = function (xdata) {
        self.telefono("");
        self.orderCode = xdata.orderCode;
        self.orderType = xdata.orderType;
        self.orderPhone = parsePhone(xdata.orderPhone);
        //self.telefono(self.orderPhone);
        self = setPhone(self, flag, self.orderPhone);
    };


    /**
     * setup section
     */
    self.setParams($_data);

    self.saveForm = function () {

        console.log('Saving form');
        var form = $("#form-frame");

        if (form.hasClass("validate")) {
            var valid = form.valid();
            if (!valid) {
                form.data("validator").focusInvalid();
                return false;
            }
        }

        if (!self.localidad()) {
            $.growl({title: 'FRAME no valido', message: ''});
            return false;
        }


        // VALIDANDO PAR FEEDER
        if (!validateParFeeder(self, true)) {
            return false;
        }

        if (self.servioDato()) {

            // VALIDANDO TIPO DE PUERTO
            if (self.tipoPuerto() == getPorts()[0][0]) {
                validated = false;
                if (!validated)
                    $.growl({title: 'Debe seleccionar un tipo de puerto', message: ''});
                return false;
            }


            var input = (null != self.puerto() && undefined != self.puerto()) ? self.puerto().trim().toUpperCase() : '';
            console.log("Preparing to validate puerto: " + input + " isValidSuggestedPort ? " + isValidSuggestedPort(input));

            if (!(puertoRegex.test('' + input)) || !isValidSuggestedPort(input)) {
                $.growl({title: 'Valor de puerto no valido para el tipo ' + self.tipoPuerto(), message: ''});
                //return false;
                validated = false;
                return false;
            }

            // VALIDANDO DSLAM
            if (!validateDslam(self.dslam())) {
                return false;
            }

            // IMPLEMENTACION ANTERIOR DE DSLAM
            /*var techs = ["alcatel", "cisco", "stinger", "zyxel", "zte"];
            var validDslam = '' != input && -1 == input.indexOf(' ') && 1 < input.length && -1 == $.inArray(input.toLowerCase(), techs);

            if (!validDslam) {
                $.growl({title: 'DSLAM no valido', message: ''});
                return false;
            }*/
        }


        var $data = {};
        $data['TELEFONO'] = self.telefono();
        $data['LOCALIDAD'] = toUpperCase(getJsonPathFrameName(self.localidad())).trim();
        $data['PAR_FEEDER'] = toUpperCase(self.parFeeder()).trim();
        $data['DIRECCION_CORRECTA'] = self.direccion() ? "SI" : "NO";
        $data['SERVICIO_DATOS'] = self.servioDato() ? "SI" : "NO";
        if (self.servioDato()) {
            $data['TIPO_PUERTO'] = toUpperCase(self.tipoPuerto()).trim();
            $data['PUERTO'] = toUpperCase(self.puerto()).trim();
            $data['DSLAM'] = toUpperCase(self.dslam()).trim();
            $data['DSLAM_TYPE'] = self.dslamType();
        }

        $data['CODE'] = self.orderCode;
        $data['TYPE'] = self.orderType;
        // 2016-GESTI-5441 - Modificar env�o de formulario de FRAME a M6 bajo ciertas condiciones.
        $data['FORM_TYPE'] = self.servioDato() ? 'FRAME+DATA' : 'FRAME';
        $data['TARJETA'] = $_data.tarjeta;

        console.log("Preparing to send...");
        console.dir(self);

        generalSendForm($_data._userLoggedCard, $_data._ip, $_data.url, $_data.tableRefreshUrl, $data);
    };


    self.fillContent = function (data) {
        self.isFillingForm = true;

        console.log('updating data');
        console.log(data);
        self.telefono(data['TELEFONO']);
        self.parFeeder(data['PAR_FEEDER']);
        self.localidad(data['LOCALIDAD']);
        self.servioDato(data['SERVICIO_DATOS'] == 'SI' ? true : false);
        if (data['SERVICIO_DATOS'] == 'SI') {
            self.isServicioDatos(true);
            self.tipoPuerto(data['TIPO_PUERTO']);
            self.puerto(data['PUERTO']);
            self.dslam(data['DSLAM']);
            self.dslamType(data['DSLAM_TYPE']);
        } else {
            self.isServicioDatos(false);
        }

        self.direccion(data['DIRECCION_CORRECTA'] == 'SI' ? true : false);

        self.isFillingForm = false;
    }

    // BEGIN 2015-GESTI-3083-2

    self.enableRefresh = true;
    self.refreshClick = function () {

        console.log('Preparing to refresh data');
        self.enableRefresh = false;
        $.ajax({
            url: $_data.lastFormUrl,
            async: true,
            type: 'POST',
            data: {
                code: self.orderCode,
                orderType: self.orderType,
                userLoggedCard: _userLoggedCard,
                ip: $_data._ip,
                card: $_data._userLoggedCard
            },
            beforeSend: function () {
                //$($this).attr('disabled', 'disabled');
            },
            success: function (data) {
                console.log('incoming data ' + data);
                console.dir(data);
                //$($this).attr('disabled', false);
                if (!$.isEmptyObject(data)) {
                    console.log('Filling form');
                    self.isFillingForm = true;
                    self = resetModel(self, ["telefono", "servioDato", "localidad"
                        , "parFeeder", "direccion", "puerto", "dslam", "dslamType"]);
                    self.telefono(data['TELEFONO']);
                    self.localidad(self.localidades()[getIndexFromFrames(self.localidades(), data['LOCALIDAD'])]);
                    self.parFeeder(data['PAR_FEEDER']);
                    self.servioDato(data['SERVICIO_DATOS'] == 'SI' ? true : false);

                    if (data['SERVICIO_DATOS'] == 'SI') {
                        self.tipoPuerto(data['TIPO_PUERTO']);
                        self.puerto(data['PUERTO']);
                        self.dslam(data['DSLAM']);
                        self.dslamType(data['DSLAM_TYPE']);

                    }
                    if (data['DIRECCION_CORRECTA'] != undefined && data['DIRECCION_CORRECTA'] != null) {
                        self.direccion(data['DIRECCION_CORRECTA']);
                    }

                    self.isFillingForm = false;
                } else {
                    $.growl({title: getAppMessage("not_posted_form"), message: ''});

                }
                self.enableRefresh = true;


            }
            /*,
            complete: function (data) {
                //console.log('data on complete' + data);
                //console.dir(data);
            }*/
        });

    }
    // END 2015-GESTI-3083-2

}