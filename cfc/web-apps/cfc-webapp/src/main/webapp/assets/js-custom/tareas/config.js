/**
 * Created by Nathaniel Calderon on 4/11/2017.
 */
function getComponentConfig() {
    var config = {
        isAbleToProcess: false
        , isAbleToExecute: false
        , isHandOperated: false
        , adjustActions: [{
            label: "Ajuste de Sistema"
            , value: "1"
        }]
        , systems: [
            {
                label: "DOMAIN"
                , name: "DOMAIN"
                , value: "21"
                , fields: [
                {
                    label: "TELEFONO"
                    , subquery: "@operator '@value'"
                    , key: "TELEFONO"
                    , operators: [
                    {"label": "=", "value": "="}
                ]
                },
                {
                    label: "LOCALIDAD"
                    , subquery: "@operator '@value'"
                    , key: "LOCALIDAD"
                    , operators: [
                    {"label": "=", "value": "="}
                ]
                },
                {
                    label: "TERMINAL"
                    , subquery: "@operator '@value'"
                    , key: "TERMINAL"
                    , operators: [
                    {"label": "=", "value": "="}
                ]
                },
                {
                    label: "PAR_LOCAL"
                    , subquery: "@operator '@value'"
                    , key: "PAR_LOCAL"
                    , operators: [
                    {"label": "=", "value": "="}
                    , {"label": "startWith", "value": "like"}
                ]
                },
                {
                    label: "PAR_FEEDER"
                    , subquery: "@operator '@value'"
                    , key: "PAR_FEEDER"
                    , operators: [
                    {"label": "=", "value": "="}
                    , {"label": "startWith", "value": "like"}
                ]
                }
            ]
            },
            {
                label: "M6"
                , name: "M6"
                , value: "24"
                , fields: [
                {
                    label: "TELEFONO"
                    , subquery: "@operator '@value'"
                    , key: "TELEFONO"
                    , operators: [
                    {"label": "=", "value": "="}
                ]
                },
                {
                    label: "PUERTO"
                    , subquery: "@operator '@value'"
                    , key: "PUERTO"
                    , operators: [
                    {"label": "=", "value": "="}

                ]
                },
                {
                    label: "DSLAM"
                    , subquery: "@operator '@value'"
                    , key: "DSLAM"
                    , operators: [
                    {"label": "=", "value": "="}

                ]
                }
            ]
            },
            {
                label: "GEFF"
                , name: "GEFF"
                , value: "22"
                , fields: [
                {
                    label: "TELEFONO"
                    , subquery: "@operator '@value'"
                    , key: "TELEFONO"
                    , operators: [
                    {"label": "=", "value": "="}
                ]
                },
                {
                    label: "CABINA"
                    , subquery: "@operator '@value'"
                    , key: "CABINA"
                    , operators: [
                    {"label": "=", "value": "="}

                ]
                },
                {
                    label: "TERMINAL"
                    , subquery: "@operator '@value'"
                    , key: "TERMINAL"
                    , operators: [
                    {"label": "=", "value": "="}

                ]
                },
                {
                    label: "SPLITTER"
                    , subquery: "@operator '@value'"
                    , key: "SPLITTER"
                    , operators: [
                    {"label": "=", "value": "="}
                    , {"label": "startWith", "value": "like"}
                ]
                },
                {
                    label: "SPLITTER_PORT"
                    , subquery: "@operator '@value'"
                    , key: "SPLITTER_PORT"
                    , operators: [
                    {"label": "=", "value": "="}
                    , {"label": "startWith", "value": "like"}
                ]
                }
            ]
            }
            /*,{
             label: "SAD"
             ,name: "SAD"
             ,value: "23"
             ,fields: []
             }*/
        ]
    }
    return config;
}

function configComponent(_self) {
    var self = _self;
    var configData = getComponentConfig();
    $('.datepicker').datepicker({
        format: "dd-M-yyyy",
        todayBtn: "linked",
        autoclose: true,
        maxViewMode: 2,
        todayHighlight: true,
        setDate: new Date()
    });
    self.whereClause = "";
    self.onSuccess = function (model, data) {

    }

    self.onError = function (model, data) {

    }

    self.isUpdating = false;

    self.isAbleToProcess = ko.observable(true);
    self.isAbleToExecute = ko.observable(true);

    self.systems = configData.systems;
    self.selectedSystem = ko.observable();

    self.beginDate = ko.observable("");
    self.endDate = ko.observable("");

    self.isHandOperated = ko.observable(false);
    self.includeSuccessForm = ko.observable(false);
    self.adjustActions = configData.adjustActions;
    self.selectedAdjustAction = ko.observable();

    self.fields = ko.observableArray([]);
    self.selectedField = ko.observable();

    self.operators = ko.observableArray([]);
    self.selectedOperator = ko.observable();

    self.value = ko.observable("");

    self.isAbleToAddRow = ko.observable(false);

    self.fieldFilters = ko.observableArray([]);
    self.indexSelected = ko.observable(null);

    self.isAllChecked = ko.observable(false);

    self.taskResults = ko.observableArray([]);
    self.forms = [];

    return self;


}