/**
 * Created by Nathaniel Calderon on 4/17/2017.
 */
function FRAMEModel($_data) {

    console.log("Crearing FRAMEModel with data ");
    console.dir($_data);
    var self = this;
    var flag = "frame";

    var puertoRegex = getPorts()[2][0];

    self.orderCode = '';
    self.orderType = '';
    self.orderPhone = '';
    self.isFillingForm = false;

    self.telefono = ko.observable(self.orderPhone);
    self.phoneIsDisabled = ko.observable(false);
    self.localidad = ko.observable();
    self.localidades = ko.observable();
    attachFrames(self.localidades);

    self.direccion = ko.observable();

    self.servioDato = ko.observable(true);
    self.isServicioDatos = ko.observable(true);

    self.parFeeder = ko.observable();

    self.cabina = ko.observable();
    self.cabina("");
    self.puertos = ko.observable(getPorts()[0]);
    self.dslamType = ko.observable();
    self.tipoPuerto = ko.observable();
    self.dslam = ko.observable();
    self.puerto = ko.observable();

    if ($_data.isViewMode === true)
        self = setupViewModel(self, $_data);

    self.tipoPuerto.extend({notify: 'always'});
    self.tipoPuerto.subscribe(function (value) {
        var ports = getPorts()[0];
        var index = ports.indexOf(value);
        puertoRegex = getPorts()[2][index];
        console.log('port.frame regex selected ' + puertoRegex);
        if (self.isFillingForm)
            return;

        var portMask = getPorts()[1][index];
        console.log('preparing to set port: ' + portMask);
        self.puerto(portMask);

        console.log(self.puerto);
        console.dir(self.puerto);

    });

    self.dslamType.extend({notify: 'always'});
    self.dslamType.subscribe(function (value) {
        if (self.isFillingForm)
            return;

        console.log('changing dslam type');
        if (self.setDslamPrefix)
            self.setDslamPrefix();
    });

    self.setDslamPrefix = function () {
        if (self.dslamType()) {
            var type = self.dslamType();

            var portType = self.tipoPuerto().toLowerCase();

            console.log('setting dslam ' + type + ' porttype => ' + portType);

            if ('indoor' == type) {
                var frame = self.localidad();

                if (!frame)
                    return;

                console.log('setting dslam with frame..');
                console.log(frame);

                if (portType.indexOf('zyxel') != -1)
                    portType = 'zyxel';


                portPrefix = getJsonPathFramePath(frame, '$..prefixes.ports.' + portType);
                self.dslam(portPrefix);

            } else if ('outdoor' == type) {
                try {
                    if (portType.indexOf('alcatel') != -1)
                        if (self.cabina() != undefined && self.cabina() != null){
                            self.dslam("A" + self.cabina());
                        } else {
                            self.dslam("A");
                        }
                    else if (portType.indexOf("zte") != -1)
                        if (self.cabina() != undefined && self.cabina() != null){
                            self.dslam("T" + self.cabina());
                        } else {
                            self.dslam("T");
                        }
                } catch (e) {
                    console.log(e)
                }
            }
        }
    }

    self.servioDato.extend({notify: 'always'});
    self.servioDato.subscribe(function (value) {
        self.isServicioDatos(value);
    });


    self.setParams = function (xdata) {
        self.telefono("");
        self.orderCode = xdata.orderCode;
        self.orderType = xdata.orderType;
        self.orderPhone = parsePhone(xdata.orderPhone);
        //self.telefono(self.orderPhone);
        self = setPhone(self, flag, self.orderPhone);
    };


    /**
     * setup section
     */
    self.setParams($_data);

    self.saveForm = function () {

        console.log('Saving form');
        var form = $("#form-frame");

        if (form.hasClass("validate")) {
            var valid = form.valid();
            if (!valid) {
                form.data("validator").focusInvalid();
                return false;
            }
        }

        if (!self.localidad()) {
            $.growl({title: 'FRAME no valido', message: ''});
            return false;
        }


        // VALIDANDO PAR FEEDER
        if (!validateParFeeder(self, true)){
            return false;
        }

        if (self.servioDato()) {

            // VALIDANDO TIPO DE PUERTO
            if (self.tipoPuerto() == getPorts()[0][0]) {
                validated = false;
                if (!validated)
                    $.growl({title: 'Debe seleccionar un tipo de puerto', message: ''});
                return false;
            }


            var input = (null != self.puerto() && undefined != self.puerto()) ? self.puerto().trim().toUpperCase() : '';
            console.log("Preparing to validate puerto: " + input + " isValidSuggestedPort ? " + isValidSuggestedPort(input));

            if (!(puertoRegex.test('' + input)) || !isValidSuggestedPort(input)) {
                $.growl({title: 'Valor de puerto no valido para el tipo ' + self.tipoPuerto(), message: ''});
                //return false;
                validated = false;
                return false;
            }

            // VALIDANDO DSLAM
            if (!validateDslam(self.dslam())){
                return false;
            }

            // IMPLEMENTACION ANTERIOR DE DSLAM
            /*var techs = ["alcatel", "cisco", "stinger", "zyxel", "zte"];
             var validDslam = '' != input && -1 == input.indexOf(' ') && 1 < input.length && -1 == $.inArray(input.toLowerCase(), techs);

             if (!validDslam) {
             $.growl({title: 'DSLAM no valido', message: ''});
             return false;
             }*/
        }


        var $data = {};
        $data['TELEFONO'] = self.telefono();
        $data['LOCALIDAD'] = toUpperCase(getJsonPathFrameName(self.localidad())).trim();
        $data['PAR_FEEDER'] = toUpperCase(self.parFeeder()).trim();
        $data['DIRECCION_CORRECTA'] = self.direccion() ? "SI" : "NO";
        $data['SERVICIO_DATOS'] = self.servioDato() ? "SI" : "NO";
        if (self.servioDato()) {
            $data['TIPO_PUERTO'] = toUpperCase(self.tipoPuerto()).trim();
            $data['PUERTO'] = toUpperCase(self.puerto()).trim();
            $data['DSLAM'] = toUpperCase(self.dslam()).trim();
            $data['DSLAM_TYPE'] = self.dslamType();
        }

        $data['CODE'] = self.orderCode;
        $data['TYPE'] = self.orderType;
        // 2016-GESTI-5441 - Modificar env�o de formulario de FRAME a M6 bajo ciertas condiciones.
        $data['FORM_TYPE'] = self.servioDato()? 'FRAME+DATA':'FRAME';
        $data['TARJETA'] = $_data.tarjeta;

        console.log("Preparing to send...");
        console.dir(self);

        generalSendForm($_data._userLoggedCard, $_data._ip, $_data.url, $_data.tableRefreshUrl, $data);
    };


    self.fillContent = function (data) {
        self.isFillingForm = true;

        console.log('updating data');
        console.log(data);
        self.telefono(data['TELEFONO']);
        self.parFeeder(data['PAR_FEEDER']);
        self.localidad(data['LOCALIDAD']);
        self.servioDato(data['SERVICIO_DATOS'] == 'SI' ? true : false);
        if (data['SERVICIO_DATOS'] == 'SI') {
            self.isServicioDatos(true);
            self.tipoPuerto(data['TIPO_PUERTO']);
            self.puerto(data['PUERTO']);
            self.dslam(data['DSLAM']);
            self.dslamType(data['DSLAM_TYPE']);
        } else {
            self.isServicioDatos(false);
        }

        self.direccion(data['DIRECCION_CORRECTA'] == 'SI' ? true : false);

        self.isFillingForm = false;
    }

    // BEGIN 2015-GESTI-3083-2

    self.enableRefresh = true;
    self.refreshClick =  function() {

        console.log('Preparing to refresh data');
        self.enableRefresh = false;
        $.ajax({
            url: $_data.lastFormUrl,
            async: true,
            type: 'POST',
            data: {
                code: self.orderCode,
                orderType: self.orderType,
                userLoggedCard: _userLoggedCard,
                ip: $_data._ip,
                card: $_data._userLoggedCard
            },
            beforeSend: function () {
                //$($this).attr('disabled', 'disabled');
            },
            success: function (data) {
                console.log('incoming data ' + data);
                console.dir(data);
                //$($this).attr('disabled', false);
                if (!$.isEmptyObject(data)) {
                    console.log('Filling form');
                    self.isFillingForm = true;
                    self = resetModel(self, ["telefono", "servioDato", "localidad"
                        , "parFeeder", "direccion", "puerto", "dslam", "dslamType"]);
                    self.telefono(data['TELEFONO']);
                    self.localidad(self.localidades()[getIndexFromFrames(self.localidades(), data['LOCALIDAD'])]);
                    self.parFeeder(data['PAR_FEEDER']);
                    self.servioDato(data['SERVICIO_DATOS'] == 'SI' ? true : false);

                    if (data['SERVICIO_DATOS'] == 'SI') {
                        self.tipoPuerto(data['TIPO_PUERTO']);
                        self.puerto(data['PUERTO']);
                        self.dslam(data['DSLAM']);
                        self.dslamType(data['DSLAM_TYPE']);

                    }
                    if (data['DIRECCION_CORRECTA'] != undefined && data['DIRECCION_CORRECTA'] != null){
                        self.direccion(data['DIRECCION_CORRECTA']);
                    }

                    self.isFillingForm = false;
                } else {
                    $.growl({title: getAppMessage("not_posted_form"), message: ''});

                }
                self.enableRefresh = true;


            }
            /*,
             complete: function (data) {
             //console.log('data on complete' + data);
             //console.dir(data);
             }*/
        });

    }
    // END 2015-GESTI-3083-2

}