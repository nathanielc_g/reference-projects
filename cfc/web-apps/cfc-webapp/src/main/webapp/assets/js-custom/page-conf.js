/**
 * Created by Christopher Herrera on 1/9/14.
 */

function setActiveMenuOption(page) {
    switch (page) {
        case 1:
            $('#menu-option-1').addClass("active").addClass("opened").addClass("active");
            $('#breadCrumb')
                .addClass('breadcrumb')
                .html(
                    $('<li />').addClass('active')
                        .html(
                            $('<strong />').html(
                                $('<i/>').addClass('entypo-home')
                            ).append('Home')
                        )
                );
            break;
        case 21:
            $('#menu-option-2').addClass("active").addClass("opened").addClass("active");
            $('#menu-option-2-1').addClass("active");
            $('#breadCrumb')
                .addClass('breadcrumb')
                .html(
                    $('<li />')
                        .html(
                            $('<a />').attr('href', '#').html(
                                $('<i/>').addClass('entypo-home')
                            ).append('Home')
                        )
                ).append(
                    $('<li />')
                        .html(
                            $('<a />').attr('href', '#').html(
                                $('<i/>').addClass('entypo-mobile')
                            ).append('Dispositivos')
                        )
                ).append(
                    $('<li />').addClass('Active')
                        .html(
                            $('<strong />').html(
                                $('<i/>').addClass('entypo-search')
                            ).append('Buscar')
                        )
                );
            break;
        case 121:
            $('#menu-option-2').addClass("active").addClass("opened").addClass("active");
            $('#menu-option-2-1').addClass("active");
            $('#breadCrumb')
                .addClass('breadcrumb')
                .html(
                $('<li />')
                    .html(
                    $('<a />').attr('href', '#').html(
                        $('<i/>').addClass('entypo-home')
                    ).append('Home')
                )
            ).append(
                $('<li />')
                    .html(
                    $('<a />').attr('href', '#').html(
                        $('<i/>').addClass('entypo-mobile')
                    ).append('Dispositivos')
                )
            ).append(
                $('<li />').addClass('Active')
                    .html(
                    $('<strong />').html(
                        $('<i/>').addClass('entypo-search')
                    ).append('Buscar')
                )
            ).append(
                $('<li />')
                    .html(
                    $('<a />').attr('href', '#').html(
                        $('<i/>').addClass('entypo-mobile')
                    ).append( '<span id="q-card"></span>' )

                )
            );
            break;
        case 122:
            $('#menu-option-2').addClass("active").addClass("opened").addClass("active");
            $('#menu-option-2-1').addClass("active");
            $('#breadCrumb')
                .addClass('breadcrumb')
                .html(
                $('<li />')
                    .html(
                    $('<a />').attr('href', '#').html(
                        $('<i/>').addClass('entypo-home')
                    ).append('Home')
                )
            ).append(
                $('<li />')
                    .html(
                    $('<a />').attr('href', '#').html(
                        $('<i/>').addClass('entypo-mobile')
                    ).append('Dispositivos')
                )
            ).append(
                $('<li />').addClass('Active')
                    .html(
                    $('<strong />').html(
                        $('<i/>').addClass('entypo-search')
                    ).append('Buscar')
                )
            ).append(
                $('<li />')
                    .html(
                    $('<a />').attr('href', '#').html(
                        $('<i/>').addClass('entypo-mobile')
                    ).append(' Facilidades de la tarjeta ').append( '<span id="q-card"></span>' )

                )
            );
            break;
        case 22:
            $('#menu-option-2').addClass("active").addClass("opened").addClass("active");
            $('#menu-option-2-2').addClass("active");
            $('#breadCrumb')
                .addClass('breadcrumb')
                .html(
                    $('<li />')
                        .html(
                            $('<a />').attr('href', '#').html(
                                $('<i/>').addClass('entypo-home')
                            ).append('Home')
                        )
                ).append(
                    $('<li />')
                        .html(
                            $('<a />').attr('href', '#').html(
                                $('<i/>').addClass('entypo-mobile')
                            ).append('Dispositivos')
                        )
                ).append(
                    $('<li />').addClass('Active')
                        .html(
                            $('<strong />').html(
                                $('<i/>').addClass('entypo-layout')
                            ).append('Lista')
                        )
                );
            break;

        case 23:
            $('#menu-option-2').addClass("active").addClass("opened").addClass("active");
            $('#menu-option-2-3').addClass("active");
            $('#breadCrumb')
                .addClass('breadcrumb')
                .html(
                    $('<li />')
                        .html(
                            $('<a />').attr('href', '#').html(
                                $('<i/>').addClass('entypo-home')
                            ).append('Home')
                        )
                ).append(
                    $('<li />')
                        .html(
                            $('<a />').attr('href', '#').html(
                                $('<i/>').addClass('entypo-mobile')
                            ).append('Dispositivos')
                        )
                ).append(
                    $('<li />').addClass('Active')
                        .html(
                            $('<strong />').html(
                                $('<i/>').addClass('entypo-user-add')
                            ).append('Crear')
                        )
                );
            break;
        case 3:
            $('#menu-option-3').addClass("active").addClass("opened").addClass("active");
            $('#breadCrumb')
                .addClass('breadcrumb')
                .html(
                    $('<li />')
                        .html(
                            $('<a />').attr('href', '#').html(
                                $('<i/>').addClass('entypo-home')
                            ).append('Home')
                        )
                ).append(
                    $('<li />')
                        .html(
                            $('<a />').attr('href', '#').html(
                                $('<i/>').addClass('entypo-chart-line')
                            ).append('Reportes')
                        )
                );
            break;
        case 4:
            $('#menu-option-4').addClass("active").addClass("opened").addClass("active");
            break;
        case 5:
            $('#menu-option-5').addClass("active").addClass("opened").addClass("active");
            break;

    }
}


