/**
 * Created by Nathaniel Calderon on 6/22/2017.
 */
function saveHandOperated(model) {
    var form_data = getBasicFormData();
    model.onSuccess = function (model, data) {
        console.log("********saveHandOperated******");
        console.log(data);
        renderResult(model, data);
    }

    model.onError = function (model, data) {

    }
    httpRequest(model.saveHandOperatedPath, form_data, model);
}

function saveProcessedBy (model) {
    var form_data = getBasicFormData();
    model.onSuccess = function (model, data) {
        console.log("********saveProcessedBy******");
        console.log(data);
        renderResult(model, data);
    }

    model.onError = function (model, data) {

    }
    httpRequest(model.saveProcessedByPath, form_data, model);
}

function renderResult(model, data){

}