/**
 * Created by Nathaniel Calderon on 4/17/2017.
 */
function PSTN_DATAModel($_data) {

    console.log("Creating PSTN_DATAModel with data ");
    console.dir($_data);
    var self = this;
    var flag = "_data";

    var puertoRegex = getPorts()[2][0];

    self.orderCode = '';
    self.orderType = '';
    self.orderPhone = '';
    self.isFillingForm = false;

    self.telefono = ko.observable(self.orderPhone);
    self.phoneIsDisabled = ko.observable(false);
    self.cuentaFija = ko.observable(false);
    self.cabina = ko.observable();
    self.parFeeder = ko.observable();
    self.terminal = ko.observable();
    self.parLocal = ko.observable();
    self.localidad = ko.observable(); //Frame field  in form
    //self.localidades = ko.observable(getLocalidades());
    self.localidades = ko.observable(); //Frame list  in form
    self.direccion = ko.observable(true);
    self.puertos = ko.observable(getPorts()[0]);
    self.tipoPuerto = ko.observable();
    self.puerto = ko.observable();
    self.dslam = ko.observable();
    self.dslamType = ko.observable();

    self.tipoPuerto.extend({notify: 'always'});
    self.tipoPuerto.subscribe(function (value) {

        var ports = getPorts()[0];
        var index = ports.indexOf(value);
        puertoRegex = getPorts()[2][index];
        console.log('port.pstn_data regex selected ' + puertoRegex);
        if (self.isFillingForm)
            return;


        var portMask = getPorts()[1][index];
        console.log('preparing to set port: ' + portMask);
        self.puerto(portMask);

        console.log(self.puerto);
        console.dir(self.puerto);

        if (self.setDslamPrefix)
            self.setDslamPrefix();
    });

    // begin 2015-GESTI-4791-1

    self = setupPSTNModel(self, flag);

    if ($_data.isViewMode === true)
        self = setupViewModel(self, $_data);

    self.numParFeeder.subscribe(function (value) {
        if (value == "Otros") {
            self.otrosParFeeder("");
            self.showOtrosParFeeder(true);
        } else {
            self.showOtrosParFeeder(false);
        }
    });

    self.numParLocal.subscribe(function (value) {
        if (value == "Otros") {
            self.otrosParFeeder("");
            self.showOtrosParLocal(true);
        } else {
            self.showOtrosParLocal(false);
        }
    });

    $("#terminal" + flag).blur(function () {
        if (!validateTerminal(self))
            return;

        var _range = getRange(getTerminalSecondSession(self.terminal()));
        self.rangeLocal(_range);
        self.rangeFeeder(_range);


    });

    self.cuentaFija.extend({notify: 'always'});
    self.cuentaFija.subscribe(function (value) {
        PSTNHideFields(self, value, flag);

        if (self.isFillingForm)
            return;

        if ('true' == value || true == value)
            self.dslamType('indoor');

        if (self.setDslamPrefix)
            self.setDslamPrefix();
    });

    self.cabina.extend({notify: 'always'});
    self.cabina.subscribe(function () {
        if (self.isFillingForm)
            return;
        self.terminal(getCabina(self) + "-");

        if (self.setDslamPrefix)
            self.setDslamPrefix();
    });


    self.localidad.extend({notify: 'always'});
    self.localidad.subscribe(function (value) {
        if (self.isFillingForm)
            return;
        console.log(value);
        if (!self.cuentaFija()) {
            self.cabina(value ? getJsonPathCabinaPrefix(value) : '');
        }
    });


    self.terminal.extend({notify: 'always'});
    self.terminal.subscribe(function () {
        if (self.isFillingForm)
            return;
        self.parLocal(getTerminalFistSession(self.terminal()) + '-');

        if (self.cuentaFija()) {
            self.parFeeder(getTerminalFistSession(self.terminal()) + '-');
        }

    });

    self.dslamType.extend({notify: 'always'});
    self.dslamType.subscribe(function (value) {
        if (self.isFillingForm)
            return;

        console.log('changing dslam type');
        if (self.setDslamPrefix)
            self.setDslamPrefix();
    });

    self.setDslamPrefix = function () {
        if (self.dslamType()) {
            var type = self.dslamType();

            var portType = self.tipoPuerto().toLowerCase();

            console.log('setting dslam ' + type + ' porttype => ' + portType);

            if ('indoor' == type) {
                var frame = self.localidad();

                if (!frame)
                    return;

                console.log('setting dslam with frame..');
                console.log(frame);

                if (portType.indexOf('zyxel') != -1)
                    portType = 'zyxel';


                portPrefix = getJsonPathFramePath(frame, '$..prefixes.ports.' + portType);
                self.dslam(portPrefix);

            } else if ('outdoor' == type) {
                try {
                    if (portType.indexOf('alcatel') != -1)
                        if (self.cabina() != undefined && self.cabina() != null){
                            self.dslam("A" + self.cabina());
                        } else {
                            self.dslam("A");
                        }
                    else if (portType.indexOf("zte") != -1)
                        if (self.cabina() != undefined && self.cabina() != null){
                            self.dslam("T" + self.cabina());
                        } else {
                            self.dslam("T");
                        }
                } catch (e) {
                    console.log(e)
                }
            }
        }
    }

    self.saveForm = function () {

        console.log('Saving form');
        if (!validatePSTNForm(self, flag))
            return false;

        var validated = true;

        // VALIDANDO TIPO DE PUERTO
        if (self.tipoPuerto() == getPorts()[0][0]) {
            $.growl({title: 'Debe seleccionar un tipo de puerto', message: ''});
            return false;
        }

        var input = (null != self.puerto() && undefined != self.puerto()) ? self.puerto().trim().toUpperCase() : '';
        console.log("Preparing to validate puerto: " + input + " isValidSuggestedPort ? " + isValidSuggestedPort(input));

        if (!(puertoRegex.test('' + input)) || !isValidSuggestedPort(input)) {
            $.growl({title: 'Valor de puerto no valido para el tipo ' + self.tipoPuerto(), message: ''});
            //return false;
            validated = false;
        }

        if (!validateDslam(self.dslam())){
            return false;
        }

        if (!validated)
            return false;

        var $data = getPSTNFormData(self, flag);
        $data['FORM_TYPE'] = 'PSTN+DATA';
        $data['TARJETA'] = $_data.tarjeta;

        $data['TIPO_PUERTO'] = toUpperCase(self.tipoPuerto()).trim();
        $data['PUERTO'] = toUpperCase(self.puerto()).trim();
        $data['DSLAM'] = toUpperCase(self.dslam()).trim();
        $data['DSLAM_TYPE'] = self.dslamType();


        console.log("Preparing to send...");
        console.dir(self);

        generalSendForm($_data._userLoggedCard, $_data._ip, $_data.url, $_data.tableRefreshUrl, $data);
    };

    self.fillContent = function (data) {

        self.isFillingForm = true;
        console.log('updating data');
        console.log(data);

        self = fillPSTNForm(self, data, flag);

        self.tipoPuerto(data['TIPO_PUERTO']);
        self.puerto(data['PUERTO']);
        self.dslam(data['DSLAM']);
        self.dslamType(data['DSLAM_TYPE']);
        self.isFillingForm = false;
    }

    self.setParams = function (xdata) {
        console.log('Setting params on PSTN DATA');
        console.dir(xdata);
        PSTNHideFields(self, false, flag);
        hideSpinner(self, false, flag);
        self.telefono("");
        self.orderCode = xdata.orderCode;
        self.orderType = xdata.orderType;
        self.orderPhone = parsePhone(xdata.orderPhone);
        //self.telefono(self.orderPhone);
        self = setPhone(self, flag, self.orderPhone);
    };

    self.setParams($_data);

    // End 2015-GESTI-4791-1

    // BEGIN 2015-GESTI-3083-2

    self.enableRefresh = true;
    self.refreshClick =  function() {

        console.log('Preparing to refresh data');
        self.enableRefresh = false;
        $.ajax({
            url: $_data.lastFormUrl,
            async: true,
            type: 'POST',
            data: {
                code: self.orderCode,
                orderType: self.orderType,
                userLoggedCard: _userLoggedCard,
                ip: $_data._ip,
                card: $_data._userLoggedCard
            },
            beforeSend: function () {
                //$($this).attr('disabled', 'disabled');
            },
            success: function (data) {
                console.log('incoming data ' + data);
                console.dir(data);
                //$($this).attr('disabled', false);
                if (!$.isEmptyObject(data)) {
                    console.log('Filling form');
                    self.isFillingForm = true;
                    self = resetModel(self, ["telefono", "cuentaFija", "localidad", "cabina"
                        , "terminal", "parLocal", "numParLocal", "otrosParLocal"
                        , "parFeeder", "numParFeeder", "otrosParFeeder", "puerto", "dslam", "dslamType", "direccion"]);
                    self = loadLastAction(self, data, flag);
                    self.tipoPuerto(data['TIPO_PUERTO']);
                    self.puerto(data['PUERTO']);
                    self.dslam(data['DSLAM']);
                    self.dslamType(data['DSLAM_TYPE']);
                    self.isFillingForm = false;
                } else {
                    $.growl({title: getAppMessage("not_posted_form"), message: ''});

                }
                self.enableRefresh = true;


            }
            /*,
             complete: function (data) {
             //console.log('data on complete' + data);
             //console.dir(data);
             }*/
        });

    }

    // END 2015-GESTI-3083-2

}