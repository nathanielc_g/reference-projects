package com.claro.cfc.web.servlet.service.model;

import com.claro.cfc.web.util.AppUtils;
import com.claro.cfc.web.util.ParamsMapUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nathaniel Calderon on 7/24/2017.
 */
public class AppConfigServiceModel {
    public Object reloadCache (Map<String, Object[]> params) {
        AppUtils.reloadCache();
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("ok", true);
        return result;
    }
}
