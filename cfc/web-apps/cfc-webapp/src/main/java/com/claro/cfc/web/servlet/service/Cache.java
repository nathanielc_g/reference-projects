package com.claro.cfc.web.servlet.service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christopher Herrera on 1/22/14.
 */
public class Cache {

    private List<Service> services;

    public Cache() {
        services = new ArrayList<Service>();
    }

    public Service getService(String serviceName) {
        for (Service service : services) {
            if (service.getClass().getName().equalsIgnoreCase(serviceName)) {
                return service;
            }
        }
        return null;
    }

    public void addService(Service newService) {
        boolean exists = false;
        for (Service service : services) {
            if (service.getClass().getName().equalsIgnoreCase(newService.getClass().getName())) {
                exists = true;
            }
        }
        if (!exists) {
            services.add(newService);
        }
    }
}
