package com.claro.cfc.web.servlet.service.model.dto;

/**
 * Created by Nathaniel Calderon on 4/7/2017.
 */
public class Form {
    private long actionId;
    private String taskTypeName;

    public long getActionId() {
        return actionId;
    }

    public void setActionId(long actionId) {
        this.actionId = actionId;
    }

    public String getTaskTypeName() {
        return taskTypeName;
    }

    public void setTaskTypeName(String taskTypeName) {
        this.taskTypeName = taskTypeName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Form)) return false;

        Form form = (Form) o;

        if (actionId != form.actionId) return false;
        return taskTypeName.equals(form.taskTypeName);

    }

    @Override
    public int hashCode() {
        int result = (int) (actionId ^ (actionId >>> 32));
        result = 31 * result + taskTypeName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Form{" +
                "actionId=" + actionId +
                ", taskTypeName='" + taskTypeName + '\'' +
                '}';
    }
}
