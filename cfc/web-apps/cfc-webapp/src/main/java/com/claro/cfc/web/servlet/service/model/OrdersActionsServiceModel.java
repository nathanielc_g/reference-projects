package com.claro.cfc.web.servlet.service.model;

import com.claro.cfc.db.dao.OrdersActionsAccess;
import com.claro.cfc.db.dao.TaskAccess;
import com.claro.cfc.db.dao.UsersAccess;
import com.claro.cfc.db.dao.service.UserLogService;
import com.claro.cfc.db.dto.*;
import com.claro.cfc.db.dto.enums.TaskStatusEnum;
import com.claro.cfc.db.dto.enums.UserLogDiscriminator;
import com.claro.cfc.enviroment.Environment;
import com.claro.cfc.enviroment.Environments;
import com.claro.cfc.transport.Attribute;
import com.claro.cfc.utils.net.RequestUtil;
import com.claro.cfc.web.util.AppUtils;
import com.claro.cfc.web.util.SaydotOrderParser;
import com.claro.cfc.web.util.ServiceModelUtil;
import com.claro.cfc.web.util.StringUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.log4j.Logger;

import java.lang.ref.Reference;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 6/1/2014.
 */
public class OrdersActionsServiceModel {
    private static Logger log = Logger.getLogger(OrdersActionsServiceModel.class);
    private static Environment env = Environments.getDefault();

    public Object updateAction(Map<String, Object[]> params) {
        Map<String, Object> response = new HashMap<String, Object>();
        final String userLoggedCard = ServiceModelUtil.getUserLoggedCard(params);
        /*final String ip = ServiceModelUtil.getIP(params);*/
        final String actionId = StringUtils.getAsNonNull(params.get("actionId")[0]);
        final String handOperatedBy = StringUtils.getAsNonNull(params.get("handOperatedBy")[0]);
        String processedBy = "";
        if(params.containsKey("processedBy"))
            processedBy = StringUtils.getAsNonNull(params.get("processedBy")[0]);

        if (handOperatedBy.isEmpty()) {
            response.put("ok", false);
            response.put("message", "There are not new values for do update.");
            return response;
        }

        log.info("Trying to update action for " + actionId);
        ActionsEntity action = OrdersActionsAccess.getActionById(Long.valueOf(actionId));

        LogAction logAction;
        if (!processedBy.isEmpty()) {
            action.setProcessedBy(userLoggedCard);
            logAction = LogAction.CHECK_ACTION_PROCESSED_BY;
        } else {
            if(handOperatedBy.equals("0")) {
                action.setHandOperatedBy("0");
                logAction = LogAction.UNCHECK_ACTION_HAND_OPERATED;
            } else {
                action.setHandOperatedBy(userLoggedCard);
                logAction = LogAction.CHECK_ACTION_HAND_OPERATED;
            }
        }

        UserLogEntity userLog = UserLogService.buildWebUserLog(logAction
                , action.getUserId()
                , new Timestamp(new Date().getTime())
                , actionId
                , userLoggedCard
                , String.format("Action %s set %s = %s", actionId, logAction, handOperatedBy)
                );

        boolean wasUpdated = OrdersActionsAccess.updateAction(action, userLog);
        response.put("ok", wasUpdated);
        if (wasUpdated)
            response.put("message", "Updated successfully");
        else
            response.put("message", "Updated failed.");

        return response;
    }

    public Object getActionResultsById(Map<String, Object[]> params) {
        Map<String, Object> response = new HashMap<String, Object>();
        final String userLoggedCard = ServiceModelUtil.getUserLoggedCard(params);
        final String ip = ServiceModelUtil.getIP(params);
        final String actionId = StringUtils.getAsNonNull(params.get("actionId")[0]);
        log.info("Trying to fetch action results for " + actionId);
        ActionsEntity action = OrdersActionsAccess.getActionById(Long.valueOf(actionId));
        // Check if action was processed
        response.put("HAND_OPERATED_BY", action.getHandOperatedBy());
        response.put("HAND_OPERATED_DISABLED", true);

        response.put("PROCESSED_BY", action.getProcessedBy());
        response.put("PROCESSED_BY_DISABLED", true);

        LinkedList<Map<String, String>> taskResults = new LinkedList<Map<String, String>>();
        response.put("TASK_RESULTS", taskResults);
        if (action.getProcessed()== null || !action.getProcessed().equals(1L))
            return response;

        TreeSet<TaskEntity> latestTasks = TaskAccess.getLatestTasksByActionId(Long.valueOf(actionId));
        boolean wasSuccessfulUpdated = true;
        for (TaskEntity task: latestTasks) {
            sortTaskLogsByCreated(task.listTaskLogEntity());

            //boolean isProcessed = task.getStatus().getStatusId() == TaskStatusEnum.PROCESSED.id();
            if (task.getStatus().getStatusId() != TaskStatusEnum.PROCESSED.id())
                wasSuccessfulUpdated = false;
            Map<String, String> result = new HashMap<String, String>();
            result.put("TYPE_NAME", task.getType().getTypeName());
            /*result.put("STATUS", task.getStatus().getStatus());*/
            /*result.put("CREATED_TASK", task.getCreated().toString());*/
            /*result.put("CREATED_LOG",  isProcessed ? task.getLogs().get(0).getCreated().toString():"");*/
            //result.put("MESSAGE_LOG", isProcessed? task.getLogs().get(0).getMessage():"");
            if (!task.getLogs().isEmpty())
                result.put("MESSAGE_LOG", task.listTaskLogEntity().get(0).getMessage() == null? "*Sin respuesta": task.listTaskLogEntity().get(0).getMessage());
            else
                result.put("MESSAGE_LOG", "*No existen logs. Estatus: " + task.getStatus().getStatus());

            if (!task.getLogs().isEmpty())
                result.put("UPDATED_ATTRS_LOG", task.listTaskLogEntity().get(0).getUpdatedAttrs() == null? "*": task.listTaskLogEntity().get(0).getUpdatedAttrs());
            else
                result.put("UPDATED_ATTRS_LOG", "*");

            taskResults.add(result);
        }
        response.put("HAND_OPERATED_DISABLED", action.getHandOperatedBy().equals("0")? wasSuccessfulUpdated:true);
        response.put("PROCESSED_BY_DISABLED",  action.getHandOperatedBy().equals("0") || !action.getProcessedBy().equals("0"));
        return response;
    }

    public Object getActionResultsByTaskId(Map<String, Object[]> params) {
        Map<String, Object> response = new HashMap<String, Object>();
        final String userLoggedCard = ServiceModelUtil.getUserLoggedCard(params);
        final String ip = ServiceModelUtil.getIP(params);
        final String taskId = StringUtils.getAsNonNull(params.get("taskId")[0]);
        final String actionId = StringUtils.getAsNonNull(params.get("actionId")[0]);
        log.info("Trying to fetch task results for " + taskId);
        ActionsEntity action = OrdersActionsAccess.getActionById(Long.valueOf(actionId));
        // Check if action was processed
        response.put("HAND_OPERATED_BY", action.getHandOperatedBy());
        response.put("HAND_OPERATED_DISABLED", true);
        LinkedList<Map<String, String>> taskResults = new LinkedList<Map<String, String>>();
        response.put("TASK_RESULTS", taskResults);
        if (action.getProcessed()== null || !action.getProcessed().equals(1L))
            return response;

        TaskEntity task = TaskAccess.getTaskById(Long.valueOf(taskId));
        boolean wasSuccessfulUpdated = true;

        sortTaskLogsByCreated(task.listTaskLogEntity());

        if (task.getStatus().getStatusId() != TaskStatusEnum.PROCESSED.id())
            wasSuccessfulUpdated = false;
        Map<String, String> result = new HashMap<String, String>();
        result.put("TYPE_NAME", task.getType().getTypeName());

        if (!task.getLogs().isEmpty())
            result.put("MESSAGE_LOG", task.listTaskLogEntity().get(0).getMessage() == null? "*Sin respuesta": task.listTaskLogEntity().get(0).getMessage());
        else
            result.put("MESSAGE_LOG", "*No existen logs. Estatus: " + task.getStatus().getStatus());

        if (!task.getLogs().isEmpty())
            result.put("UPDATED_ATTRS_LOG", task.listTaskLogEntity().get(0).getUpdatedAttrs() == null? "*": task.listTaskLogEntity().get(0).getUpdatedAttrs());
        else
            result.put("UPDATED_ATTRS_LOG", "*");

        taskResults.add(result);

        response.put("HAND_OPERATED_DISABLED", action.getHandOperatedBy().equalsIgnoreCase("0") ? wasSuccessfulUpdated : true);
        return response;
    }

    private void sortTaskLogsByCreated (List<TaskLogEntity> logs) {
        if (logs == null)
            return;
        Collections.sort(logs, new Comparator<TaskLogEntity>() {
            @Override
            public int compare(TaskLogEntity t1, TaskLogEntity t2) {
                return t2.getCreated().compareTo(t1.getCreated());
            }
        });
    }

    public Object getPendingOrdersByCard(Map<String, Object[]> params) {
        Map<String, Object> map = new HashMap<String, Object>();

        final List<String[]> arrayOrders = new LinkedList<String[]>();

        String card = StringUtils.getAsNonNull(params.get("card")[0]);
        String ip = StringUtils.getAsNonNull(params.get("ip")[0]);

        String userLoggedCard = "";
        if (null != params.get("ulc"))
            userLoggedCard = StringUtils.getAsNonNull(params.get("ulc")[0]);

        log.info("Trying to get IncomingData for Users " + card+" on environment "+env);

        JsonArray orders = SaydotOrderParser.parseToKeyValuePair(new RequestUtil().retrieveGetContent( env.saydotRetrieveOrders()+card ) );

        log.info("Pending orders " + orders.toString() + " : " + orders.size());
        Date now = new Date();

        if (null != orders) {
            for ( int i=0;i<orders.size();++i ) {
                JsonObject order = orders.get(i).getAsJsonObject();

                String type = null != order.get( SaydotOrderParser.KEY_TYPE ) ? order.get( SaydotOrderParser.KEY_TYPE ).getAsString() : "";
                String code = null != order.get( SaydotOrderParser.KEY_CODE ) ? order.get( SaydotOrderParser.KEY_CODE ).getAsString() : "";
                String date = null != order.get( SaydotOrderParser.KEY_ASSIGNED ) ? order.get( SaydotOrderParser.KEY_ASSIGNED ).getAsString() : now.toString();
                String phone = null != order.get( SaydotOrderParser.KEY_SERVICE_PHONE ) ? order.get( SaydotOrderParser.KEY_SERVICE_PHONE ).getAsString() : "";
                String locationType = null != order.get( SaydotOrderParser.KEY_LOCATION_TYPE ) ? order.get( SaydotOrderParser.KEY_LOCATION_TYPE ).getAsString() : "";


                String[] data = {
                        type,
                        "" + code,
                        date,
                        null, // Formulario PSTN
                        null, // Formulario FIBRA
                        null, // Formulario PSNT+FIBRA
                        null, // Formulario PSTN+DATA
                        null,  // Formualrio FRAME
                        phone,  // Service Phone
                        locationType //Tipo localidad
                };
                arrayOrders.add(data);
            }
        }

        UsersAccess.logUser(userLoggedCard, LogAction.DI_VIEW, "Retrieving orders/tickets for user " + card, ip, UserLogDiscriminator.WEB.toString());
        map.put("aaData", arrayOrders.toArray());
        arrayOrders.clear();
        orders = null;
        return map;
    }


    public Object getActionsByCard(Map<String, Object[]> params) {
        Map<String, Object> map = new HashMap<String, Object>();

        final List<String[]> arrayOrders = new LinkedList<String[]>();

        String card = StringUtils.getAsNonNull(params.get("card")[0]);

        log.info("Trying to get Actions for Users " + card);

        final Reference<List<ActionsEntity>> actionsRef = OrdersActionsAccess.getActionsByCard(card);
        final List<ActionsEntity> actions;

        if( null != actionsRef && null  != actionsRef.get() )
            actions= actionsRef.get();
        else
            actions = Collections.EMPTY_LIST;


        log.info("Actions orders " + actions.toString() + " : " + actions.size());
        if (null != actions) {
            for (ActionsEntity action : actions) {
                String[] data = {
                        action.getFormType(),
                        "" + action.getCode(),
                        StringUtils.getAsNonNull(action.getCreated().toString()),
                        "" + action.getActionId(),
                        null // Ver detalle
                };
                arrayOrders.add(data);
            }
            actions.clear();
        }

        map.put("aaData", arrayOrders.toArray());
        arrayOrders.clear();
        return map;
    }


    public Object getActionValuesByActionId(Map<String, Object[]> params) {
        final Map<String, String> map = new TreeMap<String, String>();

        log.info("Trying to fetch action data " + params.get("actionId")[0]);

        Long actionId = Long.valueOf(StringUtils.getAsNonNull(params.get("actionId")[0]));
        String userLoggedCard = StringUtils.getAsNonNull(params.get("userLoggedCard")[0]);
        String ip = StringUtils.getAsNonNull(params.get("ip")[0]);
        String card = StringUtils.getAsNonNull(params.get("card")[0]);

        final Reference<List<ActionValuesEntity>> valuesRef = OrdersActionsAccess.getActionValuesByActionId(actionId);
        final List<ActionValuesEntity> values;

        if( null != valuesRef && null != valuesRef.get() )
            values = valuesRef.get();
        else
            values = Collections.EMPTY_LIST;


        if (null != values) {
            for (ActionValuesEntity value : values) {
                map.put(value.getAkey(), value.getAvalue());
            }
            values.clear();
        }

        UsersAccess.logUser(userLoggedCard, LogAction.DI_SUBMITTED_VIEW, "Attempt to view Submitted form for tech " + card, ip, UserLogDiscriminator.WEB.toString());
        return map;
    }

    public Object getLastActionValuesByCode (Map<String, Object[]> params) {
        final Map<String, String> map = new TreeMap<String, String>();

        log.info("Trying to fetch action data by code = " + params.get("code")[0]);

        Long code = Long.valueOf(StringUtils.getAsNonNull(params.get("code")[0]));
        String orderType = String.valueOf(StringUtils.getAsNonNull(params.get("orderType")[0]));
        String userLoggedCard = StringUtils.getAsNonNull(params.get("userLoggedCard")[0]);
        String ip = StringUtils.getAsNonNull(params.get("ip")[0]);
        String card = StringUtils.getAsNonNull(params.get("card")[0]);

        final Reference<List<ActionValuesEntity>> valuesRef = OrdersActionsAccess.getActionValuesByCode(code, orderType);
        final List<ActionValuesEntity> values;

        if( null != valuesRef && null != valuesRef.get() )
            values = valuesRef.get();
        else
            values = Collections.EMPTY_LIST;


        if (null != values) {
            for (ActionValuesEntity value : values) {
                map.put(value.getAkey(), value.getAvalue());
            }
            values.clear();
        }

        UsersAccess.logUser(userLoggedCard, LogAction.DI_SUBMITTED_VIEW, "Attempt to view Submitted form for tech " + card, ip, UserLogDiscriminator.WEB.toString());
        return map;


    }



    public Object saveDispatchItem(Map<String, Object[]> params) {
        log.info("Preparing to save action");

        String card = null;
        String userLoggedCard = StringUtils.getAsNonNull(params.get("userLoggedCard")[0]);
        String ip = StringUtils.getAsNonNull(params.get("ip")[0]);

        final List<Attribute> attrs = new LinkedList<Attribute>();

        for (Map.Entry<String, Object[]> entry : params.entrySet()) {
            log.info("Receiving key " + entry.getKey() + " with value " + entry.getValue());
            if (!entry.getKey().contains("attr"))
                continue;

            String cleanKey = entry.getKey().replaceAll("attr", "").replaceAll("\\[", "").replaceAll("]", "");
            if ("TARJETA".equals(cleanKey)) {
                card = StringUtils.getAsNonNull(entry.getValue()[0]);
            } else {
                attrs.add(Attribute.createAttribute(cleanKey, StringUtils.getAsNonNull(entry.getValue()[0])));
            }
        }
        log.info("Preparing to save attributes " + attrs);

        long saved = -1;

        try {
            /**
             * For Tasks Scheduler Compatibility
             * {
             */
            attrs.add(Attribute.createAttribute("TARJETA", card));
            /**
             * }
             * For Tasks Scheduler Compatibility
             */
            saved = OrdersActionsAccess.saveAction(attrs, AppUtils.canPostFormOrderMultipleTimesValue());

            if (0 < saved) {
                Long actionId = saved;
                log.info("Preparing to create tasks for actionId: " + actionId);
                Attribute formType = attrs.get(attrs.indexOf(Attribute.createAttribute("FORM_TYPE", "")));
                if (null == formType) {
                    log.info("******* Could not be possible to create task for null form type on action id " + actionId);
                } else
                    TaskAccess.createTasks(formType.getValue(), actionId);
            } else {
                log.info("Skipping to create task for " + saved);
            }

            attrs.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            saved = -1;
        }

        String message;
        if (saved>0)
            message = "Ticket/Order submitted for tech " + card;
        else
            message = "There were some error trying to submit Ticket/Order for Tech " + card;

        UsersAccess.logUser(userLoggedCard, LogAction.DI_SUBMIT, message, ip, UserLogDiscriminator.WEB.toString());

        Map<String, String> map = new HashMap<String, String>();
        map.put("ok", String.valueOf(saved));
        map.put("message", message);

        return map;
    }
}
