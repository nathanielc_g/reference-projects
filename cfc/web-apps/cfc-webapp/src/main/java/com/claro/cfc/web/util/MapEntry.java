package com.claro.cfc.web.util;

import java.util.Map;

/**
 * Created by Nathaniel Calderon on 7/12/2016.
 */
public class MapEntry <K,V> implements Map.Entry<K,V> {
    private final K key;
    private V value;
    public MapEntry(final K key) {
        this.key = key;
    }

    public MapEntry(final K key, final V value) {
        this.key = key;
        this.value = value;
    }
    public K getKey() {
        return key;
    }
    public V getValue() {
        return value;
    }
    public V setValue(final V value) {
        final V oldValue = this.value;
        this.value = value;
        return oldValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MapEntry<?, ?> mapEntry = (MapEntry<?, ?>) o;

        return key.equals(mapEntry.key);

    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }
}
