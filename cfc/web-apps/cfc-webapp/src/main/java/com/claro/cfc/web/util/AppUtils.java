package com.claro.cfc.web.util;

import com.claro.cfc.db.dao.ApplicationAccess;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Created by Nathaniel Calderon on 9/28/2016.
 */
public final class AppUtils {
    private static final Logger log = Logger.getLogger(AppUtils.class.getName());
    private static boolean canPostFormOrderMultipleTimesValue;

    static {
        reloadCache();
    }

    private AppUtils() {
    }

    public final static boolean canPostFormOrderMultipleTimesValue() {
        return canPostFormOrderMultipleTimesValue;
    }

    public static void reloadCache() {
        log.info("********* Reloading App Config Values *********");
        canPostFormOrderMultipleTimesValue = ApplicationAccess.canPostOrderMultipletimes();
        log.info("Refreshing 'canPostFormMultiplesTime' value to " + canPostFormOrderMultipleTimesValue + ", at " + new Date());
    }
}
