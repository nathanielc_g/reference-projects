package com.claro.cfc.web.servlet.service.model.dto;

import com.claro.cfc.db.dto.ActionsEntity;

import java.sql.Timestamp;

/**
 * Created by Nathaniel Calderon on 6/13/2017.
 */
public class OrderAction {
    private long actionId;
    private long code;
    private String orderType;
    private String formType;
    private String userId;
    private String created;
    private String updatedAttrs = "";
    private String taskType;

    public static final OrderAction from (ActionsEntity action) {
        OrderAction instance = new OrderAction();
        instance.actionId = action.getActionId();
        instance.code = action.getCode();
        instance.orderType = action.getOrderType();
        instance.userId = action.getUserId();
        instance.created = action.getCreated().toString();
        instance.formType = action.getFormType();
        return instance;
    }

    public OrderAction() {
    }

    public long getActionId() {
        return actionId;
    }

    public void setActionId(long actionId) {
        this.actionId = actionId;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getUpdatedAttrs() {
        return updatedAttrs;
    }

    public void setUpdatedAttrs(String updateAttrs) {
        this.updatedAttrs = updateAttrs;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderAction that = (OrderAction) o;

        return actionId == that.actionId;
    }

    @Override
    public int hashCode() {
        return (int) (actionId ^ (actionId >>> 32));
    }

    @Override
    public String toString() {
        return "OrderAction{" +
                "actionId=" + actionId +
                ", code=" + code +
                ", orderType='" + orderType + '\'' +
                ", formType='" + formType + '\'' +
                ", userId='" + userId + '\'' +
                ", created='" + created + '\'' +
                ", updatedAttrs='" + updatedAttrs + '\'' +
                ", taskType='" + taskType + '\'' +
                '}';
    }

    /*private String formType;*/
    /*
    private Long processed;
    private Timestamp processedDate;
    private String handOperatedBy = "0";
    */


}
