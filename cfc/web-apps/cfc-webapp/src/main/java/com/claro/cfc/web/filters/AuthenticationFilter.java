package com.claro.cfc.web.filters;

import com.claro.api.client.paw.PAWAPIClient;
import com.claro.api.client.paw.type.ArrayOfString;
import com.claro.cfc.db.dao.UsersAccess;
import com.claro.cfc.enviroment.Environment;
import com.claro.cfc.enviroment.Environments;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christopher Herrera on 1/13/14.
 */

/**
 */
public class AuthenticationFilter implements Filter {
    private static Logger log = Logger.getLogger(AuthenticationFilter.class);
    private ServletContext ctx;
    private FilterConfig fc;

    private static PAWAPIClient.ENV pawEnv;
    static{
        Environment env = Environments.getDefault();
        pawEnv = Environment.DEVELOPMENT == env ? PAWAPIClient.ENV.DEVELOPMENT : PAWAPIClient.ENV.PRODUCTION;
    }
    private PAWAPIClient pawapiClient = PAWAPIClient.getInstance(pawEnv);

//    private PawClient pawClient;


    @Override
    public void init(FilterConfig fc) throws ServletException {
        this.fc = fc;
        ctx = fc.getServletContext();
        ctx.log(fc.getFilterName() + "Initialized ()");

//        pawClient = PawClient.getInstance();
    }

    @Override
    public void doFilter(ServletRequest req,
                         ServletResponse resp,
                         FilterChain fChain) throws IOException, ServletException {

        HttpServletRequest hReq = (HttpServletRequest) req;
        HttpServletResponse hRes = (HttpServletResponse) resp;
        HttpSession session = hReq.getSession();

        String path = hReq.getRequestURI();
        if (path.contains("/OTAUpdated")) {
            log.info("bypassedSecurity");
            fChain.doFilter(req, resp);
            return;
        }

        if (session.getAttribute("Authenticated") == null) {
            if (hReq.getParameter("usr") == null || hReq.getParameter("usr") == "") {
                String userPortal = pawapiClient.getService().getPortalURL();

                log.info("Redirecting to User portal url " + userPortal);
                hRes.sendRedirect(userPortal);
                return;
            }

            String ip = hReq.getRemoteAddr();
            String usr = hReq.getParameter("usr");
            String pswd = hReq.getParameter("pswd");
            String app = hReq.getParameter("app");
            String token = hReq.getParameter("token");
            String url = hReq.getParameter("url");
            hReq.removeAttribute("usr");
            hReq.removeAttribute("url");
            hReq.removeAttribute("app");
            hReq.removeAttribute("token");
            hReq.removeAttribute("pswd");

            session.setAttribute("usr", usr);
            session.setAttribute("token", token);
            session.setMaxInactiveInterval(15 * 60);
            Cookie userName = new Cookie("usr", usr);
            hRes.addCookie(userName);

            //Validate token
//            boolean authenticated = pawapiClient.autenticaToken(usr, app, Long.parseLong(token));
            boolean authenticated = pawapiClient.getService().autentica(usr, app, token);
            session.setAttribute("Authenticated", authenticated);
            //Insert Logs to Paw
//            pawClient.insertaLogUsr(usr, "Entro al Portal Captura Facilidades Celular", app, ip);
            pawapiClient.getService().insertaLogUsu(usr, "Entro al Portal Captura Facilidades Celular", app, ip);
            //Access and profile parameters
            //String roleName = pawClient.buscaRolDescripcion(app,usr);
//            int roleId = pawClient.buscaRol(app, usr);
            int roleId = pawapiClient.getService().buscaRol(app, usr);

            log.info("Paw Response rol " + roleId);

            List<String> accesos;
            try {
//                accesos = new ArrayList<String>(Arrays.asList(pawClient.buscaAccesos(app, roleId)));

                ArrayOfString as = pawapiClient.getService().buscaAccesos(app, roleId);
                accesos = as.getString();
            } catch (NullPointerException npe) {
                accesos = new ArrayList<String>();
            }

//            final String userCard = pawClient.getUserCardByName(usr);

            ArrayOfString userInfo = pawapiClient.getService().getUserInf(usr);
            log.info("Getting user Info: " + userInfo.getString());
            /**
             * Allways user card come in position
             */
            final String userCard = userInfo.getString().get(1);
            log.info("User card " + userCard + " for user " + usr);


            session.setAttribute("roleId", roleId);
            session.setAttribute("ip", ip);
            session.setAttribute("accesos", accesos);
            session.setAttribute("userCard", userCard);

            //session.setAttribute("roleName",roleName);

            //Crear objeto Session
            SessionInfo internalSession = new SessionInfo(usr, token, authenticated, roleId, accesos, ip, userCard);
            injectFrontInfo(internalSession);

            //Session En Json para manejar en Javascript
            Gson gson = new Gson();
            session.setAttribute("SessionJson", gson.toJson(internalSession));
            log.info("SessionJson: " + gson.toJson(internalSession));

            log.info("incoming request from IP: " + ip);


        } else {
            if (session.getAttribute("Authenticated").equals("false")) {
                hRes.sendRedirect(pawapiClient.getService().getPortalURL());
                log.info("Request From: " + session.getAttribute("usr"));
                return;
            }
        }
        fChain.doFilter(hReq, hRes);
    }


    @Override
    public void destroy() {
        //setting destroy to maximize gc collection
        pawapiClient = null;
        ctx = null;
    }


    private void injectFrontInfo(SessionInfo info) {
        long userCount = UsersAccess.getUsersCount();
        long diCount = 0;

        info.setUserCount(userCount);
        info.setDiCount(diCount);
    }

    public class SessionInfo {

        String usr;
        String token;
        boolean authenticated;
        int roleId;
        List<String> accesos;
        String ip;
        long userCount;
        long diCount;
        long taskCount;
        long taskTync;
        String userCard;

        public SessionInfo() {
        }

        public SessionInfo(String usr, String token, boolean authenticated, int roleId, List<String> accesos, String ip, String userCard) {
            this.usr = usr;
            this.token = token;
            this.authenticated = authenticated;
            this.roleId = roleId;
            this.accesos = accesos;
            this.ip = ip;
            this.userCard = userCard;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public void setUserCount(long userCount) {
            this.userCount = userCount;
        }

        public void setDiCount(long diCount) {
            this.diCount = diCount;
        }

        public String getUserCard() {
            return userCard;
        }

        public void setUserCard(String userCard) {
            this.userCard = userCard;
        }
    }
}
