package com.claro.cfc.web.util;

import java.util.Map;

/**
 * Created by Nathaniel Calderon on 3/21/2017.
 */
public final class ServiceModelUtil {
    public final static String USER_LOGGED_CARD_KEY = "userLoggedCard";
    public final static String IP_KEY = "ip";

    public static String getIP (Map<String, Object[]> source) {
        return StringUtils.getAsNonNull(source.get(IP_KEY)[0]);
    }

    public static String getUserLoggedCard (Map<String, Object[]> source) {
        return StringUtils.getAsNonNull(source.get(USER_LOGGED_CARD_KEY)[0]);
    }
}
