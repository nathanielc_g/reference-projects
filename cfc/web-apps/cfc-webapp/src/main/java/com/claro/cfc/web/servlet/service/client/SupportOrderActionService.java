package com.claro.cfc.web.servlet.service.client;

import com.claro.cfc.web.servlet.service.AbstractService;
import com.claro.cfc.web.servlet.service.model.SupportOrderActionServiceModel;

/**
 * Created by Nathaniel Calderon on 6/23/2017.
 */
public class SupportOrderActionService extends AbstractService {
    public SupportOrderActionService() {
        super(new SupportOrderActionServiceModel());
    }
}
