package com.claro.cfc.web.servlet.service.model;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.db.dao.ApplicationAccess;
import com.claro.cfc.db.dao.TerminalAccess;
import com.claro.cfc.db.dao.UsersAccess;
import com.claro.cfc.db.dao.service.TerminalService;
import com.claro.cfc.db.dao.service.UserLogService;
import com.claro.cfc.db.dao.service.UserService;
import com.claro.cfc.db.dto.*;
import com.claro.cfc.db.dto.enums.UserLogDiscriminator;
import com.claro.cfc.db.dto.enums.UserStatusEnum;
import com.claro.cfc.updater.Updater;
import com.claro.cfc.web.util.ParamsMapUtil;
import com.claro.cfc.web.util.StringUtils;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.*;

/**
 * Created by Christopher Herrera on 1/22/14.
 * <p/>
 * Service Locator pattern by Jansel
 */
public class DispositivoServiceModel {
    Gson gson = new Gson();
    private static Logger log = Logger.getLogger(DispositivoServiceModel.class);
    private static final String APPVER = "1.0";


    public Object addNewUser(Map<String, Object[]> params) {
        final SoftReference<Map<String, String>> mapRef = new SoftReference<Map<String, String>>(new HashMap<String, String>());
        final Map<String,String> map = mapRef.get();

        ParamsMapUtil paramReader = new ParamsMapUtil(params);
        LogAction logAction = LogAction.USER_ADDED;
        String pathMessage = "Usuario Creado Correctamente";

        String userCard = StringUtils.getAsNonNull(params.get("tarjeta")[0]);
        String userLoggedCard = StringUtils.getAsNonNull(params.get("userLoggedCard")[0]);
        String ip = StringUtils.getAsNonNull(params.get("ip")[0]);


        log.info("Param that come to save user " + params);
        log.info("addNewUserParams: " + gson.toJson(params));


        Timestamp now = new Timestamp(new Date().getTime());
        String phone = paramReader.getAsString("phone");
        UsersEntity user = UsersAccess.getUserByCard(userCard);
        if (null == user) {
            Timestamp expirationTime = new Timestamp(new Date(
                    paramReader.getAsInteger("expiration[]", 0) - 1900, //year
                    paramReader.getAsInteger("expiration[]", 2) - 1, //month
                    paramReader.getAsInteger("expiration[]", 1)).getTime());  //date
            user = UserService.buildUser(
                    userCard
                    , Long.valueOf(StringUtils.getAsNonNull(params.get("status")[0]))
                    , now
                    , expirationTime, phone);
        } else {
            user.setModified(now);
            user.setPhoneNumber(phone);
        }

        String imei = StringUtils.getAsNonNull(params.get("IMEI")[0]);

        TerminalEntity terminal = TerminalAccess.getTerminalByImei(imei);
        boolean canContinue = true;

        boolean okResponse = canContinue;

        if (null == terminal) {
            terminal = TerminalService.buildTerminal(
                    imei
                    , now
                    , StringUtils.getAsNonNull(params.get("model")[0])
                    ,StringUtils.getAsNonNull(params.get("brand")[0])
                    ,new Double(StringUtils.getAsNonNull(params.get("os")[0]))
                    ,new Double(APPVER));
        }

        // To avoid duplicates IMEI on multiples active users
        // Check If Terminal != 0(or !=[0...0]) and NEW_STATUS = ACTIVE
        if (!UserService.isImeiAvailable(terminal.getImei(), userCard, user.getStatus())) {
            pathMessage = "Solo puede haber un usuario activo por terminal";
            okResponse = false;
            canContinue = false;
        }

        if (canContinue) {
            UserLogEntity userLog = UserLogService.buildWebUserLog(logAction, userCard, now, ip, userLoggedCard, "User Device Created for " + userCard);
            okResponse = UsersAccess.saveUserRecord(user, terminal, userLog);
        }

        map.put("ok", String.valueOf(okResponse));
        map.put("msg", pathMessage);
        return map;
    }

    public Object updateUserAndDevice(Map<String, Object[]> params) {

        LogAction logAction = LogAction.DEVICE_UPDATE;

        String userLoggedCard = StringUtils.getAsNonNull(params.get("userLoggedCard")[0]);
        String ip = StringUtils.getAsNonNull(params.get("ip")[0]);

        String userCard = params.get("tarjeta")[0].toString();
        String imei = params.get("imei")[0].toString();
        String oldImei;

        boolean okResponse;
        String responseMessage = "Usuario actualizado correctamente";
        StringBuilder logMessage = new StringBuilder(String.format("User Device Updated to {IMEI=%s,USER_ID=%s} by ",imei,userCard)).append(userLoggedCard);

        final Timestamp modifiedAt = new Timestamp(new Date().getTime());

        if (null == params.get("oldImei") || params.get("oldImei")[0].toString().isEmpty())
            oldImei = imei;
        else {
            oldImei = params.get("oldImei")[0].toString();
            logMessage.append(", imei from ").append(oldImei).append(" To ").append(imei);
        }

        String phone = params.get("phone")[0].toString();
        String status = params.get("status")[0].toString();

        UsersEntity user = UsersAccess.getUserByCard(userCard);
        logMessage.append(", status from " + user.getStatus() + " To " + status);
        user.setStatus(Long.valueOf(status));
        user.setModified(modifiedAt);

        if (null == user)
            return buildResponse(false, "Usuario no encontrado");

        TerminalEntity terminal = TerminalAccess.getTerminalByImei(oldImei);
        if (null == terminal)
            return buildResponse(false, "Terminal a modificar no existe");

        if (!oldImei.equals(imei)) {
            terminal = TerminalService.buildTerminal(imei, modifiedAt, terminal.getTmodel(), terminal.getTbrand(), terminal.getOsVersion(), terminal.getAppVersion());
        }
        logMessage.append(", phone from " + user.getPhoneNumber() + " To " + phone);
        user.setPhoneNumber(phone);

        if (!UserService.isImeiAvailable(terminal.getImei(), userCard, user.getStatus()))
            return buildResponse(false, "Solo puede haber un usuario activo por terminal");

        if (Long.valueOf(status) == UserStatusEnum.TEMP_INACTIVE.ordinal()) {
            user.setReactiveDate(UserService.generateReactiveDate());
            logAction = LogAction.DEVICE_INACTIVE_TEMP;
        }
        UserLogEntity userLog = UserLogService.buildWebUserLog(logAction, userCard, modifiedAt, ip, userLoggedCard, logMessage.toString());
        okResponse = UsersAccess.updateUser(user, terminal, userLog);
        if(!okResponse)
            return buildResponse(false, "Error actualizando dispositivo.");
        return buildResponse(okResponse, responseMessage);
    }

    private Map<String, Object> buildResponse(boolean okResponse, String responseMessage) {
        final SoftReference<Map<String, Object>> mapRef = new SoftReference<Map<String, Object>>(new HashMap<String, Object>());
        final Map<String, Object> map = mapRef.get();
        map.put("ok", okResponse);
        map.put("msg", responseMessage);
        return map;
    }


    public Object getDataSearchTypeAhead(Map<String, Object[]> params) {

        final SoftReference<Map<String, String[]>> mapRef = new SoftReference<Map<String, String[]>>(new TreeMap<String, String[]>());
        final Map<String, String[]> map = mapRef.get();


        WeakReference<Map<String, List<String>>> mapaRef = _getDataSearchTypeAhead();
        final Map<String, List<String>> mapa;

        if( null != mapaRef && null != mapaRef.get() ){
            mapa = mapaRef.get();
        }else{
            mapa = Collections.EMPTY_MAP;
        }


        List<String> lista;
        lista = mapa.get("telefonos");
        String[] telefonos = mapa.get("telefonos").toArray(new String[lista.size()]);
        lista = mapa.get("imeis");
        String[] imeis = lista.toArray(new String[lista.size()]);
        lista = mapa.get("tarjetas");
        String[] tarjetas = lista.toArray(new String[lista.size()]);

        map.put("telefonos", telefonos);
        map.put("imeis", imeis);
        map.put("tarjetas", tarjetas);

        mapa.clear();
        return map;
    }


    public static WeakReference<Map<String, List<String>>> _getDataSearchTypeAhead() {
        Session session = HibernateHelper.getSessionFactory().openSession();
        Transaction tx = null;

        Map<String, List<String>> map = new HashMap<String, List<String>>();

        List<String> numbers = new LinkedList<String>();
        List<String> imeis = new LinkedList<String>();
        List<String> cards = new LinkedList<String>();

        List<UsersEntity> users = null;
        try {
            tx = session.beginTransaction();
            users = session.createCriteria(UsersEntity.class).list();

            if (users != null) {
                for (UsersEntity usuario : users) {
                    numbers.add(usuario.getPhoneNumber());
                    imeis.add(usuario.getTerminal().getImei());
                    cards.add(usuario.getUserId());
                }
            }
            tx.commit();
        } catch (HibernateException he) {
            log.error(he.getMessage());
            he.printStackTrace();
            if (null != tx && !tx.wasCommitted())
                tx.rollback();
        } finally {
            log.info("User Model Session Closed in getDataSearchTypeAhead!");
            if (null != session && session.isConnected()) {
                session.flush();
                session.close();

            }
            map.put("telefonos", numbers);
            map.put("imeis", imeis);
            map.put("tarjetas", cards);
            return new WeakReference<Map<String, List<String>>>(map);
        }
    }

    /**
     *
     * NOTA AQUI, SE HISO EL HACK D ELA  TARJETA DEL  TECNICO PARA CUANDO TIENE CEROS DELANTE
     *
     */
    public Object searchByCriteriaFormData(Map<String, Object[]> params) {
        SoftReference<Map<String, String>> mapRef = new SoftReference<Map<String, String>>(new TreeMap<String, String>());
        Map<String, String> map = mapRef.get();


        String userLoggedCard = StringUtils.getAsNonNull(params.get("userLoggedCard")[0]);
        String ip = StringUtils.getAsNonNull(params.get("ip")[0]);

        for (Map.Entry<String, Object[]> entry : params.entrySet()) {
            log.info("Search entry " + entry.getKey() + ", getValue " + Arrays.toString(entry.getValue()));
        }


        log.info("Trying to fetch user data " + params.get("criteria")[0] + " : " + params.get("valor")[0].toString() + " ");

        String criteria = params.get("criteria")[0].toString();
        String value = params.get("valor")[0].toString();

        UsersEntity user = null;

        if (criteria.equalsIgnoreCase("tarjeta")) {
            user = UsersAccess.getUserByCard(value);
            log.info("criteria Tarjeta ");
        } else if (criteria.equalsIgnoreCase("telefono")) {
            user = UsersAccess.getUserByPhone(value);
            log.info("criteria telefono");
        } else if (criteria.equalsIgnoreCase("imei")) {
            user = UsersAccess.getUserByImeiOrStatus(value);
            log.info("criteria imei");
        }

        final DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);

        String created = "";
        String expiried = "";
        String reactiveDate = "";
        boolean canInactiveTemp = true;
        try {
            Date date = new Date(user.getCreated().getTime());
            created = df.format(date);
            if (!"".equals(StringUtils.getAsNonNull(user.getExpirationDate()))) {
                date = new Date(user.getExpirationDate().getTime());
                expiried = df.format(date);
            }
            if (!"".equals(StringUtils.getAsNonNull(user.getReactiveDate()))){
                date = new Date(user.getReactiveDate().getTime());
                reactiveDate = df.format(date);
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                c.add(Calendar.DATE, -29); // Subtract 29 days
                canInactiveTemp = !(date.compareTo(c.getTime()) > 0);
            }
        } catch (Exception ex) {
            log.warn("Error parsing dates", ex);
        }

        /**
         * TEMPORAL HACK HERE
         */
        //if (criteria.equalsIgnoreCase("tarjeta"))
        //map.put("tarjeta", value + "");

        map.put("tarjeta", user.getUserId() + "");
        map.put("imei", user.getTerminal().getImei());
        //map.put("status", user.getStatus().equals(1L) ? "true" : "false");
        map.put("status", user.getStatus().toString());
        map.put("creacion", created);
        map.put("expiracion", expiried);
        map.put("marca", user.getTerminal().getTbrand());
        map.put("modelo", user.getTerminal().getTmodel());
        map.put("os", StringUtils.getAsNonNull(user.getTerminal().getOsVersion()));
        map.put("app", StringUtils.getAsNonNull(user.getTerminal().getAppVersion()));
        map.put("telefono", user.getPhoneNumber());
        map.put("reactiveDate", reactiveDate);
        map.put("canInactiveTemp", canInactiveTemp + "");

        UsersAccess.logUser(userLoggedCard, LogAction.DEVICE_SEARCH, "Device Search by " + criteria + " with value " + value, ip, UserLogDiscriminator.WEB.toString());
        return map;
    }


    public Object getAllDispositivos(Map<String, Object[]> params) {
        final SoftReference<Map<String, Object>> mapRef = new SoftReference<Map<String, Object>>(new TreeMap<String, Object>());

        final Map<String, Object> map = mapRef.get();
        final List<String[]> users = new LinkedList<String[]>();

        final Reference<List<UsersEntity>> allUsersRef = UsersAccess.getAllUsers();

        final List<UsersEntity> allUsers;
        if( null != allUsersRef && null != allUsersRef.get() )
            allUsers = allUsersRef.get();
        else
            allUsers = Collections.EMPTY_LIST;

        log.info("All users retrieved " + allUsers);

        final DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);

        for (UsersEntity user : allUsers) {
            String created = "";
            String expiried = "";
            String reactivation = "";
            try {
                Date date = new Date(user.getCreated().getTime());
                created = df.format(date);
                if (!"".equals(StringUtils.getAsNonNull(user.getExpirationDate()))) {
                    date = new Date(user.getExpirationDate().getTime());
                    expiried = df.format(date);
                }
                if (!"".equals(StringUtils.getAsNonNull(user.getReactiveDate()))) {
                    date = new Date(user.getReactiveDate().getTime());
                    reactivation = df.format(date);
                }
            } catch (Exception ex) {
                log.warn("Error parsing dates", ex);
            }

            String[] userData = new String[]{
                    String.valueOf(user.getUserId()),
                    user.getStatus().toString(),
                    reactivation,
                    created,
                    expiried,
                    StringUtils.getAsNonNull(user.getPhoneNumber()),
                    user.getTerminal().getTbrand(),
                    user.getTerminal().getTmodel(),
                    StringUtils.getAsNonNull(user.getTerminal().getOsVersion()),
                    StringUtils.getAsNonNull(user.getTerminal().getAppVersion()),
                    StringUtils.getAsNonNull(user.getTerminal().getImei())
//                    null
            };
            users.add(userData);
        }
        map.put("aaData", users.toArray());
        allUsers.clear();
        users.clear();
        return map;
    }

    public Object sendUpdateMessageToAll(Map<String, Object[]> params) {
        List<String> phones = UsersAccess.getPhonesByVersion(ApplicationAccess.getVersion());

        String userLoggedCard = StringUtils.getAsNonNull(params.get("userLoggedCard")[0]);
        String ip = StringUtils.getAsNonNull(params.get("ip")[0]);

        String msg;
        if (null != phones && phones.size() > 0) {
            msg = "Sending messages to " + phones;
            Updater.update(phones.toArray());
            phones.clear();
            UsersAccess.logUser(userLoggedCard, LogAction.DEVICE_APP_UPDATE, "Updated App for all devices", ip, UserLogDiscriminator.WEB.toString());
            log.info("Message: " + msg);
            return null;
        }
        //msg = " No User to send message";
        return null;
    }


    public Object sendUpdateMessageToPhone(Map<String, Object[]> params) {
        String card = StringUtils.getAsNonNull(params.get("card")[0]);
        String userLoggedCard = StringUtils.getAsNonNull(params.get("userLoggedCard")[0]);
        String ip = StringUtils.getAsNonNull(params.get("ip")[0]);

        log.info("sendUpdateMessageToPhone(" + card + ")");

        UsersEntity user = UsersAccess.getUserByCard(card);

        if (null == user)
            return null;

        UsersAccess.logUser(userLoggedCard, LogAction.DEVICE_APP_UPDATE, "Updated App for tech " + card, ip, UserLogDiscriminator.WEB.toString());
        Updater.update(user.getPhoneNumber());
        log.info("Message to phone " + user.getPhoneNumber());
        return null;
    }

}

