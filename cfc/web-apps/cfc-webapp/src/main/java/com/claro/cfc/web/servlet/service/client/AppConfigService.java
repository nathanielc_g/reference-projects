package com.claro.cfc.web.servlet.service.client;

import com.claro.cfc.web.servlet.service.AbstractService;
import com.claro.cfc.web.servlet.service.model.AppConfigServiceModel;

/**
 * Created by Nathaniel Calderon on 7/24/2017.
 */
public class AppConfigService extends AbstractService {
    public AppConfigService () {
        super(new AppConfigServiceModel());
    }
}
