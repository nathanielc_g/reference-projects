package com.claro.cfc.web.servlet.service.model;

import com.claro.cfc.cema.CEMAClient;
import com.claro.cfc.cema.message.SimpleSMS;
import com.claro.cfc.db.dao.NotificationAccess;
import com.claro.cfc.db.dao.OrdersActionsAccess;
import com.claro.cfc.db.dao.UsersAccess;
import com.claro.cfc.db.dao.service.UserLogService;
import com.claro.cfc.db.dto.ActionsEntity;
import com.claro.cfc.db.dto.LogAction;
import com.claro.cfc.db.dto.enums.UserLogDiscriminator;
import com.claro.cfc.web.servlet.service.model.dao.ActionDataAccess;
import com.claro.cfc.web.servlet.service.model.dto.OrderAction;
import com.claro.cfc.web.util.ParamsMapUtil;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created by Nathaniel Calderon on 6/13/2017.
 */
public class SupportOrderActionServiceModel {
    private static Logger log = Logger.getLogger(SupportOrderActionServiceModel.class);

    public List<OrderAction>  getNonHandOperatedActions(Map<String, Object[]> params) {
        ParamsMapUtil paramsMap = new ParamsMapUtil(params);
        log.info("Attemp to get NonhandOperatedActions by user: " + paramsMap.getAsString("_userLoggedID"));
        return getNonHandOperatedActions();
    }

    public Object saveHandOperated(Map<String, Object[]> params) {
        ParamsMapUtil paramsMap = new ParamsMapUtil(params);
        log.info("Attemp to save HandOperated by user: " + paramsMap.getAsString("_userLoggedID"));
        boolean updated = saveHandOperated(paramsMap.getAsLong("actionId"), paramsMap.getAsString("card"));
        Map<String, Object> result = new HashMap<String, Object>();
        UsersAccess.logUser(paramsMap.getAsString("_userLoggedID"), LogAction.CHECK_ACTION_HAND_OPERATED
                , String.format("Action %s set saveHandOperated by: %s", paramsMap.getAsString("actionId"), paramsMap.getAsString("card")), paramsMap.getAsString("actionId"), UserLogDiscriminator.WEB.toString());
        result.put("ok", updated);
        return result;
    }

    public Object saveProcessedBy (Map<String, Object[]> params) {
        ParamsMapUtil paramsMap = new ParamsMapUtil(params);
        log.info("Attemp to save saveProcessedBy by user: " + paramsMap.getAsString("_userLoggedID"));
        boolean updated = saveProcessedBy(paramsMap.getAsLong("actionId"), paramsMap.getAsString("card"));
        Map<String, Object> result = new HashMap<String, Object>();
        UsersAccess.logUser(paramsMap.getAsString("_userLoggedID"), LogAction.CHECK_ACTION_PROCESSED_BY
                , String.format("Action %s set saveProcessedBy by: %s", paramsMap.getAsString("actionId"), paramsMap.getAsString("card")), paramsMap.getAsString("actionId"), UserLogDiscriminator.WEB.toString());
        result.put("ok", updated);
        return result;
    }

    public List<OrderAction> findLastAction (Map<String, Object[]> params) {
        ParamsMapUtil paramsMap = new ParamsMapUtil(params);
        log.info("Attemp to get an action by user: " + paramsMap.getAsString("_userLoggedID"));
        String card = paramsMap.getAsString("card");
        long code = paramsMap.getAsLong("code");
        String type = paramsMap.getAsString("type");
        ActionsEntity action = findLastAction(card, code + "", type);
        if (action == null)
            return Collections.emptyList();
        return Arrays.asList(OrderAction.from(action));
    }

    public Object sendNotification(Map<String, Object[]> params){
        ParamsMapUtil paramsMap = new ParamsMapUtil(params);
        /*String card = paramsMap.getAsString("card");*/
        long actionId = paramsMap.getAsLong("actionId");
        String userLoggedCard = paramsMap.getAsString("userLoggedCard");
        log.info(String.format("Attemp to send HandOperated notification for Action: %s By UserLogged: %s", actionId, userLoggedCard));
        Map<String,String> attrs = NotificationAccess.getUserAndActionAttrs(actionId);
        boolean success = false;
        if (null != attrs){
            new CEMAClient().sendSMS(new SimpleSMS(attrs.get("PHONE_NUMBER"), String.format("Su solicitud para la orden/ticket %s - %s fue configurada ok.", attrs.get("CODE"), attrs.get("FORM_TYPE"))));
            UsersAccess.logUser(userLoggedCard, LogAction.CHECK_ACTION_HAND_OPERATED
                    , String.format("Action %s set HandOperared by: %s", actionId, userLoggedCard), paramsMap.getAsString("ip"), UserLogDiscriminator.WEB.toString());
            success = true;
        } else {
            log.info(String.format("Action %s was not found.", actionId));
        }
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("ok", success);
        return result;
    }

    private List<OrderAction> getNonHandOperatedActions () {
        return ActionDataAccess.getNonHandOperatedActions();
    }

    private boolean saveHandOperated (long actionId, String userId) {
        log.info("Trying to update HandOperated value to {"+userId+"} for Action{"+actionId+"} ");
        ActionsEntity action = OrdersActionsAccess.getActionById(actionId);
        action.setHandOperatedBy(userId);
        return OrdersActionsAccess.updateAction(action);
    }

    private boolean saveProcessedBy (long actionId, String userId) {
        log.info("Trying to update ProcessedBy value to {"+userId+"} for Action{"+actionId+"} ");
        ActionsEntity action = OrdersActionsAccess.getActionById(actionId);
        action.setProcessedBy(userId);
        return OrdersActionsAccess.updateAction(action);
    }

    /**
     *
     * @param card - card of the technician who posted the action
     * @param code - code of the order related to the action
     * @param type - type of the order
     * @return the ActionEntity found or null if it doesn't exist.
     */
    private ActionsEntity findLastAction (String card, String code, String type) {
        return OrdersActionsAccess.getLastAction(card, code, type);
    }

}
