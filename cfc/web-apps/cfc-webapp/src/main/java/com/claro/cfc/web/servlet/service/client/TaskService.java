package com.claro.cfc.web.servlet.service.client;

import com.claro.cfc.web.servlet.service.AbstractService;
import com.claro.cfc.web.servlet.service.model.TaskServiceModel;

/**
 * Created by Nathaniel Calderon on 4/10/2017.
 */
public class TaskService extends AbstractService{
    public TaskService () {
        super(new TaskServiceModel());
    }
}
