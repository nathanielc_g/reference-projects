package com.claro.cfc.web.servlet.report;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.db.dto.enums.UserLogDiscriminator;
import com.claro.cfc.web.util.MapEntry;
import com.claro.cfc.web.util.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 7/2/2014.
 */
public class StreamReportServlet extends BaseReportServlet {

    private static Logger log = Logger.getLogger(StreamReportServlet.class);

    private final static String queryReport = "SELECT USER_ID,TARGET,MOD_TYPE MODE_TYPE,CREATED,ACTION,DISCRIMINATOR,MESSAGE FROM USER_LOG";
    private final static String queryPartOrderBy = "ORDER BY CREATED";
    private final static String queryPartWhereDefault = "WHERE DISCRIMINATOR = '@discriminator' AND CREATED >= TO_TIMESTAMP('@created')";

    private UserLogDiscriminator discriminator = UserLogDiscriminator.DEVICE;

    private List<MapEntry<String, String>> fieldsname;

    @Override
    protected void initBase() {
        this.queryFilters = "";
        this.fieldsname = new ArrayList<MapEntry<String, String>>() {{
            add(new MapEntry<String, String>("@created", "CREATED"));
        }};
    }

    @Override
    protected List<MapEntry<String, String>> getFieldsname() {
        return fieldsname;
    }

    @Override
    protected String getQueryPartWhere() {

        if (!queryFilters.isEmpty()){
            return " where DISCRIMINATOR = '" + discriminator.toString() + "' AND " + queryFilters;
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - 15);
        String whereDefault = queryPartWhereDefault.replaceAll("@created", formatter.format(cal.getTime()));
        whereDefault = whereDefault.replaceAll("@discriminator", discriminator.toString());
        return whereDefault;
    }

    @Override
    protected String getQueryReport() {
        return queryReport;
    }

    @Override
    protected String getQueryPartOrderBy() {
        return queryPartOrderBy;
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
        super.doPost(req, res);
        String [] parts = req.getRequestURI().split("/");
        if (null == parts || 4 != parts.length)
            return;

        log.info("Trying to download file for " + Arrays.toString(parts));

        String lastPath = StringUtils.getAsNonNull(parts[3]);
        if (null == res.getOutputStream())
            return;

        log.info("User download report " + lastPath);

        res.setContentType("application/octet-stream");
        res.setHeader("Content-Disposition", "attachment;filename=actions-report.csv");

        if ("web".equalsIgnoreCase(lastPath))
            discriminator = UserLogDiscriminator.WEB;


        log.info("User download report " + lastPath + " and discriminator " + discriminator);

        res.setContentType("application/octet-stream");
        res.setHeader("Content-Disposition", "attachment;filename=logs-report-" + discriminator.toString().toLowerCase() + ".csv");

        writeOutputStreamReport(res.getOutputStream());
        //writeOutputStreamReport(resp.getOutputStream(), resp, StringUtils.getAsNonNull(parts[3]));

    }



    protected void injectHeaders(StringBuilder builder) {
        if (null == builder)
            return;
        builder.append("USER_ID").append(",").append("TARGET").append(",").append("MODE_TYPE")
                .append(",").append("CREATED").append(",").append("ACTION")
                .append(",").append("DISCRIMINATOR")
                .append(",").append("MESSAGE").append("\r\n");
    }


    protected void writeOutputStreamReport(OutputStream out)  {
        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            SQLQuery sqlQuery = session.createSQLQuery(getQuery());
            /*sqlQuery.setString("DISC", disc.toString());
            sqlQuery.setDate("CREATED", DateUtils.addDays(-15));*/

            log.info("Preparing to write line for discriminator " + discriminator);
            List results = sqlQuery.list();
            if (null == results || 0 >= results.size())
                return;

            StringBuilder builder = new StringBuilder();
            injectHeaders(builder);
            out.write(builder.toString().getBytes());
            builder.setLength(0);

            Iterator iterator = results.iterator();

            while (iterator.hasNext()) {
                Object[] row = (Object[]) iterator.next();

                for (int col = 0; col < row.length; col++) {
                    if (row.length - 1 == col && row[col] instanceof Clob) { //MESSAGE column
                        final Clob data = (Clob) row[col];
                        final String value = clobToString(data);
                        if (null != value) {
                            final String filteredValue = value.indexOf("OrdenesAsignadas") != -1 ?
                                    "Ordenes asignadas exitosamente"
                                    : value.indexOf("Declinando la orden enviada") != -1 ?
                                    "Ordenes enviadas DECLINADAS"
                                    : value;

                            builder.append(filteredValue).append(",");
                        }
                    } else
                        builder.append(row[col]).append(",");
                }
                builder.append("\r\n");
                out.write(builder.toString().getBytes());
                builder.setLength(0);
            }
            results.clear();

        } catch (Exception ex) {
            log.error("", ex);
        } finally {
            log.info("Actions Model Session Closed in writeReportLinesByCreated!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.close();
            }
        }
    }


    public static String clobToString(Clob clb) throws IOException, SQLException {
        if (clb == null)
            return "";
        StringBuffer str = new StringBuffer();
        String strng;

        BufferedReader bufferRead = new BufferedReader(clb.getCharacterStream());
        while ((strng = bufferRead.readLine()) != null)
            str.append(strng);

        return str.toString();

    }

}
