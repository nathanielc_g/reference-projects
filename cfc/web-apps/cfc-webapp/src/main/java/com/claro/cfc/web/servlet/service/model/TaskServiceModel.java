package com.claro.cfc.web.servlet.service.model;

import com.claro.cfc.db.dao.UsersAccess;
import com.claro.cfc.db.dto.LogAction;
import com.claro.cfc.db.dto.enums.UserLogDiscriminator;
import com.claro.cfc.web.servlet.service.model.dao.TaskDataAccess;
import com.claro.cfc.web.servlet.service.model.dao.sql.TaskQueryBuilder;
import com.claro.cfc.web.servlet.service.model.dto.Form;
import com.claro.cfc.web.servlet.service.model.dto.Task;
import com.claro.cfc.web.util.MapEntry;
import com.claro.cfc.web.util.ParamsMapUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created by Nathaniel Calderon on 4/7/2017.
 */
public final class TaskServiceModel {
    private static Logger log = Logger.getLogger(OrdersActionsServiceModel.class);

    public List<Task> getTasksByCustomFilter (Map<String, Object[]> params) {
        ParamsMapUtil paramsMap = new ParamsMapUtil(params);
        String query = buildQuery(paramsMap);
        log.info("Attemp to execute custom query by: " + paramsMap.getAsString("_userLoggedID"));
        return TaskDataAccess.getTasksByQuery(query);
    }

    private String buildQuery (ParamsMapUtil params) {
        Set<MapEntry<String, String>> requiredFilters = extractRequiredFilters(params);
        Set<MapEntry<String, String>> optionalFilters = extractOptionalFilters(params);
        TaskQueryBuilder taskQueryBuilder =  new TaskQueryBuilder(requiredFilters, optionalFilters);
        return taskQueryBuilder.build();
    }
    private Set<MapEntry<String,String>> extractRequiredFilters (ParamsMapUtil params) {
        Gson gson = new Gson();
        Set<MapEntry<String,String>> requiredFilters =
                gson.fromJson(params.getAsString("requiredFilters"), new TypeToken<Set<MapEntry<String, String>>>(){}.getType());
        return requiredFilters;
    }

    private Set<MapEntry<String,String>> extractOptionalFilters (ParamsMapUtil params) {
        Gson gson = new Gson();
        Set<MapEntry<String,String>> optionalFilters =
                gson.fromJson(params.getAsString("optionalFilters"), new TypeToken<Set<MapEntry<String, String>>>(){}.getType());
        return optionalFilters;
    }

    public Object submitAbortedForms (Map<String, Object[]> params) {
        ParamsMapUtil paramsMap = new ParamsMapUtil(params);
        Set<Form> forms = extractForms(paramsMap);
        log.info("Attemp to create tasks for " + forms.size() + " aborted forms by: " + paramsMap.getAsString("_userLoggedID"));
        Map<String, Object> result = new HashMap<String, Object>();
        createTasks(forms);
        UsersAccess.logUser(paramsMap.getAsString("_userLoggedID"), LogAction.ABORTED_FORMS_SUBMIT
                , "Tasks created: " + forms.size(), paramsMap.getAsString("_ip"), UserLogDiscriminator.WEB.toString());
        result.put("ok", true);
        return result;
    }

    private Set<Form> extractForms(ParamsMapUtil params) {
        Gson gson = new Gson();
        Set<Form> forms  =
                gson.fromJson(params.getAsString("forms"), new TypeToken<Set<Form>>(){}.getType());
        return forms;
    }

    private void createTasks(Set<Form> forms) {
        for (Form form: forms) {
            TaskDataAccess.createTask(form.getTaskTypeName(), form.getActionId());
        }
    }




}
