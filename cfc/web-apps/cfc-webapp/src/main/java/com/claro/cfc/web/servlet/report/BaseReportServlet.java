package com.claro.cfc.web.servlet.report;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.web.util.MapEntry;
import org.apache.log4j.Logger;
import org.hibernate.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Clob;
import java.util.List;

/**
 * Created by Nathaniel Calderon on 7/12/2016.
 */
public abstract class BaseReportServlet extends HttpServlet {

    protected String queryFilters;
    protected String columns;
    private final static Logger log = Logger.getLogger(BaseReportServlet.class);

    @Override
    public void init() throws ServletException {
        super.init();
        initBase();
    }

    protected abstract void initBase();

    protected abstract List<MapEntry<String, String>> getFieldsname();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        extractFormData(req);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        extractFormData(req);
    }

    protected void extractFormData(HttpServletRequest req) {
        queryFilters = req.getParameter("queryFilters") != null && !req.getParameter("queryFilters").isEmpty() ? req.getParameter("queryFilters") : "";
        columns = req.getParameter("columns") != null && !req.getParameter("columns").isEmpty() ? req.getParameter("columns") : "";
    }

    protected void replaceFieldsName() {
        List<MapEntry<String, String>> fieldsname = getFieldsname();
        for (MapEntry<String, String> fieldname : fieldsname) {
            queryFilters = queryFilters.replaceAll(fieldname.getKey(), fieldname.getValue());
        }
    }

    private String buildQueryStatement() {
        List<MapEntry<String, String>> fieldsname = getFieldsname();
        for (MapEntry<String, String> fieldname : fieldsname) {
            queryFilters = queryFilters.replaceAll(fieldname.getKey(), fieldname.getValue());
        }

        String queryTemplate = getQueryReport();
        queryTemplate = queryTemplate.replaceAll("@filters", getQueryPartWhere());
        queryTemplate = queryTemplate.replaceAll("@orderBy", getQueryPartOrderBy());
        return queryTemplate;
    }

    @Deprecated
    protected String getQuery() {
        replaceFieldsName();
        return getQueryReport() + " " + getQueryPartWhere() + " " + getQueryPartOrderBy();
    }

    protected String getQueryStatement() {
        return buildQueryStatement();
    }

    protected abstract String getQueryPartWhere();

    protected abstract String getQueryReport();

    protected abstract String getQueryPartOrderBy();

    protected abstract void injectHeaders(StringBuilder builder);

    @Deprecated
    protected void writeOutputStreamReport(OutputStream out) {
        writeOutputStreamReport(out, getQuery());
    }

    protected void writeOutputStreamReport(OutputStream out, String queryStaement) {
        statelessStreamReport(out, queryStaement);
    }


    private void statefullStreamReport(OutputStream out, String queryStaement) {
        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            SQLQuery sqlQuery = session.createSQLQuery(queryStaement);

            log.info("Trying to execute Query");
            List results = sqlQuery.list();
            log.info(this.getClass().getSimpleName() + " report got " + results.size() + " records.!");

            if (null == results || 0 >= results.size())
                return;

            StringBuilder builder = new StringBuilder();
            injectHeaders(builder);
            out.write(builder.toString().getBytes());
            builder.setLength(0);

            for (int idx = 0; idx < results.size(); ++idx) {
                Object[] row = (Object[]) results.get(idx);
                for (int col = 0; col < row.length; col++) {
                    builder.append(row[col]).append(",");
                }
                builder.append("\r\n");
                out.write(builder.toString().getBytes());
                builder.setLength(0);
                results.set(idx, null);
            }
            results.clear();
        } catch (Exception ex) {
            log.error(ex);
        } finally {
            log.info("Session Closed on writeOutputStreamReport by " + this.getClass().getSimpleName() + " !");
            if (session != null && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
    }

    private void statelessStreamReport(OutputStream out, String queryStaement) {
        StatelessSession session = HibernateHelper.getSessionFactory().openStatelessSession();
        try {
            SQLQuery sqlQuery = session.createSQLQuery(queryStaement);
            sqlQuery.setFetchSize(1000);

            log.info("Trying to execute Query: " + queryStaement);
            ScrollableResults results = sqlQuery.scroll(ScrollMode.FORWARD_ONLY);
            if (null == results)
                return;

            StringBuilder builder = new StringBuilder();
            injectHeaders(builder);
            out.write(builder.toString().getBytes());
            builder.setLength(0);

            int count = 0;
            while (results.next()) {
                ++count;
                Object[] row = results.get();
                for (int col = 0; col < row.length; col++) {
                    if (row[col] instanceof Clob){
                        Clob c = (Clob)row[col];
                        builder.append(c.getSubString(1, (int)c.length())).append(",");
                    }
                    else {
                        builder.append(row[col]).append(",");
                    }
                }
                builder.append("\r\n");
                out.write(builder.toString().getBytes());
                builder.setLength(0);
            }
            log.info(this.getClass().getSimpleName() + " report got " + count + " records.!");
        } catch (Exception ex) {
            log.error(ex);
        } finally {
            log.info("Session Closed on writeOutputStreamReport by " + this.getClass().getSimpleName() + " !");
            if (session != null) {
                session.close();
            }
        }
    }
}
