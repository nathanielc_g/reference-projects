package com.claro.cfc.web;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.web.servlet.service.model.dto.Task;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nathaniel Calderon on 4/11/2017.
 */
class App {
    private static final Logger log = Logger.getLogger(App.class);
    private static final String query =
            "select\n" +
            " t.ACTION_ID \"actionId\"\n" +
            ", a.CODE \"code\"\n" +
            ", t.CREATED as \"created\"\n" +
            ", a.USER_ID \"userId\"\n" +
            ", a.FORM_TYPE \"formType\"\n" +
            ", tl.RESPONSE_TYPE \"taskResponseType\"\n" +
            ", tl.MESSAGE \"taskLogMessage\"\n" +
            " from\n" +
            " (select\n" +
            " t.ACTION_ID \n" +
            ", max(t.CREATED) as CREATED\n" +
            ", max(t.TASK_ID) as TASK_ID\n" +
            " from\n" +
            " TASK t\n" +
            " where\n" +
            " t.status = 21 \n" +
            " and t.TASK_TYPE_ID = 21 \n" +
            " and t.CREATED >= TO_TIMESTAMP('06-Feb2017')\n" +
            " and t.CREATED <= TO_TIMESTAMP('07-Feb-2017')\n" +
            "group by\n" +
            "t.ACTION_ID\n" +
            ") t\n" +
            "inner join actions a\n" +
            "  on t.action_id = a.action_id\n" +
            "  inner join task_log tl\n" +
            "  on t.task_id = tl.task_id";
    public static void main (String ... args) {
        Session session = HibernateHelper.getSessionFactory().openSession();
        List<Task> tasks = new ArrayList<Task>(0);
        try {
            SQLQuery sqlQuery = session.createSQLQuery(query);
            //tasks = sqlQuery.addEntity(Task.class).list();
            tasks = sqlQuery.addScalar("actionId", new LongType())
                    .addScalar("code", new LongType())
                    .addScalar("created", new StringType())
                    .addScalar("userId", new StringType())
                    .addScalar("formType", new StringType())
                    .addScalar("taskResponseType", new StringType())
                    .addScalar("taskLogMessage", new StringType())
                    .setResultTransformer(Transformers.aliasToBean(Task.class))
                    .list();

        } catch (HibernateException ex){
            ex.printStackTrace();
        } finally {
            HibernateHelper.finalizeOpenSession(session);
        }
        System.out.println(tasks);
    }
}
