package com.claro.cfc.web.servlet.report;

import com.claro.cfc.web.util.MapEntry;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nathaniel Calderon on 5/26/2017.
 */
public class InactiveUsersReportServlet extends BaseReportServlet {
    private final static Logger log = Logger.getLogger(InactiveUsersReportServlet.class);

    private final static String queryReport =
            "select\n" +
                    "ul.USER_ID TARJETA_TECNICO\n" +
                    ",ul.CREATED FECHA_INACTIVACION\n" +
                    ",ul.MODIFIED_BY TARJETA_USUARIO\n" +
                    ",u.PHONE_NUMBER TELEFONO\n" +
                    ",t.IMEI\n" +
                    ",t.APP_VERSION\n" +
                    ",ul.MESSAGE LOG_MESSAGE\n" +
                    "from\n" +
                    "user_log ul\n" +
                    "inner join users u\n" +
                    "  on ul.USER_ID = u.USER_ID\n" +
                    "inner join terminal t\n" +
                    "  on u.imei = t.imei\n" +
                    "  where ul.ACTION = 'DEVICE_INACTIVE_TEMP'\n" +
                    "  and @filters\n";
    private final static String queryPartOrderBy = "";
    private final static String queryPartWhereDefault = "ul.created >= sysdate-7";
    private List<MapEntry<String, String>> fieldsname;

    @Override
    protected void initBase() {
        this.queryFilters = "";
        this.fieldsname = new ArrayList<MapEntry<String, String>>() {{
            add(new MapEntry<String, String>("@created", "ul.created"));
        }};
    }

    @Override
    protected List<MapEntry<String, String>> getFieldsname() {
        return fieldsname;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        super.doPost(req, res);
        res.setContentType("application/octet-stream");
        res.setHeader("Content-Disposition", "attachment;filename=userlogs-report.csv");
        log.info("*****Getting Inactivation Log report");
        super.writeOutputStreamReport(res.getOutputStream(), getQueryStatement());
    }

    @Override
    protected String getQueryReport() {
        return queryReport;
    }

    @Override
    protected String getQueryPartWhere() {
        return (queryFilters == "" ? queryPartWhereDefault : queryFilters);
    }

    @Override
    protected String getQueryPartOrderBy() {
        return queryPartOrderBy;
    }

    protected void injectHeaders(StringBuilder builder) {
        if (null == builder)
            return;

        builder.append("TARJETA_TECNICO")
                .append(",").append("FECHA_INACTIVACION")
                .append(",").append("TARJETA_USUARIO")
                .append(",").append("TELEFONO")
                .append(",").append("IMEI")
                .append(",").append("APP_VERSION")
                .append(",").append("LOG_MESSAGE")
                .append("\r\n");
    }
}
