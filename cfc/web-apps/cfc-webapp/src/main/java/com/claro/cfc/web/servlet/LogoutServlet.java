package com.claro.cfc.web.servlet;

import com.claro.api.client.paw.PAWAPIClient;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 6/29/2014.
 */
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().invalidate();
        resp.sendRedirect(PAWAPIClient.getInstance().getService().getPortalURL());
    }
}
