package com.claro.cfc.web.servlet.service.model.dao;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.db.dto.enums.TaskTypeEnum;
import com.claro.cfc.web.servlet.service.model.dto.OrderAction;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Pattern;

/**
 * Created by Nathaniel Calderon on 6/19/2017.
 */
public class ActionDataAccess {
    private static final Logger log = Logger.getLogger(ActionDataAccess.class);
    private static final String query =
            "        select\n" +
            "        tbl.action_id as \"actionId\"\n" +
            "        ,tbl.code as \"code\"\n" +
            "        ,tbl.order_type as \"orderType\"\n" +
            "        ,a.form_type as \"formType\"\n" +
            "        ,a.user_id as \"userId\"\n" +
            "        ,TO_CHAR(a.CREATED, 'DD-MON-YYYY HH24:MI') as \"created\"\n" +
            "        ,tl.updated_attrs as \"updatedAttrs\"\n" +
            "        ,tt.type_name as \"taskType\"\n" +
            "        from\n" +
            "        (\n" +
            "        select\n" +
            "        max(a.ACTION_ID) as ACTION_ID\n" +
            "        ,a.CODE as CODE\n" +
            "        ,a.ORDER_TYPE as ORDER_TYPE\n" +
            "        from\n" +
            "        actions a\n" +
            "        where\n" +
            "        a.processed = 1\n" +
            "        and a.created > TRUNC(SYSDATE)-7\n" +
            "        group by\n" +
            "        a.CODE\n" +
            "        ,a.ORDER_TYPE\n" +
            "        ,a.USER_ID\n" +
            "        ) tbl\n" +
            "        inner join actions a\n" +
            "          on a.action_id = tbl.action_id\n" +
            "        inner join task t\n" +
            "          on t.ACTION_ID = tbl.ACTION_ID\n" +
            "        inner join task_type tt\n" +
            "          on tt.TASK_TYPE_ID = t.TASK_TYPE_ID\n" +
            "        inner join TASK_LOG tl\n" +
            "          on tl.TASK_ID = t.TASK_ID\n" +
            "        where \n" +
            "        a.hand_operated_by = '0'\n" +
            "        and (t.TASK_TYPE_ID = 22 or t.TASK_TYPE_ID = 24)\n" +
            "        and t.STATUS = 2\n" +
            "        and (tl.RESPONSE_TYPE = 1 or tl.RESPONSE_TYPE = 3)\n" +
            "        order by tbl.ACTION_ID";
        /*"select\n" +
        "tbl.action_id as \"actionId\"\n" +
        ",tbl.code as \"code\"\n" +
        ",tbl.order_type as \"orderType\"\n" +
        ",a.form_type as \"formType\"\n" +
        ",tbl.user_id as \"userId\"\n" +
        ",TO_CHAR(a.CREATED, 'DD-MON-YYYY HH24:MI') as \"created\"\n" +
        ",tl.updated_attrs as \"updatedAttrs\"\n" +
        "from\n" +
        "(\n" +
        "select\n" +
        "max(a.ACTION_ID) as ACTION_ID\n" +
        ",a.CODE as CODE\n" +
        ",a.ORDER_TYPE as ORDER_TYPE\n" +
        ",a.USER_ID\n" +
        "from\n" +
        "actions a\n" +
        "where\n" +
        "a.processed = 1\n" +
        "and a.created > TRUNC(SYSDATE)-7\n" +
        "and a.hand_operated_by = '0'\n" +
        "and a.processed_by = '0'\n" +
        "group by\n" +
        "a.CODE\n" +
        ",a.ORDER_TYPE\n" +
        ",a.USER_ID\n" +
        ") tbl\n" +
        "inner join actions a\n" +
        "  on a.action_id = tbl.action_id\n" +
        "inner join task t\n" +
        "  on t.ACTION_ID = tbl.ACTION_ID\n" +
        "inner join TASK_LOG tl\n" +
        "  on tl.TASK_ID = t.TASK_ID\n" +
        "where \n" +
        "(t.TASK_TYPE_ID = 22 or t.TASK_TYPE_ID = 24)\n" +
        "and t.STATUS = 2\n" +
        "and (tl.RESPONSE_TYPE = 1 or tl.RESPONSE_TYPE = 3)\n" +
        "order by tbl.ACTION_ID\n";*/


    private interface Filter {
        public List<OrderAction> filter();
    }

    private static class GeffFilter implements Filter {
        private CopyOnWriteArrayList<OrderAction> concurrentOrderActions;
        private Pattern p = Pattern.compile("^---([T-])([S-])$");

        public GeffFilter(List<OrderAction> orderActions) {
            this.concurrentOrderActions = new CopyOnWriteArrayList<OrderAction>(orderActions);
            orderActions.clear();
            orderActions = null;
        }

        @Override
        public List<OrderAction> filter() {
            for (OrderAction order : concurrentOrderActions) {
                if(TaskTypeEnum.GEFF.toString().equals(order.getTaskType()) && p.matcher(order.getUpdatedAttrs()).matches())
                    concurrentOrderActions.remove(order);
            }
            return concurrentOrderActions;
        }
    }

    public static List<OrderAction> getNonHandOperatedActions() {
        /*log.info("Trying to get NonHandOperatedAction");*/
        Session session = HibernateHelper.getSessionFactory().openSession();
        List<OrderAction> actions = new ArrayList<OrderAction>(0);
        try {
            SQLQuery sqlQuery = session.createSQLQuery(query);
            actions = sqlQuery
                    .addScalar("actionId", LongType.INSTANCE)
                    .addScalar("code", LongType.INSTANCE)
                    .addScalar("orderType", StringType.INSTANCE)
                    .addScalar("formType", StringType.INSTANCE)
                    .addScalar("userId", StringType.INSTANCE)
                    .addScalar("created", StringType.INSTANCE)
                    .addScalar("updatedAttrs", StringType.INSTANCE)
                    .addScalar("taskType", StringType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(OrderAction.class))
                    .list();
            actions = (new GeffFilter(actions)).filter();
            //tasks = sqlQuery.list();
        } catch (HibernateException ex){
            ex.printStackTrace();
            log.info("Error at getNonHandOperatedActions call. ", ex);
            log.info("Query: "+query);
        } catch (RuntimeException ex){
            ex.printStackTrace();
            log.info("Error at getNonHandOperatedActions call. ", ex);
        }finally {
            HibernateHelper.finalizeOpenSession(session);
        }
        return actions;
    }


}
