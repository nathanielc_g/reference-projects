package com.claro.cfc.web.servlet.service.model.dto;

/**
 * Created by Nathaniel Calderon on 4/7/2017.
 */
public class Task {
    private long taskId;
    //private Timestamp processed;
    private long actionId;
    private long code;
    private String created;
    private String userId;
    private String formType;
    private String taskResponseType;
    private String taskLogMessage;

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

/*    public Timestamp getProcessed() {
        return processed;
    }

    public void setProcessed(Timestamp processed) {
        this.processed = processed;
    }*/

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public long getActionId() {
        return actionId;
    }

    public void setActionId(long actionId) {
        this.actionId = actionId;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getTaskResponseType() {
        return taskResponseType;
    }

    public void setTaskResponseType(String taskResponseType) {
        this.taskResponseType = taskResponseType;
    }

    public String getTaskLogMessage() {
        return taskLogMessage;
    }

    public void setTaskLogMessage(String taskLogMessage) {
        this.taskLogMessage = taskLogMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;

        Task task = (Task) o;

        if (actionId != task.actionId) return false;
        if (code != task.code) return false;
        if (created != null ? !created.equals(task.created) : task.created != null) return false;
        if (userId != null ? !userId.equals(task.userId) : task.userId != null) return false;
        if (formType != null ? !formType.equals(task.formType) : task.formType != null) return false;
        if (taskResponseType != null ? !taskResponseType.equals(task.taskResponseType) : task.taskResponseType != null)
            return false;
        return !(taskLogMessage != null ? !taskLogMessage.equals(task.taskLogMessage) : task.taskLogMessage != null);

    }

    @Override
    public int hashCode() {
        int result = (int) (actionId ^ (actionId >>> 32));
        result = 31 * result + (int) (code ^ (code >>> 32));
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (formType != null ? formType.hashCode() : 0);
        result = 31 * result + (taskResponseType != null ? taskResponseType.hashCode() : 0);
        result = 31 * result + (taskLogMessage != null ? taskLogMessage.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Task{" +
                "actionId=" + actionId +
                ", code=" + code +
                ", created='" + created + '\'' +
                ", userId='" + userId + '\'' +
                ", formType='" + formType + '\'' +
                ", taskResponseType='" + taskResponseType + '\'' +
                ", taskLogMessage='" + taskLogMessage + '\'' +
                '}';
    }
}
