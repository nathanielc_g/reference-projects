package com.claro.cfc.web.servlet.report;

import com.claro.cfc.web.util.MapEntry;
import com.claro.cfc.web.util.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Created by Nathaniel Calderon on 6/16/2016.
 */
public class TerminalReportServlet extends BaseReportServlet {
    private static Logger log = Logger.getLogger(TerminalReportServlet.class);

    private final static String queryReport = "select '\"' || u.user_id || '\"' TARJETA ,us.STATUS,u.created FECHA_CREACION,u.expiration_date FECHA_EXPIRACION,u.phone_number TELEFONO,t.tbrand MARCA,t.tmodel MODELO,t.os_version VERSION_ANDROID,t.app_version VERSION_APP, t.IMEI as IMEI from terminal t inner join users u on u.imei = t.imei inner join user_status us on us.STATUS_ID = u.STATUS order by u.user_id";
    private final static String queryPartOrderBy = "";
    private final static String queryPartWhereDefault = "";
    // private static final String terminalsQuery = "select '\"' || u.user_id || '\"' TARJETA ,case when u.status = 0 then 'INACTIVO' else 'ACTIVO' end STATUS,u.created FECHA_CREACION,u.expiration_date FECHA_EXPIRACION,u.phone_number TELEFONO,t.tbrand MARCA,t.tmodel MODELO,t.os_version VERSION_ANDROID,t.app_version VERSION_APP, t.IMEI as IMEI from terminal t inner join users u on u.imei = t.imei order by u.user_id";

    private List<MapEntry<String, String>> fieldsname;

    @Override
    protected void initBase() {
        this.queryFilters = "";
        this.fieldsname = new ArrayList<MapEntry<String, String>>();
    }

    @Override
    protected List<MapEntry<String, String>> getFieldsname() {
        return fieldsname;
    }

    @Override
    protected String getQueryPartWhere() {
        return (queryFilters.isEmpty()? queryPartWhereDefault : queryPartWhereDefault);
    }

    @Override
    protected String getQueryReport() {
        return queryReport;
    }

    @Override
    protected String getQueryPartOrderBy() {
        return queryPartOrderBy;
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String [] parts = req.getRequestURI().split("/");
        if (null == parts || 4 != parts.length)
            return;

        super.doPost(req, res);

        log.info("Trying to download file for " + Arrays.toString(parts));

        String lastPath = StringUtils.getAsNonNull(parts[3]);
        if (null == res.getOutputStream())
            return;

        log.info("User download report " + lastPath);

        res.setContentType("application/octet-stream");
        res.setHeader("Content-Disposition", "attachment;filename=terminals-report.csv");
        super.writeOutputStreamReport(res.getOutputStream());

    }

    protected void injectHeaders(StringBuilder builder) {
        if (null == builder)
            return;
        builder.append("TARJETA").append(",").append("STATUS").append(",").append("FECHA_CREACION")
                .append(",").append("FECHA_EXPIRACION").append(",").append("TELEFONO").append(",").append("MARCA")
                .append(",").append("MODELO")
                .append(",").append("VERSION_ANDROID")
                .append(",").append("VERSION_APP")
                .append(",").append("IMEI")
                .append("\r\n");
    }


    /*private void writeReportLinesByCreated(Date created, OutputStream out) {
        Session session = SessionFactoryHelper.getSessionFactory().openSession();
        try
        {

            SQLQuery sqlQuery = session.createSQLQuery(terminalsQuery);
            log.info("Trying to get Terminals by created: " + created);
            List results = sqlQuery.list();
            if (null == results || 0 >= results.size())
                return;

            StringBuilder builder = new StringBuilder();
            injectHeaders(builder);
            out.write(builder.toString().getBytes());
            builder.setLength(0);

            Iterator iterator = results.iterator();

            while (iterator.hasNext()) {
                Object[] row = (Object[])iterator.next();
                for (int col = 0; col < row.length; col++) {
                    builder.append(row[col]).append(",");
                }
                builder.append("\r\n");
                out.write(builder.toString().getBytes());
                builder.setLength(0);
            }
            results.clear();
        }catch(Exception ex){
            log.error(ex);
        }finally {
            log.info("Terminals Model Session Closed in writeReportLinesByCreated!");
            if(session != null && session.isConnected()){
                session.flush();
                session.close();
            }
        }
    }*/



}
