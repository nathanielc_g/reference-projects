package com.claro.cfc.web.servlet.service.model.dao.sql;

import com.claro.cfc.web.util.MapEntry;


import java.util.Set;

/**
 * Created by Nathaniel Calderon on 4/10/2017.
 */
public class TaskQueryBuilder {
    private static final long ABORTED_TASK_STATUS = 21L;
    private static final String mainQuery =
                    "select\n" +
                    " t.TASK_ID as \"taskId\"\n" +
                    ", t.ACTION_ID as \"actionId\"\n" +
                    ", TO_CHAR(t.CREATED, 'DD-MON-YYYY HH24:MI') as \"created\"\n" +
                    ", a.CODE as \"code\"\n" +
                    ", a.USER_ID as \"userId\"\n" +
                    ", a.FORM_TYPE as \"formType\"\n" +
                    ", trp.RTYPE as \"taskResponseType\"\n" +
                    ", NVL(tl.MESSAGE, '*Sin mensaje') as \"taskLogMessage\"\n" +
                    "from\n" +
                    "( \n" +
                    " select\n" +
                    " * \n" +
                    " from\n" +
                    " ( \n" +
                    " select\n" +
                    " t.ACTION_ID \n" +
                    ", max(t.CREATED) as CREATED\n" +
                    ", max(t.TASK_ID) as TASK_ID\n" +
                    " from\n" +
                    " TASK t\n" +
                    " where\n" +
                    "( t.status = @abortedTaskStatus @includeSuccessForm )\n" +
                    " and t.TASK_TYPE_ID = @taskTypeId \n" +
                    " and t.CREATED >= TO_TIMESTAMP('@beginDate')\n" +
                    " and t.CREATED <= TO_TIMESTAMP('@endDate')\n" +
                    "group by\n" +
                    "t.ACTION_ID\n" +
                    ") t\n" +
                    " where t.action_id not in \n" +
                    " ( \n"+
                    " select action_id from task where action_id = t.action_id and TASK_TYPE_ID = @taskTypeId and (status = 1 @notIncludeSuccessForms) \n" +
                    " ) \n" +
                    " ) t \n"+
                    "inner join actions a\n" +
                    "  on t.action_id = a.action_id\n" +
                    " @handOperatedByFilter " +
                    "inner join task_log tl\n" +
                    "  on t.task_id = tl.task_id \n" +
                    " inner join TASK_RESPONSE_TYPE trp \n" +
                    "  on tl.RESPONSE_TYPE = trp.RID \n" +
                    " @optionalActionValuesFilter";

    private static final String includeSuccessForm = "or t.status = 2";
    private static final String notIncludeSuccessForms = "or status = 2";
    private static final String optionalActionFilter = " and a.HAND_OPERATED_BY != 0 \n";
    private static final String OptionalActionValueFilter =
                    " inner join action_values @tableAlias \n" +
                    " on @tableAlias.action_id = a.action_id \n" +
                    "  and @tableAlias.AKEY = '@AKEY' \n" +
                    "  and @tableAlias.AVALUE @AVALUE \n";

    private Set<MapEntry<String, String>> requiredFiters;
    private Set<MapEntry<String, String>> optionalFilters;
    public TaskQueryBuilder (Set<MapEntry<String, String>> requiredFiters) {
        this.requiredFiters = requiredFiters;
    }

    public TaskQueryBuilder (Set<MapEntry<String, String>> requiredFiters, Set<MapEntry<String, String>> optionalFilters) {
        this(requiredFiters);
        this.optionalFilters = optionalFilters;
    }

    public String build () {
        return replaceValues();
    }

    private String replaceValues () {
        String query = mainQuery.replaceAll("@abortedTaskStatus", ABORTED_TASK_STATUS + "");

        for(MapEntry<String, String> filter: requiredFiters)
            query = query.replaceAll(filter.getKey().trim(), filter.getValue().trim());

        query = query.replaceAll("@optionalActionValuesFilter", replaceOptionalValues());
        return query;
    }

    private String replaceOptionalValues () {
        String query = "";
        int count = 0;
        for(MapEntry<String, String> filter: optionalFilters) {
            count++;
            String subQuery = OptionalActionValueFilter;
            subQuery = subQuery.replaceAll("@tableAlias", "av"+count);
            subQuery = subQuery.replaceAll("@AKEY", filter.getKey().trim());
            subQuery = subQuery.replaceAll("@AVALUE", filter.getValue().trim());
            query+= subQuery;
        }
        return query;
    }

}
