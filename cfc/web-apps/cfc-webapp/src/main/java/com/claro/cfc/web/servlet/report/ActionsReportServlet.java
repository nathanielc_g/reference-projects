package com.claro.cfc.web.servlet.report;


import com.claro.cfc.web.util.MapEntry;
import com.claro.cfc.web.util.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Nathaniel Calderon on 9/3/2015.
 */
public class ActionsReportServlet extends BaseReportServlet {

    private final static Logger log = Logger.getLogger(ActionsReportServlet.class);
    private final static String queryReport = "SELECT A.ACTION_ID FORM_ID, A.CODE ITEM_CODE, A.FORM_TYPE,A.CREATED, A.ORDER_TYPE, A.USER_ID, AV.AKEY, AV.AVALUE FROM ACTIONS A JOIN ACTION_VALUES AV ON  AV.ACTION_ID = A.ACTION_ID";
    private final static String queryPartOrderBy = "ORDER BY A.CODE";
    private final static String queryPartWhereDefault = " WHERE A.CREATED >= TO_TIMESTAMP('@created')";
    private List<MapEntry<String, String>> fieldsname;

    @Override
    protected void initBase() {
        this.queryFilters = "";
        this.fieldsname = new ArrayList<MapEntry<String, String>>() {{
            add(new MapEntry<String, String>("@created", "A.CREATED"));
        }};
    }

    @Override
    protected List<MapEntry<String, String>> getFieldsname() {
        return fieldsname;
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        super.doPost(req, resp);
        String [] parts = req.getRequestURI().split("/");
        if (null == parts || 4 != parts.length)
            return;

        log.info("Trying to download file for " + Arrays.toString(parts));

        String lastPath = StringUtils.getAsNonNull(parts[3]);
        if (null == resp.getOutputStream())
            return;

        log.info("User download report " + lastPath);

        resp.setContentType("application/octet-stream");
        resp.setHeader("Content-Disposition", "attachment;filename=actions-report.csv");

        super.writeOutputStreamReport(resp.getOutputStream());
        //writeOutputStreamReport(resp.getOutputStream(), resp, StringUtils.getAsNonNull(parts[3]));

    }

    @Override
    protected String getQueryPartWhere() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - 15);
        return (queryFilters.isEmpty()? queryPartWhereDefault.replaceAll("@created", formatter.format(cal.getTime())) : " where " + queryFilters);
    }

    @Override
    protected String getQueryReport() {
        return queryReport;
    }

    @Override
    protected String getQueryPartOrderBy() {
        return queryPartOrderBy;
    }

    @Override
    protected void injectHeaders(StringBuilder builder) {
        if (null == builder)
            return;
        builder.append("FORM_ID").append(",").append("ITEM_CODE").append(",").append("FORM_TYPE")
                .append(",").append("CREATED").append(",").append("ORDER_TYPE").append(",").append("USER_ID")
                .append(",").append("AKEY")
                .append(",").append("AVALUE")
                .append("\r\n");
    }

    /*private void writeOutputStreamReport(OutputStream out, HttpServletResponse res, String lastPath) {


        writeReportLinesByCreated(DateUtils.addDays(-15),out);
    }*/


    /*private void writeReportLinesByCreated(Date created, OutputStream out) {
        Session session = SessionFactoryHelper.getSessionFactory().openSession();
        try
        {

            SQLQuery sqlQuery = session.createSQLQuery(actionsByDateQuery);
            sqlQuery.setDate("CREATED", created);
            log.info("Trying to get Actions by created: " + created);
            List results = sqlQuery.list();
            if (null == results || 0 >= results.size())
                return;

            StringBuilder builder = new StringBuilder();
            injectHeaders(builder);
            out.write(builder.toString().getBytes());
            builder.setLength(0);

            Iterator iterator = results.iterator();

            while (iterator.hasNext()) {
                Object[] row = (Object[])iterator.next();
                for (int col = 0; col < row.length; col++) {
                    builder.append(row[col]).append(",");
                }
                builder.append("\r\n");
                out.write(builder.toString().getBytes());
                builder.setLength(0);
            }
			results.clear();
        }catch(Exception ex){
            log.error(ex);
        }finally {
            log.info("Actions Model Session Closed in writeReportLinesByCreated!");
            if(session != null && session.isConnected()){
                session.flush();
                session.close();
            }
        }
    }*/

//
//    private List<String> getReportLinesByCreated(Date created) {
//        Session session = SessionFactoryHelper.getSessionFactory().openSession();
//        try
//        {
//            SQLQuery sqlQuery = session.createSQLQuery(actionsByDateQuery);
//            sqlQuery.setDate("CREATED", created);
//            log.info("Trying to get Actions by created: " + created);
//            List results =  sqlQuery.list();
//
//            if (null == results || 0 >= results.size())
//                return Collections.EMPTY_LIST;
//
//            List<String> lines = new ArrayList<String>();
//            StringBuilder builder = new StringBuilder();
//            injectHeaders(builder);
//            lines.add(builder.toString());
//            builder.setLength(0);
//
//            Iterator iterator = results.iterator();
//
//            while (iterator.hasNext()) {
//                Object[] row = (Object[])iterator.next();
//                for (int col = 0; col < row.length; col++) {
//                    builder.append(row[col]).append(",");
//                }
//                builder.setLength(builder.length()-1);
//                builder.append("\r\n");
//                lines.add(builder.toString());
//                builder.setLength(0);
//            }
//            return lines;
//        }catch(Exception ex){
//            log.error(ex);
//        }finally {
//            log.info("Actions Model Session Closed in getActionsByCreated!");
//            if(session != null && session.isConnected()){
//                session.flush();
//                session.close();
//            }
//        }
//        return Collections.EMPTY_LIST;
//    }
//



}
