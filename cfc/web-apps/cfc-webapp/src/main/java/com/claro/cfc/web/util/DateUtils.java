package com.claro.cfc.web.util;

import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Nathaniel Calderon on 9/3/2015.
 */
public final class DateUtils {
    private final Logger log = Logger.getLogger(DateUtils.class);

    public static Date addDays (int days) {
        //SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, days);
        //cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + days);
        //formatter.format(cal.getTime())
        return cal.getTime();
    }


}
