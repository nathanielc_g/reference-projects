package com.claro.cfc.web.servlet.service;

import com.claro.cfc.web.servlet.service.context.InitialContext;

/**
 * Created by Christopher Herrera on 1/20/14.
 */
public final class ServiceLocator {
    private static Cache cache;

    static {
        cache = new Cache();
    }

    public static Service getService(String serviceClass) {
        Service service = cache.getService(serviceClass);

        if (service != null) {
            return service;
        }

        InitialContext ic = new InitialContext();

        Service newService = (Service) ic.lookup(serviceClass);
        cache.addService(newService);
        return newService;
    }
}
