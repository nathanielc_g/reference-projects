package com.claro.cfc.web.servlet.service;

import org.apache.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by Christopher Herrera on 1/22/14.
 */
public abstract class AbstractService implements Service {
    Logger log = Logger.getLogger(AbstractService.class);
    protected Object target;

    public AbstractService(Object target) {
        this.target = target;
    }

    public final Object execute(String name, Map<String, Object[]> params) {
        Class<?> cls = target.getClass();
        Method m;
        Object ret = null;
//        List types = new ArrayList();

//        if (params != null && !params.isEmpty()) {
//            for (Map.Entry<String, Object[]> entry : params.entrySet()) {
//                if (entry.getClass() == null) {
//                    continue;
//                }
//                types.add(entry.getClass());w
//            }
//        } else {
//            types = null;
//            params = null;
//        }


//        log.info( "Types class: "+types );

        try {
//            m = cls.getDeclaredMethod( name,Map.class );

            if (params == null ) {
                m = cls.getDeclaredMethod(name,null);
                ret = m.invoke(target);
            } else {
                m = cls.getDeclaredMethod(name, Map.class);
                ret = m.invoke(target, params);
            }

            log.info( "Returned execution method: "+ m.getName() );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            log.error(e);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            log.error(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            log.error(e);
        }


        return ret;
    }
}
