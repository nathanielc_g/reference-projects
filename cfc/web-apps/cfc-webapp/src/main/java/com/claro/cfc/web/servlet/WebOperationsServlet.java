package com.claro.cfc.web.servlet;

import com.claro.cfc.web.servlet.service.Service;
import com.claro.cfc.web.servlet.service.ServiceLocator;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by Christopher Herrera on 1/20/14.
 */
public class WebOperationsServlet implements Servlet {
    ServletConfig sc = null;
    ServletContext ctx = null;
    private static Logger log = Logger.getLogger(WebOperationsServlet.class);
    Gson gson = new Gson();

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        this.sc = servletConfig;
        ctx = sc.getServletContext();
        log.info(sc.getServletName() + " Called");
    }

    @Override
    public ServletConfig getServletConfig() {
        return sc;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {

        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;
        res.setCharacterEncoding("UTF-8");

        HttpSession session = req.getSession();

        String requestURIs = req.getRequestURI();

        StringTokenizer tokens = new StringTokenizer(requestURIs,"/");

        while( tokens.hasMoreTokens() && !tokens.nextToken().contains("operation")) continue;

        String className = tokens.nextToken();
        String methodName = tokens.nextToken();

        Map<String, Object[]> parametros = req.getParameterMap();

        log.info("Operations servlet intercept request: " + parametros);

        String serviceClass =sc.getInitParameter( className );
        log.info( "Preparing to search service "+serviceClass
        );

        log.info("Parameters as json " + gson.toJson(parametros));
        if ( serviceClass == null) {
            //redirect
            res.sendRedirect("./index.jsp");
            return;
        }
        Service serv = ServiceLocator.getService(sc.getInitParameter(className));
        if (null == serv) {
            log.error( "" );
            res.sendRedirect("./index.jsp");
            return;
        }
        Object respp = serv.execute(methodName, parametros);
        Gson gson = new Gson();

        String jsonResponse = gson.toJson(respp);

        log.info( "Response as json=> "+ jsonResponse  );
        res.setContentType("application/json");
        PrintWriter out = res.getWriter();
        out.print(jsonResponse);
        out.flush();
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {

    }
}
