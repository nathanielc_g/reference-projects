package com.claro.cfc.web.servlet.service.model.dao;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.db.dao.TaskAccess;
import com.claro.cfc.db.dto.enums.TaskTypeEnum;
import com.claro.cfc.web.servlet.service.model.dto.Task;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nathaniel Calderon on 4/7/2017.
 */
public class TaskDataAccess {

    private static final Logger log = Logger.getLogger(TaskDataAccess.class);

    public static final List<Task> getTasksByQuery(String query) {
        Session session = HibernateHelper.getSessionFactory().openSession();
        List<Task> tasks = new ArrayList<Task>(0);
        try {
            SQLQuery sqlQuery = session.createSQLQuery(query);
            tasks = sqlQuery
                    .addScalar("taskId", new LongType())
                    .addScalar("actionId", new LongType())
                    .addScalar("code", new LongType())
                    .addScalar("created", new StringType())
                    .addScalar("userId", new StringType())
                    .addScalar("formType", new StringType())
                    .addScalar("taskResponseType", new StringType())
                    .addScalar("taskLogMessage", new StringType())
                    .setResultTransformer(Transformers.aliasToBean(Task.class))
                    .list();
            //tasks = sqlQuery.list();
        } catch (HibernateException ex){
            log.info("Error trying to get tasks by custom query.", ex);
            log.info("Query: "+query);
        } finally {
            HibernateHelper.finalizeOpenSession(session);
        }
        return tasks;
    }

    public static final void createTask (String taskTypeName, long actionId) {
        TaskAccess.createTask(TaskTypeEnum.valueOf(taskTypeName), actionId);
    }


}
