package com.claro.cfc.web.servlet.service.context;

/**
 * Created by Christopher Herrera on 1/20/14.
 */
public class InitialContext {

    public Object lookup(String className) {
        Class cls;
        Object clsInstance = null;
        try {
            cls = Class.forName(className);
            clsInstance = cls.newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return clsInstance;
    }
}
