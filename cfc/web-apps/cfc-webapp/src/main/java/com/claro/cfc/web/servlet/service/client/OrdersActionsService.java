package com.claro.cfc.web.servlet.service.client;

import com.claro.cfc.web.servlet.service.AbstractService;
import com.claro.cfc.web.servlet.service.model.OrdersActionsServiceModel;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 6/1/2014.
 */
public class OrdersActionsService extends AbstractService {
    public OrdersActionsService() {
        super(new OrdersActionsServiceModel());
    }
}
