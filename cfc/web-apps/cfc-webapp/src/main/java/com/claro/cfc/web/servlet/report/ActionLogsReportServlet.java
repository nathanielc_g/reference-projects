package com.claro.cfc.web.servlet.report;

import com.claro.cfc.web.util.MapEntry;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ActionLogsReportServlet extends BaseReportServlet {
    private final static Logger log = Logger.getLogger(ActionLogsReportServlet.class);

    private final static String queryReport =
            "select\n" +
                    "ul.TARGET FORM_ID\n" +
                    ",ul.ACTION LOG_TYPE\n" +
                    ",ul.USER_ID TARJETA_TECNICO\n" +
                    ",ul.CREATED FECHA\n" +
                    ",ul.MODIFIED_BY TARJETA_USUARIO\n" +
                    ",ul.MESSAGE LOG_MESSAGE\n" +
                    "from\n" +
                    "user_log ul\n" +
                    "where ul.ACTION = 'CHECK_ACTION_HAND_OPERATED'\n" +
                    "  or ul.ACTION = 'CHECK_ACTION_PROCESSED_BY'\n" +
                    "  or ul.ACTION = 'UNCHECK_ACTION_HAND_OPERATED'\n" +
                    "  and @filters\n";
    private final static String queryPartOrderBy = "";
    private final static String queryPartWhereDefault = "ul.created >= sysdate-7";
    private List<MapEntry<String, String>> fieldsname;

    @Override
    protected void initBase() {
        this.queryFilters = "";
        this.fieldsname = new ArrayList<MapEntry<String, String>>() {{
            add(new MapEntry<String, String>("@created", "ul.created"));
        }};
    }

    @Override
    protected List<MapEntry<String, String>> getFieldsname() {
        return fieldsname;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        super.doPost(req, res);
        res.setContentType("application/octet-stream");
        res.setHeader("Content-Disposition", "attachment;filename=action-logs-report.csv");
        log.info("*****Getting Action Logs report");
        super.writeOutputStreamReport(res.getOutputStream(), getQueryStatement());
    }

    @Override
    protected String getQueryReport() {
        return queryReport;
    }

    @Override
    protected String getQueryPartWhere() {
        return (queryFilters == "" ? queryPartWhereDefault : queryFilters);
    }

    @Override
    protected String getQueryPartOrderBy() {
        return queryPartOrderBy;
    }

    protected void injectHeaders(StringBuilder builder) {
        if (null == builder)
            return;

        builder.append("FORM_ID")
                .append(",").append("LOG_TYPE")
                .append(",").append("TARJETA_TECNICO")
                .append(",").append("TELEFONO")
                .append(",").append("FECHA")
                .append(",").append("TARJETA_USUARIO")
                .append(",").append("LOG_MESSAGE")
                .append("\r\n");
    }
}
