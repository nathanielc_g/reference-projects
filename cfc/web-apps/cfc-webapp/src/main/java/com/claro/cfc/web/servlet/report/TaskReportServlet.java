package com.claro.cfc.web.servlet.report;

import com.claro.cfc.web.util.MapEntry;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 10/3/2014.
 */
public class TaskReportServlet extends BaseReportServlet {

    private final static Logger log = Logger.getLogger(TaskReportServlet.class);

//    Working one
//    private final static String queryReport = "SELECT AC.ACTION_ID FORM_ID, AC.CODE, AC.FORM_TYPE, TS.STATUS, T.CREATED, T.MODIFIED, TRT.RTYPE, TL.SUCCESS, REPLACE(TL.MESSAGE,',', ';') MESSAGE FROM TASK T JOIN ACTIONS AC ON T.ACTION_ID = AC.ACTION_ID LEFT JOIN TASK_LOG TL ON T.TASK_ID = TL.TASK_ID JOIN TASK_STATUS TS ON T.STATUS = TS.STATUS_ID LEFT JOIN TASK_RESPONSE_TYPE TRT ON TL.RESPONSE_TYPE = TRT.RID";

    private final static String queryReport =
            "WITH pivot_data AS( " +
                     "SELECT AC.ACTION_ID FORM_ID" +
                    ", AC.CODE" +
                    ", NVL(AC.FORM_TYPE, '[Empty]') FORM_TYPE" +
                    ", NVL(TS.STATUS, '[Empty]') STATUS" +
                    ", T.CREATED" +
                    ", T.MODIFIED" +
                    ", NVL(AV.AVALUE, '[Empty]') TELEPHONE" +
                    ", AC.HAND_OPERATED_BY MANUAL" +
                    ", '_' || NVL(TL.UPDATED_ATTRS, '[Empty]') UPDATED_ATTRS" +
                    ", NVL(TRT.RTYPE, '[Empty]') RTYPE" +
                    ", REPLACE(NVL(TL.MESSAGE, '[Empty]'),',', ';') MESSAGE" +
                    ", TT.TYPE_NAME " +
                    "FROM TASK T " +
                    "JOIN ACTIONS AC ON T.ACTION_ID = AC.ACTION_ID " +
                    "LEFT JOIN TASK_LOG TL ON TL.TASK_ID = T.TASK_ID " +
                    "JOIN TASK_STATUS TS ON TS.STATUS_ID = T.STATUS " +
                    "LEFT JOIN TASK_RESPONSE_TYPE TRT ON TL.RESPONSE_TYPE = TRT.RID " +
                    "JOIN TASK_TYPE TT ON T.TASK_TYPE_ID = TT.TASK_TYPE_ID " +
                    "LEFT JOIN ACTION_VALUES AV ON AC.ACTION_ID = AV.ACTION_ID AND AV.AKEY = 'TELEFONO' " +
                    "WHERE TT.TYPE_NAME IN ('DOMAIN', 'M6', 'GEFF', 'SAD') and @filters " +
                    "GROUP BY AC.ACTION_ID" +
                    ", AC.CODE" +
                    ", AC.FORM_TYPE" +
                    ", TS.STATUS" +
                    ", T.CREATED" +
                    ", T.MODIFIED" +
                    ", AV.AVALUE" +
                    ", AC.HAND_OPERATED_BY" +
                    ", TL.UPDATED_ATTRS" +
                    ", TRT.RTYPE" +
                    ", TL.MESSAGE" +
                    ", TT.TYPE_NAME)" +
                    " SELECT * " +
                    "FROM pivot_data PIVOT (" +
                    "MAX(CREATED) as CREATED" +
                    ", MAX(MODIFIED) as MODIFIED" +
                    ", MAX(STATUS) as STATUS" +
                    ", MAX(RTYPE) as RESPONSE_TYPE" +
                    ", MAX(MESSAGE) as MESSAGE FOR TYPE_NAME IN ('DOMAIN' as DOMAIN" +
                    ", 'M6' as M6, 'GEFF' as GEFF, 'SAD' as SAD)) " +
                    "T";
    private final static String queryPartOrderBy = "";
    private final static String queryPartWhereDefault = " T.CREATED >= sysdate-7";
    private List<MapEntry<String, String>> fieldsname;

    @Override
    protected void initBase() {
        this.queryFilters = "";
        this.fieldsname = new ArrayList<MapEntry<String, String>>() {{
            add(new MapEntry<String, String>("@created", "T.CREATED"));
        }};
    }

    @Override
    protected List<MapEntry<String, String>> getFieldsname() {
        return fieldsname;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        super.doPost(req, res);
        res.setContentType("application/octet-stream");
        res.setHeader("Content-Disposition", "attachment;filename=tasks-report.csv");
        log.info("Getting task reports");
        super.writeOutputStreamReport(res.getOutputStream(), getQueryStatement());
    }

    @Override
    protected String getQueryReport() {
        return queryReport;
    }

    @Override
    protected String getQueryPartWhere() {
        return (queryFilters == "" ? queryPartWhereDefault : queryFilters);
    }

    @Override
    protected String getQueryPartOrderBy() {
        return queryPartOrderBy;
    }

    protected void injectHeaders(StringBuilder builder) {
        if (null == builder)
            return;

//        Working one
//        builder.append("FORM_ID").append(",").append("DISPATCH_ITEM").append(",").append("FORM_TYPE").append(",").append("STATUS")
//                .append(",").append("TASK_CREATED").append(",").append("TASK_MODIFIED").append(",").append("RESPONSE_TYPE")
//                .append(",").append("SUCCESS").append(",").append("RESPONSE_MESSAGE").append("\r\n");

        builder.append("FORM_ID")
                .append(",").append("DISPATCH_ITEM")
                .append(",").append("FORM_TYPE")
                .append(",").append("TELEPHONE")
                .append(",").append("HAND_OPERATED_BY")
                .append(",").append("UPDATED_ATTRS")
                .append(",").append("DOMAIN_TASK_CREATED")
                .append(",").append("DOMAIN_TASK_MODIFIED")
                .append(",").append("DOMAIN_STATUS")
                .append(",").append("DOMAIN_RESPONSE_TYPE")
                .append(",").append("DOMAIN_MESSAGE")
                .append(",").append("M6_TASK_CREATED")
                .append(",").append("M6_TASK_MODIFIED")
                .append(",").append("M6_STATUS")
                .append(",").append("M6_RESPONSE_TYPE")
                .append(",").append("M6_MESSAGE")
                .append(",").append("GEFF_TASK_CREATED")
                .append(",").append("GEFF_TASK_MODIFIED")
                .append(",").append("GEFF_STATUS")
                .append(",").append("GEFF_RESPONSE_TYPE")
                .append(",").append("GEFF_MESSAGE")
                .append(",").append("SAD_TASK_CREATED")
                .append(",").append("SAD_TASK_MODIFIED")
                .append(",").append("SAD_STATUS")
                .append(",").append("SAD_RESPONSE_TYPE")
                .append(",").append("SAD_MESSAGE").append("\r\n");
    }

    /*private String queryFor3DaysBefore(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - 15);

        return "SELECT AC.ACTION_ID FORM_ID, AC.CODE, AC.FORM_TYPE, TS.STATUS, T.CREATED, T.MODIFIED, TRT.RTYPE, TL.SUCCESS, REPLACE(TL.MESSAGE,',', ';') MESSAGE FROM TASK T JOIN ACTIONS AC ON T.ACTION_ID = AC.ACTION_ID LEFT JOIN TASK_LOG TL ON T.TASK_ID = TL.TASK_ID JOIN TASK_STATUS TS ON T.STATUS = TS.STATUS_ID LEFT JOIN TASK_RESPONSE_TYPE TRT ON TL.RESPONSE_TYPE = TRT.RID WHERE T.CREATED >= TO_TIMESTAMP('"+formatter.format(cal.getTime())+"')";
    }*/
}

