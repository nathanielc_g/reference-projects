package com.claro.cfc.web.servlet.service;

import java.util.Map;

/**
 * Created by Christopher Herrera on 1/20/14.
 *
 * Service Locator pattern by Jansel (Vanwolf)
 */
public interface Service {
    public Object execute(String method, Map<String, Object[]> params);
}
