package com.claro.cfc.web.util;

import java.util.Map;

/**
 * Created by Nathaniel Calderon on 4/7/2017.
 */
public final class ParamsMapUtil {
    private Map<String, Object[]> params;
    public ParamsMapUtil(Map<String, Object[]> params) {
        this.params = params;
    }
    public String getAsString (String key) throws IllegalArgumentException {
        return getAsObject(key).toString();
    }

    public String getAsString (String key, int index) throws IllegalArgumentException {
        return getAsObject(key, index).toString();
    }

    /*public String getAsString (String key, String defaultValue) {

    }*/

    public Object getAsObject (String key) throws IllegalArgumentException {
        return getAsObject(key, 0);
    }

    public Object getAsObject (String key, int index) throws IllegalArgumentException {
        Object[] value = params.get(key);
        if (null == value)
            throw new IllegalArgumentException("Key: "+key+" cannot be found");
        return value[index];
    }

    public int getAsInteger(String key) throws IllegalArgumentException {
        return Integer.parseInt(getAsString(key));
    }

    public int getAsInteger(String key, int index) throws IllegalArgumentException {
        return Integer.parseInt(getAsString(key, index));
    }

    public long getAsLong(String key) throws IllegalArgumentException {
        return Long.parseLong(getAsString(key));
    }

    public long getAsLong (String key, int index) throws IllegalArgumentException {
        return Long.parseLong(getAsString(key, index));
    }

    public boolean exist (String key) {
        return params.containsKey(key);
    }

 }
