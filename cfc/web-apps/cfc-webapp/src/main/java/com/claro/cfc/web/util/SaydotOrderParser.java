package com.claro.cfc.web.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import org.apache.log4j.Logger;

import java.io.StringReader;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 1/29/2015.
 */
public final class SaydotOrderParser {

    private static final Logger log = Logger.getLogger(SaydotOrderParser.class);

    public static final String KEY_CODE = "CODE";
    public static final String KEY_TYPE = "TYPE";
    public static final String KEY_ASSIGNED = "FECHA_ASIGNACION";
    public static final String KEY_SERVICE_PHONE = "NUMEROSERVICIO";
    public static final String KEY_LOCATION_TYPE = "TIPO_LOCALIDAD";

    public static JsonArray parseToKeyValuePair(String jsonOrders) {
        JsonArray kvpArrayReturn = new JsonArray();

        if (null == jsonOrders || "".equals(jsonOrders)) {
            log.info("Could'nt be parsed json come to SaydotOrders; got empty");
            return kvpArrayReturn;
        }

        Gson gson = new Gson();
        try {
            JsonReader jReader = new JsonReader(new StringReader(jsonOrders.trim()));

            /**
             * Json string is possible come deformed
             */
            jReader.setLenient(true);

            JsonObject objetoResponse = gson.fromJson(jReader, JsonObject.class);
            log.debug(objetoResponse);

            JsonArray ordersArray = objetoResponse.get("OrdenesAsignadas").getAsJsonArray();

            if (null == ordersArray)
                return kvpArrayReturn;

            for (int i = 0; ordersArray.size() > i; ++i) {

                JsonArray orderArray = (JsonArray) ordersArray.get(i);

                JsonObject kvpReturn = new JsonObject();

                for (int j = 0; orderArray.size() > j; ++j) {
                    JsonObject kvp = (JsonObject) orderArray.get(j);

                    String key = kvp.get("Key").getAsString();
                    String value = kvp.get("Value").getAsString();

                    if ("CODIGO".equalsIgnoreCase(key)) {
                        kvpReturn.addProperty(KEY_CODE, value);
                    } else if ("TIPO_ORDEN".equalsIgnoreCase(key)) {
                        kvpReturn.addProperty(KEY_TYPE, value);
                    } else if (KEY_ASSIGNED.equals(key)) {
                        kvpReturn.addProperty(KEY_ASSIGNED, value);
                    } else if (KEY_SERVICE_PHONE.equals(key)) {
                        kvpReturn.addProperty(KEY_SERVICE_PHONE, value);
                    } else if (KEY_LOCATION_TYPE.equals(key))
                        kvpReturn.addProperty(KEY_LOCATION_TYPE, value);
                }
                /**
                 * Here we ensure that kvpReturn is a valid "code" and "type" object
                 */
                if (null != kvpReturn.get(KEY_CODE) && null != kvpReturn.get(KEY_TYPE))
                    kvpArrayReturn.add(kvpReturn);
            }
        } catch (Exception ex) {
            log.error("Could no parse json, due on possible malformed json ");
            log.error(ex);
        }
        return kvpArrayReturn;
    }
}
