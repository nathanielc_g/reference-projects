package com.claro.cfc.web.servlet.report;

import com.claro.cfc.web.util.MapEntry;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nathaniel Calderon on 10/21/2016.
 *
 * This servlet was created for test queries of reports.
 *
 */
public class RandomReportServlet extends BaseReportServlet {
    private final static Logger log = Logger.getLogger(RandomReportServlet.class);

    private final static String queryReport = "@filters";
    private final static String queryPartOrderBy = "";
    private final static String queryPartWhereDefault = "";
    private List<MapEntry<String, String>> fieldsname;

    @Override
    protected void initBase() {
        this.queryFilters = "";
        this.fieldsname = new ArrayList<MapEntry<String, String>>();
    }

    @Override
    protected List<MapEntry<String, String>> getFieldsname() {
        return fieldsname;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        super.doGet(req, res);
        res.setContentType("application/octet-stream");
        res.setHeader("Content-Disposition", "attachment;filename=random-report.csv");
        log.info("Getting Random report");
        super.writeOutputStreamReport(res.getOutputStream(), getQueryStatement());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        super.doPost(req, res);
        res.setContentType("application/octet-stream");
        res.setHeader("Content-Disposition", "attachment;filename=random-report.csv");
        log.info("Getting Random reports");
        super.writeOutputStreamReport(res.getOutputStream(), getQueryStatement());
    }

    @Override
    protected String getQueryReport() {
        return queryReport;
    }

    @Override
    protected String getQueryPartWhere() {
        return queryFilters;
    }

    @Override
    protected String getQueryPartOrderBy() {
        return queryPartOrderBy;
    }

    protected void injectHeaders(StringBuilder builder) {
        if (null == builder)
            return;

        String [] columns = super.columns.split(",");
        for(String column: columns){
            builder.append(column).append(",");
        }
        builder.deleteCharAt(builder.length()-1);
        builder.append("\r\n");

    }
}
