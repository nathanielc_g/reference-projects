package com.claro.cfc.web.servlet.service.client;

import com.claro.cfc.web.servlet.service.AbstractService;
import com.claro.cfc.web.servlet.service.model.DispositivoServiceModel;

/**
 * Created by Christopher Herrera on 1/20/14.
 * <p/>
 */
public class DispositivoService extends AbstractService {

    public DispositivoService() {
        super(new DispositivoServiceModel());
    }
}
