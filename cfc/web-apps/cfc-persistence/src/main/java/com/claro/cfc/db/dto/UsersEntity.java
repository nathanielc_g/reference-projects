package com.claro.cfc.db.dto;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/13/14.
 */
@Entity
@Table(name = "USERS", catalog = "")

@NamedQueries({
        @NamedQuery(name = "users.count", query = "SELECT COUNT(e) FROM UsersEntity e WHERE e.status=:status"),
        @NamedQuery(name = "users.phonesByAppVersion", query = "SELECT u.phoneNumber FROM UsersEntity u WHERE u.terminal.appVersion < :appVersion ")
})
public class UsersEntity {
    private String userId;
    private Long status;
    private Timestamp created;
    private Timestamp modified;
    private Timestamp expirationDate;
    private Timestamp reactiveDate;
    //    private List<ActionsEntity> actions;
    private TerminalEntity terminal;
    private String phoneNumber = "(809) 220-1111";

    @Id
    @Column(name = "USER_ID")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "STATUS")
    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    @Basic
    @Column(name = "CREATED")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "MODIFIED")
    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @Basic
    @Column(name = "EXPIRATION_DATE")
    public Timestamp getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Timestamp expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Basic
    @Column(name = "REACTIVE_DATE")
    public Timestamp getReactiveDate() {
        return reactiveDate;
    }

    public void setReactiveDate(Timestamp reactiveDate) {
        this.reactiveDate = reactiveDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UsersEntity)) return false;

        UsersEntity that = (UsersEntity) o;

        return userId != null ? userId.equals(that.userId) : that.userId == null;
    }

    @Override
    public int hashCode() {
        return userId != null ? userId.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "UsersEntity{" +
                "userId='" + userId + '\'' +
                ", status=" + status +
                ", created=" + created +
                ", modified=" + modified +
                ", expirationDate=" + expirationDate +
                ", reactiveDate=" + reactiveDate +
                '}';
    }

    //
//    @OneToMany(mappedBy = "user")
//    @Fetch(FetchMode.SELECT)
//    public List<ActionsEntity> getActions() {
//        return actions;
//    }
//
//    public void setActions(List<ActionsEntity> actions) {
//        this.actions = actions;
//    }

    @ManyToOne
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "IMEI", referencedColumnName = "IMEI", nullable = false)
    public TerminalEntity getTerminal() {
        return terminal;
    }

    public void setTerminal(TerminalEntity terminal) {
        this.terminal = terminal;
    }

    @Basic
    @Column(name = "PHONE_NUMBER")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
