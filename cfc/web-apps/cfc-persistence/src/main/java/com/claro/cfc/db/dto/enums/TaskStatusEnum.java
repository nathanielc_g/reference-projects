package com.claro.cfc.db.dto.enums;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/6/2014.
 */
public enum TaskStatusEnum {
    PENDING{
        @Override
        public long id() {
            return 1L;
        }
    }, PROCESSED{
        @Override
        public long id() {
            return 2L;
        }
    }, ABORTED{
        @Override
        public long id() {
            return 21L;
        }
    };


    /**
     *
     * @return  data base record id, according with the default value inserted in CFC at schema creation time.
     */
    public abstract long id();
}
