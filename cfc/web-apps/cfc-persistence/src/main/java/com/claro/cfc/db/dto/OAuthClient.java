package com.claro.cfc.db.dto;

import javax.persistence.*;

/**
 * Created by Jansel Valentin on 11/8/2016.
 **/

@Entity
@Table(name = "OAUTH_CLIENT", catalog = "")
public class OAuthClient {

    private String clientId;
    private String displayName;
    private String scopes;
    private String redirectUrl;
    private Integer tokenValidity;
    private String environment;

    @Id
    @Column(name = "CLIENT_ID")
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Basic
    @Column(name = "DISPLAY_NAME")
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Basic
    @Column(name = "SCOPES")
    public String getScopes() {
        return scopes;
    }

    public void setScopes(String scopes) {
        this.scopes = scopes;
    }

    @Basic
    @Column(name = "WEB_SERVER_REDIRECT_URL")
    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    @Basic
    @Column(name = "ACCESS_TOKEN_VALIDITY")
    public Integer getTokenValidity() {
        return tokenValidity;
    }

    public void setTokenValidity(Integer tokenValidity) {
        this.tokenValidity = tokenValidity;
    }

    @Basic
    @Column(name = "ENVIRONMENT")
    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OAuthClient that = (OAuthClient) o;

        if (clientId != that.clientId) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = (tokenValidity ^ (tokenValidity >>> 32));
        result = 31 * result + (clientId != null ? clientId.hashCode() : 0);
        result = 31 * result + (displayName != null ? displayName.hashCode() : 0);
        return result;
    }
}
