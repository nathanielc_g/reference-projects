package com.claro.cfc.db.dao.service;

import com.claro.cfc.db.dto.TerminalEntity;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Nathaniel Calderon on 5/25/2017.
 */
public class TerminalService {
    public static TerminalEntity buildTerminal (String imei, Timestamp created, String model, String brand, Double osVersion, Double appVersion) {
        TerminalEntity terminal = new TerminalEntity();
        terminal.setCreated(created);
        terminal.setImei(imei);
        terminal.setTmodel(model);
        terminal.setTbrand(brand);
        terminal.setOsVersion(osVersion);
        terminal.setAppVersion(appVersion);
        return terminal;
    }
}
