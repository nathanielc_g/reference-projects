package com.claro.cfc.db.dto;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/13/14.
 */
@Entity
@Table(name = "ACTION_VALUES", catalog = "")
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "actions.values")
public class ActionValuesEntity {
    private long valueId;
    private String akey;
    private String avalue;
    private Timestamp created;
    private ActionsEntity action;

    @Id
    @Column(name = "VALUE_ID")
    @GeneratedValue(generator = "values.sec_action_values", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "values.sec_action_values", sequenceName = "sec_action_values")
    public long getValueId() {
        return valueId;
    }

    public void setValueId(long valueId) {
        this.valueId = valueId;
    }

    @Basic
    @Column(name = "AKEY")
    public String getAkey() {
        return akey;
    }

    public void setAkey(String akey) {
        this.akey = akey;
    }

    @Basic
    @Column(name = "AVALUE")
    public String getAvalue() {
        return avalue;
    }

    public void setAvalue(String avalue) {
        this.avalue = avalue;
    }

    @Basic
    @Column(name = "CREATED")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ActionValuesEntity)) return false;

        ActionValuesEntity that = (ActionValuesEntity) o;

        return valueId == that.valueId;
    }

    @Override
    public int hashCode() {
        return (int) (valueId ^ (valueId >>> 32));
    }

    @Override
    public String toString() {
        return "ActionValuesEntity{" +
                "valueId=" + valueId +
                ", akey='" + akey + '\'' +
                ", avalue='" + avalue + '\'' +
                ", created=" + created +
                '}';
    }

    @Fetch(FetchMode.SELECT)
    @ManyToOne
    @JoinColumn(name = "ACTION_ID", referencedColumnName = "ACTION_ID", nullable = false)
    public ActionsEntity getAction() {
        return action;
    }

    public void setAction(ActionsEntity action) {
        this.action = action;
    }
}
