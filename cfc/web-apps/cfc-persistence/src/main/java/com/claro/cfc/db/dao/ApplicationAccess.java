package com.claro.cfc.db.dao;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.db.dto.enums.AppConfigEnum;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.hibernate.jdbc.Work;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 7/25/2014.
 */
public final class ApplicationAccess {

    private static final Logger log = Logger.getLogger(ApplicationAccess.class);

    private static final String mVerisonQuer      = "SELECT CONFIG_VALUE FROM APP_CONFIG where CONFIG_KEY = :CONFIG_KEY";
    private static final String mActiveRangeQuery = "SELECT CONFIG_VALUE FROM app_config where config_key = :AVAILABLE_TIME";
    public static final String CAN_POST_ORDER_MULTIPLE_TIMES = "SELECT CONFIG_VALUE FROM app_config where state = 1 and config_key = 'POST_ORDER_MULTIPLE_TIMES'";

    static class InfoDB implements Work {
        private String dbUrlConnection = "";
        @Override
        public void execute(Connection connection) throws SQLException {
            dbUrlConnection = connection.getMetaData().getURL();
        }
        public String getDbUrlConnection() {
            return dbUrlConnection;
        }
    }

    public static String getVersion() {
        log.info("Getting application version");

        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            SQLQuery sqlQuery = session.createSQLQuery(mVerisonQuer);
            sqlQuery.setParameter("CONFIG_KEY", AppConfigEnum.APP_VERSION.toString());
            sqlQuery.addScalar("CONFIG_VALUE");
            Object result = sqlQuery.uniqueResult();
            if (null == result || result.toString().isEmpty())
                return "";

            return result.toString();

        }catch(Exception ex){
            log.error("",ex);
        }finally {
            log.info("Actions Model Session Closed in getVersion!");
            if(session != null && session.isConnected()){
                session.flush();
                session.clear();
                session.close();
            }
        }
        return "";
    }

    public static String getAvailableTime () {
        Session session = HibernateHelper.getSessionFactory().openSession();
        try {

            SQLQuery sqlQuery = session.createSQLQuery(mActiveRangeQuery);
            sqlQuery.setParameter("AVAILABLE_TIME", AppConfigEnum.AVAILABLE_TIME.toString());
            sqlQuery.addScalar("CONFIG_VALUE");
            Object result = sqlQuery.uniqueResult();

            if (null == result || result.toString().isEmpty())
                return "";

            return result.toString();
        }catch(Exception ex){
            log.error("",ex);
        }finally {
            log.info("AppConfig Model Session Closed in getAvailableTime!");
            if(session != null && session.isConnected()){
                session.flush();
                session.clear();
                session.close();
            }
        }
        return "";
    }

    public static void updateAppConfig (String configKey, String configValue) {

        log.info("Entering to update ConfigValue to ConfigKey= " + configKey);
        Session session = HibernateHelper.getSessionFactory().openSession();

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.getNamedQuery("appConfig.configValue");
            query.setParameter("configKey", configKey);
            query.setParameter("configValue", configValue);
            query.executeUpdate();
            tx.commit();
        } catch (HibernateException ex) {
            if (null != tx && !tx.wasCommitted())
                tx.rollback();
            log.error("Some error trying to update config value on AppConfig");
            ex.printStackTrace();
        } finally {
            log.info("AppConfig Model Session Closed in updateAppConfig!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.close();
            }
        }
    }

    public static boolean canPostOrderMultipletimes () {
        Session session = HibernateHelper.getSessionFactory().openSession();
        try {

            SQLQuery sqlQuery = session.createSQLQuery(ApplicationAccess.CAN_POST_ORDER_MULTIPLE_TIMES);
            sqlQuery.addScalar("CONFIG_VALUE");
            Object result = sqlQuery.uniqueResult();

            if (null == result || result.toString().isEmpty())
                return false;

            return true;
        }catch(Exception ex){
            log.error("",ex);
        }finally {
            log.info("AppConfig Model Session Closed in isScatFilterEnable!");
            if(session != null && session.isConnected()){
                session.flush();
                session.clear();
                session.close();
            }
        }
        return false;
    }

    public static int executeQuery(String strQuery){
        log.info("Call on executeQuery for: " + strQuery);
        Session session = HibernateHelper.getSessionFactory().openSession();
        int updated = 0;
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createSQLQuery(strQuery);
            updated = query.executeUpdate();
            tx.commit();
        } catch (HibernateException ex) {
            if (null != tx && !tx.wasCommitted())
                tx.rollback();
            log.error("Error at executeQuery. ", ex);
            ex.printStackTrace();
        } finally {
            log.info("AppConfig Model Session Closed in executeQuery!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.close();
            }
        }
        return updated;
    }

    public static String getUrlConnection(){
        log.info("Call on getUrlConnection!");
        Session session = HibernateHelper.getSessionFactory().openSession();
        InfoDB infoDB = new InfoDB();
        try {
            session.doWork(infoDB);
        } catch (HibernateException ex) {
            log.error("Error at getUrlConnection. ", ex);
            ex.printStackTrace();
        } finally {
            log.info("AppConfig Model Session Closed in getUrlConnection!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.close();
            }
        }
        return infoDB.getDbUrlConnection();
    }

}
