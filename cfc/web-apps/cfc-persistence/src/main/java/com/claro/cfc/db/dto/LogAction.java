package com.claro.cfc.db.dto;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/31/2014.
 */
public enum LogAction {
    LOGIN,
    USER_ADDED,
    SAVE_ACTION,
    GET_INCOMING_DATA,
    GET_ACTION_STATUS,

    DEVICE_UPDATE,
    DEVICE_INACTIVE_TEMP,
    DI_SUBMIT,
    DI_VIEW,
    DI_SUBMITTED_VIEW,
    DEVICE_SEARCH,
    DEVICE_LIST,
    DEVICE_CREATE,
    DEVICE_APP_UPDATE,
    DOWNLOAD_REPORT,

    ABORTED_FORMS_SUBMIT,

    CHECK_ACTION_HAND_OPERATED,
    CHECK_ACTION_PROCESSED_BY,

    UNCHECK_ACTION_HAND_OPERATED,
    UNCHECK_ACTION_PROCESSED_BY;

}
