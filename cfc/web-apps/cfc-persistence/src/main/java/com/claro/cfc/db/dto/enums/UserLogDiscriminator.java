package com.claro.cfc.db.dto.enums;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 7/3/2014.
 */
public enum UserLogDiscriminator {
    DEVICE,
    WEB
}
