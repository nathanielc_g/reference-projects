package com.claro.cfc.db.dto;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/13/14.
 */
@Entity
@Table(name = "USER_LOG", catalog = "")
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "users.log")
public class UserLogEntity {
    private long userLogId;
    private String userId;
    private String target;
    private String modType;
    private String discriminator;
    private String action;
    private String message;
    private Timestamp created;
    private String modifiedBy;

    @Id
    @Column(name = "USER_LOG_ID")
    @GeneratedValue(generator = "logs.sec_user_log",strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "logs.sec_user_log",sequenceName = "sec_user_log")
    public long getUserLogId() {
        return userLogId;
    }

    public void setUserLogId(long userLogId) {
        this.userLogId = userLogId;
    }

    @Basic
    @Column(name = "USER_ID")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "TARGET")
    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Basic
    @Column( name="ACTION" )
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Basic
    @Column( name="MESSAGE" )
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Basic
    @Column(name = "MOD_TYPE")
    public String getModType() {
        return modType;
    }

    public void setModType(String modType) {
        this.modType = modType;
    }

    @Basic
    @Column(name = "CREATED")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name="DISCRIMINATOR" )
    public String getDiscriminator() {
        return discriminator;
    }

    public void setDiscriminator(String discriminator) {
        this.discriminator = discriminator;
    }

    @Basic
    @Column(name="MODIFIED_BY" )
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserLogEntity)) return false;

        UserLogEntity that = (UserLogEntity) o;

        return userLogId == that.userLogId;

    }

    @Override
    public int hashCode() {
        return (int) (userLogId ^ (userLogId >>> 32));
    }

    @Override
    public String toString() {
        return "UserLogEntity{" +
                "userLogId=" + userLogId +
                ", userId='" + userId + '\'' +
                ", target='" + target + '\'' +
                ", modType='" + modType + '\'' +
                ", discriminator='" + discriminator + '\'' +
                ", action='" + action + '\'' +
                ", message='" + message + '\'' +
                ", created=" + created +
                ", modifiedBy='" + modifiedBy + '\'' +
                '}';
    }
}
