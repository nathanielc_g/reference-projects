package com.claro.cfc.db.dao.service;

import com.claro.cfc.db.dao.UsersAccess;
import com.claro.cfc.db.dto.enums.UserStatusEnum;
import com.claro.cfc.db.dto.UsersEntity;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Nathaniel Calderon on 5/25/2017.
 */
public class UserService {
    public static UsersEntity buildUser(String userId, Long status,Timestamp created, Timestamp expirationTime, String phoneNumbe) {
        UsersEntity user = new UsersEntity();
        user.setCreated(created);
        user.setModified(created);
        user.setExpirationDate(expirationTime);
        user.setStatus(status);
        user.setUserId(userId);
        user.setPhoneNumber(phoneNumbe);
        return user;
    }

    public static boolean isImeiAvailable (String imei, String userId, long status) {

        boolean result = true;
        if (imei.equals("0") || imei.equals("000000000000000") || UserStatusEnum.ACTIVE.ordinal() != status)
            return result;

        List<UsersEntity> users = UsersAccess.getUsersByImei(imei);
        if(users != null) {
            for (UsersEntity u : users) {
                //if (u.getStatus().equals(1L) && !u.getUserId().equalsIgnoreCase(user.getUserId())) {
                if (u.getStatus() == UserStatusEnum.ACTIVE.ordinal() && !u.getUserId().equalsIgnoreCase(userId)) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    public static Timestamp generateReactiveDate () {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.DATE, 3); // Adding 3 days
                        /*Date today = new Date();
                        c.set(today.getYear(), today.getMonth(), today.getDate());*/
        //c.setTime(new Date()); // Now use today date.
        return new Timestamp(c.getTime().getTime());
    }



}
