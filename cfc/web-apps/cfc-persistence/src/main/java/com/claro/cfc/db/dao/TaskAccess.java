package com.claro.cfc.db.dao;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.db.dto.*;
import com.claro.cfc.db.dto.enums.TaskStatusEnum;
import com.claro.cfc.db.dto.enums.TaskTypeEnum;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 9/30/2014.
 */
public final class TaskAccess {

    private static final Logger log = Logger.getLogger(TaskAccess.class);

    public static void createTasks(String type, Long actionId) {
        new GroupTaskUnit(null).create(type, actionId);
    }

    public static void createTask(TaskTypeEnum etype, Long actionId) {
        TaskStatusEntity status = getTaskStatus(TaskStatusEnum.PENDING);
        TaskTypeEntity type     = getTaskType(etype);

        if (null == status || null == type) {
            log.info("Could not be possible to create task due  on values reported : type=" + etype + ", status: " + status + ", actionId=" + actionId);
            return;
        }

        Session session = HibernateHelper.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            Timestamp current = new Timestamp(new Date().getTime());

            TaskEntity task = new TaskEntity();
            task.setCreated(current);
            task.setStatus(status);
            task.setType(type);
            //task.setLogs(new LinkedList<TaskLogEntity>());
            task.setLogs(new HashSet<TaskLogEntity>());
            task.setActionId(actionId);

            session.saveOrUpdate(task);

            tx.commit();
        } catch (HibernateException e) {
            log.error(e);
        } finally {
            if (null != tx && !tx.wasCommitted()) {
                tx.rollback();
            }
            log.info("Task Creation Session Closed.!");
            if (null != session && session.isConnected()) {
                session.flush();
                session.close();
            }
        }

    }


    public static TaskStatusEntity getTaskStatus(TaskStatusEnum status) {
        final Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(TaskStatusEntity.class);
            criteria.add(Restrictions.eq("status", status.toString()));

            List<TaskStatusEntity> statuses = criteria.list();

            if (null != statuses && 0 != statuses.size())
                return statuses.get(0);

        } catch (HibernateException ex) {
            log.error("Error at getTaskStatus(" + status + ") call", ex);
        } finally {
            if (null != session && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return null;
    }

    public static TreeSet<TaskEntity> getLatestTasksByActionId (long actionId) {
        final Session session = HibernateHelper.getSessionFactory().openSession();
        TreeSet<TaskEntity>  tasks = null;
        try {
            Criteria criteria = session.createCriteria(TaskEntity.class);
            criteria.add(Restrictions.eq("actionId", actionId));
            // return just one per type
            tasks = filterLatestTasks(criteria.list());
            initializeLazyLogsOnTasks(tasks);
        } catch (HibernateException ex){
            log.info("Error at getTasksByActionId(" + actionId + ") call.", ex);
        } finally {
            HibernateHelper.finalizeOpenSession(session);
        }
        return tasks;
    }

    public static TaskEntity getTaskById (long taskId) {
        final Session session = HibernateHelper.getSessionFactory().openSession();
        TaskEntity  task = null;
        try {
            Criteria criteria = session.createCriteria(TaskEntity.class);
            criteria.add(Restrictions.eq("taskId", taskId));
            //tasks = filterLatestTasks(criteria.list());\
            task = (TaskEntity) criteria.uniqueResult();
            initializeLazyLogsOnTask(task);
        } catch (HibernateException ex){
            log.info("Error at getTasksById(" + taskId + ") call.", ex);
        } finally {
            HibernateHelper.finalizeOpenSession(session);
        }
        return task;
    }

    private static void initializeLazyLogsOnTasks (Collection<TaskEntity> tasks) {
        for(TaskEntity task: tasks)
            //if(task.getStatus().getStatusId() != TaskStatusEnum.PENDING.id())
            Hibernate.initialize(task.getLogs());
    }

    private static void initializeLazyLogsOnTask (TaskEntity task) {
            Hibernate.initialize(task.getLogs());
    }

    public static TreeSet<TaskEntity> filterLatestTasks(Collection<TaskEntity> tasks) {
        TreeSet<TaskEntity> latestTasks = new TreeSet(new Comparator<TaskEntity>() {
            @Override
            public int compare(TaskEntity t1, TaskEntity t2) {
                if (t2.getType() == t1.getType()){
                    int result = t2.getCreated().compareTo(t2.getCreated());
                    return result < 1 ? 0:1;
                }
                return -1;
            }
        });
        latestTasks.addAll(tasks);
        return latestTasks;
    }

    public static TaskTypeEntity getTaskType(TaskTypeEnum type) {
        final Session session = HibernateHelper.getSessionFactory().openSession();

        try {
            Criteria criteria = session.createCriteria(TaskTypeEntity.class);
            criteria.add(Restrictions.eq("typeName", type.toString()));

            List<TaskTypeEntity> types = criteria.list();

            if (null != types && 0 != types.size())
                return types.get(0);

        } catch (HibernateException ex) {
            log.error("Error at getTaskType(" + type + ") call", ex);
        } finally {
            if (null != session &&  session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return null;
    }

}
