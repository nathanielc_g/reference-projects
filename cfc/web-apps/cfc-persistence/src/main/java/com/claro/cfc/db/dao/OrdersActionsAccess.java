package com.claro.cfc.db.dao;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.db.dao.service.ActionService;
import com.claro.cfc.db.dto.*;
import com.claro.cfc.db.dto.enums.TaskStatusEnum;
import com.claro.cfc.db.dto.enums.TaskTypeEnum;
import com.claro.cfc.transport.Attribute;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;

import javax.persistence.criteria.Root;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/7/14.
 */
public class OrdersActionsAccess {
    private static Logger log = Logger.getLogger(OrdersActionsAccess.class);

    public static boolean updateAction (ActionsEntity actionsEntity) {
        Session session = HibernateHelper.getSessionFactory().openSession();
        Transaction tx = null;
        boolean result = true;
        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(actionsEntity);
            tx.commit();
        } catch (HibernateException ex) {
            if (null != tx && !tx.wasCommitted())
                tx.rollback();
            log.error("Something goes wrong trying to update an Action.");
            ex.printStackTrace();
            result = false;
        } finally {
            log.info("Action Model Session Closed in updateAction.!");
            if (null != session && session.isConnected()) {
                session.flush();
                session.close();
            }
        }
        return result;
    }

    public static boolean updateAction (ActionsEntity actionsEntity, UserLogEntity userLog) {
        Session session = HibernateHelper.getSessionFactory().openSession();
        Transaction tx = null;
        boolean result = true;
        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(actionsEntity);
            session.saveOrUpdate(userLog);
            tx.commit();
        } catch (HibernateException ex) {
            if (null != tx && !tx.wasCommitted())
                tx.rollback();
            log.error("Something goes wrong trying to update an Action.");
            ex.printStackTrace();
            result = false;
        } finally {
            log.info("Action Model Session Closed in updateAction.!");
            if (null != session && session.isConnected()) {
                session.flush();
                session.close();
            }
        }
        return result;
    }

    public static ActionsEntity getActionById (long id) {
        Session session = HibernateHelper.getSessionFactory().openSession();
        ActionsEntity action = null;
        try {
            Criteria criteria = session.createCriteria(ActionsEntity.class);
            criteria.add(Restrictions.eq("actionId", id));
            return (ActionsEntity)criteria.uniqueResult();
        } catch (HibernateException ex) {
            log.error("Error at getActionById(" + id + ") call.", ex);
        } finally {
            log.info("Orders Action Session Closed in getActionById.!");
            if (session != null && session.isConnected()) {
                //finalizeSession(session);
                HibernateHelper.finalizeOpenSession(session);
            }
        }
        return action;
    }

    public static ActionsEntity getLastAction(String card, String code, String type) {
        Session session = HibernateHelper.getSessionFactory().openSession();

        try {
            Criteria criteria = session.createCriteria(ActionsEntity.class, "action");
            criteria.setFetchMode("tasks", FetchMode.JOIN);
            //criteria.createAlias("tasks", "t");
            criteria.setFetchMode("tasks.logs", FetchMode.JOIN);

            long orderCode = 0;
            try {
                orderCode = Long.valueOf(code);
            } catch (Exception ex) {
                log.error("", ex);
                ex.printStackTrace();
            }
            criteria.add(Restrictions.eq("code", orderCode));
            criteria.add(Restrictions.eq("userId", card));
            criteria.add(Restrictions.eq("orderType", type));

            List<ActionsEntity> actions = criteria.list();
            if (actions.size() > 0) {
                ActionsEntity action = filterLatestAction(actions);
                //action.getTasks().size();
                return action;
            } else
                return  null;
            //ActionsEntity action = (ActionsEntity) criteria.uniqueResult();

            //return action;
        } catch (HibernateException ex) {
            log.error("", ex);
        } finally {
            log.info("Orders Action Session Closed.!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return null;
    }

    private static ActionsEntity  filterLatestAction (List<ActionsEntity> actions) {
        Collections.sort(actions, new Comparator<ActionsEntity>() {
            @Override
            public int compare(ActionsEntity t1, ActionsEntity t2) {
                return t2.getCreated().compareTo(t1.getCreated());
            }
        });
        return actions.get(0);
    }

    public static boolean wasActionProcessed (ActionsEntity action) {
        boolean response = true;
        TreeSet<TaskEntity> latestTasks = TaskAccess.filterLatestTasks(action.getTasks());
        for (TaskEntity task: latestTasks) {
            if(task.getType().getTaskTypeId() == TaskTypeEnum.SAD.id())
                continue;
            if(task.getStatus().getStatusId() != TaskStatusEnum.PROCESSED.id()){
                response = false;
                break;
            }
            List<TaskLogEntity> logs = task.listTaskLogEntity();
            if(logs != null && logs.size() > 0) {
                TaskLogEntity log = logs.get(0);
                if(!log.isSuccess()){
                    response = false;
                    break;
                }
            } else {
                response = false;
                break;
            }
        }
        return response;
    }

    public static Reference<List<ActionsEntity>> getActionsByCard(String card) {
        Session session = HibernateHelper.getSessionFactory().openSession();

        try {
            Criteria criteria = session.createCriteria(ActionsEntity.class);
            criteria.add(Restrictions.eq("userId", card));

            List<ActionsEntity> actions = criteria.list();

            return new WeakReference<List<ActionsEntity>>(actions);
        } catch (HibernateException ex) {
            log.error("", ex);
        } finally {
            log.info("Orders Action Session Closed in getActionsByCard.!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return null;
    }

    public static List<ActionsEntity> getActionsByCreated(Date created) {
        Session session = HibernateHelper.getSessionFactory().openSession();

        try {
            Criteria criteria = session.createCriteria(ActionsEntity.class);
            criteria.add(Restrictions.gt("created", created));
            log.info("Trying to get Actions by created = " + created.toString());

            List<ActionsEntity> actions = criteria.list();

            return actions;
        } catch (Exception ex) {
            log.error("", ex);
        } finally {
            log.info("Actions Model Session Closed in getActionsByCreated!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }

        return Collections.EMPTY_LIST;
    }

    public static Reference<List<ActionValuesEntity>> getActionValuesByActionId(Long actionId) {
        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(ActionValuesEntity.class);
            criteria.add(Restrictions.eq("action.actionId", actionId));

            List<ActionValuesEntity> actionValues = criteria.list();

            return new WeakReference<List<ActionValuesEntity>>(actionValues);
        } catch (HibernateException ex) {
            log.error("", ex);
        } finally {
            log.info("Orders Action Session Closed in getValuesByActionId.!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return null;
    }

    public static Reference<List<ActionValuesEntity>> getActionValuesByCode(Long code, String orderType) {
        final String queryLastAction = "select ActionValues.value_id , ActionValues.akey , ActionValues.avalue , ActionValues.created , ActionValues.ACTION_ID from ( select tbl.* from (select a.* from actions a where a.code= :code and a.ORDER_TYPE= :orderType order by a.created desc ) tbl where rownum <=1 ) tbl inner join action_values ActionValues on ActionValues.ACTION_ID=tbl.ACTION_ID";
        Session session = HibernateHelper.getSessionFactory().openSession();

        try {
            SQLQuery sqlQuery = session.createSQLQuery(queryLastAction);
            sqlQuery.setLong("code", code);
            sqlQuery.setString("orderType", orderType);
            sqlQuery.addEntity(ActionValuesEntity.class);

            List<ActionValuesEntity> actionValues = sqlQuery.list();

            return new WeakReference<List<ActionValuesEntity>>(actionValues);
        } catch (HibernateException ex) {
            log.error("", ex);
        } finally {
            log.info("Orders Action Session Closed in getValuesByActionId.!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return null;
    }


    /**
     *  Please don't use this method.
     * @deprecated , replaced by {@link #saveAction(List, boolean)}
     */
    @Deprecated()
    public static long saveAction(List<Attribute> attributes) {
        return saveAction(attributes, true);
    }

    public static long saveAction(List<Attribute> attributes, boolean canPostFormOrderMultipleTimes) {
        long attributesFormatError;
        Map<String, String> attributesMap = ActionService.toMap(attributes);
        if ((attributesFormatError = ActionService.validateAttributesFormat(attributesMap)) != ActionService.DATA_FORMAT_NON_ERROR)
            return attributesFormatError;

        ActionsEntity action = ActionService.buildAction(attributesMap);
        log.info("Trying to save action for order " + action.getActionId() + " of type " + action.getOrderType()+ ", card " + action.getUserId());

        long resultId = ActionService.TRANSACTION_ERROR;
        Session session = HibernateHelper.getSessionFactory().openSession();
        ActionService actionService = new ActionService(session);

        final List<ActionsEntity> actionsFound;
        /*if (AppUtils.canPostFormOrderMultipleTimes())*/
        if(canPostFormOrderMultipleTimes)
            actionsFound = actionService.findPendingActionsByRelatedOrder(action);
        else
            actionsFound = actionService.findActionsByRelatedOrder(action);

        long count = null != actionsFound ? actionsFound.size() : 0;
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            log.info("Records found " + count);
            if (0 < count) {
                log.info("Trying to delete submitted action");

                /**
                 * Deleting all pending task created
                 */
                for (ActionsEntity currentAction: actionsFound) {

                    if (null != currentAction.getProcessed() && 1L == currentAction.getProcessed())
                        return resultId;

                    actionService.deletePendingTasks(currentAction);
                    actionService.deleteAction(currentAction);
                }
                actionsFound.clear();
            }
            log.info("Action doesn't exits at all, trying to insert it");
            actionService.saveOrUpdate(action, attributes);
            tx.commit();
            resultId = action.getActionId();
        } catch (Exception e) {
            log.error("", e);
            e.printStackTrace();
        } finally {
            if (null != tx && !tx.wasCommitted()) {
                tx.rollback();
            }
            log.info("Orders Action Session Closed.!");
            HibernateHelper.finalizeOpenSession(session);
        }
        return resultId;
    }


    public static List<ActionsEntity> getActionsByDate (String from) {
        Session session = HibernateHelper.getSessionFactory().openSession();
        log.info("Trying to find actions by date: "+ from);
        try {
            Criteria criteria = session.createCriteria(ActionsEntity.class);
            criteria.add(Restrictions.ge("created", stringToTimestamp(from)));

            List<ActionsEntity> actions = criteria.list();
            for(ActionsEntity action: actions)
                Hibernate.initialize(action.getValues());

            return actions;
        } catch (HibernateException ex) {
            log.error("", ex);
        } finally {
            log.info("Orders Action Session Closed in getActionsByDate.!");
            HibernateHelper.finalizeOpenSession(session);
        }
        return null;

    }

    private static Timestamp stringToTimestamp (String from) {
        Calendar calendar = Calendar.getInstance();
        String[] dateParts = from.split("-");
        calendar.set(Integer.parseInt(dateParts[0]), Integer.parseInt(dateParts[1]), Integer.parseInt(dateParts[2]));
        return new Timestamp(calendar.getTimeInMillis());
    }
}


