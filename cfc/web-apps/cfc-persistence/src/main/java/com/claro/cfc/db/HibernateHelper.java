package com.claro.cfc.db;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * Created by Christopher Herrera on 1/23/14.
 */
public final class HibernateHelper {
    private static final Logger log = Logger.getLogger(HibernateHelper.class);
    private static final SessionFactory mSessionFactory = sharedSessionFactory();

    /*private static final ThreadLocal<SessionFactory> threadLocal = new ThreadLocal<SessionFactory>() {
        @Override
        protected SessionFactory initialValue() {
            return new AnnotationConfiguration().configure().buildSessionFactory();
        }
    };*/

    private static final SessionFactory sharedSessionFactory(){
        SessionFactory session = null;
        try {
            Configuration config = new Configuration();
        /*// Begin ********* Just for test!!! **********
        boolean test = false;
        assert (config = config.configure("hibernate.test.cfg.xml")) != null && (test = true): "SessionFactory cannot be configure using 'hibernate.test.cfg.xml' configuration file.";
        if(!test)*/
            // End ********* Just for test!!! **********
            config.configure();
            ServiceRegistry registry = new ServiceRegistryBuilder().applySettings(config.getProperties()).buildServiceRegistry();
            session = config.buildSessionFactory(registry);
            log.error("Hibernate configured successfully.\n");
        } catch (Exception ex){
            log.error("Error trying to configure Hibernate.\n", ex);
            ex.printStackTrace();
        }
        return session;
    }

    public static final SessionFactory getSessionFactory() {
//        return threadLocal.get();
        return mSessionFactory;
    }

    public static void finalizeOpenSession (Session session) {
        if (session == null)
            throw new IllegalArgumentException("Session cannot be null.");

        if(!session.isConnected())
            return;

        session.flush();
        session.clear();
        session.close();
    }
}
