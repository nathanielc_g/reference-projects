package com.claro.cfc.db.dto;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/13/14.
 */
@Entity
@Table(name = "ACTIONS", catalog = "")
@org.hibernate.annotations.NamedQueries({
//        @NamedQuery(name = "actions.getActionsCount", query = "select count(e) from ActionsEntity e where code=:code and userId=:userId and orderType=:orderType"),
        @NamedQuery(name = "actions.count", query = "select count(e) from ActionsEntity e")
})
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "actions")
public class ActionsEntity {
    private long actionId;
    private long code;
    private String userId;

    private String formType;
    private String orderType;
    private Timestamp created;
    private Long processed;
    private Timestamp processedDate;

    private String handOperatedBy = "0";
    private String processedBy = "0";

    private Set<ActionValuesEntity> values;
    private Set<TaskEntity> tasks;


    @Id
    @Column(name = "ACTION_ID")
    @GeneratedValue(generator = "actions.sec_action_id", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "actions.sec_action_id", sequenceName = "sec_action_id")
    public long getActionId() {
        return actionId;
    }

    public void setActionId(long actionId) {
        this.actionId = actionId;
    }

    @Basic
    @Column(name = "CODE")
    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    @Basic
    @Column(name = "USER_ID")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "FORM_TYPE")
    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    @Basic
    @Column(name = "ORDER_TYPE")
    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @Basic
    @Column(name = "CREATED")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }


    @Basic
    @Column(name = "PROCESSED")
    public Long getProcessed() {
        return processed;
    }

    public void setProcessed(Long processed) {
        this.processed = processed;
    }

    @Basic
    @Column(name = "PROCESSED_DATE")
    public Timestamp getProcessedDate() {
        return processedDate;
    }


    public void setProcessedDate(Timestamp processedDate) {
        this.processedDate = processedDate;
    }

    @Basic
    @Column(name = "HAND_OPERATED_BY", nullable = false)
    public String getHandOperatedBy() {
        return handOperatedBy;
    }

    public void setHandOperatedBy(String handOperated) {
        this.handOperatedBy = handOperated;
    }

    @Basic
    @Column(name = "PROCESSED_BY", nullable = false)
    public String getProcessedBy() {
        return processedBy;
    }

    public void setProcessedBy(String processedBy) {
        this.processedBy = processedBy;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ActionsEntity)) return false;

        ActionsEntity that = (ActionsEntity) o;

        return actionId == that.actionId;
    }

    @Override
    public int hashCode() {
        return (int) (actionId ^ (actionId >>> 32));
    }

    @Override
    public String toString() {
        return "ActionsEntity{" +
                "actionId=" + actionId +
                ", code=" + code +
                ", userId='" + userId + '\'' +
                ", formType='" + formType + '\'' +
                ", orderType='" + orderType + '\'' +
                ", created=" + created +
                ", processed=" + processed +
                ", processedDate=" + processedDate +
                ", handOperatedBy='" + handOperatedBy + '\'' +
                ", processedBy='" + processedBy + '\'' +
                '}';
    }



    @OneToMany(mappedBy = "action", cascade = CascadeType.REMOVE,  fetch = FetchType.LAZY)
    /*@Fetch(FetchMode.JOIN)*/
    public Set<ActionValuesEntity> getValues() {
        return values;
    }

    public void setValues(Set<ActionValuesEntity> values) {
        this.values = values;
    }

    public List<ActionValuesEntity> listOfValues() {
        return new ArrayList<ActionValuesEntity>(values);
    }

    @OneToMany(mappedBy = "action",  fetch = FetchType.LAZY)
    /*@Fetch(FetchMode.JOIN)*/
    public Set<TaskEntity> getTasks() {
        return tasks;
    }

    public void setTasks(Set<TaskEntity> tasks) {
        this.tasks = tasks;
    }

    public List<ActionValuesEntity> listOftasks() {
        return new ArrayList<ActionValuesEntity>(values);
    }
}
