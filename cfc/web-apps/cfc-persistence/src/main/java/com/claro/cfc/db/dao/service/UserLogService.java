package com.claro.cfc.db.dao.service;

import com.claro.cfc.db.dto.LogAction;
import com.claro.cfc.db.dto.enums.UserLogDiscriminator;
import com.claro.cfc.db.dto.UserLogEntity;

import java.sql.Timestamp;

/**
 * Created by Nathaniel Calderon on 5/25/2017.
 */
public class UserLogService {
    public static UserLogEntity buildWebUserLog(LogAction action, String userId, Timestamp created, String target, String modifiedBy, String message){
        UserLogEntity userLog = new UserLogEntity();
        userLog.setUserId(userId);
        userLog.setModifiedBy(modifiedBy);
        userLog.setAction(action.toString());
        userLog.setMessage(message);
        userLog.setCreated(created);
        userLog.setTarget(target);
        userLog.setModType("1");
        userLog.setDiscriminator(UserLogDiscriminator.WEB.toString());
        return userLog;
    }

    public static UserLogEntity buildDeviceUserLog (String action, String userId, Timestamp created, String target, String modifiedBy, String message) {
        UserLogEntity userLog = new UserLogEntity();
        userLog.setUserId(userId);
        userLog.setModifiedBy(modifiedBy);
        userLog.setAction(action);
        userLog.setMessage(message);
        userLog.setCreated(created);
        userLog.setTarget(target);
        userLog.setModType("1");
        userLog.setDiscriminator(UserLogDiscriminator.DEVICE.toString());
        return userLog;
    }
}
