package com.claro.cfc.db.dao;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.db.dto.*;
import com.claro.cfc.db.dto.enums.UserLogDiscriminator;
import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.ReturningWork;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.sql.*;
import java.util.*;
import java.util.Date;

public class UsersAccess {
    private static Logger log = Logger.getLogger(UsersAccess.class);

    private static final String mUserStatusQuery = "SELECT STATUS FROM USERS WHERE USER_ID = :USER_ID";
    private static final String mUsersOutOfDateQuery = "SELECT T.APP_VERSION, U.PHONE_NUMBER FROM USERS U INNER JOIN TERMINAL T  ON U.IMEI = T.IMEI  WHERE T.APP_VERSION < :APP_VERSION and u.USER_ID = :USER_ID ";

    public static String getUsersOutOfDate (String card) {
        log.info("Getting Users Out of Date");

        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            SQLQuery sqlQuery = session.createSQLQuery(mUsersOutOfDateQuery);
            // Since version 30335 mobile App doesn't allow do any operation while App is out of date.
            sqlQuery.setParameter("APP_VERSION", 30335);
            sqlQuery.setParameter("USER_ID", card);
            sqlQuery.addScalar("PHONE_NUMBER");
            Object result = sqlQuery.uniqueResult();
            if (null == result || result.toString().isEmpty())
                return "";

            return result.toString();

        }catch(Exception ex){
            log.error("",ex);
        }finally {
            log.info("User Model Session Closed in getUserOutOfDate!");
            if(session != null && session.isConnected()){
                session.flush();
                session.clear();
                session.close();
            }
        }
        return "";
    }

    public static UsersEntity getUserByCard(final String card) {
        final Reference<List<UsersEntity>> users =  getUsersByCriteriaSelection(new CriteriaSelection() {
            @Override
            public void selectCriteria(Criteria criteria) {
                criteria.add(Restrictions.eq("userId", card));
            }
        });

        if (null != users && null != users.get() && 0 < users.get().size())
            return users.get().get(0);
        return null;
    }

    public static UsersEntity getEagerUserByCard(final String card){
        return getUserByCriteriaSelection(new CriteriaSelection() {
            @Override
            public void selectCriteria(Criteria criteria) {
                criteria.add(Restrictions.eq("userId", card));
            }
        });
    }

    public static final long getUserStatus( String card ){
        final long noValue = -1;

        Session session = HibernateHelper.getSessionFactory().openSession();
        try {

            SQLQuery sqlQuery = session.createSQLQuery(mUserStatusQuery);
            sqlQuery.setString("USER_ID", card);
            log.info( "Getting user status for "+card );
            List results =  sqlQuery.list();

            if (null == results || 0 >= results.size())
                return noValue;

            Iterator iterator = results.iterator();

            while (iterator.hasNext()) {
                Object row = iterator.next();

                long status = noValue;
                if( null != row )
                    status = Long.valueOf(row+"");
                return status;
            }
            results.clear();
        }catch(Exception ex){
            log.error("",ex);
            ex.printStackTrace();
        }finally {
            log.info("Actions Model Session Closed in getUserStatus!");
            if(session != null && session.isConnected()){
                session.flush();
                session.clear();
                session.close();
            }
        }
        return noValue;
    }

    public static UsersEntity getUserByPhone(final String phone) {
        final Reference<List<UsersEntity>> users =  getUsersByCriteriaSelection(new CriteriaSelection() {
            @Override
            public void selectCriteria(Criteria criteria) {
                //criteria.createAlias("terminal", "t");
                criteria.add(Restrictions.eq("phoneNumber", phone));
            }
        });

        if (null != users && null != users.get() && 0 < users.get().size())
            return users.get().get(0);
        return null;
    }

    public static List<String> getPhonesByVersion(String version) {
        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            Query query = session.getNamedQuery("users.phonesByAppVersion");
            query.setParameter("appVersion",  Double.parseDouble(version));
            List<String> phones = query.list();
            return phones;
        } finally {
            log.info("User Model Session Closed in getTerminal!");
            if (null != session && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
    }


    public static UsersEntity getUserByImei(final String imei) {
        final Reference<List<UsersEntity>> users =  getUsersByCriteriaSelection(new CriteriaSelection() {
            @Override
            public void selectCriteria(Criteria criteria) {
                criteria.add(Restrictions.eq("terminal.imei", imei));
            }
        });

        if (null != users && null != users.get() && 0 < users.get().size())
            return users.get().get(0);
        return null;
    }

    public static List<UsersEntity> getUsersByImei(final String imei) {
        Reference<List<UsersEntity>> users =  getUsersByCriteriaSelection(new CriteriaSelection() {
            @Override
            public void selectCriteria(Criteria criteria) {
                criteria.add(Restrictions.eq("terminal.imei", imei));

            }
        });

        if (null != users && null != users.get() && 0 < users.get().size())
            return users.get();

        return null;
    }

    public static UsersEntity getUserByImeiOrStatus(final String imei) {
        Reference<List<UsersEntity>> users =  getUsersByCriteriaSelection(new CriteriaSelection() {
            @Override
            public void selectCriteria(Criteria criteria) {
                criteria.add(Restrictions.eq("terminal.imei", imei));
                criteria.add(Restrictions.eq("status", 1L));

            }
        });

        if (null != users && null != users.get() && 0 < users.get().size())
            return users.get().get(0);

        users =  getUsersByCriteriaSelection(new CriteriaSelection() {
            @Override
            public void selectCriteria(Criteria criteria) {
                criteria.add(Restrictions.eq("terminal.imei", imei));
            }
        });
        if (null != users && null != users.get() && 0 < users.get().size())
            return users.get().get(0);
        return null;
    }


    public static UsersEntity getUserByCardAndImei(final String card, final String imei) {
        final Reference<List<UsersEntity>> users =  getUsersByCriteriaSelection(new CriteriaSelection() {
            @Override
            public void selectCriteria(Criteria criteria) {
                criteria.add(Restrictions.eq("userId", card));
                criteria.add(Restrictions.eq("terminal.imei", imei));
            }
        });

        try {
            if (null != users && null != users.get() && 0 < users.get().size())
                return users.get().get(0);
            return null;
        } finally {
            if (null != users)
                users.clear();
        }
    }


    public static Reference<List<UsersEntity>> getAllUsers() {
        log.info("Trying to retrieve all users");
        return getUsersByCriteriaSelection(null);
    }


    private static Reference<List<UsersEntity>> getUsersByCriteriaSelection(CriteriaSelection selection) {
        log.info("Trying to retrieve all users");

        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(UsersEntity.class);
            if (null != selection)
                selection.selectCriteria(criteria);

            List<UsersEntity> users = criteria.list();

            return new WeakReference<List<UsersEntity>>(users);
        } catch (HibernateException ex) {
            log.error("",ex);
        } finally {
            log.info("User Model Session Closed.!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return null;
    }

    private static UsersEntity getUserByCriteriaSelection(CriteriaSelection selection) {
        log.info("Trying to retrieve user by: "+ selection);
        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(UsersEntity.class);
            if (null != selection)
                selection.selectCriteria(criteria);

            List<UsersEntity> users = criteria.list();
            if (users.size() <= 0)
                return null;

            UsersEntity user = users.get(0);
            Hibernate.initialize(user.getTerminal());
            return user;
        } catch (HibernateException ex) {
            log.error("",ex);
        } finally {
            log.info("User Model Session Closed.!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return null;
    }


    public static boolean updateUser(UsersEntity user, TerminalEntity terminal, UserLogEntity log) {
        return saveUserRecord(user, terminal, log);
    }

    public static boolean saveUserRecord(UsersEntity user, TerminalEntity terminal, UserLogEntity userLog) {
        log.info("Trying to save user ");
        Session session = HibernateHelper.getSessionFactory().openSession();

        Transaction tx = null;
        boolean state = true;

        try {
            tx = session.beginTransaction();

            terminal.setUsers(new ArrayList<UsersEntity>());
            session.saveOrUpdate(terminal);

            user.setTerminal(terminal);
            terminal.getUsers().add(user);
            session.saveOrUpdate(user);

            session.save(userLog);

            tx.commit();
        } catch (HibernateException ex) {
            log.info(ex);
            if (null != tx && !tx.wasCommitted())
                tx.rollback();
            state = false;
        } finally {
            log.info("User Model Session Closed.!");
            if (null != session && session.isConnected()) {
                session.flush();
                session.close();
            }
        }
        return state;
    }


    public static boolean logUser(String card, UserLogEntity userLog) {
        if (null == card) return false;

        Session session = HibernateHelper.getSessionFactory().openSession();

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(userLog);

            tx.commit();
        } catch (HibernateException ex) {
            log.error("",ex);
            if (null != tx && !tx.wasCommitted())
                tx.rollback();
            return false;
        } finally {
            log.info("User Model Session Closed in log User.!");
            if (null != session && session.isConnected()) {
                session.flush();
                session.close();
            }
        }
        return true;
    }


    public static boolean logUser(String userCard, LogAction action, String msg, String target, String discr) {
        if (null == userCard)
            return false;

        SoftReference<UserLogEntity> userLogRef = new SoftReference<UserLogEntity>(new UserLogEntity());
        UserLogEntity userLog = userLogRef.get();

        userLog.setUserId(userCard);
        userLog.setAction(action.toString());
        userLog.setMessage(msg);
        userLog.setCreated(new Timestamp(new Date().getTime()));
        userLog.setTarget(target);
        userLog.setModType("1");
        userLog.setDiscriminator(discr);

        return logUser(userCard, userLog);
    }


    public static long getUsersCount() {
        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            Query query = session.getNamedQuery("users.count");
            query.setParameter("status", 1L);
            long count = 0;

            try {
                count = (Long) query.uniqueResult();
            } catch (Exception ex) {
                log.error("Error while trying to fetch users for index ");
                log.error("",ex);
            }

            return count;
        } finally {
            log.info("User Model Session Closed in count Users.!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
    }

    public static List<UserLogEntity> getUserLogsByDiscriminator(UserLogDiscriminator dic) {
        return getUserLogsByDiscriminator(dic,null);
    }

    public static List<UserLogEntity> getUserLogsByDiscriminator(UserLogDiscriminator dic,Date afterThat ){

        log.info( "getUserLogsByDiscriminator[ "+dic+" ]" );
        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(UserLogEntity.class);
            criteria.add(Restrictions.eq("discriminator", dic.toString()));

            if( null != afterThat )
                criteria.add(Restrictions.gt("created", afterThat));

            List<UserLogEntity> logs = criteria.list();

            return logs;
        } catch (HibernateException ex) {
            log.error("",ex);
        } finally {
            log.info("User Model Session Closed in getUserLogsByDiscriminator!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return Collections.EMPTY_LIST;
    }

    public static boolean bulkUsers( final String issuerSystem, final String bulkCards ){
        final long start = System.currentTimeMillis();

        Session session = HibernateHelper.getSessionFactory().openSession();
        try{
            return session.doReturningWork(new ReturningWork<Boolean>() {
                @Override
                public Boolean execute(Connection connection) throws SQLException {
                    try {
                        CallableStatement statement = connection.prepareCall("{CALL USER_PKG.BULK_INSERT_DELETE_USERS(?,?,?)}");
                        statement.setString(1, issuerSystem);
                        statement.setString(2, bulkCards);
                        statement.registerOutParameter(3, OracleTypes.CHAR);

                        statement.executeUpdate();
                        return statement.getBoolean(3);
                    }catch ( Exception ex){
                        log.info("",ex);
                    }
                    return false;
                }
            });
        }catch( HibernateException ex){
            log.error("",ex);
        }finally {
            long end = System.currentTimeMillis();
            log.info("User Model Session Closed in bulkUsers( took "+(end-start)/1E3+" secs to complete)!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.close();
            }
        }
        return false;
    }

    private interface CriteriaSelection {
        void selectCriteria(Criteria criteria);
    }
}
