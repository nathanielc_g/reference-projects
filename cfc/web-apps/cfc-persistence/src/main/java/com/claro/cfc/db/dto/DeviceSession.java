package com.claro.cfc.db.dto;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Jansel Valentin on 2/15/2016.
 **/

@Entity
@Table(name = "DEVICE_SESSION", catalog = "")
public class DeviceSession {

    private long id;
    private String deviceImei;
    private String deviceSerial;
    private String userCard;
    private String sessionJson;
    private Timestamp created;

    public DeviceSession() {
    }

    @Id
    @Column(name = "SESSION_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "device.sec_device_session")
    @SequenceGenerator(name = "device.sec_device_session", sequenceName = "sec_device_session")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "DEVICE_IMEI")
    public String getDeviceImei() {
        return deviceImei;
    }

    public void setDeviceImei(String deviceImei) {
        this.deviceImei = deviceImei;
    }

    @Basic
    @Column(name="DEVICE_SERIAL")
    public String getDeviceSerial() {
        return deviceSerial;
    }

    public void setDeviceSerial(String deviceSerial) {
        this.deviceSerial = deviceSerial;
    }

    @Basic
    @Column(name="USER_ID")
    public String getUserCard() {
        return userCard;
    }

    public void setUserCard(String userCard) {
        this.userCard = userCard;
    }

    @Basic
    @Column(name="SESSION_JSON")
    public String getSessionJson() {
        return sessionJson;
    }

    public void setSessionJson(String sessionJson) {
        this.sessionJson = sessionJson;
    }

    @Basic
    @Column(name = "CREATED")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "DeviceSession{" +
                "id=" + id +
                ", deviceImei='" + deviceImei + '\'' +
                ", deviceSerial='" + deviceSerial + '\'' +
                ", userCard='" + userCard + '\'' +
                ", sessionJson='" + sessionJson + '\'' +
                ", created=" + created +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DeviceSession)) return false;

        DeviceSession that = (DeviceSession) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
