package com.claro.cfc.db.dto.enums;

public enum TaskResponseTypeEnum {
    NO_ERROR{
        @Override
        public long id() {
            return 1L;
        }
    }, NO_CHANGE{
        @Override
        public long id() {
            return 2L;
        }
    }, ASSIGNED{
        @Override
        public long id() {
            return 3L;
        }
    }, NOT_FOUND{
        @Override
        public long id() {
            return 4L;
        }
    }, UNKNOWN{
        @Override
        public long id() {
            return 5L;
        }
    };


    /**
     *
     * @return  data base record id, according with the default value inserted in CFC at schema creation time.
     */
    public abstract long id();
}
