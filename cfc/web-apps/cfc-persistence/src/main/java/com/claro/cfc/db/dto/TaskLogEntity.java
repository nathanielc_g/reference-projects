package com.claro.cfc.db.dto;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/13/14.
 */
@Entity
@Table(name = "TASK_LOG",  catalog = "")
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "task.log")
public class TaskLogEntity {
    private long taskLogId;
    private String outgoingXml;
    private String ingoingXml;
    private Timestamp created;
    private TaskEntity task;
    private boolean success;
    private long responseType;
    private String message;
    private String updatedAttrs;


    @Id
    @Column(name = "TASK_LOG_ID")
    public long getTaskLogId() {
        return taskLogId;
    }

    public void setTaskLogId(long taskLogId) {
        this.taskLogId = taskLogId;
    }

    @Basic
    @Column(name = "OUTGOING_XML")
    public String getOutgoingXml() {
        return outgoingXml;
    }

    public void setOutgoingXml(String outgoingXml) {
        this.outgoingXml = outgoingXml;
    }

    @Basic
    @Column(name = "INGOING_XML")
    public String getIngoingXml() {
        return ingoingXml;
    }

    public void setIngoingXml(String ingoingXml) {
        this.ingoingXml = ingoingXml;
    }

    @Basic
    @Column(name = "CREATED")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "MESSAGE")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Basic
    @Column(name = "UPDATED_ATTRS")
    public String getUpdatedAttrs() {
        return updatedAttrs;
    }

    public void setUpdatedAttrs(String updatedAttrs) {
        this.updatedAttrs = updatedAttrs;
    }

    @Basic
    @Column(name = "SUCCESS")
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Basic
    @Column(name = "RESPONSE_TYPE")
    public long getResponseType() {
        return responseType;
    }

    public void setResponseType(long responseType) {
        this.responseType = responseType;
    }

    @Override
    public String toString() {
        return "TaskLogEntity{" +
                "taskLogId=" + taskLogId +
                ", outgoingXml='" + outgoingXml + '\'' +
                ", ingoingXml='" + ingoingXml + '\'' +
                ", created=" + created +
                ", task=" + task +
                ", success=" + success +
                ", responseType=" + responseType +
                ", message='" + message + '\'' +
                ", updatedAttrs='" + updatedAttrs + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskLogEntity)) return false;

        TaskLogEntity that = (TaskLogEntity) o;

        return taskLogId == that.taskLogId;
    }

    @Override
    public int hashCode() {
        return (int) (taskLogId ^ (taskLogId >>> 32));
    }

    @ManyToOne
    @JoinColumn(name = "TASK_ID", referencedColumnName = "TASK_ID", nullable = false)
    public TaskEntity getTask() {
        return task;
    }

    public void setTask(TaskEntity task) {
        this.task = task;
    }


}
