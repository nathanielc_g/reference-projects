package com.claro.cfc.db.dto;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/13/14.
 */
@Entity
@Table(name = "TASK_TYPE",  catalog = "")
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "task.type")
public class TaskTypeEntity {
    private long taskTypeId;
    private String typeName;
//    private List<TaskEntity> tasks;

    @Id
    @Column(name = "TASK_TYPE_ID")
    public long getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(long taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    @Basic
    @Column(name = "TYPE_NAME")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public String toString() {
        return "TaskTypeEntity{" +
                "taskTypeId=" + taskTypeId +
                ", typeName='" + typeName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskTypeEntity)) return false;

        TaskTypeEntity that = (TaskTypeEntity) o;

        return taskTypeId == that.taskTypeId;
    }

    @Override
    public int hashCode() {
        return (int) (taskTypeId ^ (taskTypeId >>> 32));
    }

    //    @OneToMany(mappedBy = "type")
//    public List<TaskEntity> getTasks() {
//        return tasks;
//    }
//
//    public void setTasks(List<TaskEntity> tasks) {
//        this.tasks = tasks;
//    }
}
