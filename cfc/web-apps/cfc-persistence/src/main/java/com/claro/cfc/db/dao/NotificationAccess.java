package com.claro.cfc.db.dao;

import com.claro.cfc.db.HibernateHelper;
import org.apache.log4j.Logger;
import org.hibernate.*;

import java.util.*;

/**
 * Created by Nathaniel Calderon on 7/17/2017.
 */
public class NotificationAccess {
    private static final Logger log = Logger.getLogger(NotificationAccess.class);
    private static final String queryPhoneAndActionType = "SELECT a.CODE, a.FORM_TYPE, u.PHONE_NUMBER FROM ACTIONS a\n" +
            "inner join USERS u\n" +
            "  on u.user_id = a.user_id\n" +
            "inner join terminal t\n" +
            "  on t.IMEI = u.IMEI\n" +
            "where\n" +
            "  action_id = :action_id and rownum = 1";
    private static final int BATCH_MAX_RESULT = 500;

    public static  Map<String,String> getUserAndActionAttrs(long actionId) {
        final StatelessSession session = HibernateHelper.getSessionFactory().openStatelessSession();
        try {
            log.info("\n\n************* Getting phone, for action " + actionId + " *************\n");
            SQLQuery query = session.createSQLQuery(queryPhoneAndActionType);
            query.setReadOnly(true).setCacheable(false).setFetchSize(BATCH_MAX_RESULT);
            query.setLong("action_id", actionId);
            ScrollableResults results = query.scroll(ScrollMode.FORWARD_ONLY);
            if (null != results && results.next()) {
                Object[] row = results.get();
                Map<String,String> map = new HashMap<String, String>(3);
                map.put("CODE", row[0] + "");
                map.put("FORM_TYPE", row[1] + "");
                map.put("PHONE_NUMBER", row[2] + "");
                return map;
            }
            return null;
        } catch (HibernateException ex) {
            log.error("Error at getPhone call ", ex);
        } finally {
            if (null != session)
                session.close();
        }
        return null;

    }


}
