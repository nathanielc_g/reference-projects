package com.claro.cfc.db.dto;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Jansel Valentin on 11/8/2016.
 **/
@Entity
@Table(name = "OAUTH_ACCESS_TOKEN", catalog = "")
public class OAuthAccessToken {

    private long tokenId;
    private String token;
    private String clientId;
    private Integer tokenValidity;
    private String consentedScopes;
    private Timestamp created;
    private Timestamp validUntil;


    @Id
    @Column(name = "TOKEN_ID")
    @GeneratedValue(generator = "oauth.sec_oauth_access_token", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "oauth.sec_oauth_access_token", sequenceName = "SEC_OAUTH_ACCESS_TOKEN")
    public long getTokenId() {
        return tokenId;
    }

    public void setTokenId(long tokenId) {
        this.tokenId = tokenId;
    }

    @Basic
    @Column(name = "TOKEN")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Basic
    @Column(name = "CLIENT_ID")
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Basic
    @Column(name = "TOKEN_VALIDITY")
    public Integer getTokenValidity() {
        return tokenValidity;
    }

    public void setTokenValidity(Integer tokenValidity) {
        this.tokenValidity = tokenValidity;
    }

    @Basic
    @Column(name = "CONSENTED_SCOPES")
    public String getConsentedScopes() {
        return consentedScopes;
    }

    public void setConsentedScopes(String consentedScopes) {
        this.consentedScopes = consentedScopes;
    }

    @Basic
    @Column(name = "VALID_UNTIL")
    public Timestamp getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Timestamp validUntil) {
        this.validUntil = validUntil;
    }

    @Basic
    @Column(name = "CREATED")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OAuthAccessToken that = (OAuthAccessToken) o;

        if (tokenId != that.tokenId) return false;
        if (clientId != that.clientId) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = (int)(tokenId ^ (tokenId >>> 32));
        result = 31 * result + (clientId != null ? clientId.hashCode() : 0);
        result = 31 * result + (token != null ? token.hashCode() : 0);
        result = 31 * result + (consentedScopes != null ? consentedScopes.hashCode() : 0);
        return result;
    }
}
