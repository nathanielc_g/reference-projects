package com.claro.cfc.db.dto.enums;

/**
 * Created by Nathaniel Calderon on 8/30/2016.
 */
public enum AppConfigEnum {
    APP_VERSION
    ,AVAILABLE_TIME
    ;
}
