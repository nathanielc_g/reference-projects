package com.claro.cfc.db.dao;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.db.dto.TerminalEntity;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 6/1/2014.
 */
public class TerminalAccess {
    private static Logger log = Logger.getLogger(TerminalAccess.class);

    private static final String terminalVersionByCard = "SELECT T.APP_VERSION AS VERSION FROM USERS U INNER JOIN TERMINAL T  ON U.IMEI = T.IMEI  WHERE U.USER_ID=:card";

    public static TerminalEntity getTerminalByImei(String imei) {
        Session session = HibernateHelper.getSessionFactory().openSession();

        try {
            Criteria criteria = session.createCriteria(TerminalEntity.class);
            criteria.add(Restrictions.eq("imei", imei));
            List<TerminalEntity> all = criteria.list();

            if (0 < all.size()) {
                return all.get(0);
            }
        } finally {
            log.info("Terminal Model Session Closed in getTerminal!");
            if (null != session && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return null;
    }

    public static TerminalEntity saveOrUpdateTerminal(TerminalEntity terminal) {
        Session session = HibernateHelper.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(terminal);
            tx.commit();
        } catch (HibernateException ex) {
            if (null != tx && !tx.wasCommitted())
                tx.rollback();
            log.error("Some error trying to add new terminal");
            ex.printStackTrace();
        } finally {
            log.info("Terminal Model Session Closed in addTerminal.!");
            if (null != session && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return terminal;
    }

    public static void updateAppVersion (String imei, double version) {
        log.info("Entering to update app version to Terminal IMEI: " + imei);
        Session session = HibernateHelper.getSessionFactory().openSession();

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.getNamedQuery("terminals.updateAppVersionByImei");
            query.setParameter("version", version);
            query.setParameter("imei", imei);
            query.executeUpdate();
            tx.commit();
        } catch (HibernateException ex) {
            if (null != tx && !tx.wasCommitted())
                tx.rollback();
            log.error("Some error trying to update app version on terminal");
            ex.printStackTrace();
        } finally {
            log.info("Terminal Model Session Closed in updateAppVersion.!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.close();
            }
        }
    }

    public static void updateAppVersion() {
        log.info("Entering to update app version");
        String version = ApplicationAccess.getVersion();
        if (version.isEmpty()){
            return;
        }
        Session session = HibernateHelper.getSessionFactory().openSession();

        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            /*Criteria criteria = session.createCriteria(AppConfigEntity.class);
            List<AppConfigEntity> apps = criteria.list();
            if (null == apps || 0 == apps.size())
                return;

            AppConfigEntity config = apps.get(0);
            apps.clear();*/

            Query query = session.getNamedQuery("terminals.updateAppVersion");
            query.setParameter("version",version);
            query.executeUpdate();
            tx.commit();

        } catch (HibernateException ex) {
            if (null != tx && !tx.wasCommitted())
                tx.rollback();
            log.error("Some error trying to update app version on terminal");
            ex.printStackTrace();
        } finally {
            log.info("Terminal Model Session Closed in updateAppVersion.!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.close();
            }
        }
    }
}
