package com.claro.cfc.db.dao;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.db.dto.DeviceSession;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Jansel Valentin on 2/15/2016.
 **/
public final class DeviceSessions {

    private static final Logger log = Logger.getLogger(DeviceSessions.class);

    public static void saveSession(DeviceSession deviceSession){
        if( null == deviceSession){
            log.info( "Impossible save null device session" );
            return;
        }
        Session session = HibernateHelper.getSessionFactory().openSession();

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(deviceSession);
            tx.commit();
        }catch ( Exception ex ){
            log.error(ex);
        }finally {
            if (null != tx && !tx.wasCommitted()) {
                tx.rollback();
            }
            log.info("saveSession() Session Closed.!");
            if (null != session && session.isConnected()) {
                session.flush();
                session.close();
            }
        }
    }

    public static List getUserSessions(String card){
        Session session = HibernateHelper.getSessionFactory().openSession();

        try {
            Query sqlQuery = session.createSQLQuery("SELECT SESSION_JSON FROM DEVICE_SESSION WHERE USER_ID = :card");
            sqlQuery.setString("card", card);

            List results = sqlQuery.list();
            if (null == results || 0 >= results.size())
                return Collections.emptyList();

            final Gson gson = new Gson();

            ArrayList sessions = new ArrayList(results.size());

            Iterator iterator = results.iterator();
            while (iterator.hasNext()) {
                Object sessionJson = iterator.next();
                if( sessionJson instanceof Clob)
                    try {
                        sessions.add(gson.fromJson(clobToString((Clob) sessionJson), JsonObject.class));
                    }catch( Exception ex ){
                        ex.printStackTrace();
                        log.error("",ex);
                    }
            }
            results.clear();
            return sessions;
        } catch (Exception ex) {
            log.error("", ex);
        } finally {
            log.info("Session Closed in getUserSessions!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return Collections.emptyList();
    }

    public static List getUserSessionsByDate (String card, String beginDate){
        Session session = HibernateHelper.getSessionFactory().openSession();

        try {
            Query sqlQuery = session.createSQLQuery("SELECT SESSION_JSON FROM DEVICE_SESSION WHERE USER_ID = :card and CREATED >= >= TO_TIMESTAMP(:beginDate)");
            sqlQuery.setString("card", card);
            sqlQuery.setString("beginDate", card);
            List results = sqlQuery.list();
            if (null == results || 0 >= results.size())
                return Collections.emptyList();

            final Gson gson = new Gson();

            ArrayList sessions = new ArrayList(results.size());

            Iterator iterator = results.iterator();
            while (iterator.hasNext()) {
                Object sessionJson = iterator.next();
                if( sessionJson instanceof Clob)
                    try {
                        sessions.add(gson.fromJson(clobToString((Clob) sessionJson), JsonObject.class));
                    }catch( Exception ex ){
                        ex.printStackTrace();
                        log.error("",ex);
                    }
            }
            results.clear();

            return sessions;
        } catch (Exception ex) {
            log.error("", ex);
        } finally {
            log.info("Session Closed in getUserSessions!");
            if (session != null && session.isConnected()) {
                session.flush();
                session.close();
            }
        }
        return Collections.emptyList();
    }


    public static String clobToString(Clob clb) throws IOException, SQLException {
        if (clb == null)
            return "";
        StringBuffer str = new StringBuffer();
        String strng;

        BufferedReader bufferRead = new BufferedReader(clb.getCharacterStream());
        while ((strng = bufferRead.readLine()) != null)
            str.append(strng);

        return str.toString();

    }
}
