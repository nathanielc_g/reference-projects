package com.claro.cfc.db.dao;

import com.claro.cfc.db.HibernateHelper;
import com.claro.cfc.db.dto.OAuthAccessToken;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by Jansel Valentin on 11/8/2016.
 **/
public final class OAuthAccess {
    private static final Logger log = Logger.getLogger(OAuthAccess.class);

    private OAuthAccess(){}


    public static String getClientIdByToken( String token ){
        final String mQuery = "SELECT oc.CLIENT_ID FROM OAUTH_CLIENT oc JOIN OAUTH_ACCESS_TOKEN oat on oat.CLIENT_ID = oc.CLIENT_ID WHERE oat.TOKEN = :TOKEN";
        log.info("Getting Oauth Client id token: "+token);

        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            SQLQuery sqlQuery = session.createSQLQuery(mQuery);
            sqlQuery.setParameter("TOKEN", token);
            Object result = sqlQuery.uniqueResult();
            if (null == result || result.toString().isEmpty())
                return null;
            return result.toString();

        }catch(Exception ex){
            log.error("",ex);
        }finally {
            log.info("OAuth Model Session Closed in getClientIdByToken!");
            if(session != null && session.isConnected()){
                session.clear();
                session.flush();
                session.close();
            }
        }
        return null;
    }

    public static boolean containsClient(String clientId){
        final String mQuery = "SELECT CLIENT_ID FROM OAUTH_CLIENT  WHERE CLIENT_ID = :CLIENT_ID";
        log.info("Verifying OAuth Client "+clientId);

        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            SQLQuery sqlQuery = session.createSQLQuery(mQuery);
            sqlQuery.setParameter("CLIENT_ID", clientId);
            Object result = sqlQuery.uniqueResult();
            if (null == result || result.toString().isEmpty())
                return false;
            return true;

        }catch(Exception ex){
            log.error("",ex);
        }finally {
            log.info("OAuth Model Session Closed in containsClient!");
            if(session != null && session.isConnected()){
                session.clear();
                session.flush();
                session.close();
            }
        }
        return false;
    }


    public static OAuthAccessToken getOrCreateAccessToken(String clientId,String scope){
        OAuthAccessToken token = getAccessTokenByClientId(clientId);
        if( null == token ){
            log.info( "No valid access token found for client "+clientId+", creating new one" );

            long tokenValidity = getClientTokenValidity(clientId);
            Date now = new Date();
            Timestamp validUntil = new Timestamp(now.getTime()+(long)(tokenValidity*1E3));

            token = new OAuthAccessToken();
            token.setValidUntil(validUntil);
            token.setTokenValidity((int)tokenValidity);
            token.setClientId(clientId);
            token.setToken(generatePseudoToken());
            token.setCreated(new Timestamp(now.getTime()));
            token.setConsentedScopes(scope);

            Session session = HibernateHelper.getSessionFactory().openSession();
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.save(token);
                tx.commit();
            } catch (HibernateException ex) {
                log.error("",ex);
                if (null != tx && !tx.wasCommitted())
                    tx.rollback();
                return null;
            } finally {
                log.info("OAuth Model Session Closed in getOrCreateAccessToken.!");
                if (null != session && session.isConnected()) {
                    session.flush();
                    session.close();
                }
            }
        }
        return token;
    }


    public static OAuthAccessToken getAccessTokenByClientId( String clientId){
        final Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(OAuthAccessToken.class);
            criteria.add(Restrictions.eq("clientId", clientId));
            criteria.add(Restrictions.gt("validUntil", new Date()));

            List<OAuthAccessToken> types = criteria.list();
            if (null != types && 0 != types.size())
                return types.get(0);

        } catch (HibernateException ex) {
            log.error("Error at getAccessTokenByClientId(" + clientId + ") call", ex);
        } finally {
            if (null != session &&  session.isConnected()) {
                session.flush();
                session.close();
            }
        }
        return null;
    }

    public static OAuthAccessToken getAccessToken(String token){
        final Session session = HibernateHelper.getSessionFactory().openSession();

        try {
            Criteria criteria = session.createCriteria(OAuthAccessToken.class);
            criteria.add(Restrictions.eq("token", token));

            List<OAuthAccessToken> types = criteria.list();
            if (null != types && 0 != types.size())
                return types.get(0);

        } catch (HibernateException ex) {
            log.error("Error at getAccessToken(" + token + ") call", ex);
        } finally {
            if (null != session &&  session.isConnected()) {
                session.flush();
                session.close();
            }
        }
        return null;
    }

    public static String getClientScopesByAccessToken(String accessToken) {
        final String mQuery = "SELECT oc.SCOPES FROM OAUTH_CLIENT oc JOIN OAUTH_ACCESS_TOKEN oat on oat.CLIENT_ID = oc.CLIENT_ID WHERE oat.TOKEN = :TOKEN";
        log.info("Getting Oauth Client Scopes token: "+accessToken);

        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            SQLQuery sqlQuery = session.createSQLQuery(mQuery);
            sqlQuery.setParameter("TOKEN", accessToken);
            Object result = sqlQuery.uniqueResult();
            if (null == result || result.toString().isEmpty())
                return null;
            return result.toString();

        }catch(Exception ex){
            log.error("",ex);
        }finally {
            log.info("OAuth Model Session Closed in getClientScopesByAccessToken!");
            if(session != null && session.isConnected()){
                session.clear();
                session.flush();
                session.close();
            }
        }
        return null;
    }

    public static boolean isTokenExpired(String token){
        if( null == token )
            return true;
        OAuthAccessToken authToken = getAccessToken(token);
        if( null != authToken ) {
            int lifetime = (int) ((authToken.getValidUntil().getTime() - new Date().getTime()) / 1E3);
            log.info("Access Token has " + lifetime + " seconds left to expire");
            return lifetime <= 0;
        }
        return true;
    }


    public static int getClientTokenValidity(String clientId){
        final String mQuery = "SELECT ACCESS_TOKEN_VALIDITY FROM OAUTH_CLIENT WHERE CLIENT_ID = :CLIENT_ID";
        log.info("Getting OAuth Client validity token "+clientId);

        Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            SQLQuery sqlQuery = session.createSQLQuery(mQuery);
            sqlQuery.setParameter("CLIENT_ID", clientId);
            Object result = sqlQuery.uniqueResult();
            if (null == result || result.toString().isEmpty())
                return 0;
            return ((BigDecimal)result).intValue();

        }catch(Exception ex){
            log.error("",ex);
        }finally {
            log.info("OAuth Model Session Closed in getClientTokenValidity!");
            if(session != null && session.isConnected()){
                session.clear();
                session.flush();
                session.close();
            }
        }
        return 0;
    }


    private static String generatePseudoToken(){
//        return UUID.randomUUID().toString();
        return new BigInteger(130, new SecureRandom()).toString(32);
    }
}
