package com.claro.cfc.db.dto.enums;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 9/30/2014.
 */
public enum TaskTypeEnum {
    DOMAIN{
        @Override
        public long id() {
            return 21L;
        }
    },GEFF{
        @Override
        public long id() {
            return 22L;
        }
    },SAD{
        @Override
        public long id() {
            return 23L;
        }
    },M6{
        @Override
        public long id() {
            return 24L;
        }
    };

    /**
     *
     * @return  data base record id, according with the default value inserted in CFC at schema creation time.
     */
    public abstract long id();
}
