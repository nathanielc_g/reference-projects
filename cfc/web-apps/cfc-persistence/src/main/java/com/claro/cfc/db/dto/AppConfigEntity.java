package com.claro.cfc.db.dto;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 6/1/2014.
 */

@Entity
@Table(name = "APP_CONFIG",  catalog = "")
@org.hibernate.annotations.NamedQueries(
    @org.hibernate.annotations.NamedQuery(name = "appConfig.configValue", query = "UPDATE AppConfigEntity SET configValue=:configValue where configKey=:configKey")
)
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "app.config")

public class AppConfigEntity {

    private Long cid;
    private String configKey;
    private String configValue;
    private String description;


    public AppConfigEntity() {
    }


    @Id
    @Column(name = "CID")
    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }


    @Basic
    @Column(name = "CONFIG_KEY")
    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    @Basic
    @Column(name = "CONFIG_VALUE")
    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    @Basic
    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AppConfigEntity)) return false;

        AppConfigEntity that = (AppConfigEntity) o;

        return cid != null ? cid.equals(that.cid) : that.cid == null;
    }

    @Override
    public int hashCode() {
        return cid != null ? cid.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "AppConfigEntity{" +
                "cid=" + cid +
                ", configKey='" + configKey + '\'' +
                ", configValue='" + configValue + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
