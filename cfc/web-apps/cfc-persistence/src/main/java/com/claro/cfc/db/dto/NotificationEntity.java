package com.claro.cfc.db.dto;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Nathaniel Calderon on 7/12/2017.
 */
@Entity
@Table(name = "NOTIFICATION", schema = "APPCAPFAC", catalog = "")
public class NotificationEntity {
    private long notifId;
    private long notifType;
    private long actionId;
    private Timestamp created;
    private Timestamp expiration;
    private long status;
    private String notifyVia;
    private String sendTo = "";
    private String message = "";

    @Id
    @Column(name = "NOTIF_ID")
    @GeneratedValue(generator = "notification.sec_notif_id", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "notification.sec_notif_id", sequenceName = "sec_notif_id")
    public long getNotifId() {
        return notifId;
    }

    public void setNotifId(long notifId) {
        this.notifId = notifId;
    }

    @Basic
    @Column(name = "NOTIF_TYPE")
    public long getNotifType() {
        return notifType;
    }

    public void setNotifType(long notifType) {
        this.notifType = notifType;
    }

    @Basic
    @Column(name = "ACTION_ID")
    public long getActionId() {
        return actionId;
    }

    public void setActionId(long actionId) {
        this.actionId = actionId;
    }

    @Basic
    @Column(name = "CREATED", insertable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "EXPIRATION", insertable = false)
    public Timestamp getExpiration() {
        return expiration;
    }

    public void setExpiration(Timestamp expiration) {
        this.expiration = expiration;
    }

    @Basic
    @Column(name = "STATUS", insertable = false)
    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    @Basic
    @Column(name = "NOTIFY_VIA")
    public String getNotifyVia() {
        return notifyVia;
    }

    public void setNotifyVia(String notifyVia) {
        this.notifyVia = notifyVia;
    }

    @Basic
    @Column(name = "MESSAGE")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Basic
    @Column(name = "SEND_TO")
    public String getSendTo() {
        return sendTo;
    }

    public NotificationEntity setSendTo(String sendTo) {
        this.sendTo = sendTo;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NotificationEntity)) return false;

        NotificationEntity that = (NotificationEntity) o;

        return notifId == that.notifId;
    }

    @Override
    public int hashCode() {
        return (int) (notifId ^ (notifId >>> 32));
    }

    @Override
    public String toString() {
        return "NotificationEntity{" +
                "notifId=" + notifId +
                ", notifType=" + notifType +
                ", actionId=" + actionId +
                ", created=" + created +
                ", expiration=" + expiration +
                ", status=" + status +
                ", notifyVia='" + notifyVia + '\'' +
                ", sendTo='" + sendTo + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
