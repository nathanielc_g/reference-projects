package com.claro.cfc.db.dto;

import org.hibernate.annotations.*;
import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/13/14.
 */
@Entity
@Table(name = "TASK", catalog = "")
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "task")
public class TaskEntity {
    private long taskId;
    private long actionId;
    private Timestamp created;
    private TaskTypeEntity type;
    private TaskStatusEntity status;
    private Set<TaskLogEntity> logs;
    private ActionsEntity action;


    @Id
    @Column(name = "TASK_ID")
    @GeneratedValue(generator = "tasks.sec_task",strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "tasks.sec_task",sequenceName = "sec_task")
    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }


    @Basic
    @Column(name = "ACTION_ID")
    public long getActionId() {
        return actionId;
    }

    public void setActionId(long actionId) {
        this.actionId = actionId;
    }

    @Basic
    @Column(name = "CREATED")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskEntity)) return false;

        TaskEntity that = (TaskEntity) o;

        return taskId == that.taskId;
    }

    @Override
    public int hashCode() {
        return (int) (taskId ^ (taskId >>> 32));
    }

    @Override
    public String toString() {
        return "TaskEntity{" +
                "taskId=" + taskId +
                ", actionId=" + actionId +
                ", created=" + created +
                ", type=" + type +
                ", status=" + status +
                '}';
    }

    @ManyToOne
    @JoinColumn(name = "TASK_TYPE_ID", referencedColumnName = "TASK_TYPE_ID", nullable = false)
    public TaskTypeEntity getType() {
        return type;
    }

    public void setType(TaskTypeEntity type) {
        this.type = type;
    }

    @ManyToOne
    @JoinColumn(name = "STATUS", referencedColumnName = "STATUS_ID")
    public TaskStatusEntity getStatus() {
        return status;
    }

    public void setStatus(TaskStatusEntity status) {
        this.status = status;
    }

    @OneToMany(mappedBy = "task", fetch = FetchType.LAZY)
    /*@Fetch(FetchMode.JOIN)*/
    public Set<TaskLogEntity> getLogs() {
        return logs;
    }

    public void setLogs(Set<TaskLogEntity> logs) {
        this.logs = logs;
    }

    public List<TaskLogEntity> listTaskLogEntity() {
        return new ArrayList<TaskLogEntity>(logs);
    }

    /*@Fetch(FetchMode.JOIN)*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTION_ID", referencedColumnName = "ACTION_ID", nullable = false, insertable = false, updatable = false)
    public ActionsEntity getAction() {
        return action;
    }

    public void setAction(ActionsEntity action) {
        this.action = action;
    }
}
