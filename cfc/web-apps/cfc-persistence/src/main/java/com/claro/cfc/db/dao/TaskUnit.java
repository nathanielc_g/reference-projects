package com.claro.cfc.db.dao;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 9/30/2014.
 */
abstract class TaskUnit{

    protected TaskUnit nextChain;

    public void handleNext(String type, long actionId){
        if( null == nextChain() )
            return;
        nextChain().create(type,actionId);
    }

    public TaskUnit( TaskUnit handler ) {
        this.nextChain = handler;
    }


    public  TaskUnit nextChain(){
        return nextChain;
    }

    public abstract void create(String type, long actionId );
}
