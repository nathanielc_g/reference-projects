package com.claro.cfc.db.dto.enums;

/**
 * Created by Nathaniel Calderon on 9/7/2016.
 */
public enum UserStatusEnum {
    INACTIVE
    ,ACTIVE
    ,TEMP_INACTIVE;
}
