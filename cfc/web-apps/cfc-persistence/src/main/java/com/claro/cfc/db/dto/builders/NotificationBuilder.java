package com.claro.cfc.db.dto.builders;

import com.claro.cfc.db.dto.NotificationEntity;
import com.claro.cfc.db.dto.enums.NotificationTypeEnum;
import com.claro.cfc.db.dto.enums.NotifyViaEnum;

import javax.management.Notification;

/**
 * Created by Nathaniel Calderon on 7/12/2017.
 */
public class NotificationBuilder {
    private long actionId;
    private NotificationTypeEnum notifType;
    private NotifyViaEnum notifyVia;
    private String message;

    public NotificationBuilder() {
    }

  /*  public NotificationBuilder(long actionId, NotificationTypeEnum notifType, NotifyViaEnum notifyVia, String message) {
        this.actionId = actionId;
        this.notifType = notifType;
        this.notifyVia = notifyVia;
        this.message = message;
    }
*/
    public NotificationBuilder setActionId(long actionId) {
        this.actionId = actionId;
        return this;
    }

    public NotificationBuilder setNotifType(NotificationTypeEnum notifType) {
        this.notifType = notifType;
        return this;
    }

    public NotificationBuilder setNotifyVia(NotifyViaEnum notifyVia) {
        this.notifyVia = notifyVia;
        return this;
    }

    public NotificationBuilder setMessage(String message) {
        this.message = message;
        return this;
    }

    public NotificationEntity build(){
        NotificationEntity notification = new NotificationEntity();
        notification.setActionId(this.actionId);
        notification.setNotifType(this.notifType.code());
        notification.setNotifyVia(this.notifyVia.name());
        notification.setMessage(this.message);
        return notification;
    }
}
