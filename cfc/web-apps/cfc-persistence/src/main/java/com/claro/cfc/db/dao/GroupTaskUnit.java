package com.claro.cfc.db.dao;

import com.claro.cfc.db.dto.enums.TaskTypeEnum;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 9/30/2014.
 */
class GroupTaskUnit extends TaskUnit {

    private  List<TaskUnit> taskUnits;

    public GroupTaskUnit(TaskUnit unit) {
        super(unit);
        taskUnits = new LinkedList<TaskUnit>();
        taskUnits.add(new M6TaskUnit(new GEFFTaskUnit( new DomainTaskUnit( new SADTaskUnit( null )))));
    }


    @Override
    public void create(String type, long actionId) {
        if (null != nextChain())
            nextChain.create(type, actionId);
        else {
            for (int i = 0; i < taskUnits.size(); ++i) {
                TaskUnit unit = taskUnits.get(i);
                unit.create(type, actionId);
            }
        }
    }


    static class M6TaskUnit extends TaskUnit {
        M6TaskUnit(TaskUnit handler) {
            super(handler);
        }

        @Override
        public void create(String type, long actionId) {
            if (type.equalsIgnoreCase("PSTN+DATA") || type.equalsIgnoreCase("FRAME+DATA")) {
                TaskAccess.createTask(TaskTypeEnum.M6,actionId);
            }
            handleNext(type, actionId);
        }
    }

    static class GEFFTaskUnit extends TaskUnit {

        GEFFTaskUnit(TaskUnit handler) {
            super(handler);
        }

        @Override
        public void create(String type, long actionId) {
            if (type.equalsIgnoreCase("PSTN+FIBRA") || type.equalsIgnoreCase("FIBRA")) {
                TaskAccess.createTask(TaskTypeEnum.GEFF,actionId);
            }
            handleNext(type, actionId);
        }
    }

    static class DomainTaskUnit extends TaskUnit{
        DomainTaskUnit(TaskUnit handler) {
            super(handler);
        }

        @Override
        public void create(String type, long actionId) {
            if (type.equalsIgnoreCase("PSTN") || type.equalsIgnoreCase("PSTN+DATA") || type.equalsIgnoreCase("PSTN+FIBRA") ) {
                TaskAccess.createTask(TaskTypeEnum.DOMAIN,actionId);
            }
            handleNext(type, actionId);
        }
    }

    static class SADTaskUnit extends TaskUnit{
        public SADTaskUnit(TaskUnit handler) {
            super(handler);
        }

        @Override
        public void create(String type, long actionId) {
            // since req. 2016-GESTI-0279, all forms now support SAD tasks
            //            if( type.equalsIgnoreCase("DTH") ){
            //                TaskAccess.createTask( TaskTypeEnum.SAD,actionId );
            //            }
            // @NathanielCalderon: since req. 2016-GESTI-1745, FRAME Form doesn't support SAD Tasks but then all others Forms still support it.
            if(!type.equalsIgnoreCase("FRAME") && !type.equalsIgnoreCase("FRAME+DATA"))
                TaskAccess.createTask(TaskTypeEnum.SAD, actionId);
            handleNext(type,actionId);
        }
    }
}
