package com.claro.cfc.db.dto;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/13/14.
 */
@Entity
@Table(name = "TASK_STATUS", catalog = "")
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "task.status")
public class TaskStatusEntity {
    private long statusId;
    private String status;
//    private List<TaskEntity> tasks;

    @Id
    @Column(name = "STATUS_ID")
    public long getStatusId() {
        return statusId;
    }

    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }

    @Basic
    @Column(name = "STATUS")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TaskStatusEntity{" +
                "statusId=" + statusId +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskStatusEntity)) return false;

        TaskStatusEntity that = (TaskStatusEntity) o;

        return statusId == that.statusId;
    }

    @Override
    public int hashCode() {
        return (int) (statusId ^ (statusId >>> 32));
    }

    //    @OneToMany(mappedBy = "status")
//    public List<TaskEntity> getTasks() {
//        return tasks;
//    }
//
//    public void setTasks(List<TaskEntity> tasks) {
//        this.tasks = tasks;
//    }
}
