package com.claro.cfc.db.dao.service;

import com.claro.cfc.db.dao.OrdersActionsAccess;
import com.claro.cfc.db.dto.ActionValuesEntity;
import com.claro.cfc.db.dto.ActionsEntity;
import com.claro.cfc.db.dto.TaskEntity;
import com.claro.cfc.db.dto.enums.TaskStatusEnum;
import com.claro.cfc.transport.Attribute;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.LongType;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Nathaniel Calderon on 5/4/2017.
 */
public class ActionService {

    private static Logger log = Logger.getLogger(OrdersActionsAccess.class);

    public static final long NON_PROCESSED_ACTION = 0L;
    private static final long PROCESSED_ACTION = 1L;

    public static final long DATA_FORMAT_NON_ERROR = 0L;
    public static final long DATA_FORMAT_ERROR = -1L;
    public static final long TRANSACTION_ERROR = -2L;

    private final Session currentSession;


    public ActionService (Session session) {
        if (!session.isConnected())
            throw new IllegalStateException("Session must be connected.");
        this.currentSession = session;

    }

    public static ActionsEntity buildAction (Map<String, String> attributes) {
        ActionsEntity actionsEntity = new ActionsEntity();
        actionsEntity.setCode(Long.valueOf(attributes.get("CODE")));
        actionsEntity.setFormType(attributes.get("FORM_TYPE"));
        actionsEntity.setOrderType(attributes.get("TYPE"));
        actionsEntity.setUserId(attributes.get("TARJETA"));
        //actionsEntity.setValues(new ArrayList<ActionValuesEntity>());
        actionsEntity.setValues(new HashSet<ActionValuesEntity>());
        return actionsEntity;
    }

    public static Map<String, String> toMap (List<Attribute> attributes) {
        if (null == attributes || null == attributes)
            return null;
        Map<String, String> map = new HashMap<String, String>();
        for(Attribute attribute: attributes)
            map.put(attribute.getName(), attribute.getValue());
        return map;
    }

    public static long validateAttributesFormat (Map<String, String> attributes) {
        if (null == attributes || null == attributes) {
            log.info("Refusing action submission due to illegal argument");
            return DATA_FORMAT_ERROR;
        }

        log.info("Acting on " + attributes);

        if (!attributes.containsKey("TARJETA")) {
            log.info("Refusing action submission due to missing tarjeta");
            return DATA_FORMAT_ERROR;
        }

        if (!attributes.containsKey("CODE")) {
            log.info("Refusing action submission due to missing code");
            return DATA_FORMAT_ERROR;
        }

        long orderCode = -1;
        try {
            orderCode = Long.valueOf(attributes.get("CODE"));
        } catch (Exception ex) {
            log.error("", ex);
            ex.printStackTrace();
        }
        if(orderCode < 0){
            log.info("Refusing action submission due to wrong code");
            return DATA_FORMAT_ERROR;
        }

        if (!attributes.containsKey("TYPE")) {
            log.info("Refusing action submission due to missing type");
            return DATA_FORMAT_ERROR;
        }

        if (!attributes.containsKey("FORM_TYPE")) {
            log.info("Refusing action submission due to missing form type");
            return DATA_FORMAT_ERROR;
        }
        return DATA_FORMAT_NON_ERROR;
    }

    public List<ActionsEntity> findActionsByRelatedOrder (ActionsEntity actionsEntity) {
        return (List<ActionsEntity>) buildCriteriaToFindActionByOrder(actionsEntity).list();
    }

    public List<ActionsEntity> findPendingActionsByRelatedOrder (ActionsEntity actionsEntity) {
        Criteria criteria = buildCriteriaToFindActionByOrder(actionsEntity);
        criteria.add(Restrictions.sqlRestriction("NVL(processed, 0) != ?", PROCESSED_ACTION, LongType.INSTANCE));
        return (List<ActionsEntity>) criteria.list();
    }

    private Criteria buildCriteriaToFindActionByOrder(ActionsEntity actionsEntity) {
        Criteria criteria = currentSession.createCriteria(ActionsEntity.class);
        criteria.add(Restrictions.eq("code", actionsEntity.getCode()));
        criteria.add(Restrictions.eq("userId", actionsEntity.getUserId()));
        criteria.add(Restrictions.eq("orderType", actionsEntity.getOrderType()));
        return criteria;
    }


    public void deletePendingTasks(ActionsEntity actionsEntity) {
        List<TaskEntity> foundTasks = findPendingTasksByAction(actionsEntity);
        for(TaskEntity task: foundTasks)
            deleteTask(task);
    }

    public List<TaskEntity> findPendingTasksByAction (ActionsEntity actionsEntity) {
        Query taskQuery = currentSession.createQuery("from TaskEntity where actionId=? and status.statusId=?");
        taskQuery.setParameter(0, actionsEntity.getActionId());
        taskQuery.setParameter(1, TaskStatusEnum.PENDING.id());
        return taskQuery.list();
    }

    public void deleteTask(TaskEntity taskEntity) {
        currentSession.delete(taskEntity);
    }

    public void deleteAction (ActionsEntity actionsEntity) {
        /*deleteNotification(actionsEntity);*/
        currentSession.delete(actionsEntity);
    }

    /*private void deleteNotification (ActionsEntity actionsEntity) {
        Query q = currentSession.createQuery("delete from NotificationEntity where actionId = :actionId");
        q.setLong("actionId", actionsEntity.getActionId());
        q.executeUpdate();
    }*/

    public void saveOrUpdate (ActionsEntity actionsEntity, List<Attribute> attributes) {
        Timestamp current = new Timestamp(new Date().getTime());
        actionsEntity.setCreated(current);
        currentSession.saveOrUpdate(actionsEntity);
        saveOrUpdateAttributes(actionsEntity, attributes, current);
    }

    private void saveOrUpdateAttributes(ActionsEntity actionsEntity, List<Attribute> attributes, Timestamp current) {
        for(Attribute attribute: attributes)
            saveOrUpdateAttribute(actionsEntity, attribute, current);
    }

    private void saveOrUpdateAttribute(ActionsEntity actionsEntity, Attribute attribute, Timestamp current) {
        ActionValuesEntity value = new ActionValuesEntity();
        value.setCreated(current);
        value.setAction(actionsEntity);
        value.setAkey(attribute.getName());
        if(attribute.getName().equals("TELEFONO"))
            value.setAvalue(attribute.getValue().replace( "-","" ).replace("(","").replace(")","").replace(" ",""));
        else
            value.setAvalue(attribute.getValue());
        currentSession.saveOrUpdate(value);
    }

}
