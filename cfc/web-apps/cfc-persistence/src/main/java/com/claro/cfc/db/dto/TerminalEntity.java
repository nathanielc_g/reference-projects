package com.claro.cfc.db.dto;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/13/14.
 */
@Entity
@Table(name = "TERMINAL")
@org.hibernate.annotations.NamedQueries({
        @NamedQuery(name = "terminals.updateAppVersion", query="UPDATE TerminalEntity SET appVersion=:version" ),
        @NamedQuery(name = "terminals.updateAppVersionByImei", query="UPDATE TerminalEntity SET appVersion=:version where imei=:imei" )
        //@NamedQuery(name = "terminals.terminalVersionByCard", query="SELECT T.APP_VERSION AS VERSION FROM USERS U INNER JOIN TERMINAL T  ON U.IMEI = T.IMEI  WHERE U.USER_ID=:card" )
})
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "terminal")
public class TerminalEntity {
    private String imei;
    private String tbrand;
    private String tmodel;
    private Timestamp created;
    private Double osVersion;
    private Double appVersion;
    private List<UsersEntity> users;


    @Id
    @Column(name = "IMEI")
    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    @Basic
    @Column(name = "TBRAND")
    public String getTbrand() {
        return tbrand;
    }

    public void setTbrand(String tbrand) {
        this.tbrand = tbrand;
    }

    @Basic
    @Column(name = "TMODEL")
    public String getTmodel() {
        return tmodel;
    }

    public void setTmodel(String tmodel) {
        this.tmodel = tmodel;
    }

    @Basic
    @Column(name = "CREATED")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "OS_VERSION")
    public Double getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(Double osVersion) {
        this.osVersion = osVersion;
    }

    @Basic
    @Column(name = "APP_VERSION")
    public Double getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(Double appVersion) {
        this.appVersion = appVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TerminalEntity)) return false;

        TerminalEntity that = (TerminalEntity) o;

        return imei != null ? imei.equals(that.imei) : that.imei == null;
    }

    @Override
    public int hashCode() {
        return imei != null ? imei.hashCode() : 0;
    }

    @OneToMany(mappedBy = "terminal",fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    public List<UsersEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UsersEntity> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "TerminalEntity{" +
                "imei='" + imei + '\'' +
                ", tbrand='" + tbrand + '\'' +
                ", tmodel='" + tmodel + '\'' +
                ", created=" + created +
                ", osVersion=" + osVersion +
                ", appVersion=" + appVersion +
                '}';
    }
}
