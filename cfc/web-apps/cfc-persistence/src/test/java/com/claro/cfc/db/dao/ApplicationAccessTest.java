package com.claro.cfc.db.dao;

import com.claro.cfc.db.dao.ApplicationAccess;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * Created by Nathaniel Calderon on 5/3/2017.
 */
public class ApplicationAccessTest {
    @Test(enabled = false)
    public void testIfCanPostOrderMultipletimes () {
        assertTrue(ApplicationAccess.canPostOrderMultipletimes());
    }

    @Test(enabled = false)
    public void testIfCannotPostOrderMultipletimes () {
        assertFalse(ApplicationAccess.canPostOrderMultipletimes());
    }
}
