package com.claro.cfc.db.dao;

import com.claro.cfc.db.dao.OrdersActionsAccess;
import com.claro.cfc.db.dao.TaskAccess;
import com.claro.cfc.db.dto.ActionValuesEntity;
import com.claro.cfc.db.dto.ActionsEntity;
import com.claro.cfc.db.dto.TaskEntity;
import com.claro.cfc.transport.Attribute;
import org.testng.annotations.Test;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;

import static org.testng.Assert.*;

/**
 * Created by Nathaniel Calderon on 5/3/2017.
 */
public class OrderActionAccessTest {
    @Test(enabled = false)
    public void getActionByDateTest () {
        List<ActionsEntity> actions = OrdersActionsAccess.getActionsByDate("2017-01-01");
        for(ActionsEntity action: actions)
            saveAction(action);

        assertEquals(1, 1);
    }

    private void saveAction (ActionsEntity action) {
        List<ActionValuesEntity> values = action.listOfValues();
        List<Attribute> attributes = new ArrayList<Attribute>();
        for(ActionValuesEntity value: values)
            attributes.add(new Attribute(value.getAkey(), value.getAkey().equals("CODE")? generateNewRandomCode(value.getAvalue()): value.getAvalue()));
        long actionId = OrdersActionsAccess.saveAction(attributes, ApplicationAccess.canPostOrderMultipletimes());
        Attribute formType = attributes.get(attributes.indexOf(Attribute.createAttribute("FORM_TYPE", "")));
        TaskAccess.createTasks(formType.getValue(), actionId);
    }

    private String generateNewRandomCode (String currentCode) {
        return currentCode.substring(0,4) + String.format("%04d", new Random().nextInt(1000));
    }

    @Test(enabled = false)
    public void testSaveAction () {


        String card = "13026";
        List<Attribute> attributes = new ArrayList<Attribute>();
        attributes.add(new Attribute("TELEFONO","809-547-1372"));
        attributes.add(new Attribute("CABINA","CD23J"));
        attributes.add(new Attribute("PAR_LOCAL","CD23J-0926"));
        attributes.add(new Attribute("LOCALIDAD","DUARTE (5ESS)"));
        attributes.add(new Attribute("CUENTA_FIJA","NO"));
        attributes.add(new Attribute("PAR_FEEDER","D05-0034"));
        attributes.add(new Attribute("TERMINAL","CD23J-9J"));
        attributes.add(new Attribute("DIRECCION_CORRECTA","SI"));
        attributes.add(new Attribute("TERMINAL_FO","FCBVA3L-K31"));
        attributes.add(new Attribute("CABINA_FO","FCBVA3L"));
        attributes.add(new Attribute("SPLITTER_PORT","SPT-FCBVA3L-K31-SP06"));
        attributes.add(new Attribute("SPLITTER","SPT-FCBVA3L-K31"));
        attributes.add(new Attribute("CODE","120527285"));
        attributes.add(new Attribute("TYPE","ORDEN"));
        attributes.add(new Attribute("FORM_TYPE","PSTN+FIBRA"));
        attributes.add(new Attribute("imei","013766005953352"));
        attributes.add(new Attribute("COORDVALIDS","False"));
        attributes.add(new Attribute("ID_DIRECCION","45887352"));
        attributes.add(new Attribute("LATITUD",""));
        attributes.add(new Attribute("LONGITUD",""));
        attributes.add(new Attribute("COORDATTEMPTS","true"));
        attributes.add(new Attribute("TARJETA","13026"));

        assertNotEquals(OrdersActionsAccess.saveAction(attributes, ApplicationAccess.canPostOrderMultipletimes()), -1L, "Format is incorrect");
        assertNotEquals(OrdersActionsAccess.saveAction(attributes, ApplicationAccess.canPostOrderMultipletimes()), -2L, "There were some errors on transaction");
    }

    @Test(enabled = true)
    public void testGetLastAction () {

        ActionsEntity actionsEntity = OrdersActionsAccess.getLastAction("13026", "120668778", "ORDEN");
        assertTrue(actionsEntity.getTasks().size()>0);
        assertTrue(actionsEntity.getCode() == 120668778);
        assertTrue(OrdersActionsAccess.getLastAction("13026", "1205272851", "ORDEN") == null);
    }
}
