package com.claro.cfc.db.dao;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class UsersAccessTest {
    @BeforeMethod
    public void setUp() throws Exception {
    }

    @AfterMethod
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetUsersOutOfDate() throws Exception {
    }

    @Test
    public void testGetUserByCard() throws Exception {
    }

    @Test
    public void testGetEagerUserByCard() throws Exception {
    }

    @Test
    public void testGetUserStatus() throws Exception {
    }

    @Test
    public void testGetUserByPhone() throws Exception {
    }

    @Test
    public void testGetPhonesByVersion() throws Exception {
        assertTrue(UsersAccess.getPhonesByVersion("30343").size() > 0);
    }

    @Test
    public void testGetUserByImei() throws Exception {
    }

    @Test
    public void testGetUsersByImei() throws Exception {
    }

    @Test
    public void testGetUserByImeiOrStatus() throws Exception {
    }

    @Test
    public void testGetUserByCardAndImei() throws Exception {
    }

    @Test
    public void testGetAllUsers() throws Exception {
    }

    @Test
    public void testUpdateUser() throws Exception {
    }

    @Test
    public void testSaveUserRecord() throws Exception {
    }

    @Test
    public void testLogUser() throws Exception {
    }

    @Test
    public void testLogUser1() throws Exception {
    }

    @Test
    public void testGetUsersCount() throws Exception {
        assertTrue(UsersAccess.getUsersCount() > 1);
    }

    @Test
    public void testGetUserLogsByDiscriminator() throws Exception {
    }

    @Test
    public void testGetUserLogsByDiscriminator1() throws Exception {
    }

    @Test
    public void testBulkUsers() throws Exception {
    }

}