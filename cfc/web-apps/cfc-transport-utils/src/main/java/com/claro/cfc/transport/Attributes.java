package com.claro.cfc.transport;

import java.util.List;

/**
 * Created by Nathaniel Calderon on 7/12/2017.
 */
public final class Attributes {
    private Attributes() {
    }

    public static Attribute find(List<Attribute> attrs, String attrName){
        Attribute attr = Attribute.createAttribute(attrName, "");
        int idx = attrs.indexOf(attr);
        if(idx == -1)
            return null;
        return attrs.get(idx);
    }

    public static Attribute findOrCreate(List<Attribute> attrs, String attrName, String newValue){
        Attribute attr = Attribute.createAttribute(attrName, "");
        int idx = attrs.indexOf(attr);
        if(idx == -1)
            return attr;
        return attrs.get(idx);
    }
}
