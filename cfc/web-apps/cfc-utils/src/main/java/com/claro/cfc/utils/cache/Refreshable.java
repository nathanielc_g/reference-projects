package com.claro.cfc.utils.cache;

/**
 * Created by Jansel Valentin on 9/29/2016.
 **/
public interface Refreshable {
    void refresh();
}
