package com.claro.cfc.utils.alert;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jansel Valentin on 2/15/2016.
 **/
public final class AlertEmitters {

    private static final Logger logger = Logger.getLogger(AlertEmitters.class);
    private static int instance = 1;

    private static Map<String,AlertEmitter> emitters = new HashMap<String, AlertEmitter>();

    public static void registerEmitter(String name,AlertEmitter emitter){
        if( null == emitter )
            return;
        emitters.put(name,emitter);
    }

    public static void unregister(String name){
        emitters.remove(name);
    }

    public static AlertEmitter getEmitter(String emitter){
        return emitters.get(emitter);
    }
}
