package com.claro.cfc.utils;

import org.apache.log4j.Logger;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by Jansel Valentin on 10/3/2016.
 **/
public final class Retry {
    private Retry(){}

    private static final Logger log = Logger.getLogger(Retry.class);

    public static final <Ret> Ret backoff(Operation<? super Ret> op, int maxRetries){
        return backoff(op,maxRetries,TimeUnit.MILLISECONDS);
    }


    public static final <Ret> Ret backoff(Operation<? super Ret> op, int maxRetries, TimeUnit unit) {
        Ret result = null;

        boolean fulfilled = false;
        int retry = 1;
        while( !fulfilled && maxRetries >= retry ){
            try{
                result = (Ret) op.run();
                log.info( "Retry operation done successfully with result "+result );
                fulfilled = true;
            }catch ( Exception ex ){
                log.error( "Failed to perform operation retry #"+retry+1 );
            }
            if(!fulfilled){
                try{
                    int backoff = 1;
                    if( maxRetries >= retry ) {
                        backoff = (int) Math.floor(Math.pow(2, retry) - 1);
                        backoff = new Random( backoff +1 ).nextInt( backoff + 1 );
                    }
                    unit.sleep(backoff);
                }catch ( InterruptedException ex ){
                }
                ++retry;
            }
        }
        return result;
    }


    public interface Operation<Ret>{
        Ret run() throws Exception;
    }
}
