package com.claro.cfc.utils.alert.paw;

import com.claro.api.client.paw.PAWAPIClient;
import com.claro.cfc.utils.alert.Alert;
import com.claro.cfc.utils.alert.AlertEmitter;
import com.claro.cfc.utils.alert.EmailAlert;
import org.apache.log4j.Logger;

/**
 * Created by Jansel Valentin on 2/15/2016.
 **/
public class Emailer implements AlertEmitter {

    private static final Logger log = Logger.getLogger(Emailer.class);

    private PAWAPIClient pawapiClient;

    public Emailer(PAWAPIClient pawapiClient) {
        this.pawapiClient = pawapiClient;
    }


    @Override
    public void emit(Alert alert, String enviroment) {
        if (null != pawapiClient && alert instanceof EmailAlert) {
            final EmailAlert eAlert = (EmailAlert) alert;

            final EmailAlert.Preset preset = eAlert.getPreset();

            final String toChain = join(preset.getTo());
            try {
                pawapiClient.getService().sendMail(preset.getFrom(), toChain, preset.getSubject() + ":" + enviroment, preset.getMessage());
            }catch( Exception ex){
                log.error(ex);
            }
        }
    }

    private String join(String[] emails){
        StringBuilder builder = new StringBuilder();
        for( String email : emails ){
            if( 0 != builder.length() )
                builder.append( "," );
            builder.append( email );
        }
        return builder.toString();
    }
}
