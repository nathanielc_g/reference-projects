package com.claro.cfc.utils.alert;

/**
 * Created by Jansel Valentin on 2/15/2016.
 **/
public interface AlertEmitter {
    void emit(Alert alert, String enviroment);
}
