package com.claro.cfc.utils.memory;

/**
 * Created by Jansel Valentin on 2/2/2015.
 */
public abstract class ResourceHandler<T> {
    public abstract T create();

    public abstract void clean(T resource);
}
