package com.claro.cfc.utils.alert;

/**
 * Created by Jansel Valentin on 2/15/2016.
 **/
public class EmailAlert extends Alert {

    private Preset preset;

    public EmailAlert() {
    }

    public Preset getPreset() {
        return preset;
    }

    public void setPreset(Preset preset) {
        this.preset = preset;
    }

    public static class Preset{
        private String from;
        private String[] to;
        private String subject;
        private String message;

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String[] getTo() {
            return to;
        }

        public void setTo(String[] to) {
            this.to = to;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
