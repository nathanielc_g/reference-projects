package com.claro.cfc.utils.alert;

/**
 * Created by Jansel Valentin on 2/15/2016.
 **/
public abstract  class Alert {
    protected String emitter;
    protected boolean enabled;

    public String getEmitter() {
        return emitter;
    }

    public void setEmitter(String emitter) {
        this.emitter = emitter;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
