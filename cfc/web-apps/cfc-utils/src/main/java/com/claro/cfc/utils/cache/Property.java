package com.claro.cfc.utils.cache;

/**
 * Created by Jansel Valentin on 9/29/2016.
 **/
public abstract class Property<Type> {
    protected Type value;

    public Property(Type value) {
        this.value = value;
    }

    public Type getValue() {
        return value;
    }

    public void setValue(Type value) {
        this.value = value;
    }
}
