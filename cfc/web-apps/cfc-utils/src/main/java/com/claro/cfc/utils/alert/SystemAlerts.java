package com.claro.cfc.utils.alert;

import com.google.gson.Gson;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import org.apache.log4j.Logger;

import java.io.InputStream;

/**
 * Created by Jansel Valentin on 2/15/2016.
 **/
public final class SystemAlerts {

    private static final Logger logger = Logger.getLogger(SystemAlerts.class);
    private static Object alerts;
    private static Gson gson;

    public static void emit(String alert, String enviroment) {
        final Alert lAlert = alertForPath(alert);
        if( null != lAlert && lAlert.enabled ){

            AlertEmitter emitter;
            if( null != (emitter = AlertEmitters.getEmitter(lAlert.getEmitter()))) {
                emitter.emit(lAlert, enviroment);
            }else{
                logger.info( "There isn't "+lAlert.getEmitter()+" emitter registered" );
            }
        }else{
            logger.info( "alert "+alert+" is currently disabled" );
        }
    }


    private static void loadAlerts(){
        if( null != alerts )
            return;

        InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("system-alerts.json");
        if( null != in ) {
            try {
                alerts = Configuration.defaultConfiguration().jsonProvider().parse(in, "UTF-8");
            }catch ( Exception ex ){
                logger.error( "Error trying to load system-alerts.json file" );
                logger.error( "",ex );
            }
        }
        gson = new Gson();
    }


    private static Alert alertForPath(String path) throws  IllegalArgumentException{
        if( null == alerts)
            loadAlerts();
        if( null == path )
            throw new IllegalArgumentException("There must be a valid alert path");

        Class<? extends Alert> alertType = null;
        if( path.contains("email") ) {
            alertType = EmailAlert.class;
        }
        if( null == alertType )
            throw new IllegalArgumentException(path+" kind of alert is not recognized");

        try {
            Object alert = JsonPath.read(alerts,"$.alerts."+path);
            return gson.fromJson( gson.toJson(alert),alertType );
        } catch (Exception ex) {
            logger.error("",ex);
        }
        return null;
    }
}
