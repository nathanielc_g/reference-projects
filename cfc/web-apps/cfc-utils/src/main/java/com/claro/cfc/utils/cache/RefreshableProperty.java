package com.claro.cfc.utils.cache;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by Jansel Valentin on 9/29/2016.
 **/
public class RefreshableProperty<Type> extends Property<Type> implements Refreshable {
    private AtomicReference<Type> ref;

    private boolean autoRefreshable;
    private long refreshInterval;
    private TimeUnit unit = TimeUnit.MINUTES;

    private ValueAdapter<? extends Type> valueAdapter;

    public RefreshableProperty(Type value) {
        super(null);
        ref = new AtomicReference<Type>(value);
    }

    public RefreshableProperty(boolean autoRefreshable, ValueAdapter<? extends Type> valueAdapter) {
        this(autoRefreshable, 60, TimeUnit.MINUTES, valueAdapter);
    }

    public RefreshableProperty(boolean autoRefreshable, long refreshInterval, TimeUnit unit, ValueAdapter<? extends Type> valueAdapter) {
        super(null);
        this.refreshInterval = refreshInterval;
        this.autoRefreshable = autoRefreshable;
        this.unit = unit;
        this.valueAdapter = valueAdapter;

        checkRef();
        startPurgeWorker();
    }

    @Override
    public void refresh() {
        if (null != valueAdapter && null != ref)
            ref.set(valueAdapter.get());
    }

    @Override
    public Type getValue() {
        checkRef();
        return ref.get();
    }

    @Override
    public void setValue(Type value) {
        checkRef();
        ref.set(value);
    }

    private void checkRef() {
        if (null == ref) {
            ref = new AtomicReference<Type>();
            if (null != valueAdapter)
                ref.set(valueAdapter.get());
        }
    }

    private void startPurgeWorker() {
        if (!autoRefreshable)
            return;
        if (0 >= refreshInterval)
            refreshInterval = 60;

        ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
        exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                refresh();
            }
        }, refreshInterval, refreshInterval, unit);
    }

    public interface ValueAdapter<T> {
        T get();
    }
}
