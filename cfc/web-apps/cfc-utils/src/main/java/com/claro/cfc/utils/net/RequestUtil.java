package com.claro.cfc.utils.net;

import com.claro.cfc.utils.memory.DeferredReference;
import com.claro.cfc.utils.memory.DeferredReferenceShelf;
import com.claro.cfc.utils.net.memory.ByteBufferShelvesManager;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 1/29/2015.
 */
public final class RequestUtil {

    public String retrieveGetContent(String url) {

        boolean skipResponse = false;

        HttpURLConnection conn = null;
        String output = null;
        try {
            URL urlContent = new URL(url);
            conn = (HttpURLConnection) urlContent.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.setReadTimeout(300000);
            conn.setConnectTimeout(300000);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed: Http  Code:" + conn.getResponseCode());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            skipResponse = true;
        } catch (IOException e) {
            e.printStackTrace();
            skipResponse = true;
        }

        if (!skipResponse) {
            /**
             * Modified by Jansel
             *
             * Best way to manage Out of Memory
             */
            ReadableByteChannel channel = null;
            StringBuilder builder = new StringBuilder();

            try {
                channel = Channels.newChannel(conn.getInputStream());

                DeferredReference<ByteBuffer> ref = null;
                DeferredReferenceShelf shelf = ByteBufferShelvesManager.get();
                try {
                    synchronized (shelf) {
                        ref = shelf.take();
                    }
                }catch ( Exception ex) {
                    ex.printStackTrace();
                }
                ByteBuffer buffer = ref.getResource();

                int i;
                for (; ; ) {
                    i = channel.read(buffer);
                    buffer.flip();

                    builder.append(Charset.forName("UTF-8").decode(buffer));
                    buffer.clear();
                    if (0 >= i)
                        break;
                }
                buffer.clear();
                output = builder.toString();
                builder.setLength(0);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } finally {
                try {
                    if (null != conn)
                        conn.disconnect();

                    if (null != channel)
                        channel.close();

                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
        return output;
    }
}
