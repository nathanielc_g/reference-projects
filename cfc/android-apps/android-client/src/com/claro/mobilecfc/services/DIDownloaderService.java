package com.claro.mobilecfc.services;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.*;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import com.claro.mobilecfc.activities.prefs.UserKnowledge;
import com.claro.mobilecfc.db.contracts.DIListContract;
import com.claro.mobilecfc.db.contracts.DispatchItemsContract;
import com.claro.mobilecfc.utils.Constants;
import com.claro.mobilecfc.utils.HttpRestRequester;
import com.claro.mobilecfc.utils.HttpUtils;
import com.claro.mobilecfc.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.tjeannin.provigen.ProviGenBaseContract;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.StringReader;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.*;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/12/14.
 */
public class DIDownloaderService extends IntentService {
    private static final String NAME = "DownloaderService";

    public static final int ACTION_BIND_SUBSCRIBE = 1;
    public static final int ACTION_BIND_PING = 2;
    public static final int ACTION_BIND_PING_AND_SUBSCRIBE = 3;
    public static final int ACTION_BIND_NOTIFICATION_ISSUED = 4;

    private boolean loadingData;

    private SparseArray<Messenger> listeners = new SparseArray<>();


    public DIDownloaderService() {
        super(NAME);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new Messenger(handler).getBinder();
    }


    @Override
    protected void onHandleIntent(Intent intent) {

        Log.e(NAME, "Trying to handle onHandleIntent");

        String card = UserKnowledge.getUserCard(this);
        if (TextUtils.isEmpty(card)) {
            Log.e(NAME, "can't begin dispatch items downloader due to missed card");
            return;
        }

        loadingData = true;
        publish(loadingData);

        Log.e(NAME, "Begin Dispatch Items download process for card " + card);
        String deviceId = Utils.getDeviceImei(this);

        String path = HttpUtils.matchSymbol(HttpUtils.PATH_TEMPLATE_ITEMS, "@", "", new String[]{card, deviceId});
        String url = HttpUtils.SERVER_URL + path;

        Log.e(NAME, "Attempt to make request to " + url);
        HttpGet get = new HttpGet(url);


        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, HttpRestRequester.CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, HttpRestRequester.SOCKET_TIMEOUT);

        HttpClient client = new DefaultHttpClient(params);
        HttpResponse response = null;
        try {
            response = client.execute(get);
        } catch (IOException e) {
            Log.e(NAME, "Some error happens at download time " + e);
            e.printStackTrace();
        }

        if (response != null && response.getStatusLine().getStatusCode() == 200) {
            try {
                String line = EntityUtils.toString(response.getEntity());
                processResponse(line);
            } catch (IOException ioe) {
                Log.e(NAME, "Some error happens at download time " + ioe);
                ioe.printStackTrace();
            }
        } else {
            if (null != response)
                Log.e(NAME, "Some error happens and retrieved error code " + response.getStatusLine().getStatusCode() + ", exiting...");
            else
                Log.e(NAME, "Something go completely wrong with null response");
        }

        Log.e(NAME, "Exiting from download ------- ");
        loadingData = false;
        publish(loadingData);
    }

    private void processResponse(String json) {
        Log.e(NAME, "Parsing response: " + json);
        // TODO: ELIMINAR ESTA LINEA
        //json = "{\"OrdenesAsignadas\":[[{\"Key\":\"CODIGO\",\"Value\":\"23993002\"},{\"Key\":\"NUMEROSERVICIO\",\"Value\":\"1010102055\"},{\"Key\":\"TECNICO\",\"Value\":\"57225\"},{\"Key\":\"ISASIGNADA\",\"Value\":\"1\"},{\"Key\":\"TIPO_ORDEN\",\"Value\":\"AVERIA\"},{\"Key\":\"SOLICITUD\",\"Value\":\"23993002\"},{\"Key\":\"COMPROMISO_COPC\",\"Value\":\"Apr 21 2016 11:59PM\"},{\"Key\":\"FECHA_COMPROMISO\",\"Value\":\"Apr 21 2016 11:59PM\"},{\"Key\":\"FECHA_ASIGNACION\",\"Value\":\"4/25/2016 12:50:31 PM\"},{\"Key\":\"NUMEROCONTACTO\",\"Value\":\"8093630204\"},{\"Key\":\"IDCONTACTO\",\"Value\":\"8314218\"},{\"Key\":\"ID_DIRECCION\",\"Value\":\"43657322\"},{\"Key\":\"TICKET_RECLAMACION\",\"Value\":\"NO\"},{\"Key\":\"NUMERO_CASO\",\"Value\":\"23993002\"},{\"Key\":\"TIPO_PRIORIDAD\",\"Value\":\"AVERIAS VENCIDAS >= 3 DIAS\"},{\"Key\":\"SECUENCIA_TRABAJO\",\"Value\":\"8\"},{\"Key\":\"TANDA\",\"Value\":\"Ma�ana\"},{\"Key\":\"CANT_DIAS\",\"Value\":\"3\"},{\"Key\":\"CLIENTE\",\"Value\":\"ENTREPRISES,CXA.\"},{\"Key\":\"DIRECCION\",\"Value\":\"C\\\\BOHECHIO No.35 QUISQUEYA, SANTO DOMINGO DE GUZMAN\"},{\"Key\":\"SECTOR\",\"Value\":\"QUISQUEYA\"},{\"Key\":\"CIUDAD\",\"Value\":\"SANTO DOMINGO DE GUZMAN\"},{\"Key\":\"DISTRITO\",\"Value\":\"METRO 1 (11)\"},{\"Key\":\"AVISAR\",\"Value\":\"ENTREPRISES,CXA. - 8093630204\"},{\"Key\":\"ULTIMO_CODIGO\",\"Value\":\"1502 - 15|Inspecci�n - 02|NO Acceso, Servicio Ok\"},{\"Key\":\"TECNICO_ULTIMO_CODIGO\",\"Value\":\"57225 - RICARDO COLON\"},{\"Key\":\"CELULAR_TECNICO\",\"Value\":\"8097235826\"},{\"Key\":\"FEEDER\",\"Value\":\" : \"},{\"Key\":\"FAC_LOCAL\",\"Value\":\" : \"},{\"Key\":\"TERMINAL\",\"Value\":\"\"},{\"Key\":\"FLAGDATO\",\"Value\":\"1\"},{\"Key\":\"TECNOLOGIA\",\"Value\":\"Satelital\"},{\"Key\":\"FECHA_DESPACHO\",\"Value\":\"4/25/2016 12:00:00 AM\"},{\"Key\":\"TIPO_LOCALIDAD\",\"Value\":\"DTH\"},{\"Key\":\"LONGITUD\",\"Value\":\"0.0\"},{\"Key\":\"LATITUD\",\"Value\":\"0.0\"},{\"Key\":\"COORDVALIDS\",\"Value\":\"False\"},{\"Key\":\"NOMBRE_TECNICO\",\"Value\":\"RICARDO COLON\"},{\"Key\":\"SUPERVISOR\",\"Value\":\"OMAR BALLISTA\"},{\"Key\":\"PROCESO\",\"Value\":\"DTH\"},{\"Key\":\"GRUPO_TRABAJO\",\"Value\":\"Distrito-Bloque\"},{\"Key\":\"COLA\",\"Value\":\"AVDTH\"},{\"Key\":\"FECHA_CREACION\",\"Value\":\"Apr 20 2016  8:12PM\"},{\"Key\":\"SINTOMA\",\"Value\":\"Intermitencia De Senal\"},{\"Key\":\"PRUEBA4TEL\",\"Value\":\"\"},{\"Key\":\"CONTACTO\",\"Value\":\"MIRIAM DIA\"},{\"Key\":\"FECHA_VISITA\",\"Value\":\"Apr 20 2016  8:10PM\"},{\"Key\":\"TANDA_VISITA\",\"Value\":\"Ma�ana\"},{\"Key\":\"CABLE\",\"Value\":\"\"},{\"Key\":\"CABINA\",\"Value\":\"\"},{\"Key\":\"SERIAL_EQUIPO\",\"Value\":\"\"},{\"Key\":\"POLIGONO\",\"Value\":\"\"},{\"Key\":\"PUERTO\",\"Value\":\"\"},{\"Key\":\"FACILIDADES_FIBRA\",\"Value\":\"\"},{\"Key\":\"SEGMENTO\",\"Value\":\"E\"},{\"Key\":\"CONFIRMACION_SERVICIO\",\"Value\":\"CONFIRMACION: HABLE CON LA SRA. MIRIAM, ME INFORMA SERVICIO YA FUE MIGRADO Y AUN ESTA INTERMITENTE LA SENAL. FAVOR VERIFICAR!!! \"},{\"Key\":\"REFERENCIA_DIRECCION\",\"Value\":\"\"},{\"Key\":\"DATOS_SERIALES\",\"Value\":\"Serial:1682769454,Tipo:SD,Fecha:2015/12/12,SmartCard:1303597793,Garantia:NO\\rSerial:1682618502,Tipo:SD,Fecha:2016/01/06,SmartCard:1303597793,Garantia:NO\"},{\"Key\":\"DATOS_CONTACTO\",\"Value\":\"TipoContacto:Adicional,TipoDato:Esposo/ Esposa,Contacto:CTE,8097722020\\rTipoContacto:Alterno,TipoDato:Telefono Trabajo,Contacto:8099999999\\rTipoContacto:Alterno,TipoDato:Fax,Contacto:8099999999\\rTipoContacto:Alterno,TipoDato:Telefono Trabajo (Extension),Contacto:9999\"}]],\"hasError\":false,\"ErrorDescription\":null}";
        Gson gson = new Gson();
        try {
            try {
                JsonReader jReader = new JsonReader(new StringReader(json.trim()));

                /**
                 * Json string is possible come deformed
                 */
                jReader.setLenient(true);

                JsonObject objetoResponse = gson.fromJson(jReader, JsonObject.class);

                JsonArray ordersArray = objetoResponse.get("OrdenesAsignadas").getAsJsonArray();
                if (null == ordersArray)
                    return;

                Reference<List<RemoteOrder>> remoteOrdersRef = new WeakReference<List<RemoteOrder>>(new LinkedList<RemoteOrder>());

                for (int i = 0; ordersArray.size() > i; ++i) {

                    JsonArray orderArray = (JsonArray) ordersArray.get(i);
                    RemoteOrder dOrder = new RemoteOrder();

                    for (int j = 0; orderArray.size() > j; ++j) {
                        JsonObject kvp = (JsonObject) orderArray.get(j);
                        String key = kvp.get("Key").getAsString();
                        String value = kvp.get("Value").getAsString();

                        if ("CODIGO".equalsIgnoreCase(key)) {
                            dOrder.code = value;
                        } else if ("TIPO_ORDEN".equalsIgnoreCase(key)) {
                            dOrder.type = value;
                        } else {
                            if (null == dOrder.attrs)
                                dOrder.attrs = new LinkedList<JsonObject>();

                            dOrder.attrs.add(kvp);
                        }
                    }
                    List<RemoteOrder> orders = remoteOrdersRef.get();
                    if (null == orders)
                        remoteOrdersRef = new WeakReference<List<RemoteOrder>>(orders = new LinkedList<RemoteOrder>());

                    orders.add(dOrder);
                }
                List<RemoteOrder> orders = remoteOrdersRef.get();
                if (null != orders) {
                    deleteLocalOrders();
                    persistOrders(orders);
                    /*
                    *** Previous way to retain and release orders ***

                    processRetainsOrders(orders);
                    processReleaseOrders(orders);
                    */
                } else {
                    Log.e(NAME, "Serious problem, garbage collected all remote orders");
                }

            } catch (Exception ex) {
                Log.e(NAME, "Error while parsing json");
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void deleteLocalOrders() {
        Log.e(NAME, "Processing deletion of local orders....");
        ContentResolver resolver = getContentResolver();
        resolver.delete(DIListContract.CONTENT_URI, null, null);
        resolver.delete(DispatchItemsContract.CONTENT_URI, null, null);
    }

    private void persistOrders(List<RemoteOrder> remoteOrders) {
        if (null != remoteOrders) {
            Log.e(NAME, "Processing Insertion....");

            ContentResolver resolver = getContentResolver();

            for (int i = 0; remoteOrders.size() > i; ++i) {
                RemoteOrder dItem = remoteOrders.get(i);

                if (null != dItem && null != dItem.attrs && 0 < dItem.attrs.size()) {
                    String type = dItem.type;
                    String code = dItem.code;
                    String tech = null;
                    String priority = "";

                    List<JsonObject> attrs = dItem.attrs;

                    ContentValues[] attributes = new ContentValues[attrs.size()];

                    for (int j = 0; attrs.size() > j; ++j) {
                        attributes[j] = new ContentValues();

                        JsonObject kvp = attrs.get(j);

                        String name = kvp.get("Key").getAsString();
                        String value = kvp.get("Value").getAsString();

                        attributes[j].put(DispatchItemsContract.KEY, name);
                        attributes[j].put(DispatchItemsContract.VALUE, value);
                        attributes[j].put(DispatchItemsContract.CODE, code);

                        if ("tecnologia".equals(name.toLowerCase()))
                            tech = value;
                        if ("tipo_fecha".equals(name.toLowerCase()))
                            priority = value;
                    }
                    resolver.bulkInsert(DispatchItemsContract.CONTENT_URI, attributes);

                    ContentValues order = new ContentValues();
                    order.put(DIListContract.CODE, code);
                    order.put(DIListContract.TECHNOLOGY, (null != tech) ? tech : "UNKNOWN");
                    order.put(DIListContract.COMMITMENT_DATE, new Date().toString());
                    order.put(DIListContract.STATUS, 1);
                    order.put(DIListContract.TYPE, type);
                    order.put(DIListContract.PRIORITY, priority);

                    try {
                        resolver.insert(DIListContract.CONTENT_URI, order);
                    } catch (Exception ex) {
                        Log.e(NAME, "Error trying to insert ID with  code " + code);
                    }
                }
            }
        } else {
            Log.e(NAME, "Look as empty retain orders");
        }
    }

    @Deprecated
    private void processRetainsOrders(List<RemoteOrder> eRetain) {
        if (null != eRetain) {
            Log.e(NAME, "Processing Insertion....");

            ContentResolver resolver = getContentResolver();

            StringBuilder builder = new StringBuilder();

            for (int i = 0; eRetain.size() > i; ++i) {
                RemoteOrder dItem = eRetain.get(i);

                if (null != dItem && null != dItem.attrs && 0 < dItem.attrs.size()) {
                    String type = dItem.type;
                    String code = dItem.code;
                    String tech = null;
                    String priority = "";

                    builder.append(DIListContract.CODE).append(" ='").append(code).append("' ")
                            .append(" AND ")
                            .append(DIListContract.TYPE).append(" ='").append(type).append("' ");

                    Cursor c = null;
                    try {
                        c = resolver.query(DIListContract.CONTENT_URI, new String[]{ProviGenBaseContract._ID}, builder.toString(), null, null);
                        builder.setLength(0);
                        if (null != c && c.moveToFirst()) {
                            Log.e(NAME, "Skipping existing order " + code + " of type " + type);
                            continue;
                        }
                    } finally {
                        if (null != c)
                            c.close();
                    }

                    List<JsonObject> attrs = dItem.attrs;

                    ContentValues[] attributes = new ContentValues[attrs.size()];

                    for (int j = 0; attrs.size() > j; ++j) {
                        attributes[j] = new ContentValues();

                        JsonObject kvp = attrs.get(j);

                        String name = kvp.get("Key").getAsString();
                        String value = kvp.get("Value").getAsString();

                        attributes[j].put(DispatchItemsContract.KEY, name);
                        attributes[j].put(DispatchItemsContract.VALUE, value);
                        attributes[j].put(DispatchItemsContract.CODE, code);

                        if ("tecnologia".equals(name.toLowerCase()))
                            tech = value;
                        if ("tipo_fecha".equals(name.toLowerCase()))
                            priority = value;
                    }
                    resolver.bulkInsert(DispatchItemsContract.CONTENT_URI, attributes);

                    ContentValues order = new ContentValues();
                    order.put(DIListContract.CODE, code);
                    order.put(DIListContract.TECHNOLOGY, (null != tech) ? tech : "UNKNOWN");
                    order.put(DIListContract.COMMITMENT_DATE, new Date().toString());
                    order.put(DIListContract.STATUS, 1);
                    order.put(DIListContract.TYPE, type);
                    order.put(DIListContract.PRIORITY, priority);

                    try {
                        resolver.insert(DIListContract.CONTENT_URI, order);
                    } catch (Exception ex) {
                        Log.e(NAME, "Error trying to insert ID with  code " + code);
                    }
                }
            }
        } else {
            Log.e(NAME, "Look as empty retain orders");
        }
    }


    @Deprecated
    private void processReleaseOrders(List<RemoteOrder> orders) {
        if (null != orders) {
            Log.e(NAME, "Processing release....");

            ContentResolver resolver = getContentResolver();

            Set<String> remoteOrders = new HashSet<String>();
            Set<String> dbOrders = new HashSet<String>();

            Cursor cOrders = resolver.query(DIListContract.CONTENT_URI, new String[]{DIListContract.CODE}, null, null, null);
            Iterator<RemoteOrder> it = orders.iterator();

            /**
             * This allow to use only one loop fo both remote and local db orders
             */
            try {
                boolean cursorHasElement;
                while ((cursorHasElement = cOrders.moveToNext()) || it.hasNext()) {
                    try {
                        if (cursorHasElement)
                            dbOrders.add(cOrders.getString(0));

                        if (it.hasNext())
                            remoteOrders.add(it.next().code);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            } finally {
                if (null != cOrders)
                    cOrders.close();
            }

            Log.e(NAME, "Getting remote orders to prepare to release: " + remoteOrders);
            Log.e(NAME, "Getting db orders to prepare to release: " + dbOrders);
            dbOrders.removeAll(remoteOrders);

            if (0 >= dbOrders.size())
                return;

            StringBuilder inClause = new StringBuilder();
            StringBuilder query = new StringBuilder();

            SharedPreferences pref = getSharedPreferences(Constants.STATUS_FLAG_PREFERENCES, MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();

            inClause.append("( ");

            int i = 0;
            for (String order : dbOrders) {
                if (0 != i)
                    inClause.append(",");
                inClause.append("'").append(order).append("'");
                ++i;
                editor.remove(order);
            }
            inClause.append(" )");
            editor.commit();

            Log.e(NAME, "Preparing to remove  orders :" + inClause);


            query.append(DIListContract.CODE).append(" IN " + inClause.toString());

            resolver.delete(DIListContract.CONTENT_URI, query.toString(), null);
            resolver.delete(DispatchItemsContract.CONTENT_URI,query.toString(),null);

            query.setLength(0);
            inClause.setLength(0);
        }
    }


    private void ping(Message msg) {
        Log.e(NAME, "Trying to perform ping request");
        if (null != msg.replyTo) {
            Message reply = Message.obtain(null, ACTION_BIND_NOTIFICATION_ISSUED, loadingData);
            try {
                msg.replyTo.send(reply);
            } catch (RemoteException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Para esta accion se requiere que en arg1 se envie un ID unico del subcriber.
     */
    private void subscribe(Message msg) {
        Log.e(NAME, "Trying to perform subscribe request");
        if (null != msg.replyTo) {
            listeners.remove(msg.arg1);
            listeners.put(msg.arg1, msg.replyTo);
        }
    }

    private void publish(boolean status) {
        if (null == listeners || 0 == listeners.size())
            return;

        for (int i = 0; listeners.size() > i; ++i) {
            int key = listeners.keyAt(i);
            Messenger messenger = listeners.get(key);
            try {
                Message msg = Message.obtain(null, ACTION_BIND_NOTIFICATION_ISSUED, status);
                messenger.send(msg);
            } catch (RemoteException ex) {
                Log.e(NAME, "Looks like a dead");
                listeners.delete(key);
            }
        }
    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.e(NAME, "Handling message");
            switch (msg.what) {
                case ACTION_BIND_PING:
                    ping(msg);
                    break;
                case ACTION_BIND_SUBSCRIBE:
                    subscribe(msg);
                    break;
                case ACTION_BIND_PING_AND_SUBSCRIBE: {
                    Log.e(NAME, "Trying to perform ping and subscribe request");
                    ping(msg);
                    subscribe(msg);
                    break;
                }
                default:
                    break;
            }
        }
    };


    private static class RemoteOrder {
        String code;
        String type;
        List<JsonObject> attrs;
    }
}

