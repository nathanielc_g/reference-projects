package com.claro.mobilecfc.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.provider.Telephony;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.activities.prefs.UserKnowledge;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import javax.security.auth.login.LoginException;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Christopher Herrera on 2/18/14.
 * <p>
 * Modified by jansel
 */
public final class Utils {

    public enum APNStatus {
        VERIFIED,
        NOT_VERIFIED,
        UNKNOWN
    }

    private static final String TAG = Utils.class.getSimpleName();

    public static final String DOWNLOAD_EXTERNAL_PART = "/download/";

    public static final String DOWNLOAD_EXTERNAL_URI = Environment.getExternalStorageDirectory().getAbsolutePath().concat(DOWNLOAD_EXTERNAL_PART);

    public static final String getDeviceImei(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getDeviceId();

    }

    public static final boolean updaterInstalled(Context context) {
        PackageManager pm = context.getPackageManager();
        PackageInfo info;

        try {
            info = pm.getPackageInfo("com.claro.otau", PackageManager.GET_ACTIVITIES);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        if (null == info)
            return false;
        return true;
    }

    public static final boolean olderCFCInstalled(Context context) {
        PackageManager pm = context.getPackageManager();
        PackageInfo info;

        try {
            info = pm.getPackageInfo("com.claro.cfcmobile", PackageManager.GET_ACTIVITIES);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        if (null == info)
            return false;
        return true;
    }


    public static final boolean isAlowedToSyncFrames(Context context) {
        String lastSyncDateStr = UserKnowledge.getLastDateFrameSync(context);
        if (TextUtils.isEmpty(lastSyncDateStr))
            return true;

        try {
            DateFormat format = new SimpleDateFormat("MMM d, y");
            Date lastSyncDate = format.parse(lastSyncDateStr);

            Calendar cal = Calendar.getInstance();
            String recentDateStr = format.format(cal.getTime());
            Date recentDate = format.parse(recentDateStr);

            return lastSyncDate.before(recentDate);
        } catch (Exception ex) {
            ex.printStackTrace();
            return true;
        }
    }

    public static final void updateSyncDateFrame(Context context) {
        DateFormat format = new SimpleDateFormat("MMM d, y");
        Calendar cal = Calendar.getInstance();
        String recentDateStr = format.format(cal.getTime());

        UserKnowledge.setLastDateFrameSync(context, recentDateStr);
    }

    public static final int getLastSetupVersion (Context context) {
        SharedPreferences sp = context.getSharedPreferences("Setups", Context.MODE_PRIVATE);
        return sp.getInt("Setup.version", -1);
    }


    public static final int getVersionCode(Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            return info.versionCode;
        } catch (PackageManager.NameNotFoundException ex) {
            ex.printStackTrace();
        }
        return 0;
    }


    public static APNStatus isApnConfigured(final Context context) {
        final String apn = "facilidadesclaro.net";
        APNStatus apnStatus = APNStatus.NOT_VERIFIED;
        final Uri PREFERRED_APN_URI = Uri.parse("content://telephony/carriers/preferapn");

        try {
            Cursor parser = context.getContentResolver().query(PREFERRED_APN_URI, null, null, null, null);
            parser.moveToLast();
            while (parser.isBeforeFirst() == false) {
                int index = parser.getColumnIndex("apn");
                String n = parser.getString(index);
                if (n.equals(apn)) {
                    apnStatus = APNStatus.VERIFIED;
                    break;
                }
                parser.moveToPrevious();
            }
            if (null != parser)
                parser.close();
        } catch (Exception e) {
            e.printStackTrace();

            apnStatus = APNStatus.UNKNOWN;
        }

        return apnStatus;
    }

    public static boolean isApnConfiguredXML(Context context) {
        final String apnName = "facilidadesclaro.net";
        FileReader reader = null;
        boolean configured = false;

        try {
            reader = new FileReader("/etc/apns-conf.xml");

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(reader);

            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String simOperator = telephonyManager.getSimOperator();
            if (TextUtils.isEmpty(simOperator)) {
                Log.e(TAG, "unable to get sim operator - so unable to get mms config");
                return false;
            }

            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG && xpp.getName().equals("apn")) {
                    HashMap<String, String> attributes = new HashMap<String, String>();
                    for (int i = 0; i < xpp.getAttributeCount(); i++) {
                        attributes.put(xpp.getAttributeName(i), xpp.getAttributeValue(i));
                    }
                    if (attributes.containsKey("mcc") && attributes.containsKey("mnc") && simOperator.equals(attributes.get("mcc") + attributes.get("mnc"))) {
                        if (attributes.containsKey("apn")) {
                            configured = attributes.containsValue(apnName);
                        }
                    }
                }
                eventType = xpp.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                }
            }
        }
        Log.e(TAG, "isApnConfiguredXML: " + configured);
        return configured;
    }


}
