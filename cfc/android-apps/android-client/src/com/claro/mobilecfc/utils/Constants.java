package com.claro.mobilecfc.utils;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/16/14.
 * <p/>
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad. Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad.Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 */
public final class Constants {

    /*FORM TYPE*/
    public static final String FORM_FRAME_DATA = "FRAME+DATA";


    public static final String PHONE = "TELEFONO";
    public static final String CABINA = "CABINA";
    public static final String CABINA_FO = "CABINA_FO";
    public static final String CUENTA_FIJA = "CUENTA_FIJA";
    public static final String PAR_FEEDER = "PAR_FEEDER";
    public static final String SERVICIO_DATOS = "SERVICIO_DATOS";
    public static final String TERMINAL = "TERMINAL";
    public static final String PAR_LOCAL = "PAR_LOCAL";
    public static final String DIRECCION_CORRECTA = "DIRECCION_CORRECTA";
    public static final String TIPO_PUERTO = "TIPO_PUERTO";
    public static final String DSLAM = "DSLAM";

    /**
     * [DSLAM_TYPE] - Available since req.2015-GESTI-4791-2
     */
    public static final String DSLAM_TYPE = "DSLAM_TYPE";

    public static final String LOCALIDAD = "LOCALIDAD";
    public static final String PUERTO = "PUERTO";
    public static final String TERMINAL_FO = "TERMINAL_FO";
    public static final String SPLITTER_PORT = "SPLITTER_PORT";
    public static final String SPLITTER = "SPLITTER";

    // DTH FIELD NAMES
    public static final String LATITUD = "LATITUD";
    public static final String LONGITUD = "LONGITUD";
    public static final String ID_DIRECCION = "ID_DIRECCION";
    public static final String COORDVALIDS = "COORDVALIDS";
    public static final String COORDATTEMPTS = "COORDATTEMPTS";



    public static final String REGEX_PHONE = "8(0|2|4)9\\-\\d{3}-\\d{4}";
    public static final String REGEX_FEEDER = "^[a-zA-Z0-9]+-\\d{4}";

    /*
        Invalid Values:

         CO-....
         Co-....
         CO#....
         Co#....
         C0…… (Cualquier carácter después)
         M0…..(Cualquier carácter después)
         ##-.... ningún digito sin carácter antes del guion
         [0123456789]-....
         [a-zA-Z]-.... no permita un solo carácter antes del guion

    */

    public static final String REGEX_PRE_HYPHEN_FEEDER = "^(M0|C0|C[Oo](\\d|-)|\\d+-|[a-zA-Z]-).*$";

    // @Deprecated Since 19-08-2016 By Requirement 2016-GESTI-3915
    // public static final String REGEX_PRE_HYPHEN_FEEDER = "^(C[Oo](\\d|-)|C\\d-|\\d\\d-|[023456789]-|[a-zA-Z]-).*$";

    // @Deprecated Since 10-05-2016 By Requeriment 2016-GESTI-3915
    // public static final String REGEX_PRE_HYPHEN_FEEDER = "^(C[Oo](\\d|-)|C\\d-|\\d\\d-|[023456789]-).*$";

    // @Deprecated 05-19-2016
    //public static final String REGEX_PRE_HYPHEN_FEEDER = "^(C[Oo](\d|-)|C0-).*$";
    //
    //public static final String REGEX_TERMINAL_FO = "FC.{1,10}-[A-Z]\\d{1,2}";
    public static final String REGEX_TERMINAL_FO = "FC[a-zA-Z0-9]{1,10}-[a-zA-Z]\\d{1,3}";

    public static final String REGEX_FOUR_DIGITS = "\\d{4}";

    /**
     * [REGEX_DSLAM] - Available since req.2015-GESTI-4791-2
     */


    public static final String REGEX_DSLAM = "^[a-zA-Z][a-zA-Z0-9]+[a-zA-Z][1-9][0-9]?$";
    //public static final String REGEX_DSLAM = ".+?[1-9]$";

    // LAST STABLE REGEX 02-25-2016
    //public static final String REGEX_TERMINAL = "^[a-zA-Z0-9]+-(?=[^0])([1-9]|[1-3][0-9])?[a-zA-Z](x|X)?";
    // BETA REGEX 02-29-2016
    //public static final String REGEX_TERMINAL = "^[a-zA-Z0-9]+(?!0)([0-9])+-(?=[^0])([1-9]|[1-3][0-9])?[a-zA-Z](x|X)?";
    public static final String REGEX_TERMINAL = "^([1-9][a-zA-Z0-9]*|[a-zA-Z][a-zA-Z0-9]+)-(?=[^0])([1-9]|[1-3][0-9])?[a-zA-Z](x|X)?";

    public static final String REGEX_PRE_HYPHEN_TERMINAL = "^(M0|C0|C[Oo](\\d|-)|\\d+-|[a-zA-Z]-).*$";
    // @Deprecated 05-19-2016
    //public static final String REGEX_PRE_HYPHEN_TERMINAL = "^(C[Oo](\d|-)|C0-).*$";

    // old regex terminal
    //public static final String REGEX_TERMINAL = "^[a-zA-Z0-9]+-[a-zA-Z0-9]+$";
    public static final String REGEX_PAR_LOCAL = "^[a-zA-Z0-9]+-\\d{4}";

    // LAST STABLE REGEX 02-25-2016
    //public static final String REGEX_CABINA = "^[a-zA-Z0-9-]+$";
    // BETA REGEX 03-01-2016
    public static final String REGEX_CABINA = "^[a-zA-Z0-9]+$";

    /**
     *
     *
     *  Cabina-FO: FC[A-Z].{1,5}
     Terminal-FO: {Cabina ingresada}-[A-Z]\d{1,2}
     Splitter: SPT-{Terminal ingresado}
     Splitterport: SPT-{TERMINAL ingresado}-SP.{2,3}

     *
     */

    public static final String STATUS_FLAG_PREFERENCES = "status.flag";


    public static final int DEFAULT_HELP_PORT_OFFSET = 131617;
}
