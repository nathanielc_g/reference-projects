package com.claro.mobilecfc.activities.fragments.forms;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.dto.Attribute;
import com.claro.mobilecfc.dto.FormValues;
import com.claro.mobilecfc.dto.Frames;
import com.claro.mobilecfc.utils.Constants;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/15/14.
 * <p/>
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad. Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad.Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 */


public class PSTNDataFormFragment extends PSTNFormFragment {

    private static final int DSLAM_OUTDOOR = 1<<2;

    private static final int DSLAM_INDOOR  = 2<<2;

    private static final int DSLAM_NONE  = -1;

    private static final String FORM_TYPE = "PSTN+DATA";

    private Set<String> techs;

    private EditText dslam;
    private Spinner puerto;
    private boolean isFillingPuerto = false;
    private EditText puertoMask;
    private RadioButton dslamIndoorRadio;
    private RadioButton dslamOutdoorRadio;

    private String[] portRegex;
    private String [] portsType;

    private int port;

    private int dslamType = DSLAM_NONE;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pstn_data_form,container,false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    protected String getFormType() {
        return FORM_TYPE;
    }

    @Override
    protected List<Attribute> getAttributes() {
        List<Attribute> attributes = super.getAttributes();


        String puertoStr = puerto.getSelectedItem().toString();
        String puertoMaskStr = puertoMask.getText().toString();


        attributes.add(Attribute.create(Constants.TIPO_PUERTO, puertoStr.toUpperCase().trim()) );
        attributes.add(Attribute.create(Constants.PUERTO, puertoMaskStr) );
        attributes.add(Attribute.create(Constants.DSLAM, dslam.getText().toString().trim().toUpperCase()) );
        attributes.add(Attribute.create(Constants.DSLAM_TYPE, stringifyDSLAMType(dslamType).toUpperCase()));

        return attributes;
    }

    @Override
    protected void enabledComponents(boolean enabled) {

    }

    protected void init() {
        super.init();
        initComponents();

        techs =  new HashSet<>();
        techs.addAll(Arrays.asList("alcatel", "cisco", "stinger", "zyxel", "zte"));
    }

    @Override
    protected void setHelpListener() {
        super.setHelpListener();

        final View.OnClickListener LISTENER = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( R.id.help_puerto == view.getId() ){
                    Log.e( "****","Displaying port help" );
                    displayHelp(Constants.DEFAULT_HELP_PORT_OFFSET+port);
                }else
                    displayHelp(view.getId());
            }
        };
        ImageButton icon = ( ImageButton ) getActivity().findViewById( R.id.help_puerto);
        icon.setOnClickListener(LISTENER);

        icon = ( ImageButton ) getActivity().findViewById( R.id.help_dslam);
        icon.setOnClickListener(LISTENER);
    }

    private void initComponents() {
        portRegex = getActivity().getResources().getStringArray( R.array.ports_regex );

        portsType = getActivity().getResources().getStringArray(R.array.fibra_puerto_labels);;


        puerto      = (Spinner) getActivity().findViewById(R.id.edit_puerto);
        dslam       = (EditText) getActivity().findViewById( R.id.edit_dslam);
        puertoMask = (EditText) getActivity().findViewById(R.id.edit_puerto_specificacion);

        final String[] puertosMasks = getResources().getStringArray(R.array.fibra_tipo_puerto_masks);

        puerto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (isFillingPuerto){
                    isFillingPuerto = false;
                    return;
                }

                puertoMask.setHint(puertosMasks[position]);
                puertoMask.setText(puertosMasks[position]);
//                puertoMask.setMask(puertosMasks[position]);
                port = position;

                setPortPrefix();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        dslamIndoorRadio = (RadioButton) getActivity().findViewById(R.id.radio_dslam_indoor);
        dslamIndoorRadio.setOnClickListener(DSLAM_TYPE_SELECTION_LISTENER);

        dslamOutdoorRadio = (RadioButton) getActivity().findViewById(R.id.radio_dslam_outdoor );
        dslamOutdoorRadio.setOnClickListener(DSLAM_TYPE_SELECTION_LISTENER);


    }

    protected boolean validateDslam () {

        boolean validated = true;
        String input = TextUtils.isEmpty(dslam.getText().toString()) ? "" : dslam.getText().toString().trim().toUpperCase();

        validated &= !TextUtils.isEmpty(input) && !input.contains(" ") && 1<input.length() && !techs.contains(input.toLowerCase());
        validated &= Pattern.compile(Constants.REGEX_DSLAM).matcher( input ).matches();

        if( !validated || input.matches(".*(DSLAM|COPC|PRUEBA).*")){
            toast(R.string.validation_dslam);
            validated = false;
            return validated;
        }

        if(isCapturingCoordinates()) {
            Toast.makeText(getActivity(), R.string.wait_captured_coordinate, Toast.LENGTH_LONG).show();
            return false;
        }
        return validated;

    }

    @Override
    protected boolean validateInputs() {
        boolean validated = super.validateInputs();

        if(!validated){
            return validated;
        }

        // PORT TYPE
        String default_message = getResources().getStringArray(R.array.fibra_puerto_labels)[0];
        String input = TextUtils.isEmpty(puerto.getSelectedItem().toString()) ? "" : puerto.getSelectedItem().toString().trim().toUpperCase();
        if (default_message.equalsIgnoreCase(input)) {
            validated = false;
            toast(R.string.validation_port_type);
            return validated;
        }

        // DSLAM TYPE
        if( DSLAM_NONE == dslamType ){
            toast(R.string.validation_dslam_type);
            return false;
        }

        // DSLAM
        if(!validateDslam()){
            return false;
        }

        // PORT
        input = TextUtils.isEmpty(puertoMask.getText().toString()) ? "" : puertoMask.getText().toString().trim().toUpperCase();
        // SE CAMBIO FORMA DE OBTENER TIPO PUERTO. Se obtiene ahora desde el componente de la vista
        // validated &= Pattern.compile(portRegex[port]).matcher( input ).matches();
        validated &= Pattern.compile(portRegex[puerto.getSelectedItemPosition()]).matcher( input ).matches();
        validated &= isValidSuggestedPort(input);

        if( !validated ){
            toast( R.string.validation_port );
            return validated;
        }

        return validated;
    }

    @Override
    protected void fillForm(FormValues formValues) {
        super.fillForm(formValues);
        isRefreshing();

        setTipoPuerto(formValues.getPath(Constants.TIPO_PUERTO));
        setDslamType(formValues.getPath(Constants.DSLAM_TYPE));
        dslam.setText(formValues.getPath(Constants.DSLAM));
        setPort(formValues.getPath(Constants.PUERTO));

    }



    private void isRefreshing() {
        isFillingPuerto = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        memento.addToHistory("dslam", dslam.getText().toString());

        memento.addToHistory("puerto", puerto.getSelectedItemPosition() + "");
        memento.saveMemento(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        /**
         * loaded by super
         */
//        memento.loadMemento( getActivity() );

        dslam.setText(memento.getFromHistory("dslam") + "");
        int selection;
        try {
            selection = Integer.parseInt(memento.getFromHistory("puerto") + "");
        } catch (Exception e) {
            selection = 0;
        }
        this.puerto.setSelection(selection);
    }

    protected void setPort(String puerto) {
        if (puerto == null)
            return;

        puertoMask.setText(puerto);
    }

    protected void setTipoPuerto(String puerto) {
        if (puerto == null) {
            isFillingPuerto = false;
            return;
        }

        int selection = 0;
        try {
            for (int i = 0; i < portsType.length; i++){
                if(portsType[i].equalsIgnoreCase(puerto)) {
                    selection = i;
                    break;
                }
            }
        } catch (Exception e) {
            selection = 0;
        }
        this.puerto.setSelection(selection);
    }


    /**
     * This method validate that suggested port were editted correctly and none suggested characterr
     * numbers appear in the resulted value.
     */
    private boolean isValidSuggestedPort(String value){
        value = value.replaceAll("\\D", " ");
        value = value.trim();
        String[] numbers = value.split(" ");
        Log.e("***********", "Resulted array of numbers: " + Arrays.toString(numbers));
        if( null == numbers )
            return false;

        for( String number : numbers ){
            if( " ".equals( number ))
                continue;
            if( number.startsWith( "0" ) )
                return false;
        }
        return true;
    }

    @Override
    protected void onFrameSelected(Frames frames, int frame) {
        super.onFrameSelected(frames, frame);
         setPortPrefix();
    }

    @Override
    protected void onCuentaFijaChange(boolean isCuentaFija) {
        super.onCuentaFijaChange(isCuentaFija);

        if( null != dslamIndoorRadio && null != dslamOutdoorRadio ){
            dslamIndoorRadio.setChecked(isCuentaFija);
            dslamOutdoorRadio.setClickable(!isCuentaFija);

            if( isCuentaFija )
                dslamIndoorRadio.performClick();
        }
    }

    @Override
    protected void onCabinaChange(String value) {
        super.onCabinaChange(value);

        if( null == dslam )
            return;

        if( DSLAM_OUTDOOR == dslamType ){
            if( null != puerto ){
                String port = puerto.getSelectedItem()+"";

                if( port.toLowerCase().contains( "alcatel" ) )
                    dslam.setText( "A"+value );
                else if( port.toLowerCase().contains("zte"))
                    dslam.setText( "Z"+value );
            }
        }
    }


    protected void setPortPrefix( ){
        if( null != puerto ){
            if( DSLAM_OUTDOOR == dslamType ){
                String port = puerto.getSelectedItem()+"";

                if( port.toLowerCase().contains( "alcatel" ) )
                    dslam.setText( "A"+getCabina() );
                else if( port.toLowerCase().contains("zte"))
                    dslam.setText( "T"+getCabina() );

            }else if( DSLAM_INDOOR == dslamType ){
                if( null == puerto || null == dslam )
                    return;
                String port = puerto.getSelectedItem()+"";

                if( 0>getSelectedFrame() )
                    return;

                String selectedPort = port.toLowerCase();
                if( selectedPort.toLowerCase().contains("zyxel")) //Same prefixes for Zyxel ADSL & Zyxel SHDSL
                    selectedPort = "zyxel";

                String portDslamPrefix = getFrames().getPortPrefix( getSelectedFrame(),selectedPort );

                dslam.setText( portDslamPrefix );
            }
        }
    }


    protected String stringifyDSLAMType(int type){
        return DSLAM_INDOOR == type ? "Indoor" : DSLAM_OUTDOOR == type ? "Outdoor" : "UNKNOWN";
    }

    protected void setDslamType(String dslamTypeValue) {
        if(dslamTypeValue == null){
            dslamType = DSLAM_NONE;
            dslamOutdoorRadio.setChecked(false);
            dslamIndoorRadio.setChecked(false);
            return;
        }

        if ("OUTDOOR".equalsIgnoreCase(dslamTypeValue)) {
            dslamType = DSLAM_OUTDOOR;
            dslamIndoorRadio.setChecked(false);
            dslamOutdoorRadio.setChecked(true);

        } else {
            dslamType = DSLAM_INDOOR;
            dslamOutdoorRadio.setChecked(false);
            dslamIndoorRadio.setChecked(true);
        }
    }

    protected final View.OnClickListener DSLAM_TYPE_SELECTION_LISTENER = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.radio_dslam_indoor) {
                dslamType = DSLAM_INDOOR;
            } else {
                dslamType = DSLAM_OUTDOOR;
            }
            setPortPrefix();
        }
    };
}
