package com.claro.mobilecfc.activities.fragments.forms;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.dto.Attribute;
import com.claro.mobilecfc.widget.TextView;

import java.util.List;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 11/18/2014.
 */
public class PreviewDialog extends DialogFragment {

    private OnPreviewDismissListener listener;
    private List<Attribute> attributes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle("Previsualizacion");
        getDialog().setCancelable(false);

        View view = inflater.inflate(R.layout.fragment_preview_dialog, container, false);

        Button accept = (Button) view.findViewById(R.id.preview_done);
        Button cancel = (Button) view.findViewById(R.id.preview_cancel);

        final View.OnClickListener LISTENER = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( null == listener )
                    return;
                if (view.getId() == R.id.preview_done)
                    listener.onActionResult(OnPreviewDismissListener.RESULT_DONE,PreviewDialog.this);
                else
                    listener.onActionResult(OnPreviewDismissListener.RESULT_CANCEL,PreviewDialog.this);
            }
        };

        accept.setOnClickListener(LISTENER);
        cancel.setOnClickListener(LISTENER);

        TextView content = (TextView) view.findViewById(R.id.preview_content);
        if( null != attributes && 0<attributes.size() )
            fillContent(content,attributes);
        return view;
    }


    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setListener(OnPreviewDismissListener listener) {
        this.listener = listener;
    }

    public OnPreviewDismissListener getListener() {
        return listener;
    }

    private void fillContent(TextView content, List<Attribute> attributes ){
        StringBuilder builder = new StringBuilder();

        for(Attribute att: attributes ){
            String name = att.getName().toUpperCase();
            name = name.replaceAll( "_"," " );
            if("IMEI".equals(name) || "CODE".equals(name) || "TYPE".equals(name) || "FORM TYPE".equals(name) )
                continue;

            name +=":";
            String value = TextUtils.isEmpty(att.getValue()) ? "" : att.getValue().toLowerCase();
            builder.append( String.format("%-12s%-12s",name,value))
                   .append( "\n" );
        }
        content.setText( builder.toString() );
        builder.setLength(0);
    }

    public interface OnPreviewDismissListener {
        int RESULT_DONE = 1;
        int RESULT_CANCEL = 2;

        void onActionResult(int result,PreviewDialog dialog);
    }
}
