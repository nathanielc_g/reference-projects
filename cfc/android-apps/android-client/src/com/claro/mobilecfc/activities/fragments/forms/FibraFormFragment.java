package com.claro.mobilecfc.activities.fragments.forms;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.activities.fragments.forms.prefs.Memento;
import com.claro.mobilecfc.dto.Attribute;
import com.claro.mobilecfc.dto.FormValues;
import com.claro.mobilecfc.dto.Frames;
import com.claro.mobilecfc.utils.Constants;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/15/14.
 * <p/>
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad. Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad.Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 */


public class FibraFormFragment extends BaseFormFragment {

    private static final String FORM_TYPE = "FIBRA";

    private EditText phone;
    private Spinner localidad;
    private boolean isFillingLocalidad = false;
    private EditText terminalFo;

    //    private EditText direccion;
    private boolean sameAddress;
    private short addressState = -1;
    private EditText splitterPort;

    private Memento memento;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        memento = new Memento(this.getClass().getSimpleName() + ":" + orderNumber);
        setSupportCaptureCoordinate(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fibra_form, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    @Override
    protected String getFormType() {
        return FORM_TYPE;
    }

    @Override
    protected List<Attribute> getAttributes() {
        List<Attribute> attributes = new ArrayList<Attribute>();

        attributes.add(Attribute.create(Constants.PHONE, phone.getText().toString()));
        attributes.add(Attribute.create(Constants.LOCALIDAD, localidad.getSelectedItem().toString()));

        attributes.add(Attribute.create(Constants.TERMINAL_FO, terminalFo.getText().toString().trim().toUpperCase()));
        attributes.add(Attribute.create(Constants.CABINA_FO, getCabina(terminalFo.getText().toString().trim().toUpperCase())));
        attributes.add(Attribute.create(Constants.DIRECCION_CORRECTA, sameAddress? "SI": "NO"));
        attributes.add(Attribute.create(Constants.SPLITTER_PORT, splitterPort.getText().toString().trim().toUpperCase()));
        attributes.add(Attribute.create(Constants.SPLITTER, "SPT-" + terminalFo.getText().toString().trim().toUpperCase()));

        return attributes;
    }

    @Override
    protected void enabledComponents(boolean enabled) {
    }

    private void init() {
        initComponents();
    }


    private void initComponents() {
        phone = (EditText) getActivity().findViewById(R.id.edit_phone);
        localidad = (Spinner) getActivity().findViewById(R.id.edit_localidad);

        final List<String> frames = new LinkedList<>(getFrames().listNames());

        if( null != frames) {
            frames.add(0, "Seleccione FRAME");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, frames);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            localidad.setAdapter(adapter);

            localidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                    if (isFillingLocalidad) {
                        isFillingLocalidad = false;
                        return;
                    }
                    if (0 == position)
                        return;

                    int idx = position; //Compact position to remove "Seleccione Prefix" index
                    if (0 > idx)
                        return;

                    onFrameSelected(getFrames(), idx-1);

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            /*localidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    if (0 == position)
                        return;

                    int idx = position; //Compact position to remove "Seleccione Prefix" index
                    if (0 > idx)
                        return;

                    return frames.get(idx);

                }

            });*/
        }

        terminalFo = (EditText) getActivity().findViewById(R.id.edit_terminal_fo);

        splitterPort = (EditText) getActivity().findViewById(R.id.edit_splitter_port);
        terminalFo.addTextChangedListener(new TextWatcher() {
//            CharSequence sequence = "";
//            boolean validating;

            @Override
            public void beforeTextChanged(CharSequence sequence, int i, int i2, int i3) {
//                this.sequence = sequence;
            }

            @Override
            public void onTextChanged(CharSequence sequence, int i, int i2, int i3) {
                if (null != terminalFo) {
                    splitterPort.setText("SPT-" + terminalFo.getText() + "-SP");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        terminalFo.setText("FC");

        RadioButton radio = (RadioButton) getActivity().findViewById(R.id.radio_direccion_yes);
        radio.setOnClickListener(ADDRESS_SELECTION_LISTENER);

        radio = (RadioButton) getActivity().findViewById(R.id.radio_direccion_no);
        radio.setOnClickListener(ADDRESS_SELECTION_LISTENER);


        this.setHelpListener();
    }


    private String getCabina(String terminal) {
        if (null == terminal)
            return "";
        String cabina = terminal.substring(0, Math.abs(terminal.indexOf('-')));
        return cabina;
    }


    @Override
    protected boolean validateInputs() {
        boolean validated = true;

        String input = TextUtils.isEmpty(phone.getText().toString()) ? "" : phone.getText().toString().trim().toUpperCase();
        validated &= Pattern.compile(Constants.REGEX_PHONE).matcher(input).matches();
        if (!validated) {
            toast(R.string.validation_phone);
            return validated;
        }

        if (localidad.getSelectedItemPosition() == 0) {
            toast(R.string.validation_localidad, Toast.LENGTH_SHORT);
            return false;
        }


        input = TextUtils.isEmpty(terminalFo.getText().toString()) ? "" : terminalFo.getText().toString().trim().toUpperCase();
        validated &= Pattern.compile(Constants.REGEX_TERMINAL_FO).matcher(input).matches();
        if (!validated) {
            toast(R.string.validation_terminal_fo);
            return validated;
        }

        input = TextUtils.isEmpty(splitterPort.getText().toString()) ? "" : splitterPort.getText().toString().trim().toUpperCase();
        validated &= Pattern.compile("SPT-" + terminalFo.getText().toString().trim().toUpperCase() + "-SP0[1-8]").matcher(input).matches();
        if (!validated) {
            toast(R.string.validation_splitter_port);
            return validated;
        }

        if (-1 == addressState) {
            toast(R.string.validation_address);
            return false;
        }

        if(isCapturingCoordinates()) {
            Toast.makeText(getActivity(), R.string.wait_captured_coordinate, Toast.LENGTH_LONG).show();
            return false;
        }
        return validated;
    }

    @Override
    public void onStop() {
        super.onStop();

        memento.addToHistory("phone", phone.getText().toString());
        memento.addToHistory("localidad", localidad.getSelectedItemPosition()+"" );
        memento.addToHistory("terminalFo", terminalFo.getText().toString());
        memento.addToHistory("splitterPort", splitterPort.getText().toString());
        memento.saveMemento(getActivity());
    }

    @Override
    protected void fillForm(FormValues formValues) {
        isRefreshing();

        // SET PHONE
        setPhone(phone, formValues.getPath(Constants.PHONE));

        // LOCALIDAD
        String localidadVal = formValues.getPath(Constants.LOCALIDAD);
        if (localidadVal != null) {
            int idx = getFrames().listNames().indexOf(localidadVal) + 1;
            if (idx != -1) {
                setLocalidad(idx + "");
            }
        }

        // 2015-GESTI-3083-2
        setTerminalFo(formValues.getPath(Constants.TERMINAL_FO));
        setSplitterPort(formValues.getPath(Constants.SPLITTER_PORT));

        // DIRECCION CORRECTA
        String direccionCorrecta = formValues.getPath(Constants.DIRECCION_CORRECTA);
        if (direccionCorrecta == null){
            setNoneDireccionCorrecta();
        }else {
            if ( direccionCorrecta.equalsIgnoreCase("SI")) {
                addressState = 1;
                sameAddress = true;
                setDireccionCorrecta(true);
            } else {
                addressState = 2;
                sameAddress = false;
                setDireccionCorrecta(false);
            }
        }


    }

    private void isRefreshing () {
        isFillingLocalidad = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        memento.loadMemento(getActivity());

        // LOCALIDAD
        setLocalidad(memento.getFromHistory("localidad") + "");

        // SET PHONE
        setPhone(phone,memento.getFromHistory("phone") + "");

        setTerminalFo(memento.getFromHistory("terminalFo"));
        setSplitterPort(memento.getFromHistory("splitterPort"));


    }

    protected void setTerminalFo (Object terminalFo) {
        if (null != terminalFo && !TextUtils.isEmpty(terminalFo + ""))
            this.terminalFo.setText(terminalFo + "");

    }

    protected void setSplitterPort (Object splitterPort) {
        if (null != splitterPort && !TextUtils.isEmpty(splitterPort+ ""))
            this.splitterPort.setText(splitterPort + "");

    }

    protected void setLocalidad (String localidad) {
        int selection;
        try {
            selection = Integer.valueOf(localidad);
        } catch (Exception e) {
            selection = 0;
        }
        this.localidad.setSelection(selection);
    }

    protected void setHelpListener() {
        final View.OnClickListener LISTENER = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayHelp(view.getId());
            }
        };

        ImageButton icon = (ImageButton) getActivity().findViewById(R.id.help_terminalFo);
        icon.setOnClickListener(LISTENER);

        icon = (ImageButton) getActivity().findViewById(R.id.help_splitterPort);
        icon.setOnClickListener(LISTENER);
    }

    @Override
    protected void onPostSent() {
        memento.removeMemento(getActivity());
    }

    protected final View.OnClickListener ADDRESS_SELECTION_LISTENER = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.radio_direccion_yes) {
                addressState = 1;
                sameAddress = true;
            } else {
                addressState = 2;
                sameAddress = false;
            }
        }
    };


    // PSTN + FIBRA

    protected void onFrameSelected(Frames frames, int frame) {
        String terminalFoPrefix = frames.getTerminalFoPrefix(frame);
        setTerminalFo("FC" + (TextUtils.isEmpty(terminalFoPrefix) ? "" : terminalFoPrefix));
    }
}

