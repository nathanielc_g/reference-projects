package com.claro.mobilecfc.activities.fragments.forms;

import android.content.*;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.*;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.activities.FormActivity;
import com.claro.mobilecfc.activities.HomeActivity;
import com.claro.mobilecfc.activities.prefs.UserKnowledge;
import com.claro.mobilecfc.db.contracts.DIListContract;
import com.claro.mobilecfc.db.contracts.FrameListContract;
import com.claro.mobilecfc.db.contracts.FramesContract;
import com.claro.mobilecfc.dto.Attribute;
import com.claro.mobilecfc.dto.FormValues;
import com.claro.mobilecfc.dto.Frames;
import com.claro.mobilecfc.robospice.request.ActionSaveRequest;
import com.claro.mobilecfc.robospice.request.LastActionRequest;
import com.claro.mobilecfc.robospice.response.LastActionResponse;
import com.claro.mobilecfc.robospice.response.OKResponse;
import com.claro.mobilecfc.robospice.services.CfcService;
import com.claro.mobilecfc.utils.Constants;
import com.claro.mobilecfc.utils.HttpRestRequester;
import com.claro.mobilecfc.utils.HttpUtils;
import com.claro.mobilecfc.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.view.View.GONE;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/15/14.
 * <p/>
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad. Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad.Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 */
public abstract class BaseFormFragment extends Fragment implements PreviewDialog.OnPreviewDismissListener {

    private static final String TAG = "BaseFormFragment";

    protected boolean mementoOn = true;
    private static final int RESCHEDULE_COORDINATE_FETCH = 1 << 3;

    //private boolean isRefreshed;
    protected MenuItem itemSend;
    protected MenuItem itemRefreshForm;

    protected String orderNumber;
    protected String orderType;
    protected String orderPhone;
    protected String latitude;
    protected String longitude;
    protected String addressId;
    protected String validCoordinate;


    private List<Attribute> attributes;
    private SpiceManager spiceManager = new SpiceManager(CfcService.class);
    private Frames frames;

    protected Handler mLocationHandler;
    protected LocationManager mLocationManager;
    protected Location mLocation;
    protected LocationListener mLocationListener;
    protected boolean coordinateCaptureAttempt;
    private boolean supportCaptureCoordinate;

//    public Location currentBestLocation;
    // The maximum distance the user should travel between location updates.
    public final static int MAX_DISTANCE = 1;
    // The mininum time that should pass before the user gets a location update.
    public final static long MIN_TIME = 1000 * 3;

    // Working one: 270000;
    // 180000 - 3 MINS
    public final static long MIN_INTERVAL_TIME = 180000; // 3 MINS
    // The maximum time that should pass before the user gets a location update.
    public final static long MAX_TIME = 180000; // 3 MINS
    public final static float MIN_ACCURACY = 6.8f;
    // protected final static String notLocationMessage = "La posicion actual no es la mejor posicion. Favor intentar desde otro punto mas despejado o reiniciar el telefono.";

    private Button captureCoordinateButton;
    private CaptureCoordinateDialog captureDialog;
//    private Button cancelLookupButton;
//    private ViewGroup coordinateFeedbackContent;

    protected boolean capturingCoordinates;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        //this.isRefreshed = false;
        if (null != getArguments()) {
            orderNumber = getArguments().getString(FormActivity.EXTRAS_FORM_ORDER_NUMBER);
            orderType = getArguments().getString(FormActivity.EXTRAS_FORM_ORDER_TYPE);
            orderPhone = getArguments().getString(FormActivity.EXTRAS_FORM_ORDER_PHONE);

            validCoordinate = FormUtils.getOrderDetailKey(getActivity(), orderNumber, Constants.COORDVALIDS);
            latitude = FormUtils.getOrderDetailKey(getActivity(), orderNumber, Constants.LATITUD);
            longitude = FormUtils.getOrderDetailKey(getActivity(), orderNumber, Constants.LONGITUD);
            addressId = FormUtils.getOrderDetailKey(getActivity(), orderNumber, Constants.ID_DIRECCION);
            Log.e(TAG, "Getting coordinate values (valid=" + validCoordinate + ", lat=" + latitude + ", lng=" + longitude + ", addressId=" + addressId + ")");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        spiceManager.start(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        spiceManager.shouldStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (supportCaptureCoordinate) {
            if (null != mLocationListener && null != mLocationListener)
                mLocationManager.removeUpdates(mLocationListener);
        } else {
            Log.e(TAG, this.getClass().getCanonicalName() + " wants to destroy coordinates itself");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (supportCaptureCoordinate) {
            mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            if (!TextUtils.isEmpty(validCoordinate) && validCoordinate.equalsIgnoreCase("true")) {
                showCoordinateButton(false);
            } else {
                setupCoordinateComponents();
            }
        } else {
            Log.e(TAG, this.getClass().getSimpleName() + " wants to handle coordinates by itself");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.form_menu, menu);
        itemSend = menu.findItem(R.id.action_form_send);
        itemRefreshForm = menu.findItem(R.id.action_refresh);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_form_send:
                if (!HttpUtils.isNetworkAvailable(getActivity())) {
                    Toast.makeText(getActivity(), R.string.not_network, Toast.LENGTH_SHORT).show();
                    return true;
                }
                sendForm();
                return true;

            case R.id.action_refresh:
                if (!HttpUtils.isNetworkAvailable(getActivity())) {
                    Toast.makeText(getActivity(), R.string.not_network, Toast.LENGTH_SHORT).show();
                    return true;
                }
                loadLastAction();
                return true;
            default:
                return false;

        }
    }

    protected void setSupportCaptureCoordinate(boolean supportCaptureCoordinate) {
        this.supportCaptureCoordinate = supportCaptureCoordinate;
    }

    protected void setupCoordinateComponents() {
        captureCoordinateButton = (Button) getActivity().findViewById(R.id.coordinate_btn_capture);
        captureDialog = new CaptureCoordinateDialog();
        captureDialog.setCancelable(false);

        captureCoordinateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                coordinateCaptureAttempt = true;
                requestCoordinateLocation();
            }
        });
        captureDialog.setOnDialogDismissListener(new CaptureCoordinateDialog.OnDialogDismissListener() {
            @Override
            public void onActionResult(int result, CaptureCoordinateDialog dialog) {
                if (CaptureCoordinateDialog.OnDialogDismissListener.RESULT_DISMISS == result)
                    if (isCapturingCoordinates())
                        cancelCoordinateLookup();
            }
        });

    }

    protected void showCoordinateButton(boolean show) {
        getActivity().findViewById(R.id.capture_coordinates_container).setVisibility(show ? View.VISIBLE : GONE);
    }

    protected void requestCoordinateLocation() {
        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(getActivity(), "Por favor active el GPS", Toast.LENGTH_LONG).show();
            return;
        }

        final CountDownLatch latch = new CountDownLatch(1);

        new Thread() {
            @Override
            public void run() {
                Looper.prepare();

                if (null == mLocationHandler)
                    mLocationHandler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            if (RESCHEDULE_COORDINATE_FETCH == msg.what) {
                                if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                    //mLocationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, getCoordinateLocationListener(), Looper.myLooper());
//                                    currentBestLocation = null;
                                    mLocation = null;
                                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MAX_DISTANCE, getCoordinateLocationListener(), Looper.myLooper());
                                    capturingCoordinates = true;
                                    if (null != captureDialog) {
                                        captureDialog.setAddress(null);
                                        captureDialog.show(getFragmentManager(), "captureCoordinateDialog");
                                    }
                                } else {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(), "GPS Provider is not enabled.!", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        }
                    };

                mLocationHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (null != mLocationListener && null != mLocationManager) {
                            mLocationManager.removeUpdates(getCoordinateLocationListener());
                            capturingCoordinates = false;
                                /*if(isBestLocation(currentBestLocation)){
                                    setLocation(currentBestLocation);
                                    return;
                                }*/
                            if (getActivity() != null)
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (captureDialog != null)
                                            captureDialog.dismiss();

                                        Toast.makeText(getActivity(), getActivity().getText(R.string.timeout_capturing_coordinate), Toast.LENGTH_LONG).show();
                                    }

                                });

                        }
                    }
                }, MAX_TIME);
                latch.countDown();
                Looper.loop();
            }
        }.start();

        try {
            latch.await();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        mLocationHandler.sendEmptyMessage(RESCHEDULE_COORDINATE_FETCH);
    }

    protected void cancelCoordinateLookup() {
        capturingCoordinates = false;
        if (null != mLocationListener && null != mLocationManager) {
            if (mLocationHandler != null) {
                mLocationHandler.removeCallbacksAndMessages(null);
            }

            mLocationManager.removeUpdates(mLocationListener);
        }
        latitude = longitude = "";
    }

    private LocationListener getCoordinateLocationListener() {
        if (null != mLocationListener)
            return mLocationListener;

        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(final Location loc) {
                mLocation = loc;

                if (isBestLocation(mLocation)) {

                    //setLocation(currentBestLocation);
                    setLocation(mLocation);

                    mLocationManager.removeUpdates(mLocationListener);
                    mLocationHandler.removeCallbacksAndMessages(null);

                    capturingCoordinates = false;

                }

                /*new Thread(new Runnable() {
                    @Override
                    public void run() {

                    }
                }).start();*/
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }

            @Override
            public void onProviderDisabled(String s) {
            }
        };
        return mLocationListener;
    }

    public boolean isBestLocation(final Location location) {
        //final long minTime = SystemClock.elapsedRealtime() - 600000L;
        // 5 MINUTES
        final long minTime = System.currentTimeMillis() - MIN_INTERVAL_TIME;
        if (location == null) {
            return false;
        }
        float accuracy = location.getAccuracy();
        long time = location.getTime();
        if (time > minTime && accuracy <= MIN_ACCURACY) {
//            currentBestLocation = location;
            return true;
        } else {
            return false;
        }

    }

    private void setLocation(final Location loc) {
        /*getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), "Setting location from " + provider, Toast.LENGTH_LONG).show();
            }
        });*/
        mLocation = loc;
        latitude = loc.getLatitude() + "";
        longitude = loc.getLongitude() + "";

        final String location = getLocationAddress(loc);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (null != captureDialog)
                    captureDialog.setAddress(location);
            }
        });
    }

    protected String getLocationAddress(Location loc) {
        if (null == loc)
            return "";

        return getLocationAddress(loc.getLatitude(), loc.getLongitude());
    }

    protected String getLocationAddress(double latitude, double longitude) {
        return getLocationAddress(latitude + "", longitude + "");
    }

    protected String getLocationAddress(String latitude, String longitude) {

        if(!HttpUtils.isNetworkAvailable(getActivity()))
            return "";

        try {
            String formattedPath = HttpUtils.PATH_LOCATION_ADDRESS;
            formattedPath = formattedPath.replace("latlng", latitude + "," + longitude);

            String urlRequest = HttpUtils.SERVER_URL + formattedPath;
            Log.e(TAG, "Getting Address from endpoint " + urlRequest);
            String response = HttpRestRequester.makeHttpGetRequest(urlRequest);

            Log.e(TAG, "location response " + response);
            if (null != response) {
                JsonObject jsonRespose = new Gson().fromJson(response, JsonObject.class);
                JsonElement ele;
                if (null != (ele = jsonRespose.get("address"))) {
                    return ele.getAsString();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return getResources().getString(R.string.coordinates_successfully_found_without_address);
    }


    protected void sendForm() {
        if (!validateInputs())
            return;
        attributes = getAttributes();


        Attribute code = new Attribute(DIListContract.CODE, orderNumber);
        Attribute type = new Attribute(DIListContract.TYPE, orderType);
        Attribute imei = new Attribute("imei", Utils.getDeviceImei(getActivity()));
        Attribute formType = new Attribute("FORM_TYPE", getFormType());

        attributes.add(code);
        attributes.add(type);
        attributes.add(formType);
        attributes.add(imei);

        String lng = "",
                lat = "",
                cordValid = validCoordinate;

        if (supportCaptureCoordinate) {
            if (null != mLocation) {
                lat = Location.convert(mLocation.getLatitude(), Location.FORMAT_DEGREES);
                lng = Location.convert(mLocation.getLongitude(), Location.FORMAT_DEGREES);
                cordValid = "true";
            } else if (latitude != "" && longitude != "") {
                lat = latitude;
                lng = longitude;
                cordValid = "true";
            }
            attributes.add(Attribute.create(Constants.COORDVALIDS, cordValid));
            attributes.add(Attribute.create(Constants.ID_DIRECCION, addressId));
            attributes.add(Attribute.create(Constants.LATITUD, lat));
            attributes.add(Attribute.create(Constants.LONGITUD, lng));
            attributes.add(Attribute.create(Constants.COORDATTEMPTS, String.valueOf(coordinateCaptureAttempt)));
        }


        PreviewDialog preview = new PreviewDialog();
        preview.setAttributes(attributes);
        preview.setListener(this);
        preview.show(getActivity().getSupportFragmentManager(), "preview.dialog");
    }


    @Override
    public void onActionResult(int result, PreviewDialog dialog) {
        dialog.dismiss();
        if (null == attributes)
            return;
        if (PreviewDialog.OnPreviewDismissListener.RESULT_DONE == result) {
            Log.d(TAG, "Started performLoginRequest");
            ActionSaveRequest request = new ActionSaveRequest(UserKnowledge.getUserCard(getActivity()), attributes);

            String cacheKey = request.createCacheKey();

            flipLoading(true);
            enabledComponents(false);
            spiceManager.execute(request, cacheKey, DurationInMillis.ALWAYS_EXPIRED, new ActionSendResponseListener(orderNumber, orderType));
        }
    }


    private void flipLoading(boolean loading) {
        if (loading && null != itemSend.getActionView())
            return;
        if (loading) {
            itemSend.setActionView(R.layout.actionbar_progress_layout);
        } else {
            itemSend.setActionView(null);
        }
    }


    protected void setPhone(TextView phone, String suggestedPhone) {
        if (null == phone) {
            Log.e("****", "Wrong phone text field provided");
            return;
        }

        final String candidatePhone = TextUtils.isEmpty(orderPhone) ? suggestedPhone : orderPhone;

        if (!TextUtils.isEmpty(candidatePhone)) {
            Log.e("****", "Setting candidate phone " + candidatePhone);

            String input = candidatePhone.replaceAll("\\D", "");
            if (6 > input.length())
                return;
            String phoneCode = input.substring(0, 3);
            String nextThree = input.substring(3, 6);
            String lastFour = input.substring(6);

            input = phoneCode + "-" + nextThree + "-" + lastFour;
            phone.setText(input);

            boolean valid = Pattern.compile(Constants.REGEX_PHONE).matcher(input).matches();
            //phone.setEnabled( !(valid && !TextUtils.isEmpty(orderPhone)) ); //disable the field only for incoming phone, not phone set by user and saved to memento
        } else {
            Log.e("****", "No candidate phone got.! ");
        }
    }

    protected void setDireccionCorrecta(boolean direccionCorrecta) {
        if (direccionCorrecta) {
            ((RadioButton) getActivity().findViewById(R.id.radio_direccion_no)).setChecked(false);
            ((RadioButton) getActivity().findViewById(R.id.radio_direccion_yes)).setChecked(true);
        } else {
            ((RadioButton) getActivity().findViewById(R.id.radio_direccion_yes)).setChecked(false);
            ((RadioButton) getActivity().findViewById(R.id.radio_direccion_no)).setChecked(true);
        }
    }

    protected void setNoneDireccionCorrecta() {
        ((RadioButton) getActivity().findViewById(R.id.radio_direccion_yes)).setChecked(false);
        ((RadioButton) getActivity().findViewById(R.id.radio_direccion_no)).setChecked(false);
    }


    private void returnToHome() {
        Intent home = new Intent(getActivity(), HomeActivity.class);
        home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(home);
        getActivity().finish();
    }


    @Deprecated
    /**
     *  Not needed anymore
     */
    private void removeSentOrder(String code, String type) {
        Log.e(TAG, "Removing order " + code + " of type " + type);
        ContentResolver resolver = getActivity().getContentResolver();
        StringBuilder builder = new StringBuilder();
        builder.append(DIListContract.CODE)
                .append(" LIKE '" + code + "' AND ")
                .append(DIListContract.TYPE)
                .append(" LIKE '" + type + "' ");

        Log.e(TAG, "Trying to remove with statements: " + builder.toString());

        resolver.delete(DIListContract.CONTENT_URI, builder.toString(), null);
        builder.setLength(0);
    }


    private void setOrderSentFlag(String code, String type) {
        SharedPreferences pref = getActivity().getSharedPreferences(Constants.STATUS_FLAG_PREFERENCES, Context.MODE_PRIVATE);
        pref.edit().putBoolean(code, true).commit();
    }

    protected boolean validInput(CharSequence sequence) {
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9-]+$");
        Matcher matcher = pattern.matcher(sequence.toString());
        return matcher.find();
    }

    protected void displayHelp(int kind) {
        HelpDialog helper = new HelpDialog();
        Bundle extras = new Bundle();
        extras.putInt(HelpDialog.EXTRA_KIND, kind);
        helper.setArguments(extras);
        helper.show(getActivity().getSupportFragmentManager(), "dialog.helper");
    }

    /**
     * @Deprecated use instead loadFramesObject
     */
    @Deprecated
    protected List<String> loadFrames() {
        List<String> frames = new LinkedList<>();

        try {
            ContentResolver resolver = getActivity().getContentResolver();
            Cursor cursor = resolver.query(FrameListContract.CONTENT_URI, new String[]{FrameListContract.NAME}, null, null, null);
            try {
                if (cursor.moveToNext()) {
                    do {
                        frames.add(cursor.getString(0));
                    } while (cursor.moveToNext());
                }
            } finally {
                if (null != cursor)
                    cursor.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return frames;
    }

    protected Frames getFrames() {
        if (null != frames && frames.isLoaded())
            return frames;
        return frames = loadFramesObject();
    }


    private Frames loadFramesObject() {
        Frames frames = new Frames();
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            Cursor cursor = resolver.query(FramesContract.CONTENT_URI, new String[]{FramesContract.DATA}, null, null, null);
            try {
                if (cursor.moveToNext()) {
                    do {
                        frames.load(cursor.getBlob(0));
                        break;
                    } while (cursor.moveToNext());
                }
            } finally {
                if (null != cursor)
                    cursor.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return frames;
    }

    protected void loadLastAction() {

        Log.d(TAG, "Started perfomingLastActionRequest");
        LastActionRequest request = new LastActionRequest(UserKnowledge.getUserCard(getActivity()), this.orderNumber, this.orderType);

        String cacheKey = request.createCacheKey();
        flipLoading(true);
        enabledComponents(false);
        spiceManager.execute(request, cacheKey, DurationInMillis.ALWAYS_EXPIRED, new LastActionResponseListener());

        /*List<Attribute> attributes = new ArrayList<>();

        return attributes;*/

    }

    private class ActionSendResponseListener implements RequestListener<OKResponse> {
        private String code;
        private String type;

        public ActionSendResponseListener(String code, String type) {
            this.code = code;
            this.type = type;
        }

        @Override
        public void onRequestFailure(SpiceException e) {
            Toast.makeText(getActivity(), R.string.unable_to_send_form, Toast.LENGTH_LONG).show();
            flipLoading(false);
            enabledComponents(true);
        }

        @Override
        public void onRequestSuccess(OKResponse okResponse) {
            flipLoading(false);
            enabledComponents(true);

            if (null == okResponse) {
                Toast.makeText(getActivity(), R.string.server_unavailable, Toast.LENGTH_SHORT).show();
                return;
            }
            if (!okResponse.isOk()) {
                if (okResponse.getOkCode() == -2) {
                    Toast.makeText(getActivity(), R.string.submitted_form, Toast.LENGTH_LONG).show();
                } if (okResponse.getOkCode() == -3) {
                    Toast.makeText(getActivity(), R.string.inactive_to_post_form, Toast.LENGTH_LONG).show();
                }   else {
                    Toast.makeText(getActivity(), R.string.unable_to_send_form, Toast.LENGTH_LONG).show();
                }

                return;
            }

            try {
                onPostSent();
//                    removeSentOrder(code, type);
                setOrderSentFlag(code, type);
                Toast.makeText(getActivity(), R.string.form_sent, Toast.LENGTH_LONG).show();

            } catch (Exception ex) {
                Log.e(TAG, "Exception while trying to remove order ");
                ex.printStackTrace();
            }
            returnToHome();

        }
    }


    public class LastActionResponseListener implements RequestListener<LastActionResponse> {
        public LastActionResponseListener() {

        }

        @Override
        public void onRequestFailure(SpiceException e) {
            //Toast.makeText(getActivity(), R.string.unable_to_send_form, Toast.LENGTH_LONG).show();
            flipLoading(false);
            //enabledComponents(true);*/
        }

        @Override
        public void onRequestSuccess(LastActionResponse lastActionResponse) {
            flipLoading(false);
            if (lastActionResponse == null) {
                Toast.makeText(getActivity(), R.string.server_unavailable, Toast.LENGTH_SHORT).show();
                return;
            }
            Log.i(TAG, "Json: " + lastActionResponse.toString());
            if (lastActionResponse.isFormValueEmpty()) {
                Toast.makeText(getActivity(), R.string.not_posted_form, Toast.LENGTH_SHORT).show();
                return;
            }

            fillForm(lastActionResponse.getFormValues());
        }
    }


    protected void toast(int res) {
        toast(res, Toast.LENGTH_LONG);
    }

    protected void toast(int res, int length) {
        Toast.makeText(getActivity(), res, length).show();
    }
//
//    protected void enabledCoordinateComponents(boolean enabled){
//        captureCoordinateButton.setClickable(enabled);
//        captureCoordinateButton.setVisibility(enabled ? View.VISIBLE : View.GONE);
//
//        cancelLookupButton.setClickable(!enabled);
//        cancelLookupButton.setVisibility(!enabled ? View.VISIBLE : View.GONE);
//    }

    public boolean isCapturingCoordinates() {
        return capturingCoordinates;
    }

    protected abstract void fillForm(FormValues formValues);

    protected abstract String getFormType();

    protected abstract List<Attribute> getAttributes();

    protected abstract void enabledComponents(boolean enabled);

    protected abstract boolean validateInputs();

    protected abstract void onPostSent();
}
