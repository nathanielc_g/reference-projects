package com.claro.mobilecfc.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.*;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.activities.prefs.UserKnowledge;
import com.claro.mobilecfc.robospice.request.LoginRequest;
import com.claro.mobilecfc.robospice.response.OKResponse;
import com.claro.mobilecfc.robospice.services.CfcService;
import com.claro.mobilecfc.services.DIDownloaderService;
import com.claro.mobilecfc.utils.HttpUtils;
import com.claro.mobilecfc.utils.Utils;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by Christopher Herrera on 2/20/14.
 * <p/>
 * Refactored by Jansel R. Abreu (Vanwolf)
 */
public class LoginActivity extends Activity {

    private static String TAG = "SplashScreen";

    protected static final int FLIP_LOADING = 0;
    protected static final int FLIP_ERROR = 1;

    private static final int BUTTON_FLIP_CHILD = 0;
    private static final int FEEDBACK_FLIP_CHILD = 1;


    private ViewFlipper flipper;
    private ViewFlipper flipper2;
    private TextView imei;
    private EditText tarjetaEdit;
    private SpiceManager spiceManager = new SpiceManager(CfcService.class);
    private String credencialTarjeta;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        imei = (TextView) findViewById(R.id.imei_edit);
        tarjetaEdit = (EditText) findViewById(R.id.tarjeta_edit);
        tarjetaEdit.setRawInputType(Configuration.KEYBOARD_12KEY);
        imei.setText(Utils.getDeviceImei(this));
    }


    @Override
    protected void onStart() {
        super.onStart();
        spiceManager.start(this);
        checkNetworkConnection();
    }


    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    protected void checkNetworkConnection(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                if( !HttpUtils.isNetworkAvailable(LoginActivity.this) ){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LoginActivity.this, R.string.not_network, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();
    }


    private void tryLogUser(String card, String deviceId) {
        TextView loadingStatus = (TextView) findViewById(R.id.feedback_loading_text);
        loadingStatus.setText(getResources().getText(R.string.login_feedback));

        enabledWidgets(false);
        fadeSelfFlipView(R.id.feedback_flipper2, FEEDBACK_FLIP_CHILD);
        fadeFlipView(R.id.feedback_flipper, FLIP_LOADING);
        performLoginRequest(card,deviceId);
    }


    private void persistCard() {
        UserKnowledge.setUserCard(this, tarjetaEdit.getText().toString());
        startDIDownloaderService();
    }


    private void startDIDownloaderService() {
        Intent service = new Intent(this, DIDownloaderService.class);
        startService(service);
    }


    private void performLoginRequest(final String card, final String imei) {
        TextView loadingStatus = (TextView) findViewById(R.id.feedback_loading_text);
        loadingStatus.setText(getResources().getText(R.string.login_feedback));

        enabledWidgets(false);
        fadeFlipView(R.id.feedback_flipper, FLIP_LOADING);


        Log.d(TAG, "Started performLoginRequest");
        LoginRequest lr = new LoginRequest(card, imei);
        String cacheKey = lr.createCacheKey();

        spiceManager.execute(lr, cacheKey, DurationInMillis.ALWAYS_EXPIRED, new LoginRequestListener());
    }


    public class LoginRequestListener implements RequestListener<OKResponse> {
        @Override
        public void onRequestFailure(SpiceException e) {
            loginFaseUnPassed(false);
        }

        @Override
        public void onRequestSuccess(OKResponse loginResponse) {
            if( null == loginResponse ){
                loginFaseUnPassed( false );
                return;
            }
            if (loginResponse.isOk()) {
                enabledWidgets(true);
                persistCard();
                passActivity();
            } else {
                loginFaseUnPassed(true);
            }
        }
    }


    public void onLogin(View view) {
        credencialTarjeta = tarjetaEdit.getText().toString();

        if (null != credencialTarjeta && credencialTarjeta.length() > 0) {
            tryLogUser(credencialTarjeta, Utils.getDeviceImei(this));
        } else {
            Toast.makeText(this, "Favor intente de nuevo", Toast.LENGTH_SHORT).show();
        }
    }


    public void onRetry(View view) {
        onLogin(view);
    }


    private void enabledWidgets(boolean enabled) {
        Button sigin = (Button) findViewById(R.id.button1);
        sigin.setEnabled(enabled);
    }


    private void passActivity() {
        UserKnowledge.setUserLogged(this, true);
        Class<?> activity = null;
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    private void loginFaseUnPassed( boolean badCrendentials ) {
        int feedbackResource = R.string.server_unavailable;

        if( badCrendentials )
            feedbackResource = R.string.unable_authentication;

        Toast.makeText(this,feedbackResource,Toast.LENGTH_LONG).show();
//        TextView error = (TextView) findViewById(R.id.feedback_error_text);
//        error.setText(getResources().getString(feedbackResource));

        fadeFlipView(R.id.feedback_flipper, FLIP_ERROR);
    }

    protected void fadeSelfFlipView(int flipperId, int child) {
        if (flipper2 == null) {
            flipper2 = (ViewFlipper) findViewById(flipperId);
            flipper2.setInAnimation(this, android.R.anim.fade_in);
            flipper2.setOutAnimation(this, android.R.anim.fade_out);
        }
        flipper2.setDisplayedChild(child);
    }

    protected void fadeFlipView(int flipperId, int child) {
        if (flipper == null) {
            flipper = (ViewFlipper) findViewById(flipperId);
            flipper.setInAnimation(this, android.R.anim.fade_in);
            flipper.setOutAnimation(this, android.R.anim.fade_out);
        }
        flipper.setDisplayedChild(child);
    }

}