package com.claro.mobilecfc.activities.fragments.forms;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.activities.fragments.forms.prefs.Memento;
import com.claro.mobilecfc.dto.Attribute;
import com.claro.mobilecfc.dto.FormValues;
import com.claro.mobilecfc.dto.Frames;
import com.claro.mobilecfc.utils.Constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * <p/>
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad. Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad.Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 */
public class PSTNFormFragment extends BaseFormFragment {

    private static final String FORM_TYPE = "PSTN";

    private TextView cabinaLabel;
    private TextView parLocalLabel;

    private EditText phone;
    private EditText cabina;
    private CheckBox cuentaFija;

    private LinearLayout linearParFeeder;
    private EditText parFeeder;
    private Spinner numParFeeder;
    private boolean isFillingNumParFeeder = false;
    private EditText otrosParFeeder;

    private EditText terminal;

    private LinearLayout linearParLocal;
    private EditText parLocal;
    private Spinner numParLocal;
    private boolean isFillingNumParLocal = false;
    private EditText otrosParLocal;

    private Spinner localidad;
    private boolean isFillingLocalidad = false;
    //    private EditText direccion;
    private short addressState = -1;
    private boolean sameAddress;

    protected Memento memento;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        memento = new Memento(this.getClass().getSimpleName() + ":" + orderNumber);
        setSupportCaptureCoordinate(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pstn_form, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }


    @Override
    protected String getFormType() {
        return FORM_TYPE;
    }

    @Override
    protected List<Attribute> getAttributes() {
        List<Attribute> attributes = new ArrayList<Attribute>();

        attributes.add(Attribute.create(Constants.PHONE, phone.getText().toString()));

        if (!cuentaFija.isChecked()) {
            attributes.add(Attribute.create(Constants.CABINA, cabina.getText().toString().trim().toUpperCase()));
            attributes.add(Attribute.create(Constants.PAR_LOCAL, getParLocal().trim().toUpperCase()));
        }

        attributes.add(Attribute.create(Constants.LOCALIDAD, localidad.getSelectedItem().toString()));
        attributes.add(Attribute.create(Constants.CUENTA_FIJA, cuentaFija.isChecked() ? "SI" : "NO"));
        attributes.add(Attribute.create(Constants.PAR_FEEDER, getParFeeder().trim().toUpperCase()));
        attributes.add(Attribute.create(Constants.TERMINAL, terminal.getText().toString().trim().toUpperCase()));
        attributes.add(Attribute.create(Constants.DIRECCION_CORRECTA, sameAddress? "SI": "NO"));

        return attributes;
    }


    @Override
    protected void enabledComponents(boolean enabled) {

    }

    protected void init() {
        initComponents();
    }

    private AdapterView.OnItemSelectedListener parlocalListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if(isFillingNumParLocal) {
                isFillingNumParLocal = false;
                return;
            }

            if (numParLocal.getSelectedItem().toString().equalsIgnoreCase("Otros")) {
               // if (!isRefreshingForm(true)) {
                    otrosParLocal.setText("");
                //}
                otrosParLocal.setVisibility(View.VISIBLE);
            } else {
                otrosParLocal.setVisibility(View.GONE);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private AdapterView.OnItemSelectedListener parFeederlListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            if(isFillingNumParFeeder){
                isFillingNumParFeeder = false;
                return;
            }

            if (numParFeeder.getSelectedItem().toString().equalsIgnoreCase("Otros")) {
                otrosParFeeder.setText("");
                otrosParFeeder.setVisibility(View.VISIBLE);
            } else {
                otrosParFeeder.setVisibility(View.GONE);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private void initComponents() {

        cabinaLabel = (TextView) getActivity().findViewById(R.id.label_cabina);
        parLocalLabel = (TextView) getActivity().findViewById(R.id.label_par_local);

        phone = (EditText) getActivity().findViewById(R.id.edit_phone);
        cabina = (EditText) getActivity().findViewById(R.id.edit_cabina);
        cabina.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b){
                    if (!TextUtils.isEmpty(cabina.getText()))
                        cabina.setSelection(cabina.getText().length());
                }
            }
        });

        cuentaFija = (CheckBox) getActivity().findViewById(R.id.edit_cuenta_fija);

        linearParFeeder = (LinearLayout) getActivity().findViewById(R.id.linear_par_feeder);
        parFeeder = (EditText) getActivity().findViewById(R.id.edit_par_feeder);
        numParFeeder = (Spinner) getActivity().findViewById(R.id.spinner_num_par_feeder);
        numParFeeder.setOnItemSelectedListener(parFeederlListener);
        otrosParFeeder = (EditText) getActivity().findViewById(R.id.edit_otros_par_feeder);
        otrosParFeeder.setVisibility(View.GONE);
        numParFeeder.setVisibility(View.GONE);


        terminal = (EditText) getActivity().findViewById(R.id.edit_terminal);
        terminal.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    //String terminalSecondSession = getTerminalSecondSession();
                    if (!validateTerminal(true))
                        return;

                    /*if(cuentaFija.isChecked()){
                        setAdapterToSpinnerPar(getTerminalSecondSession(), numParFeeder);
                    }else {
                        setAdapterToSpinnerPar(getTerminalSecondSession(), numParLocal);
                    }*/


                }
            }
        });

        linearParLocal = (LinearLayout) getActivity().findViewById(R.id.linear_par_local);
        parLocal = (EditText) getActivity().findViewById(R.id.edit_par_local);
        numParLocal = (Spinner) getActivity().findViewById(R.id.spinner_num_par_local);

        numParLocal.setOnItemSelectedListener(parlocalListener);
        otrosParLocal = (EditText) getActivity().findViewById(R.id.edit_otros_par_local);
        otrosParLocal.setVisibility(View.GONE);


        cabina.addTextChangedListener(cabinaReporterWatcher);
        terminal.addTextChangedListener(terminalReporterWatcher);
        //parFeeder.addTextChangedListener(terminalReporterWatcher);

        localidad = (Spinner) getActivity().findViewById(R.id.edit_localidad);

//        List<String> frames = loadFrames();
        final List<String> frames = new LinkedList<>(getFrames().listNames());

        if (null != frames) {
            frames.add(0, "Seleccione FRAME");

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, frames);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            localidad.setAdapter(adapter);

            localidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                    if (isFillingLocalidad) {
                        isFillingLocalidad = false;
                        return;
                    }


                    if (0 == position)
                        return;

                    int idx = position - 1; //Compact position to remove "Seleccione Prefix" index
                    if (0 > idx)
                        return;

                    if (!cuentaFija.isChecked()) {
                        String prefix = getFrames().getCabinaPrefix(position - 1);
                        // 2015-GESTI-3083-4:  Los FRAME que no tengan prefijo deberán tener “C” como constante.
                        cabina.setText(TextUtils.isEmpty(prefix) ? "C" : prefix);
                        if (!TextUtils.isEmpty(cabina.getText()))
                            cabina.setSelection(cabina.getText().length());
                    }


                    onFrameSelected(getFrames(), idx);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }

        final ImageButton cabinaHelp = (ImageButton) getActivity().findViewById(R.id.help_cabina);
        final ImageButton parLocalHelp = (ImageButton) getActivity().findViewById(R.id.help_parLocal);


        /// cuenta fija input logic
        cuentaFija.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {

                cabina.setText("");

                int visibility;
                int visibilityCuentaFija;
                if (checked) {
                    visibility = View.GONE;
                    visibilityCuentaFija = View.VISIBLE;
                } else {
                    visibility = View.VISIBLE;
                    visibilityCuentaFija = View.GONE;
                }
                cabina.setVisibility(visibility);
                cabinaLabel.setVisibility(visibility);

                parLocal.setVisibility(visibility);

                parLocalLabel.setVisibility(visibility);
                numParLocal.setVisibility(visibility);


                otrosParLocal.setVisibility(View.GONE);

                cabinaHelp.setVisibility(visibility);
                parLocalHelp.setVisibility(visibility);


                numParFeeder.setVisibility(visibilityCuentaFija);

                otrosParFeeder.setVisibility(View.GONE);


                terminal.setText(getTerminalFistSession() + "-" + getTerminalSecondSession());
                if(!TextUtils.isEmpty(terminal.getText()))
                    terminal.setSelection(terminal.getText().length());

                setReadOnlyEditPar(checked);

                onCuentaFijaChange(checked);
            }
        });

        RadioButton radio = (RadioButton) getActivity().findViewById(R.id.radio_direccion_yes);
        radio.setOnClickListener(ADDRESS_SELECTION_LISTENER);

        radio = (RadioButton) getActivity().findViewById(R.id.radio_direccion_no);
        radio.setOnClickListener(ADDRESS_SELECTION_LISTENER);

        this.setHelpListener();

    }


    private void setReadOnlyEditPar(boolean readOnly) {
        if (readOnly) {
            /*parLocal.setKeyListener(null);
            parFeeder.setKeyListener(null);*/

            parLocal.setEnabled(false);
            parFeeder.setEnabled(false);
            /*parLocal.setFocusable(false); parLocal.setClickable(false);
            parFeeder.setFocusable(false); parFeeder.setClickable(false);*/
        } else {

            parLocal.setEnabled(false);
            parFeeder.setEnabled(true);
            /*parLocal.setFocusable(true); parLocal.setClickable(true);
            parFeeder.setFocusable(true); parFeeder.setClickable(true);*/

            /*parLocal.setKeyListener((KeyListener)parLocal.getTag());
            parFeeder.setKeyListener((KeyListener)parFeeder.getTag())*/
            ;
        }
    }

    @Override
    protected boolean validateInputs() {
        boolean validated = true;

        String input = TextUtils.isEmpty(phone.getText().toString()) ? "" : phone.getText().toString().trim().toUpperCase();

        validated &= Pattern.compile(Constants.REGEX_PHONE).matcher(input).matches();

        if (!validated) {
            toast(R.string.validation_phone);
            return validated;
        }

        if (!cuentaFija.isChecked()) {
            input = TextUtils.isEmpty(cabina.getText().toString()) ? "" : cabina.getText().toString().trim().toUpperCase();

            validated &= Pattern.compile(Constants.REGEX_CABINA).matcher(input).matches();
            if (!validated) {
                toast(R.string.validation_cabina);
                return validated;
            }

        }

        if (localidad.getSelectedItemPosition() == 0) {
            toast(R.string.validation_localidad, Toast.LENGTH_SHORT);
            return false;
        }


        // TERMINAL
        if(!validateTerminal()){
            return false;
        }

        // PAR FEEDER
        if(!validateParFeeder()){
            return false;
        }

        /*input = TextUtils.isEmpty(getParFeeder()) ? "" : getParFeeder().trim().toUpperCase();

        validated &= Pattern.compile(Constants.REGEX_FEEDER).matcher(input).matches();
        if (!validated) {
            toast(R.string.validation_par_feeder);
            return validated;
        }*/


        if (!cuentaFija.isChecked()) {

            input = TextUtils.isEmpty(getParLocal()) ? "" : getParLocal().trim().toUpperCase();

            validated &= Pattern.compile(Constants.REGEX_PAR_LOCAL).matcher(input).matches();
            if (!validated) {
                toast(R.string.validation_par_local);
                return validated;
            }
        }


        if (-1 == addressState) {
            toast(R.string.validation_address);
            return false;
        }

        if(isCapturingCoordinates()) {
            Toast.makeText(getActivity(), R.string.wait_captured_coordinate, Toast.LENGTH_LONG).show();
            return false;
        }

        return validated;
    }

    private void setAdapterToSpinnerPar(String terminalSecondSession, Spinner numPar) {
        setAdapterToSpinnerPar(terminalSecondSession, numPar, false, null, false);

    }

    // setup spinner par feeder
    private void setAdapterToSpinnerPar(String terminalSecondSession, Spinner numPar, boolean setValue ,String parValue, boolean isFeeeder) {
        //(?=\-)-[0-9]{1,2}
        if (terminalSecondSession  == null)
            terminalSecondSession = "";

        String strNumSecondSession = terminalSecondSession.replaceAll("[a-zA-Z]{1,2}", "");//.substring(0);

        int numSecondSession = 0;
        try {
            numSecondSession = strNumSecondSession.length() > 0 ? Integer.parseInt(strNumSecondSession) : 0;
        }catch (NumberFormatException ex){
            ex.printStackTrace();
            Log.i(getTag(), ex.getMessage());
        }
        //int numSecondSession = strNumSecondSession.length() > 0 ? Integer.parseInt(strNumSecondSession) : 0;
        String charSecondSession = numSecondSession == 0 ? terminalSecondSession : terminalSecondSession.replaceAll(numSecondSession + "", "");
        String firstCharacter = charSecondSession.length() > 0 ? charSecondSession.substring(0, 1) : "";
        int min = (numSecondSession * 100) + 1;
        int max = (numSecondSession * 100) + 50;

        if (Pattern.compile("[n-z]|[N-Z]").matcher(firstCharacter).matches()) {
            min = (numSecondSession * 100) + 51;
            max = (numSecondSession * 100) + 100;
        }

        List<String> items = new ArrayList<>();
        items.add("Seleccionar");
        for (int i = min; i <= max; i++) {
            items.add(String.format("%04d", i));
        }
        items.add("Otros");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_spinner_item, items);
        numPar.setAdapter(adapter);

        if (!setValue)
            return;

        if (isFeeeder) {
            setParFeeder(parValue, min, max);
        }else {
            setParLocal(parValue, min, max);

        }
    }



    private void setParFeeder (String parValue, int min, int max) {
        if (cuentaFija.isChecked()) {
            if (parValue != null && !TextUtils.isEmpty(parValue)) {
                //this.parFeeder.setText(parValue.indexOf("-"));
                this.parFeeder.setText(parValue.substring(0,parValue.indexOf("-")+1));
                setNumPar(parValue, min, max, numParFeeder, otrosParFeeder);
            }
        } else {
            this.parFeeder.setText(parValue);
        }
    }

    private void setNumPar (String parValue, int min, int max, Spinner numPar, EditText otros) {
        int numParValue = 0;
        int idx = 0;
        try {
            numParValue = Integer.parseInt(parValue.substring(parValue.indexOf("-") + 1));
            idx = numParValue - min;
            if (idx > 50) {
                idx -= 50;
            }
            idx += 1;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (numParValue >= min && numParValue <= max) {
            numPar.setSelection(idx);
            otros.setText("");
            otros.setVisibility(View.GONE);
        } else {
            numPar.setSelection(51);
            otros.setText(String.valueOf(String.format("%04d", numParValue)));
            otros.setVisibility(View.VISIBLE);
        }
    }

    // Return concatenate values of parfeeder fields if cuentaFija is checked
    private String getParFeeder() {
        String parFeederVal = parFeeder.getText().toString();
        if (cuentaFija.isChecked()) {
            StringBuilder sb = new StringBuilder();
            //sb.setLength(0);
            sb.append(parFeederVal);
            if (numParFeeder.getSelectedItem() != null && !numParFeeder.getSelectedItem().toString().equalsIgnoreCase("Seleccionar")) {
                if (numParFeeder.getSelectedItem().toString().equalsIgnoreCase("Otros")) {
                    sb.append(otrosParFeeder.getText().toString());
                } else {
                    sb.append(numParFeeder.getSelectedItem().toString());
                }
            }
            parFeederVal = sb.toString();
            sb.setLength(0);
        }
        return parFeederVal;

    }

    private void setParLocal (String parValue, int min, int max) {
        if (!cuentaFija.isChecked()) {
            if (parValue != null && !TextUtils.isEmpty(parValue)) {
                this.parLocal.setText(parValue.substring(0,parValue.indexOf("-")+1));
                setNumPar(parValue, min, max, numParLocal, otrosParLocal);
            }
        } else {
            this.parLocal.setText(parValue);
        }
    }

    private String getParLocal() {
        StringBuilder sb = new StringBuilder();
        sb.append(parLocal.getText().toString());
        if (!cuentaFija.isChecked()) {
            String numParLocalVal = numParLocal.getSelectedItem() != null && !numParLocal.getSelectedItem().toString().equalsIgnoreCase("Seleccionar") ? numParLocal.getSelectedItem().toString() : "";
            if (numParLocalVal.equalsIgnoreCase("Otros")) {
                sb.append(otrosParLocal.getText().toString());
            } else {
                sb.append(numParLocalVal);
            }
        }
        return sb.toString();

    }

    protected String getCabina() {
        return cabina.getText().toString();
    }


    private String getTerminalFistSession() {
        int indexHyphen = terminal.getText().toString().indexOf("-");
        if (indexHyphen <= 0) {
            return "";
        }
        return terminal.getText().toString().substring(0, indexHyphen);
    }

    private String getTerminalSecondSession() {
        int indexHyphen = terminal.getText().toString().indexOf("-");
        if (indexHyphen <= 0) {
            return "";
        }
        return terminal.getText().toString().substring(indexHyphen + 1);
    }

    private boolean validateParFeeder() {
        return validateParFeeder(true);
    }

    private boolean validateParFeeder(boolean showMessage) {
        boolean validated = true;

        String input = TextUtils.isEmpty(getParFeeder()) ? "" : getParFeeder().trim().toUpperCase();


        validated &= Pattern.compile(Constants.REGEX_FEEDER).matcher(input).matches();
        if (!validated || Pattern.compile(Constants.REGEX_PRE_HYPHEN_FEEDER).matcher(input).matches()) {

            validated = false;
            if(showMessage) {
                toast(R.string.validation_par_feeder, Toast.LENGTH_SHORT);
            }
            return validated;
        }

        return validated;
    }

    private boolean validateTerminal() {
        return validateTerminal(false);
    }

    private boolean validateTerminal(boolean showMessage) {
        boolean validated = true;

        String input = TextUtils.isEmpty(terminal.getText().toString()) ? "" : terminal.getText().toString().trim().toUpperCase();

        validated &= Pattern.compile(Constants.REGEX_TERMINAL).matcher(input).matches();

        /*
        * This is a complex validation
        *
        * If validated == true && CuentaFija == true? TERMINAL need to be equals to cabina until prehyphen("-").
        * Else validated == true && CuentaFija == false?
        * TERMINAL need to be a valid value for TERMINAL_REGEX and must not match for PREHYPHEN_REGEX
        *
        * */
        if (
                !validated
                ||
                (!cuentaFija.isChecked() && !Pattern.compile("^" + cabina.getText().toString().trim().toUpperCase() + "-.*$").matcher(input).matches())
                ||
                Pattern.compile(Constants.REGEX_PRE_HYPHEN_TERMINAL).matcher(input).matches()
                        /*||
                !Pattern.compile("^" + cabina.getText().toString().trim().toUpperCase() + "-.*$").matcher(input).matches()*/)
        {
        //if (!validated || Pattern.compile(Constants.REGEX_PRE_HYPHEN_TERMINAL).matcher(input).matches()) {
            validated = false;
            toast(R.string.validation_terminal, Toast.LENGTH_SHORT);
            return validated;
        }

        return validated;
    }


    private TextWatcher cabinaReporterWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence sequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence sequence, int i, int i2, int i3) {
            String terminalFirstSession = getCabina() + "-";
            terminal.setText(terminalFirstSession);
            if(!TextUtils.isEmpty(terminal.getText()))
                terminal.setSelection(terminal.getText().length());
            onCabinaChange( getCabina() );
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };

    private TextWatcher terminalReporterWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String terminalFirstSession = getTerminalFistSession() + "-";
            parLocal.setText(terminalFirstSession);
            if(cuentaFija.isChecked()){
                parFeeder.setText(terminalFirstSession);
            }

            if (terminal.getText().length() < 3)
                return;

            if (!validateTerminal())
                return;

            if (cuentaFija.isChecked()) {
                setAdapterToSpinnerPar(getTerminalSecondSession(), numParFeeder);
            } else {
                setAdapterToSpinnerPar(getTerminalSecondSession(), numParLocal);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    public void onStop() {
        super.onStop();
        memento.addToHistory("phone", phone.getText().toString());
        memento.addToHistory("cabina", cabina.getText().toString());
        memento.addToHistory("cuentaFija", "" + (cuentaFija.isChecked() ? 1 : 0));
        memento.addToHistory("parFeeder", getParFeeder());
        memento.addToHistory("terminal", terminal.getText().toString());
        memento.addToHistory("parLocal", getParLocal());
        memento.addToHistory("localidad", localidad.getSelectedItemPosition() + "");

        memento.saveMemento(getActivity());
    }

    @Override
    protected void fillForm(FormValues formValues) {
        Log.e(this.getTag(), "Filling form from lastActionPosted");
        isRefreshing();
        // SET PHONE
        setPhone(phone, formValues.getPath(Constants.PHONE));

        // LOCALIDAD
        String localidadVal = formValues.getPath(Constants.LOCALIDAD);
        if (localidadVal != null) {
            int idx = getFrames().listNames().indexOf(localidadVal) + 1;
            if (idx != -1) {
                setLocalidad(idx + "");
            }
        }
        cabina.setText(formValues.getPath(Constants.CABINA));
        cuentaFija.setChecked("SI".equals(formValues.getPath(Constants.CUENTA_FIJA)));

        terminal.setText(formValues.getPath(Constants.TERMINAL));

        setReadOnlyEditPar(cuentaFija.isChecked());

        setAdapterToSpinnerPar(getTerminalSecondSession(), numParFeeder, true, formValues.getPath(Constants.PAR_FEEDER), true);
        setAdapterToSpinnerPar(getTerminalSecondSession(), numParLocal, true, formValues.getPath(Constants.PAR_LOCAL), false);

        // DIRECCION CORRECTA
        String direccionCorrecta = formValues.getPath(Constants.DIRECCION_CORRECTA);
        if (direccionCorrecta == null){
            setNoneDireccionCorrecta();
        }else {
            if ( direccionCorrecta.equalsIgnoreCase("SI")) {
                addressState = 1;
                sameAddress = true;
                setDireccionCorrecta(true);
            } else {
                addressState = 2;
                sameAddress = false;
                setDireccionCorrecta(false);
            }
        }

    }

    private void isRefreshing () {
        isFillingNumParFeeder = true;
        isFillingNumParLocal = true;
        isFillingLocalidad = true;
    }


    protected void setLocalidad (String localidad) {
        int selection;
        try {
            selection = Integer.valueOf(localidad);
        } catch (Exception e) {
            selection = 0;
        }
        this.localidad.setSelection(selection);
    }



    @Override
    public void onResume() {
        super.onResume();
        Log.e("***********", "PSTNFormFragment.onResume");

        memento.loadMemento(getActivity());
//        phone.setText(memento.getFromHistory("phone") + "");
        cabina.setText(memento.getFromHistory("cabina") + "");
        cuentaFija.setChecked("1".equals(memento.getFromHistory("cuentaFija")));
        parFeeder.setText(memento.getFromHistory("parFeeder") + "");
        parLocal.setText(memento.getFromHistory("parLocal") + "");
        terminal.setText(memento.getFromHistory("terminal") + "");

        // LOCALIDAD
        setLocalidad(memento.getFromHistory("localidad") + "");

        // SET PHONE
        setPhone(phone,memento.getFromHistory("phone") + "");


        //f(parFeeder.getKeyListener() != null){
        //parFeeder.setTag(parFeeder.getKeyListener());
        //}

        //if(parLocal.getKeyListener() != null) {
        //parLocal.setTag(parLocal.getKeyListener());
        //}

        setReadOnlyEditPar(cuentaFija.isChecked());

        setAdapterToSpinnerPar(getTerminalSecondSession(), numParFeeder);
        setAdapterToSpinnerPar(getTerminalSecondSession(), numParLocal);

    }


    @Override
    protected void onPostSent() {
        memento.removeMemento(getActivity());
    }


    protected void  setHelpListener() {
        final View.OnClickListener LISTENER = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayHelp(view.getId());
            }
        };

        ImageButton icon = (ImageButton) getActivity().findViewById(R.id.help_parFeeder);
        icon.setOnClickListener(LISTENER);

        icon = (ImageButton) getActivity().findViewById(R.id.help_cabina);
        icon.setOnClickListener(LISTENER);


        icon = (ImageButton) getActivity().findViewById(R.id.help_parLocal);
        icon.setOnClickListener(LISTENER);

        icon = (ImageButton) getActivity().findViewById(R.id.help_terminal);
        icon.setOnClickListener(LISTENER);
    }

    protected void onFrameSelected(Frames frames, int frame ) {}

    protected void onCabinaChange(String value){
    }

    protected void onCuentaFijaChange( boolean checked ){}

    protected int getSelectedFrame(){
        if( null == localidad )
            return -1;

        int idx = localidad.getSelectedItemPosition() - 1; //Compact position to remove "Seleccione Prefix" index
        return idx;
    }

    protected final View.OnClickListener ADDRESS_SELECTION_LISTENER = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.radio_direccion_yes) {
                addressState = 1;
                sameAddress = true;
            } else {
                addressState = 2;
                sameAddress = false;
            }
        }
    };
}


