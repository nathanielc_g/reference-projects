package com.claro.mobilecfc.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.activities.prefs.UserKnowledge;
import com.claro.mobilecfc.robospice.request.VersionRequest;
import com.claro.mobilecfc.robospice.response.VersionResponse;
import com.claro.mobilecfc.robospice.services.CfcService;
import com.claro.mobilecfc.utils.Utils;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by Jansel R. Abreu on 5/13/2015.
 */
public class InvalidApnActivity extends Activity {

    public static boolean isAPNConfigured = true;

    private SpiceManager spiceManager = new SpiceManager(CfcService.class);

    private TextView warnText;

    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_invalid_apn);

        warnText = (com.claro.mobilecfc.widget.TextView) findViewById(R.id.tv_warn_message);

        if (Utils.isApnConfigured(this) == Utils.APNStatus.UNKNOWN) {
            warnText.setVisibility(View.INVISIBLE);
            showLoadingDialog();
            makeRequest();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        spiceManager.start(this);
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    @Override
    protected void onPause() {
        closeLoadingDialog();
        super.onPause();
    }

    private void makeRequest() {
        VersionRequest request = new VersionRequest();
        String cacheKey = request.createCacheKey();

        spiceManager.execute(request, cacheKey, DurationInMillis.ALWAYS_EXPIRED, new RequestListener<VersionResponse>() {
            @Override
            public void onRequestFailure(SpiceException e) {
                closeLoadingDialog();

                onFailure();
            }

            @Override
            public void onRequestSuccess(VersionResponse versionResponse) {
                closeLoadingDialog();

                if (versionResponse != null) {
                    Class<?> activity;

                    if (!UserKnowledge.isConditionUpTaked(InvalidApnActivity.this)) {
                        activity = ConditionActivity.class;
                    } else if (!UserKnowledge.isUserLogged(InvalidApnActivity.this)) {
                        activity = LoginActivity.class;
                    } else
                        activity = HomeActivity.class;

                    Intent intent = new Intent(InvalidApnActivity.this, activity);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else {
                    onFailure();
                }
            }
        });

    }

    private void onFailure() {
        isAPNConfigured = false;
        warnText.setVisibility(View.VISIBLE);
    }

    private void showLoadingDialog() {
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Contactando con los servidores de CFC");
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.show();
    }

    private void closeLoadingDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }
}
