package com.claro.mobilecfc.activities.fragments.forms;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.claro.mobilecfc.R;

/**
 * Created by Jansel Valentin on 3/11/2016.
 **/
public class CaptureCoordinateDialog extends DialogFragment {

    private static int LOADING = 0;
    private static int ADDRESS = 1;

    private ViewFlipper flipper;
    private TextView textAddress;
    private OnDialogDismissListener listener;

    private boolean hasAddress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
        b.setTitle( "Capturando coordenadas" );
        b.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (null != listener) {
                    listener.onActionResult(OnDialogDismissListener.RESULT_DISMISS, CaptureCoordinateDialog.this);
                    dialogInterface.dismiss();
                }
            }
        });
//        b.setPositiveButton("OK", null); // let us inject our custom positive button t not dismiss until coordinates are captured


        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View content = inflater.inflate( R.layout.fragment_capture_coordinate_dialog,null);
        flipper = (ViewFlipper)content.findViewById( R.id.capture_coordinate_flipper );
        textAddress = (TextView) content.findViewById( R.id.coordinate_text_address);

        flipper.setInAnimation(getActivity(), android.R.anim.fade_in);
        flipper.setOutAnimation(getActivity(), android.R.anim.fade_out);

        b.setView(content);
        final AlertDialog dialog = b.create();
        return dialog;
    }

    public void setAddress(String address){
        hasAddress = !TextUtils.isEmpty(address);
        if( hasAddress ) {
            textAddress.setText(address);
            flipToChild(ADDRESS);
        }else{
            flipToChild(LOADING);
        }

        if( null != getDialog() ){
            Button dialogButton = ((AlertDialog)getDialog()).getButton( DialogInterface.BUTTON_NEGATIVE);
            if( null != dialogButton && hasAddress )
                dialogButton.setText( "OK" );
            else
                dialogButton.setText( "Cancelar" );
        }
    }

    protected void flipToChild(int child) {
        if( null != flipper )
            flipper.setDisplayedChild(child);
    }

    public void setOnDialogDismissListener(OnDialogDismissListener listener) {
        this.listener = listener;
    }

    public interface OnDialogDismissListener {
        int RESULT_DISMISS = 1;
        void onActionResult(int result,CaptureCoordinateDialog dialog);
    }
}
