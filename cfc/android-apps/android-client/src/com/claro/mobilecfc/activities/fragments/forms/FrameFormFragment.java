package com.claro.mobilecfc.activities.fragments.forms;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.activities.fragments.forms.prefs.Memento;
import com.claro.mobilecfc.dto.Attribute;
import com.claro.mobilecfc.dto.FormValues;
import com.claro.mobilecfc.dto.Frames;
import com.claro.mobilecfc.utils.Constants;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/15/14.
 * <p/>
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad. Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad.Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 */

public class FrameFormFragment extends BaseFormFragment {


    private static final String FORM_TYPE = "FRAME";

    private EditText phone;
    private Spinner localidad;
    private boolean isFillingLocalidad = false;
    private EditText parFeeder;
    private CheckBox servicioDatos;

    private Memento memento;


    // LOGIC  FROM FRAGMENT DATA
    private static final int DSLAM_OUTDOOR = 1 << 2;

    private static final int DSLAM_INDOOR = 2 << 2;

    private static final int DSLAM_NONE = -1;

    private Spinner puerto;
    private boolean isFillingPuerto = false;

    private RadioButton dslamIndoorRadio;
    private RadioButton dslamOutdoorRadio;
    private int dslamType = DSLAM_NONE;

    private EditText dslam;

    private Set<String> techs;
    private String[] portRegex;
    private String[] portsType;
    private EditText puertoMask;


    private int port;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        memento = new Memento(this.getClass().getSimpleName() + ":" + orderNumber);
        setSupportCaptureCoordinate(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_frame_form, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    @Override
    protected String getFormType() {
        return servicioDatos.isChecked()? Constants.FORM_FRAME_DATA : FORM_TYPE;
    }

    @Override
    protected List<Attribute> getAttributes() {
        List<Attribute> attributes = new ArrayList<Attribute>();


        attributes.add(Attribute.create(Constants.PHONE, phone.getText().toString()));
        attributes.add(Attribute.create(Constants.LOCALIDAD, localidad.getSelectedItem().toString()));
        attributes.add(Attribute.create(Constants.PAR_FEEDER, parFeeder.getText().toString().trim().toUpperCase()));
        attributes.add(Attribute.create(Constants.SERVICIO_DATOS, servicioDatos.isChecked() ? "SI" : "NO"));

        // FIELDS DATA
        if (servicioDatos.isChecked()) {

            String puertoStr = puerto.getSelectedItem().toString();
            String puertoMaskStr = puertoMask.getText().toString();


            attributes.add(Attribute.create(Constants.TIPO_PUERTO, puertoStr.toUpperCase().trim()));
            attributes.add(Attribute.create(Constants.PUERTO, puertoMaskStr));
            attributes.add(Attribute.create(Constants.DSLAM, dslam.getText().toString().trim().toUpperCase()));
            attributes.add(Attribute.create(Constants.DSLAM_TYPE, stringifyDSLAMType(dslamType).toUpperCase()));

        }

        /*// TIPO DE PUERTO
        String default_message = getResources().getStringArray(R.array.fibra_puerto_labels)[0];
        String input = TextUtils.isEmpty(puerto.getSelectedItem().toString()) ? "" : puerto.getSelectedItem().toString().trim().toUpperCase();
        if (!default_message.equalsIgnoreCase(input)) {
            attributes.add(Attribute.create(Constants.TIPO_PUERTO, puertoStr.toUpperCase()));
        }else {
            attributes.add(Attribute.create(Constants.TIPO_PUERTO, ""));
        }
        attributes.add(Attribute.create(Constants.DSLAM, dslam.getText().toString().toUpperCase()));*/

        return attributes;
    }

    @Override
    protected void enabledComponents(boolean enabled) {

    }

    private void init() {
        initComponents();

        techs = new HashSet<>();
        techs.addAll(Arrays.asList("alcatel", "cisco", "stinger", "zyxel", "zte"));
    }


    private void initComponents() {
        showCoordinateButton(false);

        phone = (EditText) getActivity().findViewById(R.id.edit_phone);
        localidad = (Spinner) getActivity().findViewById(R.id.edit_localidad);

        final List<String> frames = new LinkedList<>(getFrames().listNames());
        if (null != frames) {

            frames.add(0, "Seleccione FRAME");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, frames);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            localidad.setAdapter(adapter);

            localidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    if (isFillingLocalidad) {
                        isFillingLocalidad = false;
                        return;
                    }

                    if (0 == position)
                        return;

                    int idx = position; //Compact position to remove "Seleccione Prefix" index
                    if (0 > idx)
                        return;

                    onFrameSelected(getFrames(), idx);

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }

        parFeeder = (EditText) getActivity().findViewById(R.id.edit_par_feeder);
        servicioDatos = (CheckBox) getActivity().findViewById(R.id.edit_servicio_dato);
        servicioDatos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                int visibility;
                if (checked) {
                    visibility = View.VISIBLE;
                } else {
                    visibility = View.GONE;
                }

                setServicioDatosVisibilty(visibility);
                /*getActivity().findViewById(R.id.container_dslam).setVisibility(visibility);
                dslam.setVisibility(visibility);
                getActivity().findViewById(R.id.label_puerto_especificacion).setVisibility(visibility);
                puerto.setVisibility(visibility);
                getActivity().findViewById(R.id.container_puerto).setVisibility(visibility);
                puertoMask.setVisibility( visibility );*/
            }
        });


        // LOGIC FROM FRAGMENT DATA


        portsType = getActivity().getResources().getStringArray(R.array.fibra_puerto_labels);
        puerto = (Spinner) getActivity().findViewById(R.id.edit_puerto);
        final String[] puertosMasks = getResources().getStringArray(R.array.fibra_tipo_puerto_masks);
        puerto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (isFillingPuerto) {
                    isFillingPuerto = false;
                    return;
                }

                puertoMask.setHint(puertosMasks[position]);
                puertoMask.setText(puertosMasks[position]);
//                puertoMask.setMask(puertosMasks[position]);
                port = position;

                setPortPrefix();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        dslamIndoorRadio = (RadioButton) getActivity().findViewById(R.id.radio_dslam_indoor);
        dslamIndoorRadio.setOnClickListener(DSLAM_TYPE_SELECTION_LISTENER);

        dslamOutdoorRadio = (RadioButton) getActivity().findViewById(R.id.radio_dslam_outdoor);
        dslamOutdoorRadio.setOnClickListener(DSLAM_TYPE_SELECTION_LISTENER);


        dslam = (EditText) getActivity().findViewById(R.id.edit_dslam);

        puertoMask = (EditText) getActivity().findViewById(R.id.edit_puerto_specificacion);
        portRegex = getActivity().getResources().getStringArray(R.array.ports_regex);

        this.setHelpListener();
    }

    private void setServicioDatosVisibilty(int visibility) {
        getActivity().findViewById(R.id.label_port_type).setVisibility(visibility);
        puerto.setVisibility(visibility);
        getActivity().findViewById(R.id.label_dslam_type).setVisibility(visibility);
        dslamIndoorRadio.setVisibility(visibility);
        dslamOutdoorRadio.setVisibility(visibility);
        getActivity().findViewById(R.id.label_dslam).setVisibility(visibility);
        dslam.setVisibility(visibility);
        getActivity().findViewById(R.id.label_port).setVisibility(visibility);
        puertoMask.setVisibility(visibility);

        /*getActivity().findViewById(R.id.container_dslam).setVisibility(visibility);
        dslam.setVisibility(visibility);
        getActivity().findViewById(R.id.label_puerto_especificacion).setVisibility(visibility);
        puerto.setVisibility(visibility);
        getActivity().findViewById(R.id.container_puerto).setVisibility(visibility);
        puertoMask.setVisibility( visibility );*/


    }

    @Override
    protected boolean validateInputs() {
        boolean validated = true;

        String input;

        validated &= Pattern.compile(Constants.REGEX_PHONE).matcher(phone.getText().toString()).matches();
        if (!validated) {

            toast(R.string.validation_phone);
            return validated;
        }

        // PAR FEEDER
        if (!validateParFeeder()) {
            return false;
        }

        /*input = TextUtils.isEmpty(parFeeder.getText().toString()) ? "" : parFeeder.getText().toString().trim().toUpperCase();

        validated &= Pattern.compile(Constants.REGEX_FEEDER).matcher(input).matches();
        if (!validated) {
            toast(R.string.validation_par_feeder);
            return validated;
        }*/

        if (servicioDatos.isChecked()) {

            // LOGIC FROM FRAGMENT DATA

            // PORT TYPE
            String default_message = getResources().getStringArray(R.array.fibra_puerto_labels)[0];
            input = TextUtils.isEmpty(puerto.getSelectedItem().toString()) ? "" : puerto.getSelectedItem().toString().trim().toUpperCase();
            if (default_message.equalsIgnoreCase(input)) {
                validated = false;
                toast(R.string.validation_port_type);
                return validated;
            }

            // DSLAM TYPE
            if (DSLAM_NONE == dslamType) {
                toast(R.string.validation_dslam_type);
                return false;
            }

            // DSLAM
            if (!validateDslam()) {
                return false;
            }

            // PORT
            input = TextUtils.isEmpty(puertoMask.getText().toString()) ? "" : puertoMask.getText().toString().trim().toUpperCase();
            // SE CAMBIO FORMA DE OBTENER TIPO PUERTO. Se obtiene ahora desde el componente de la vista
            // validated &= Pattern.compile(portRegex[port]).matcher( input ).matches();
            validated &= Pattern.compile(portRegex[puerto.getSelectedItemPosition()]).matcher(input).matches();
            validated &= isValidSuggestedPort(input);

            if (!validated) {
                toast(R.string.validation_port);
                return validated;
            }
        }

        if (isCapturingCoordinates()) {
            Toast.makeText(getActivity(), R.string.wait_captured_coordinate, Toast.LENGTH_LONG).show();
            return false;
        }

        return validated;
    }

    protected void setLocalidad(String localidad) {
        int selection;
        try {
            selection = Integer.valueOf(localidad);
        } catch (Exception e) {
            selection = 0;
        }
        this.localidad.setSelection(selection);
    }


    @Override
    protected void fillForm(FormValues formValues) {
        isRefreshing();

        // PHONE
        setPhone(phone, formValues.getPath(Constants.PHONE));

        // PAR FEEDER
        parFeeder.setText(formValues.getPath(Constants.PAR_FEEDER));
        // LOCALIDAD
        String localidadVal = formValues.getPath(Constants.LOCALIDAD);
        if (localidadVal != null) {
            int idx = getFrames().listNames().indexOf(localidadVal) + 1;
            if (idx != -1) {
                setLocalidad(idx + "");
            }
        }
        // SERVICIO DE DATOS
        servicioDatos.setChecked("SI".equals(formValues.getPath(Constants.SERVICIO_DATOS)));
        int visibility = View.GONE;
        if ("SI".equals(formValues.getPath(Constants.SERVICIO_DATOS))) {
            visibility = View.VISIBLE;

            // LOGIC FROM FRAGMENT DATA
            setTipoPuerto(formValues.getPath(Constants.TIPO_PUERTO));
            setDslamType(formValues.getPath(Constants.DSLAM_TYPE));
            dslam.setText(formValues.getPath(Constants.DSLAM));
            setPort(formValues.getPath(Constants.PUERTO));
        }
        setServicioDatosVisibilty(visibility);

    }

    private void isRefreshing() {

        isFillingLocalidad = true;

        // LOGIC FROM FRAGMENT DATA
        isFillingPuerto = true;

    }

    @Override
    public void onStop() {
        super.onStop();

        memento.addToHistory("phone", phone.getText().toString() + "");
        memento.addToHistory("localidad", localidad.getSelectedItemPosition() + "");

        // LOGIC FROM FRAGMENT DATA
        memento.addToHistory("dslam", dslam.getText().toString());
        memento.addToHistory("puerto", puerto.getSelectedItemPosition() + "");

        memento.saveMemento(getActivity());
    }


    @Override
    public void onResume() {
        super.onResume();
        memento.loadMemento(getActivity());
        // SET PHONE
        setPhone(phone, memento.getFromHistory("phone") + "");
        // LOCALIDAD
        setLocalidad(memento.getFromHistory("localidad") + "");

        // LOGIC FROM FRAGMENT DATA
        dslam.setText(memento.getFromHistory("dslam") + "");
        int selection;
        try {
            selection = Integer.parseInt(memento.getFromHistory("puerto") + "");
        } catch (Exception e) {
            selection = 0;
        }
        this.puerto.setSelection(selection);

    }

    @Override
    protected void onPostSent() {
        memento.removeMemento(getActivity());
    }

    protected void setHelpListener() {
        final View.OnClickListener LISTENER = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayHelp(view.getId());
            }
        };

        ImageButton icon = (ImageButton) getActivity().findViewById(R.id.help_parFeeder);
        icon.setOnClickListener(LISTENER);

        // LOGIG FROM FRAGMENT DATA
        final View.OnClickListener LISTENER_DATA = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (R.id.help_puerto == view.getId()) {
                    Log.e("****", "Displaying port help");
                    displayHelp(Constants.DEFAULT_HELP_PORT_OFFSET + port);
                } else
                    displayHelp(view.getId());
            }
        };
        icon = (ImageButton) getActivity().findViewById(R.id.help_puerto);
        icon.setOnClickListener(LISTENER_DATA);

        icon = (ImageButton) getActivity().findViewById(R.id.help_dslam);
        icon.setOnClickListener(LISTENER_DATA);
    }

    protected int getSelectedFrame() {
        if (null == localidad)
            return -1;

        int idx = localidad.getSelectedItemPosition() - 1; //Compact position to remove "Seleccione Prefix" index
        return idx;
    }

    // LOGIC FROM FRAGMENT PSTN
    private boolean validateParFeeder() {
        return validateParFeeder(true);
    }

    private boolean validateParFeeder(boolean showMessage) {
        boolean validated = true;

        String input = TextUtils.isEmpty(parFeeder.getText().toString()) ? "" : parFeeder.getText().toString().trim().toUpperCase();


        validated &= Pattern.compile(Constants.REGEX_FEEDER).matcher(input).matches();
        if (!validated || Pattern.compile(Constants.REGEX_PRE_HYPHEN_FEEDER).matcher(input).matches()) {

            validated = false;
            if (showMessage) {
                toast(R.string.validation_par_feeder, Toast.LENGTH_SHORT);
            }
            return validated;
        }

        return validated;
    }


    // LOGIC FROM FRAGMENT DATA

    protected boolean validateDslam() {

        boolean validated = true;
        String input = TextUtils.isEmpty(dslam.getText().toString()) ? "" : dslam.getText().toString().trim().toUpperCase();

        validated &= !TextUtils.isEmpty(input) && !input.contains(" ") && 1 < input.length() && !techs.contains(input.toLowerCase());
        validated &= Pattern.compile(Constants.REGEX_DSLAM).matcher(input).matches();

        if (!validated || input.matches(".*(DSLAM|COPC|PRUEBA).*")) {
            toast(R.string.validation_dslam);
            validated = false;
            return validated;
        }
        return validated;

    }

    protected void onFrameSelected(Frames frames, int frame) {
        setPortPrefix();
    }

    protected void setDslamType(String dslamTypeValue) {
        if (dslamTypeValue == null) {
            dslamType = DSLAM_NONE;
            dslamOutdoorRadio.setChecked(false);
            dslamIndoorRadio.setChecked(false);
            return;
        }

        if ("OUTDOOR".equalsIgnoreCase(dslamTypeValue)) {
            dslamType = DSLAM_OUTDOOR;
            dslamIndoorRadio.setChecked(false);
            dslamOutdoorRadio.setChecked(true);

        } else {
            dslamType = DSLAM_INDOOR;
            dslamOutdoorRadio.setChecked(false);
            dslamIndoorRadio.setChecked(true);
        }
    }

    protected final View.OnClickListener DSLAM_TYPE_SELECTION_LISTENER = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.radio_dslam_indoor) {
                dslamType = DSLAM_INDOOR;
            } else {
                dslamType = DSLAM_OUTDOOR;
            }
            setPortPrefix();
        }
    };

    protected String stringifyDSLAMType(int type) {
        return DSLAM_INDOOR == type ? "Indoor" : DSLAM_OUTDOOR == type ? "Outdoor" : "UNKNOWN";
    }

    /**
     * This method validate that suggested port were editted correctly and none suggested characterr
     * numbers appear in the resulted value.
     */
    private boolean isValidSuggestedPort(String value) {
        value = value.replaceAll("\\D", " ");
        value = value.trim();
        String[] numbers = value.split(" ");
        Log.e("***********", "Resulted array of numbers: " + Arrays.toString(numbers));
        if (null == numbers)
            return false;

        for (String number : numbers) {
            if (" ".equals(number))
                continue;
            if (number.startsWith("0"))
                return false;
        }
        return true;
    }

    protected void setPortPrefix() {
        if (null != puerto) {
            if (DSLAM_OUTDOOR == dslamType) {
                String port = puerto.getSelectedItem() + "";

                if (port.toLowerCase().contains("alcatel"))
                    dslam.setText("A" + getCabina());
                else if (port.toLowerCase().contains("zte"))
                    dslam.setText("T" + getCabina());

            } else if (DSLAM_INDOOR == dslamType) {
                if (null == puerto || null == dslam)
                    return;
                String port = puerto.getSelectedItem() + "";

                if (0 > getSelectedFrame())
                    return;

                String selectedPort = port.toLowerCase();
                if (selectedPort.toLowerCase().contains("zyxel")) //Same prefixes for Zyxel ADSL & Zyxel SHDSL
                    selectedPort = "zyxel";

                String portDslamPrefix = getFrames().getPortPrefix(getSelectedFrame(), selectedPort);

                dslam.setText(portDslamPrefix);
            }
        }
    }

    // NO EXISTE CABINA EN ESTE FORMULARIO ESTE METODO ES PARA MANTENER EL FUNCIONAMIENTO
    // IGUAL DE LOS CAMPOS COMUNES CON FORMULARIO DE PSTN+DATA
    protected String getCabina() {
        return "";
    }


    protected void setPort(String puerto) {
        if (puerto == null) {
            return;
        }


        puertoMask.setText(puerto);
    }

    protected void setTipoPuerto(String tipoPuerto) {
        if (tipoPuerto == null) {
            isFillingPuerto = false;
            return;
        }

        int selection = 0;
        try {
            for (int i = 0; i < portsType.length; i++) {
                if (portsType[i].equalsIgnoreCase(tipoPuerto)) {
                    selection = i;
                    break;
                }
            }
        } catch (Exception e) {
            selection = 0;
        }
        this.puerto.setSelection(selection);
    }


}
