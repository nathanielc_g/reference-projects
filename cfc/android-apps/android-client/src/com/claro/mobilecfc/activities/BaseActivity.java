package com.claro.mobilecfc.activities;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.db.contracts.FrameListContract;
import com.claro.mobilecfc.db.contracts.FramesContract;
import com.claro.mobilecfc.robospice.request.FramesRequest;
import com.claro.mobilecfc.robospice.request.VersionRequest;
import com.claro.mobilecfc.robospice.response.FrameListResponse;
import com.claro.mobilecfc.robospice.response.FramesResponse;
import com.claro.mobilecfc.robospice.response.VersionResponse;
import com.claro.mobilecfc.robospice.services.CfcService;
import com.claro.mobilecfc.utils.HttpUtils;
import com.claro.mobilecfc.utils.Utils;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.List;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 4/23/14.
 * <p/>
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad. Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad.Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 */
public class BaseActivity extends ActionBarActivity {

    private VersionResponse version;

    private SpiceManager spiceManager = new SpiceManager(CfcService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 2016-GESTI-5106: Bloqueo de aplicación CFC para fines de actualización.
        checkRemoteVersion();

        //checkOlderCFCVersion();
        //checkRemoteVersion();
        checkFrameListValidity();
        checkNetworkConnection();
    }

    @Override
    protected void onStart() {
        super.onStart();
        spiceManager.start(this);

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!InvalidApnActivity.isAPNConfigured) {
                    startActivity(new Intent(BaseActivity.this, InvalidApnActivity.class));
                    finish();
                }

            }
        }).start();
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }


    protected void checkRemoteVersion() {

        Log.e("BaseActivity", "checkRemoteVersion");

        if (!Utils.updaterInstalled(this)) {
            Log.e("checkRemoteVersion", "Updater not installed, bypass");
            return;
        }

        if (!HttpUtils.isNetworkAvailable(this)) {
            Log.e("checkRemoteVersion", "Not possible to check remote version, due on not connection.");
            return;
        }

        /*int lastKnowSetupVersion = Utils.getLastSetupVersion(this);
        if(lastKnowSetupVersion >= Utils.getVersionCode(this)){

        }*/

        VersionRequest request = new VersionRequest();
        String cacheKey = request.createCacheKey();

        spiceManager.execute(request, cacheKey, DurationInMillis.ALWAYS_EXPIRED, new VersionRequestListener());
    }


    protected void checkNetworkConnection() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!HttpUtils.isNetworkAvailable(BaseActivity.this)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(BaseActivity.this, R.string.not_network, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();
    }

    protected void checkOlderCFCVersion() {
        Log.e("BaseActivity", "checkRemoteVersion");

        if (Utils.olderCFCInstalled(this)) {
            Log.e("checkOlderCFCVersion", "Older CFC version  installed, uninstalling..");

            new AlertDialog.Builder(this)
                    .setTitle("Actualizacion de CFC")
                    .setMessage("Presionar Aceptar para desinstalar vieja version")
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).create().show();


            PackageManager pm = getPackageManager();
            try {
                ApplicationInfo info = pm.getApplicationInfo("com.claro.cfcmobile", 0);
                Intent intent = new Intent(Intent.ACTION_DELETE, Uri.fromParts("package", pm.getPackageArchiveInfo(info.sourceDir, 0).packageName, null));
                startActivity(intent);
            } catch (PackageManager.NameNotFoundException ex) {
                Toast.makeText(this, R.string.applist_app_not_installed, Toast.LENGTH_LONG).show();
            }
        }
        Log.e("checkOlderCFCVersion", "Older CFC version not  installed");
    }


    protected void checkFrameListValidity() {
        Log.e("BaseActivity", "checkFrameListValidity");

        if (!Utils.isAlowedToSyncFrames(this)) {
            Log.e("checkFrameListValidity", "Frames up-to-date");
            return;
        }

        if (!HttpUtils.isNetworkAvailable(this)) {
            Log.e("checkFrameListValidity", "Not possible to update frames, due on not connection.");
            return;
        }

//        FrameListRequest request = new FrameListRequest();
        FramesRequest request = new FramesRequest();
        String cacheKey = request.createCacheKey();

        spiceManager.execute(request, cacheKey, DurationInMillis.ALWAYS_EXPIRED, new FramesRequestListener());
    }


    public class VersionRequestListener implements RequestListener<VersionResponse> {

        @Override
        public void onRequestFailure(SpiceException e) {
        }

        @Override
        public void onRequestSuccess(VersionResponse version) {
            if (null == version) {
                Log.e("VersionRequestListener", "Got null version");
                return;
            }

            int remoteVersion = 0;
            try {
                remoteVersion = Double.valueOf(version.getVersion()).intValue();
            } catch (Exception ex) {
                //Ignored
            }

            Log.e("BaseActivity", "remoteVersion = " + remoteVersion + ", localVersion= " + Utils.getVersionCode(BaseActivity.this));

            if (remoteVersion == Utils.getVersionCode(BaseActivity.this)) {
                Log.e("VersionRequestListener", "Server reported same version");
                return;
            }

            startActivity(new Intent(BaseActivity.this, OldVersionActivity.class));
            finish();

            /*Toast.makeText(BaseActivity.this, R.string.new_app_version, Toast.LENGTH_LONG).show();

            BaseActivity.this.version = version;
            Intent updater = new Intent("android.intent.action.VIEW");
            updater.addCategory("android.intent.category.BROWSABLE");
            updater.setData(Uri.parse(version.getUpdateLink()));

            Log.e("BaseActivity", "Got version: " + version);
            startActivity(updater);*/
//            startActivityForResult(updater, UPDATER_REQUEST_CODE);
        }

    }

    /**
     * @Deprecated use instead FramesRequestListener
     */
    @Deprecated
    public class FrameListRequestListener implements RequestListener<FrameListResponse> {

        @Override
        public void onRequestFailure(SpiceException e) {
        }

        @Override
        public void onRequestSuccess(FrameListResponse frameList) {
            if (null == frameList || TextUtils.isEmpty(frameList.toString())) {
                Log.e("FrameListRequestListene", "Got null frame list");
                return;
            }

            List<String> frames = frameList.getFrames();

            ContentValues[] values = new ContentValues[frameList.getFrames().size()];
            for (int i = 0; i < frames.size(); ++i) {
                values[i] = new ContentValues();
                values[i].put(FrameListContract.NAME, frames.get(i));
            }

            ContentResolver resolver = getContentResolver();
            resolver.delete(FrameListContract.CONTENT_URI, null, null);
            resolver.bulkInsert(FrameListContract.CONTENT_URI, values);

            Utils.updateSyncDateFrame(BaseActivity.this);
        }
    }

    public class FramesRequestListener implements RequestListener<FramesResponse> {

        @Override
        public void onRequestFailure(SpiceException e) {
        }

        @Override
        public void onRequestSuccess(FramesResponse frames) {
            if (null == frames || TextUtils.isEmpty(frames.toString())) {
                Log.e("FramesRequestListener", "Got null frames");
                return;
            }

            byte[] data = frames.toBytes();

            ContentValues values = new ContentValues();
            values.put(FramesContract.DATA, data);

            ContentResolver resolver = getContentResolver();
            resolver.delete(FramesContract.CONTENT_URI, null, null);
            resolver.insert(FramesContract.CONTENT_URI, values);

            if( null != data && 0<data.length)
                Utils.updateSyncDateFrame(BaseActivity.this);
        }
    }
}