package com.claro.mobilecfc.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.claro.mobilecfc.R;

/**
 * Created by Christopher Herrera on 2/20/14.
 *
 * @Refactored completely by Jansel R. Abreu (Vanwolf),
 */
public class HomeActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e("********", "OnNewIntent");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e( "***","onStart" );
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e( "***","onStart" );
    }
}