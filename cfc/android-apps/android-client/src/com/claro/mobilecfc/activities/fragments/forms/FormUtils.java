package com.claro.mobilecfc.activities.fragments.forms;

import android.content.Context;
import android.database.Cursor;
import com.claro.mobilecfc.db.contracts.DispatchItemsContract;

/**
 * Created by Jansel Valentin on 3/11/2016.
 **/
public final class FormUtils {
    public static String getOrderDetailKey(Context context, String codeOrder, String key) {
        String inClause = "( '" + key + "' )";

        StringBuilder sb = new StringBuilder();
        sb.append("TRIM(" + DispatchItemsContract.VALUE + ") <> '' ");
        sb.append(" AND TRIM(" + DispatchItemsContract.VALUE + ") <> '-' ");
        sb.append(" AND " + DispatchItemsContract.VALUE + " IS NOT NULL ");
        sb.append(" AND " + DispatchItemsContract.CODE + " = " + codeOrder);
        sb.append(" AND TRIM(" + DispatchItemsContract.KEY + ") IN ");
        sb.append(inClause);

        String selection = sb.toString();
        Cursor c = context.getContentResolver().query(DispatchItemsContract.CONTENT_URI, null, selection, null, null);
        try {
            if (c.moveToFirst()) {
                String value = "";
                String lKey = c.getString(c.getColumnIndexOrThrow(DispatchItemsContract.KEY));
                if (null != key && key.equals(lKey))
                    value = c.getString(c.getColumnIndexOrThrow(DispatchItemsContract.VALUE));
                return value;
            }
        } finally {
            if (null != c)
                c.close();
        }
        return null;
    }
}
