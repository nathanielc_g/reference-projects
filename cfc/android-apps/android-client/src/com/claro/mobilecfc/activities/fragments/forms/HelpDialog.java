package com.claro.mobilecfc.activities.fragments.forms;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.utils.Constants;
import com.claro.mobilecfc.widget.TextView;

import java.util.HashMap;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 11/18/2014.
 */
public class HelpDialog extends DialogFragment{

    public static final String EXTRA_KIND = "extra.kind";

    private static HashMap<Integer,String> helps;
    static {
        fillHelps();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().setTitle("Ejemplo");
        View view = inflater.inflate( R.layout.fragment_help_dialog,container,false);
        TextView content = (TextView) view.findViewById( R.id.help_dialog_content);

        int kind = getArguments().getInt(EXTRA_KIND,-1);
        String help = "not found for kind "+kind;
        if( helps.containsKey(kind) )
            help = helps.get(kind);

        help+="\n";
        content.setText(help);
        return view;
    }


    private static void fillHelps(){
        if( null == helps )
            helps = new HashMap<Integer, String>();

        helps.put(R.id.help_parFeeder,"Par Feeder\n\nEj.: P16-0088, C04-0012");
        helps.put(R.id.help_cabina,"Cabina\n\nEj.: CVM6D, CBH7");
        helps.put(R.id.help_parLocal,"Par Local\n\nEj.: CVM6D-0169, CBH7-0096");
        helps.put(R.id.help_terminal,"Terminal\n\nEj.: CVM6D-4N, CBH7-T, \n\t\tP16-4A(Cuenta Fija)");
        helps.put(R.id.help_dslam,
                "Puertos\n\nAlcatel Ej.: A27FEBRERO8, ACA10A1\n" +
                "Cisco   Ej.: BARAHONA2, MONTECRISTI2\n" +
                "Stinger Ej.: SBOCACHICA3, SSANFRANCISCO4\n" +
                "Zixel   Ej.: ZAZUA10, ZCONSTANZA8\n" +
                "ZTE     Ej.: TCA2E1, TCVM6D2");
        helps.put(R.id.help_terminalFo,"Terminal FO\n\nEj.: FCGAC3C-A1, FCBVA1A-K30");
        helps.put(R.id.help_splitterPort,"Splitter Port\n\nEj.: SPT-FCGAC3C-A1-SP05,\n\t\tSPT-FCBVA1A-K30-SP01");

        helps.put(Constants.DEFAULT_HELP_PORT_OFFSET,"Alcatel\n\nEj. 1-1-1-1, \n\t1-1-1-11, \n\t1-1-11-1, \n\t1-1-11-11"); //Alcatel
        helps.put(Constants.DEFAULT_HELP_PORT_OFFSET+1,"CISCO\n\nEj. TARJ 1-PTO 1, TARJ 11-PTO 1 "); //CISCO
        helps.put(Constants.DEFAULT_HELP_PORT_OFFSET+2,"Stinger\n\nEj. SHELF 1-TARJ 1-PTO 1, \n\tSHELF 1-TARJ 1-PTO 11, \n\tSHELF 1-TARJ 11-PTO 1, \n\tSHELF 1-TARJ 11-PTO 11");//Stinger
        helps.put(Constants.DEFAULT_HELP_PORT_OFFSET+3,"ZTE\n\nEj. 1-1, 1-11");//ZTE
        helps.put(Constants.DEFAULT_HELP_PORT_OFFSET+4,"Zyxel ADSL\n\nEj. SLOT 1-PUERTO-1 ADSL, \n\tSLOT 1-PUERTO-11 ADSL, \n\tSLOT 11-PUERTO-1 ADSL, \n\tSLOT 11-PUERTO-11 ADSL");//Zyxel ADSL
        helps.put(Constants.DEFAULT_HELP_PORT_OFFSET+5,"Zyxel SHDSLn\n\nEj. SLOT 1-SHDSL PORT 1, \n\tSLOT 1-SHDSL PORT 11");//Zyxel SHDSL
    }
}
