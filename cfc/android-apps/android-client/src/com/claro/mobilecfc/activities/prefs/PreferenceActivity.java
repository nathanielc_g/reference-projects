package com.claro.mobilecfc.activities.prefs;

import android.os.Bundle;
import android.util.Log;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.activities.fragments.prefs.BasePreferenceFragment;
import com.claro.mobilecfc.activities.fragments.prefs.SyncFragment;

import java.net.URLClassLoader;
import java.util.List;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 4/25/14.
 */
public class PreferenceActivity extends android.preference.PreferenceActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setTitle( "Configuracion" );
    }

    @Override
    public void onBuildHeaders(List<Header> target) {
        super.onBuildHeaders(target);
        loadHeadersFromResource(R.xml.main_prefs,target);
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        // Valid fragments must be subclass of BasePreferenceFragment
        Log.e("isValidFragment", "****** Validating fragment: " + fragmentName + "*************");
        boolean valid = false;
        try {
            valid = BasePreferenceFragment.class.isAssignableFrom(Class.forName(fragmentName));
        } catch (ClassNotFoundException ex) {
            Log.e("isValidFragment", "*********** Invalid fragment: **********" + fragmentName);
            valid = false;
        }
        return valid;
        //return true;
        //return SyncFragment.class.getSimpleName().equals(fragmentName);
    }
}
