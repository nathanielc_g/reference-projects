package com.claro.mobilecfc.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.activities.prefs.UserKnowledge;
import com.claro.mobilecfc.robospice.request.VersionRequest;
import com.claro.mobilecfc.robospice.response.VersionResponse;
import com.claro.mobilecfc.robospice.services.CfcService;
import com.claro.mobilecfc.utils.HttpUtils;
import com.claro.mobilecfc.utils.Utils;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by Nathaniel Calderon on 11/3/2016.
 */
public class OldVersionActivity extends Activity {

    private VersionResponse version;

    private SpiceManager spiceManager = new SpiceManager(CfcService.class);

    private TextView warnText;
    private Button updateApp;

    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_old_version);

        warnText = (com.claro.mobilecfc.widget.TextView) findViewById(R.id.tv_warn_message);
        updateApp = (Button) findViewById(R.id.update_app);
        updateApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateApp.setEnabled(false);
                updateApp.setVisibility(View.GONE);
                updateApp();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        spiceManager.start(this);
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    @Override
    protected void onPause() {
        //closeLoadingDialog();
        super.onPause();
    }

    private void updateApp () {

        Log.e("BaseActivity", "checkRemoteVersion");

        if (!Utils.updaterInstalled(this)) {
            Log.e("checkRemoteVersion", "Updater not installed, bypass");
            return;
        }

        if (!HttpUtils.isNetworkAvailable(this)) {
            Log.e("checkRemoteVersion", "Not possible to check remote version, due on not connection.");
            return;
        }

        VersionRequest request = new VersionRequest();
        String cacheKey = request.createCacheKey();

        spiceManager.execute(request, cacheKey, DurationInMillis.ALWAYS_EXPIRED, new VersionRequestListener());
    }

    public class VersionRequestListener implements RequestListener<VersionResponse> {

        @Override
        public void onRequestFailure(SpiceException e) {
        }

        @Override
        public void onRequestSuccess(VersionResponse version) {
            //updateApp.setEnabled(true);
            if (null == version) {
                Log.e("VersionRequestListener", "Got null version");
                return;
            }

            int remoteVersion = 0;
            try {
                remoteVersion = Double.valueOf(version.getVersion()).intValue();
            } catch (Exception ex) {
                //Ignored
            }

            Log.e("BaseActivity", "remoteVersion = " + remoteVersion + ", localVersion= " + Utils.getVersionCode(OldVersionActivity.this));

            if (remoteVersion == Utils.getVersionCode(OldVersionActivity.this)) {
                Log.e("VersionRequestListener", "Server reported same version");
                return;
            }



            Toast.makeText(OldVersionActivity.this, R.string.new_app_version, Toast.LENGTH_LONG).show();

            OldVersionActivity.this.version = version;
            Intent updater = new Intent("android.intent.action.VIEW");
            updater.addCategory("android.intent.category.BROWSABLE");
            updater.setData(Uri.parse(version.getUpdateLink()));

            Log.e("BaseActivity", "Got version: " + version);
            startActivity(updater);
//            startActivityForResult(updater, UPDATER_REQUEST_CODE);
        }

    }

}
