package com.claro.mobilecfc.activities.fragments.forms;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.dto.Attribute;
import com.claro.mobilecfc.dto.FormValues;
import com.claro.mobilecfc.dto.Frames;
import com.claro.mobilecfc.utils.Constants;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 5/15/14.
 * <p/>
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad. Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 * Este sistema y sus recursos son propiedad de CLARO y es para el uso exclusivo del Personal
 * autorizado por esta entidad.Su uso indebido puede conllevar acciones disciplinarias y/o
 * legales.Toda actividad realizada en este sistema está siendo registrada y monitoreada.
 */


public class PSTNFibraFormFragment extends PSTNFormFragment {
    private static final String FORM_TYPE = "PSTN+FIBRA";

    private EditText terminalFo;
    private EditText splitterPort;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pstn_fibra_form, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    protected String getFormType() {
        return FORM_TYPE;
    }

    @Override
    protected List<Attribute> getAttributes() {
        List<Attribute> attributes = super.getAttributes();

        attributes.add(Attribute.create(Constants.TERMINAL_FO, terminalFo.getText().toString().trim().toUpperCase()));
        attributes.add(Attribute.create(Constants.CABINA_FO, getCabina(terminalFo.getText().toString().trim().toUpperCase())));
        attributes.add(Attribute.create(Constants.SPLITTER_PORT, splitterPort.getText().toString().trim().toUpperCase()) );
        attributes.add(Attribute.create(Constants.SPLITTER, "SPT-"+terminalFo.getText().toString().trim().toUpperCase() ));

        return attributes;
    }

    @Override
    protected void enabledComponents(boolean enabled) {
    }

    protected void init() {
        super.init();
        initComponents();
    }

    private String getCabina( String terminal ){
        if( null == terminal )
            return  "";
        String cabina = terminal.substring(0, terminal.indexOf('-'));
        return cabina;
    }

    @Override
    protected void onFrameSelected(Frames frames, int frame) {
        String terminalFoPrefix = frames.getTerminalFoPrefix(frame);
        setTerminalFo("FC" + (TextUtils.isEmpty(terminalFoPrefix) ? "" : terminalFoPrefix));
    }



    private void initComponents() {

        /*localidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                terminalFo.setText(getTerminalFoPrefix("") + "-");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
*/
        terminalFo = (EditText) getActivity().findViewById(R.id.edit_terminal_fo);

        splitterPort = (EditText) getActivity().findViewById( R.id.edit_splitter_port );
        terminalFo.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence sequence, int i, int i2, int i3) {
            }
            @Override
            public void onTextChanged(CharSequence sequence, int i, int i2, int i3) {
                if( null != terminalFo ){

                    splitterPort.setText( "SPT-"+terminalFo.getText()+"-SP" );
                    //splitterPort.setText( "SPT-"+terminalFo.getText());
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        terminalFo.setText("FC");
    }


    @Override
    protected boolean validateInputs() {
        boolean validated = super.validateInputs();

        String input = TextUtils.isEmpty(terminalFo.getText().toString()) ? "" : terminalFo.getText().toString().trim().toUpperCase();

        validated &= Pattern.compile(Constants.REGEX_TERMINAL_FO).matcher( input ).matches();
        if( !validated ){
            toast( R.string.validation_terminal_fo );
            return validated;
        }

        input = TextUtils.isEmpty(splitterPort.getText().toString()) ? "" : splitterPort.getText().toString().trim().toUpperCase();

        validated &= Pattern.compile("SPT-" + terminalFo.getText().toString().trim().toUpperCase() + "-SP0[1-8]").matcher( input ).matches();
        if( !validated ){
            toast( R.string.validation_splitter_port );
            return validated;
        }

        if(isCapturingCoordinates()) {
            Toast.makeText(getActivity(), R.string.wait_captured_coordinate, Toast.LENGTH_LONG).show();
            return false;
        }
        return validated;
    }

    @Override
    protected void fillForm(FormValues formValues) {
        super.fillForm(formValues);
        setTerminalFo(formValues.getPath(Constants.TERMINAL_FO));
        setSplitterPort(formValues.getPath(Constants.SPLITTER_PORT));

    }

    @Override
    public void onStop() {
        super.onStop();
        memento.addToHistory( "terminalFo",terminalFo.getText().toString() );
        memento.addToHistory("splitterPort", splitterPort.getText().toString());
        memento.saveMemento(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        /**
         * loaded by super
         */
//        memento.loadMemento( getActivity() );
        setTerminalFo(memento.getFromHistory("terminalFo"));
        setSplitterPort(memento.getFromHistory( "splitterPort" ));
    }

    protected void setTerminalFo (Object terminalFo) {
        if( null != terminalFo && !TextUtils.isEmpty(terminalFo + ""))
            this.terminalFo.setText( terminalFo +"" );
    }

    protected void setSplitterPort (Object splitterPort) {
        if( null != splitterPort && !TextUtils.isEmpty(splitterPort+"" ))
            this.splitterPort.setText( splitterPort +"" );

    }




    protected void setHelpListener() {
        super.setHelpListener();

        final View.OnClickListener LISTENER = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayHelp(view.getId());
            }
        };

        ImageButton icon = (ImageButton) getActivity().findViewById(R.id.help_terminalFo);
        icon.setOnClickListener(LISTENER);

        icon = (ImageButton) getActivity().findViewById(R.id.help_splitterPort);
        icon.setOnClickListener(LISTENER);
    }
}
