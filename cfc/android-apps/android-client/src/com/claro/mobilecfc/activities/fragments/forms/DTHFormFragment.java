package com.claro.mobilecfc.activities.fragments.forms;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.*;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.claro.mobilecfc.R;
import com.claro.mobilecfc.activities.fragments.forms.prefs.Memento;
import com.claro.mobilecfc.dto.Attribute;
import com.claro.mobilecfc.dto.FormValues;
import com.claro.mobilecfc.utils.Constants;
import com.claro.mobilecfc.utils.HttpUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Jansel R. Abreu on 7/8/2015.
 */
public class DTHFormFragment extends BaseFormFragment {
    private static final String FORM_TYPE = "DTH";

    private static final int LOADING = 0;
    private static final int COORDINATES_REQUITED = 1;
    private static final int COORDINATES_NOT_REQUIRED = 2;

    private static final int RESCHEDULE_FETCH = 1 << 2;

    private ViewFlipper flipper;

    private ViewGroup emptyFormContent;
    private ViewGroup feedbackContent;

    private Handler locHandler;
    private LocationManager lmanager;
    private Location location;

    private LocationListener listener;


    private Button sendFormButton;
    private Button captureCoordinatesButton;
    //private Button cancelLookupButton;

    private TextView bypassCoordinatesText;
    private ImageView bypassCoordinatesImage;

    //private boolean lookupCanceled = false;
    private boolean retried = false;

    private Memento memento;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        memento = new Memento(this.getClass().getSimpleName() + ":" + orderNumber);
        setSupportCaptureCoordinate(false); // we want to handle coordinate by ourselves
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dth_form, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    @Override
    protected String getFormType() {
        return FORM_TYPE;
    }

    @Override
    protected List<Attribute> getAttributes() {
        List<Attribute> attrs = new ArrayList<>();

        // boolean valid = !lookupCanceled;
        String lng = "",
                lat = "";

        if (null != location) {
            lat = Location.convert(location.getLatitude(), Location.FORMAT_DEGREES);
            lng = Location.convert(location.getLongitude(), Location.FORMAT_DEGREES);
        } else {
            lat = latitude;
            lng = longitude;
        }

        attrs.add(Attribute.create(Constants.COORDVALIDS, validCoordinate));
        attrs.add(Attribute.create(Constants.ID_DIRECCION, addressId));
        attrs.add(Attribute.create(Constants.LATITUD, lat));
        attrs.add(Attribute.create(Constants.LONGITUD, lng));
        attrs.add(Attribute.create(Constants.COORDATTEMPTS, validCoordinate.equalsIgnoreCase("true")? "true":String.valueOf(coordinateCaptureAttempt)));

        return attrs;
    }


    @Override
    protected void enabledComponents(boolean enabled) {
//        captureCoordinatesButton.setAlpha(enabled ? 1 : .5f);
        captureCoordinatesButton.setClickable(enabled);
        captureCoordinatesButton.setVisibility(enabled ? View.VISIBLE : View.GONE);

        /*cancelLookupButton.setClickable(!enabled);
        cancelLookupButton.setVisibility(!enabled ? View.VISIBLE : View.GONE);*/

        sendFormButton.setAlpha(enabled ? 1 : .5f);
        sendFormButton.setClickable(enabled);

        if(null != itemRefreshForm && null != itemSend){
            itemRefreshForm.setEnabled(enabled);
            itemSend.setEnabled(enabled);
        }
    }


    @Override
    protected boolean validateInputs() {
        if (null == location && !"true".equalsIgnoreCase(validCoordinate) && !coordinateCaptureAttempt
                //!lookupCanceled
                ) {
            Toast.makeText(getActivity(), "Por favor capturar las coordenadas del cliente.", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    @Override
    protected void onPostSent() {
        mementoOn = false;
        memento.removeMemento(getActivity());
    }

    @Override
    protected void fillForm(FormValues formValues) {
        // 2015-GESTI-3083-2
        coordinateCaptureAttempt = !TextUtils.isEmpty(formValues.getPath(Constants.COORDATTEMPTS, coordinateCaptureAttempt+"")) ? Boolean.parseBoolean(formValues.getPath(Constants.COORDATTEMPTS)): false;
        validCoordinate = formValues.getPath(Constants.COORDVALIDS, validCoordinate);
        addressId = formValues.getPath(Constants.ID_DIRECCION, addressId);
        latitude = formValues.getPath(Constants.LATITUD, latitude);
        longitude = formValues.getPath(Constants.LONGITUD, longitude);
        showViewOnwMode(Boolean.parseBoolean(validCoordinate), coordinateCaptureAttempt);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("***********", "DTHFormFragment.onResume");

        if (null != memento) {
            memento.loadMemento(getActivity());

            coordinateCaptureAttempt = Boolean.parseBoolean(memento.getFromHistory(Constants.COORDATTEMPTS).toString());
            validCoordinate = memento.getFromHistory(Constants.COORDVALIDS).toString();
            addressId = memento.getFromHistory(Constants.ID_DIRECCION).toString();

            /*if (!TextUtils.isEmpty(memento.getFromHistory("lat").toString()) &&
                    !TextUtils.isEmpty(memento.getFromHistory("lng").toString())) {*/
            TextView address = (TextView) getActivity().findViewById(R.id.coordinates_address);
            if(validCoordinate.equalsIgnoreCase("true")){
                latitude = memento.getFromHistory("lat").toString();
                longitude = memento.getFromHistory("lng").toString();
                String location = getResources().getString(R.string.coordinates_successfully_found_without_address);
                if(HttpUtils.isNetworkAvailable(getActivity()))
                    location = getLocationAddress(latitude, longitude);

                if(!isCapturingCoordinates())
                    showLocationFound(address, location, latitude, longitude);

            } else if(coordinateCaptureAttempt) {
                if(!isCapturingCoordinates())
                    showNotLocationFound(true, address);
            }

        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mementoOn && null != memento) {
            Log.e("******", "Saving memento for DTH form");
            if (null != location) {
                memento.addToHistory("lat", location.getLatitude());
                memento.addToHistory("lng", location.getLongitude());
            } else if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude)) {
                memento.addToHistory("lat", latitude);
                memento.addToHistory("lng", longitude);
            }

            memento.addToHistory(Constants.COORDVALIDS, validCoordinate);
            memento.addToHistory(Constants.ID_DIRECCION, addressId);
            memento.addToHistory(Constants.COORDATTEMPTS, String.valueOf(coordinateCaptureAttempt));

            memento.saveMemento(getActivity());
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelCoordinateLookup();
    }

    private void init() {
        lmanager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
//        validCoordinates    = FormUtils.getOrderDetailKey(getActivity(), orderNumber, Constants.COORDVALIDS);
//        knownLatitude       = FormUtils.getOrderDetailKey(getActivity(), orderNumber, Constants.LATITUD);
//        knownLongitude      = FormUtils.getOrderDetailKey(getActivity(), orderNumber, Constants.LONGITUD);
//        addressId           = FormUtils.getOrderDetailKey(getActivity(), orderNumber, Constants.ID_DIRECCION);


        Log.e("*******", "Getting COORDVALIDS:" + validCoordinate + " for order: " + orderNumber);

        emptyFormContent = (ViewGroup) getActivity().findViewById(R.id.emtpy_form_content);
        feedbackContent = (ViewGroup) getActivity().findViewById(R.id.feedback_content);
        final TextView feedbackText = (TextView) feedbackContent.findViewById(R.id.feedback_loading_text);
        feedbackText.setText(R.string.wait_take_long);

        bypassCoordinatesImage = (ImageView) getActivity().findViewById(R.id.ic_warning_bypass_coordinates);

        bypassCoordinatesText = (TextView) getActivity().findViewById(R.id.by_pass_coordinates_text);

        captureCoordinatesButton = (Button) getActivity().findViewById(R.id.capture_coordinates);
        captureCoordinatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestGpsLocation();
            }
        });

        /*cancelLookupButton = (Button) getActivity().findViewById(R.id.cancel_lookup);
        cancelLookupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (locHandler != null) {
                    locHandler.removeCallbacksAndMessages(null);
                }

                //lookupCanceled = true;
                if (null != listener && null != lmanager)
                    lmanager.removeUpdates(listener);

                latitude = longitude = "";

                // 2016-GESTI-5929,2016-GESTI-5971
                enabledComponents(true);
                if (null != feedbackContent)
                    feedbackContent.setVisibility(View.GONE);
                *//*
                sendFormButton.setAlpha(1);
                sendFormButton.setClickable(true);
                bypassCoordinatesImage.setVisibility(View.VISIBLE);
                bypassCoordinatesText.setText(getActivity().getText(R.string.coordinates_lookup_cancelled));
                flipToChild(COORDINATES_NOT_REQUIRED);
                *//*
            }
        });*/

        sendFormButton = (Button) getActivity().findViewById(R.id.button_send_form);
        sendFormButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendForm();
            }
        });

        showViewOnwMode(Boolean.parseBoolean(validCoordinate), coordinateCaptureAttempt);
    }

    private void showViewOnwMode (boolean validCoordinate, boolean coordinateCaptureAttempt){

        TextView address = (TextView) getActivity().findViewById(R.id.coordinates_address);
        if(validCoordinate) {
            //flipToChild(COORDINATES_NOT_REQUIRED);
            String location = getResources().getString(R.string.coordinates_successfully_found_without_address);
            if(HttpUtils.isNetworkAvailable(getActivity()))
                location = getLocationAddress(latitude, longitude);

            showLocationFound(address, location, latitude, longitude);
            return;
        }

        if(coordinateCaptureAttempt) {
            showNotLocationFound(coordinateCaptureAttempt, address);
            return;
        }

        if (lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            enabledComponents(true);
            flipToChild(COORDINATES_REQUITED);
        } else {
            if (null != feedbackContent) {
                feedbackContent.setVisibility(View.GONE);
            }
            //enabledComponents(false);
            flipToChild(LOADING);
            buildGPSUnavailabilityDialog();
        }
    }

    private void buildGPSUnavailabilityDialog() {
        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle("Active GPS")
                .setMessage("Por favor active el GPS")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getActivity().finish();
                    }
                }).create();
        dialog.show();
    }

    @Override()
    protected void cancelCoordinateLookup() {
        capturingCoordinates = false;
        if (null != listener && null != lmanager) {
            if (locHandler != null) {
                locHandler.removeCallbacksAndMessages(null);
            }

            lmanager.removeUpdates(listener);
        }
    }


    private void requestGpsLocation() {
        final TextView addressTxt = (TextView) getActivity().findViewById(R.id.coordinates_address);
        addressTxt.setText("");
        if (!lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(getActivity(), "Por favor active el GPS", Toast.LENGTH_LONG).show();
            return;
        }

        feedbackContent.setVisibility(View.VISIBLE);
        enabledComponents(false);

        final CountDownLatch latch = new CountDownLatch(1);

        new Thread() {
            @Override
            public void run() {
                Looper.prepare();

                if (null == locHandler)
                    locHandler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            if (RESCHEDULE_FETCH == msg.what) {
                                if (lmanager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                    /*lmanager.requestSingleUpdate(LocationManager.GPS_PROVIDER, getLocationListener(), Looper.myLooper());*/
//                                    currentBestLocation = null;
                                    location = null;
                                    lmanager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MAX_DISTANCE, getLocationListener(), Looper.myLooper());
                                    capturingCoordinates = true;

                                } else {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(), "GPS Provider is not enabled.!", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }


                            }
                        }
                    };

                locHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (null != listener && null != lmanager) {
                            lmanager.removeUpdates(listener);
                            capturingCoordinates = false;
                                /*if(isBestLocation(currentBestLocation)){
                                    setLocation(currentBestLocation);
                                    return;
                                }*/

                            final TextView address = (TextView) getActivity().findViewById(R.id.coordinates_address);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showNotLocationFound(retried, address);
                                    retried = true;
                                }
                            });


                        }

                    }
                }, MAX_TIME);
                latch.countDown();
                Looper.loop();
            }
        }.start();

        try {
            latch.await();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        locHandler.sendEmptyMessage(RESCHEDULE_FETCH);
    }

    private void showNotLocationFound (boolean retried, TextView address) {
        address.setText(Html.fromHtml("<strong>" + getActivity().getText(R.string.timeout_capturing_coordinate) + "</strong>"));
        address.append("\n\n");
        enabledComponents(true);
        if (null != feedbackContent)
            feedbackContent.setVisibility(View.GONE);

        if(retried) {
            coordinateCaptureAttempt = true;
            sendFormButton.setAlpha(1);
            sendFormButton.setClickable(true);
            bypassCoordinatesImage.setVisibility(View.VISIBLE);
            bypassCoordinatesText.setText(getActivity().getText(R.string.coordinates_lookup_cancelled));
            flipToChild(COORDINATES_NOT_REQUIRED);
        }
    }


    private LocationListener getLocationListener() {
        if (null != listener)
            return listener;

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(final Location loc) {
                location = loc;

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Accurancy:  " + loc.getAccuracy(), Toast.LENGTH_LONG).show();
                    }
                });

                if (isBestLocation(location)) {

                    setLocation(location);
                    lmanager.removeUpdates(listener);
                    locHandler.removeCallbacksAndMessages(null);
                    capturingCoordinates = false;
                }

//                locHandler.sendEmptyMessage( RESCHEDULE_FETCH );
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }

            @Override
            public void onProviderDisabled(String s) {
            }
        };
        return listener;
    }

    private void setLocation(final Location loc) {
        location = loc;
        latitude = loc.getLatitude() + "";
        longitude = loc.getLongitude() + "";
        validCoordinate = "true";

        final String location = getLocationAddress(loc);
        final TextView address = (TextView) getActivity().findViewById(R.id.coordinates_address);
        if (address == null)
            return;

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showLocationFound(address, location, latitude, longitude);
            }
        });
    }



    private void showLocationFound (TextView address, String location, String latitude, String longitude) {
        flipToChild(COORDINATES_REQUITED);
        address.setText(Html.fromHtml("<strong>" + getResources().getString(R.string.coordinates_successfully_found) + "</strong>"));
        //address.setText(Html.fromHtml("<strong>Direccion encontrada, si esta direccion no es valida por favor intente nuevamente.</strong>"));
        address.append("\n\n");
        address.append(location);
        address.append("\n\n");
        address.append("Latitude: " + latitude);
        address.append("\n\n");
        address.append("Longitude: " + longitude);
        enabledComponents(true);
        if (null != feedbackContent)
            feedbackContent.setVisibility(View.GONE);
    }

    private void setLocationPrueba(final Location loc) {
        final String location = getLocationAddress(loc);
        final TextView address = (TextView) getActivity().findViewById(R.id.coordinates_address);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                address.append("\n");
                address.append(Html.fromHtml("<strong> Provider: " + loc.getProvider() + " , Accurancy: " + loc.getAccuracy() + " , Time: " + loc.getTime() + "</strong>"));
                address.append("\n");
                address.append(location);
                /*enabledComponents(true);
                if (null != feedbackContent)
                    feedbackContent.setVisibility(View.GONE);*/
            }
        });
    }

//    private String getLocationVerbose(Location loc) {
//        if (null == loc)
//            return "";
//
//        try {
//            String formattedPath = HttpUtils.PATH_LOCATION_ADDRESS;
//            formattedPath = formattedPath.replace("latlng", "" + loc.getLatitude() + "," + loc.getLongitude());
//
//            String urlRequest = HttpUtils.SERVER_URL + formattedPath;
//            Log.e("********", "Getting Address from endpoint " + urlRequest);
//            String response = HttpRestRequester.makeHttpGetRequest(urlRequest);
//
//            Log.e("DTHForm", "DTHForm.lcoation response " + response);
//            if (null != response) {
//                JsonObject jsonRespose = new Gson().fromJson(response, JsonObject.class);
//                JsonElement ele;
//                if (null != (ele = jsonRespose.get("address"))) {
//                    return ele.getAsString();
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//        return "Error obteniendo direccion";
//    }


    protected void flipToChild(int child) {
        if (flipper == null) {
            flipper = (ViewFlipper) getActivity().findViewById(R.id.feedback_flipper);
            flipper.setInAnimation(getActivity(), android.R.anim.fade_in);
            flipper.setOutAnimation(getActivity(), android.R.anim.fade_out);
        }

        int childLayout = child;
        if(child>1)
            childLayout-=1;
        flipper.setDisplayedChild(childLayout);

        boolean isEmptyFormVisible = child > 1;
        if(null != emptyFormContent)
            emptyFormContent.setVisibility(isEmptyFormVisible? View.VISIBLE: View.GONE);
    }


}
