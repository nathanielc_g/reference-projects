package com.claro.mobilecfc.robospice.request;

import android.content.Context;
import android.util.Log;
import com.claro.mobilecfc.activities.prefs.UserKnowledge;
import com.claro.mobilecfc.robospice.response.OKResponse;
import com.claro.mobilecfc.utils.HttpRestRequester;
import com.claro.mobilecfc.utils.HttpUtils;
import com.claro.mobilecfc.utils.Utils;
import com.google.gson.Gson;
import com.octo.android.robospice.request.SpiceRequest;

/**
 * Created by Alvaro De la Cruz on 6/15/2016.
 */
public class SetupRequest extends SpiceRequest<OKResponse> {

    private static final String TAG = SetupRequest.class.getSimpleName();

    private String card;
    private String deviceId;

    private Context context;

    public SetupRequest(Context context) {
        super(OKResponse.class);
        this.context = context;

        this.card = UserKnowledge.getUserCard(context);
        this.deviceId = Utils.getDeviceImei(context);

        Log.e(TAG, "Request Created(" + deviceId + "," + card + ")");
    }

    @Override
    public OKResponse loadDataFromNetwork() throws Exception {

        String path = HttpUtils.matchSymbol(HttpUtils.PATH_UPDATE_VERSION, "@", "", new String[]{card, deviceId});
        String url = HttpUtils.SERVER_URL + path;

        Log.e(TAG, "Posting the current appVersion");

        String response = HttpRestRequester.makeHttpPostRequest(url, new SetupRequestBody().toJson());
        if (response == null) {
            Log.e(TAG, "loadDataFromNetwork: Response is null");
            return null;
        }

        return new Gson().fromJson(response, OKResponse.class);
    }

    public String getCacheKey() {
        return "SetupRequest";
    }

    private class SetupRequestBody {
        private String appVersion;

        SetupRequestBody() {
            this.appVersion = "" + Utils.getVersionCode(context);
        }

        String toJson() {
            return new Gson().toJson(this);
        }
    }
}
