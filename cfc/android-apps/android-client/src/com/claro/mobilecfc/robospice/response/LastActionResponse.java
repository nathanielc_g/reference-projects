package com.claro.mobilecfc.robospice.response;

import android.text.TextUtils;
import android.util.Log;
import com.claro.mobilecfc.dto.Attribute;
import com.claro.mobilecfc.dto.FormValues;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Nathaniel Calderon on 2/2/2016.
 */
public class LastActionResponse  {
    private final String document;
    /*private List<Attribute> attributes;*/
    private FormValues formValues;

    public LastActionResponse(String document) {
        this.document = document;

        this.formValues = new FormValues(document);

        /*Log.e("LastActionResponse", "Parsing response: " + document);
        Gson gson = new Gson();
        JsonReader jReader = new JsonReader(new StringReader(document.trim()));

        *//**
         * Json string is possible come deformed
         *//*
        jReader.setLenient(true);
        JsonObject objetoResponse = gson.fromJson(jReader, JsonObject.class);
        this.attributes = transformGsonToAttributes(objetoResponse);*/

    }

    public boolean isFormValueEmpty() {
        return this.formValues.isEmpty();
    }

    /*private List<Attribute> transformGsonToAttributes (JsonObject jsonObject) {
        List<Attribute> attrs = new ArrayList<>();
        Set<Map.Entry<String,JsonElement>> entrySet=jsonObject.entrySet();
        for (Map.Entry<String,JsonElement> entry:entrySet) {
            attrs.add(new Attribute(entry.getKey(), entry.getValue().getAsString()));
        }

        return attrs;
    }*/

    /*public List<Attribute> getAttributes () {
        return this.attributes;
    }*/

    public FormValues getFormValues () {
      return this.formValues;
    }

    /*public byte[] toBytes() {
        return TextUtils.isEmpty(document) ? new byte[0] : document.getBytes();
    }*/


    @Override
    public String toString() {
        return document;
    }
}
