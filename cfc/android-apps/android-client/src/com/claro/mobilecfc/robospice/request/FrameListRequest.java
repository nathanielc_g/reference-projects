package com.claro.mobilecfc.robospice.request;

import android.util.Log;
import com.claro.mobilecfc.robospice.response.FrameListResponse;
import com.claro.mobilecfc.utils.HttpRestRequester;
import com.claro.mobilecfc.utils.HttpUtils;
import com.google.gson.Gson;
import com.octo.android.robospice.request.SpiceRequest;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 3/20/2015.
 *
 * @Deprecated use instead JsonPath aware FrameRequest
 */

@Deprecated
public class FrameListRequest extends SpiceRequest<FrameListResponse> {

    public FrameListRequest() {
        super(FrameListResponse.class);
    }

    @Override
    public FrameListResponse loadDataFromNetwork() throws Exception {

        String urlRequest = HttpUtils.SERVER_URL + HttpUtils.PATH_FRAMES;
        Log.e( "********","Getting Frames from endpoint "+urlRequest );
        String response = HttpRestRequester.makeHttpGetRequest(urlRequest);

        Log.e("FrameListResponse", "FrameListResponse response " + response);
        try {
            return new Gson().fromJson(response, FrameListResponse.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


    public String createCacheKey() {
        return "FrameListRequest";
    }
}
