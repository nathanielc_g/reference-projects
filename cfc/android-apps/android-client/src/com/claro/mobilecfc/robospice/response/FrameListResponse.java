package com.claro.mobilecfc.robospice.response;

import java.util.List;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 3/20/2015.
 *
 * @Deprecated use instead JsonPath aware FrameList
 */

@Deprecated
public class FrameListResponse {
    private List<String> frames;

    public FrameListResponse( List<String> frames ){
        this.frames = frames;
    }

    public List<String> getFrames() {
        return frames;
    }

    public void setFrames(List<String> frames) {
        this.frames = frames;
    }
}
