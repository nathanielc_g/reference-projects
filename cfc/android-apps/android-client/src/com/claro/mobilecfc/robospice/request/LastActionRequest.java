package com.claro.mobilecfc.robospice.request;

import android.util.Log;
import com.claro.mobilecfc.db.contracts.DIListContract;
import com.claro.mobilecfc.dto.Attribute;
import com.claro.mobilecfc.robospice.response.FramesResponse;
import com.claro.mobilecfc.robospice.response.LastActionResponse;
import com.claro.mobilecfc.utils.HttpRestRequester;
import com.claro.mobilecfc.utils.HttpUtils;
import com.octo.android.robospice.request.SpiceRequest;
import org.apache.http.protocol.HTTP;

import java.util.List;

/**
 * Created by Nathaniel Calderon on 2/2/2016.
 */
public class LastActionRequest extends SpiceRequest<LastActionResponse> {

    private String card;
    private String code;
    private String orderType;

    public LastActionRequest(String card, String code, String orderType) {
        super(LastActionResponse.class);
        this.card = card;
        this.code = code;
        this.orderType = orderType;
    }

    @Override
    public LastActionResponse loadDataFromNetwork()  {
        String path = HttpUtils.matchSymbol(HttpUtils.PATH_LAST_ACTION, "@", "", new String[]{card, code, orderType});
        String url  = HttpUtils.SERVER_URL + path;
        Log.e("********", "Getting Last Action from endpoint " + url);
        String response = HttpRestRequester.makeHttpGetRequest(url);
        Log.e("LastActionResponse", "LastActionResponse response " + response);
        if (response != null){
            return new LastActionResponse(response);
        }
        return null;
    }


    public String createCacheKey() {
        return "LastActionRequest";
    }
}