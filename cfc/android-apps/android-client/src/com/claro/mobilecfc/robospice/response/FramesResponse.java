package com.claro.mobilecfc.robospice.response;

import android.text.TextUtils;

/**
 * Created by Jansel Valentin on 11/16/2015.
 */
public class FramesResponse {
    private final String document;

    public FramesResponse(String document) {
        this.document = document;
    }

    public byte[] toBytes() {
        return TextUtils.isEmpty(document) ? new byte[0] : document.getBytes();
    }


    @Override
    public String toString() {
        return document;
    }
}
