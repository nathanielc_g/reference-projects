package com.claro.mobilecfc.robospice.request;

import android.util.Log;
import com.claro.mobilecfc.robospice.response.FramesResponse;
import com.claro.mobilecfc.utils.HttpRestRequester;
import com.claro.mobilecfc.utils.HttpUtils;
import com.octo.android.robospice.request.SpiceRequest;

/**
 * Created by Jansel Valentin on 11/16/2015.
 */
public class FramesRequest extends SpiceRequest<FramesResponse> {

    public FramesRequest() {
        super(FramesResponse.class);
    }

    @Override
    public FramesResponse loadDataFromNetwork() throws Exception {

        String urlRequest = HttpUtils.SERVER_URL + HttpUtils.PATH_FRAMES_V3;
        Log.e("********", "Getting Frames from endpoint " + urlRequest);
        String response = HttpRestRequester.makeHttpGetRequest(urlRequest);

        Log.e("FrameResponse", "FrameListResponse response " + response);
        try {
            return new FramesResponse(response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


    public String createCacheKey() {
        return "FrameRequest";
    }
}
