package com.claro.mobilecfc.db.contracts;

import android.net.Uri;
import com.tjeannin.provigen.Constraint;
import com.tjeannin.provigen.ProviGenBaseContract;
import com.tjeannin.provigen.annotation.Column;
import com.tjeannin.provigen.annotation.ContentUri;
import com.tjeannin.provigen.annotation.Contract;
import com.tjeannin.provigen.annotation.NotNull;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 3/20/2015.
 *
 * @Deprecated use instead JsonPath aware FrameContract
 */
@Contract(version = 3)
@Deprecated
public abstract interface FrameListContract extends ProviGenBaseContract {


    @Column(Column.Type.TEXT)
    @NotNull(Constraint.OnConflict.ABORT)
    public static final String NAME = "NAME";

    @ContentUri
    public static final Uri CONTENT_URI = Uri.parse("content://com.claro.mobilecfc/framelist");

}
