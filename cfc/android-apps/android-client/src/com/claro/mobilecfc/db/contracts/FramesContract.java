package com.claro.mobilecfc.db.contracts;

import android.net.Uri;
import com.tjeannin.provigen.Constraint;
import com.tjeannin.provigen.annotation.Column;
import com.tjeannin.provigen.annotation.ContentUri;
import com.tjeannin.provigen.annotation.Contract;
import com.tjeannin.provigen.annotation.NotNull;

/**
 * Created by Jansel Valentin on 11/16/2015.
 */

@Contract(version = 3)
public interface FramesContract {

    @Column(Column.Type.BLOB)
    @NotNull(Constraint.OnConflict.ABORT)
    String DATA = "DATA";

    @ContentUri
    Uri CONTENT_URI = Uri.parse("content://com.claro.mobilecfc/frames");

}
