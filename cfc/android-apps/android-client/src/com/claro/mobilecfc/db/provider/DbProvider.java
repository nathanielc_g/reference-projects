package com.claro.mobilecfc.db.provider;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.claro.mobilecfc.db.contracts.DIListContract;
import com.claro.mobilecfc.db.contracts.DispatchItemsContract;
import com.claro.mobilecfc.db.contracts.FrameListContract;
import com.claro.mobilecfc.db.contracts.FramesContract;
import com.tjeannin.provigen.InvalidContractException;
import com.tjeannin.provigen.ProviGenProvider;

/**
 * Created by Christopher Herrera on 2/20/14.
 */
public class DbProvider extends ProviGenProvider {

    public static final String TAG = "CFCProvider";

    public DbProvider() throws InvalidContractException {
        super(new Class[] {DispatchItemsContract.class, DIListContract.class, FrameListContract.class, FramesContract.class});
    }

    @Override
    public void onCreateDatabase(SQLiteDatabase database) {
        Log.d(TAG, "Database Created");
        super.onCreateDatabase(database);
    }
}
