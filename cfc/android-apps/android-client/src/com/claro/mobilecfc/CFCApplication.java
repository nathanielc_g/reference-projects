package com.claro.mobilecfc;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import com.claro.buggy.Buggy;
import com.claro.buggy.ReportInterceptor;
import com.claro.buggy.core.net.Endpoint;
import com.claro.buggy.core.net.EndpointProvider;
import com.claro.mobilecfc.activities.prefs.UserKnowledge;
import com.claro.mobilecfc.robospice.services.CfcService;
import com.claro.mobilecfc.utils.HttpUtils;
import com.claro.mobilecfc.utils.Utils;
import com.octo.android.robospice.SpiceManager;
import org.json.JSONObject;

/**
 * Created by Jansel Valentin on 2/15/2016.
 **/
public class CFCApplication extends Application {

    private static SpiceManager spiceManager = new SpiceManager(CfcService.class);

    @Override
    public void onCreate() {
        super.onCreate();

        configureBuggy(this);
        runMigrations(this);
        runSetup(this);
    }

    public static SpiceManager getSpiceManager() {
        return spiceManager;
    }

    public static void runSpiceManager(Context context, boolean run) {
        if (run)
            spiceManager.start(context);
        else
            spiceManager.shouldStop();
    }

    private void runMigrations(Context context) {
        BootStrap.run(new BootStrap.Configuration() {
            @Override
            public boolean runMigrations() {
                return true;
            }

            @Override
            public boolean runSetups() {
                return false;
            }
        }, context);
    }

    private void runSetup(Context context) {
        BootStrap.run(new BootStrap.Configuration() {
            @Override
            public boolean runMigrations() {
                return false;
            }

            @Override
            public boolean runSetups() {
                return true;
            }
        }, context);
    }



    private void configureBuggy(final Application context) {
        // init buggy lib
        final SessionEndpoint sessions = new SessionEndpoint(this);
        Buggy.init(context, new EndpointProvider() {
            @Override
            public Endpoint sessions() {
                return sessions;
            }
        });

        Buggy.setInterceptor(new ReportInterceptor() {
            @Override
            public void intercept(JSONObject crash) {
                if (null != crash) {
                    final String imei = Utils.getDeviceImei(context);
                    final String card = UserKnowledge.getUserCard(context);
                    try {
                        crash.put("user_id", card);
                        crash.put("imei", imei);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }


    private class SessionEndpoint implements Endpoint {
        private Context context;

        public SessionEndpoint(Context context) {
            this.context = context;
        }

        @Override
        public String endpoint() {
            final String imei = Utils.getDeviceImei(context);
            final String card = UserKnowledge.getUserCard(context);

            String path = HttpUtils.matchSymbol(HttpUtils.PATH_TEMPLATE_SESSIONS, "@", "", new String[]{card, imei});
            String url = HttpUtils.SERVER_URL + path;

            return url;
        }
    }
}
