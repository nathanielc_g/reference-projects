package com.claro.mobilecfc;

import android.content.Context;
import android.util.Log;
import com.claro.mobilecfc.migrations.Migrations;
import com.claro.mobilecfc.setup.Setups;

/**
 * Created by Jansel Valentin on 11/16/2015.
 */
public final class BootStrap {

    private static final String TAG = "Bootstrap";

    public static final void run(Configuration config, Context context) {
        if (null == config)
            return;
//      int version = Utils.getVersionCode(context);
        if (config.runMigrations()) {
            try {
                Migrations.run(context);
            } catch (Exception ex) {
                Log.e(TAG, "Failed to execute migrations", ex);
            }
        }
        if (config.runSetups())
            Setups.run(context, new Setups.OnDoneListener() {
                @Override
                public void onDone(boolean done) {
                    Log.e(TAG, "Setup done: " + done);
                }
            });
    }

    public static abstract class Configuration {
        public abstract boolean runMigrations();

        public abstract boolean runSetups();
    }
}
