package com.claro.mobilecfc.setup;

import android.content.Context;

/**
 * Created by Alvaro De la Cruz on 6/15/2016.
 */
public interface Setup {
    void run(Context context);

    boolean isReady();
}
