package com.claro.mobilecfc.setup;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;
import com.claro.mobilecfc.CFCApplication;
import com.claro.mobilecfc.activities.prefs.UserKnowledge;
import com.claro.mobilecfc.robospice.request.SetupRequest;
import com.claro.mobilecfc.robospice.response.OKResponse;
import com.claro.mobilecfc.utils.Utils;
import com.google.gson.Gson;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by Alvaro De la Cruz on 6/15/2016.
 */
public final class Setups {

    private static final String TAG = Setups.class.getSimpleName();

    public static final String PREF_NAME = "Setups";
    public static final String SETUP_VERSION = "Setup.version";

    public static void run(final Context context, final OnDoneListener listener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                CFCApplication.runSpiceManager(context, true);

                int lastKnowSetupVersion = getLastSetupVersion(context);

                if (UserKnowledge.getUserCard(context).isEmpty() ||
                        lastKnowSetupVersion == Utils.getVersionCode(context))
                    return;

                Log.e(TAG, "Starting Setup request");

                SetupRequest setupRequest = new SetupRequest(context);
                CFCApplication.getSpiceManager().execute(setupRequest, setupRequest.getCacheKey(), DurationInMillis.ALWAYS_EXPIRED, new SetupResponseListener(context, listener));
            }
        }).start();
    }

    private static int getLastSetupVersion(Context context) {
        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return sp.getInt(SETUP_VERSION, -1);
    }

    private static void setLastSetupVersion(Context context) {
        int setupVersion = Utils.getVersionCode(context) == 0 ? -1 : Utils.getVersionCode(context);

        SharedPreferences sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        sp.edit().putInt(SETUP_VERSION, setupVersion).apply();
    }

    private static class SetupResponseListener implements RequestListener<OKResponse> {
        Context context;
        OnDoneListener listener;

        SetupResponseListener(Context context, OnDoneListener listener) {
            this.context = context;
            this.listener = listener;
        }

        @Override
        public void onRequestFailure(SpiceException e) {
            if (e != null) {
                Log.e(TAG, "onRequestFailure: " + e.getLocalizedMessage());
                Toast.makeText(context, "Unable to send application current version", Toast.LENGTH_LONG).show();
            }
            CFCApplication.runSpiceManager(context, false);

            listener.onDone(false);
        }

        @Override
        public void onRequestSuccess(OKResponse okResponse) {
            if (okResponse != null) {
                Log.e(TAG, "onRequestSuccess: " + new Gson().toJson(okResponse));

                if (okResponse.isOk())
                    setLastSetupVersion(context);
            }
            CFCApplication.runSpiceManager(context, false);

            listener.onDone(true);
        }
    }

    public interface OnDoneListener {
        void onDone(boolean done);
    }
}
