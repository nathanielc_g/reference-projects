package com.claro.mobilecfc.migrations.upgrade_30316;

import android.content.ContentResolver;
import android.content.Context;
import android.util.Log;
import com.claro.mobilecfc.activities.prefs.UserKnowledge;
import com.claro.mobilecfc.db.contracts.FramesContract;
import com.claro.mobilecfc.migrations.Migrations;

/**
 * Created by Nathaniel Calderon on 2/2/2016.
 */
public class Migration   implements com.claro.mobilecfc.migrations.Migration{
    private static final int version = 30316;

    @Override
    public void run(Context context) {
        Log.e(Migrations.TAG + "[v" + version + "]", "Cleaning frames data table and Frames Cache Preferences");
        cleanFrameList(context);
    }

    private void cleanFrameList(Context context){
        UserKnowledge.setLastDateFrameSync(context, "");
    }


    @Override
    public boolean isReady() {
        return true;
    }

    @Override
    public int getVersion() {
        return version;
    }
}
