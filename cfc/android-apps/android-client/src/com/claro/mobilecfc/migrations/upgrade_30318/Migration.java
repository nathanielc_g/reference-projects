package com.claro.mobilecfc.migrations.upgrade_30318;

import android.content.ContentResolver;
import android.content.Context;
import android.util.Log;
import com.claro.mobilecfc.activities.prefs.UserKnowledge;
import com.claro.mobilecfc.db.contracts.DIListContract;
import com.claro.mobilecfc.db.contracts.DispatchItemsContract;
import com.claro.mobilecfc.migrations.Migrations;

/**
 * Created by Nathaniel Calderon on 2/2/2016.
 */
public class Migration implements com.claro.mobilecfc.migrations.Migration{
    private static final int version = 30318;

    @Override
    public void run(Context context) {
        Log.e(Migrations.TAG + "[v" + version + "]", "Cleaning local saved orders to solve phone validation issues" );
        cleanFrameList(context);
    }

    private void cleanFrameList(Context context){
        if( null != context ) {
            try {
                UserKnowledge.setLastDateFrameSync(context, "");
                ContentResolver resolver = context.getContentResolver();

                Log.e(Migrations.TAG+"[v"+version+"]","deleting all orders...");
                resolver.delete(DIListContract.CONTENT_URI, null, null);

                Log.e(Migrations.TAG+"[v"+version+"]","deleting all orders details...");
                resolver.delete(DispatchItemsContract.CONTENT_URI, null, null);

            }catch( Exception e ){
                e.printStackTrace();
            }
        }else{
            Log.e(Migrations.TAG+"[v"+version+"]","some major problems no valid context got");
        }
    }


    @Override
    public boolean isReady() {
        return true;
    }

    @Override
    public int getVersion() {
        return version;
    }
}
