package com.claro.mobilecfc.migrations;

import android.content.Context;

/**
 * Created by Jansel Valentin on 11/16/2015.
 */
public interface Migration{
    void run(Context context);

    boolean isReady();

    int getVersion();
}
