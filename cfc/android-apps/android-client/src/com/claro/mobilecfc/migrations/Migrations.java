package com.claro.mobilecfc.migrations;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import dalvik.system.DexFile;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Jansel Valentin on 11/17/2015.
 */
public final class Migrations {

    public static final String TAG = "Migration";

    public static final String PREF_NAME = "Migration";
    public static final String VERSION_NAME = "Migration.version";

    private static final int initialUpgrade = 30311;

    public static void run(Context context) throws Exception {

        int lastKnownMigration = getLastKnownMigration(context);

        int lastUpgrade = -1;

        Set<String> migrations = loadMigrationClasses(context);
        if (null != migrations) {
            for (String m : migrations) {
                try {
                    Class<?> klass = Class.forName(m);
                    if (Migration.class.isAssignableFrom(klass)) {
                        Migration migration = (Migration) klass.newInstance();

                        lastUpgrade = lastUpgrade < migration.getVersion() ? migration.getVersion() : lastUpgrade;

                        if( migration.isReady() && lastKnownMigration < migration.getVersion() ) {
                            migration.run(context);
                        }else {
                            Log.e(TAG, "Migration " + migration.getVersion() + " isn't ready to run or has executed");
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        Log.e(TAG, "Saving last migration version "+lastUpgrade );
        setLastKnownMigration(context,lastUpgrade);
    }

    private static int getLastKnownMigration(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return prefs.getInt( VERSION_NAME,-1 );
    }


    public static void setLastKnownMigration(Context context,int latestVersion){
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        prefs.edit().putInt( VERSION_NAME,latestVersion ).commit();
    }

    /**
     * @TODO change this awkward migration loading by one really loadable
     */
    private static final Set<String> loadMigrationClasses(Context context) throws IOException {
        String rootPackage = Migrations.class.getPackage().getName() + "."; // The dot exclude parent package
        if (TextUtils.isEmpty(rootPackage))
            return null;

        Set<String> classes = new TreeSet<>();
        try {
            DexFile df = new DexFile(context.getPackageCodePath());
            for (Enumeration<String> iter = df.entries(); iter.hasMoreElements(); ) {
                String className = iter.nextElement();

                if (className.startsWith(rootPackage) && className.contains("upgrade")) {
                    Log.e("***", "analysing class " + className);
                    final String version = className.replaceAll(".*?upgrade_(\\d+)\\.Migration", "$1");
                    try {
                        int intVersion = Integer.parseInt(version);

                        boolean apply = initialUpgrade < intVersion  ;
                        if (apply)
                            classes.add(className);

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return classes;
    }
}
