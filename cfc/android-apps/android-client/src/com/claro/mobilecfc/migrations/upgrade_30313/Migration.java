package com.claro.mobilecfc.migrations.upgrade_30313;

import android.content.ContentResolver;
import android.content.Context;
import android.util.Log;
import com.claro.mobilecfc.db.contracts.FramesContract;
import com.claro.mobilecfc.migrations.Migrations;

/**
 * Created by Jansel Valentin on 11/16/2015.
 */
public final class Migration implements com.claro.mobilecfc.migrations.Migration {
    private static final int version = 30313;

    @Override
    public void run(Context context) {
        Log.e(Migrations.TAG + "[v" + version + "]", "Cleaning frames data table");
        cleanFrameList(context);
    }

    private void cleanFrameList(Context context) {
        ContentResolver resolver = context.getContentResolver();
        resolver.delete(FramesContract.CONTENT_URI, null, null);
    }


    @Override
    public boolean isReady() {
        return true;
    }

    @Override
    public int getVersion() {
        return version;
    }
}
