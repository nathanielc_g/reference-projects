package com.claro.mobilecfc.enums;
import com.claro.mobilecfc.R;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 11/25/2014.
 */
public enum OrderPriority {

    UNKNOWM{
        @Override
        public int color() {
            return android.R.color.transparent;
        }
    },
    COMPROMISO_SGTE{
        @Override
        public int color() {
            return R.color.blue_base_translucent_1;
        }
    },
    COMPROMISO_FUTURO{
        @Override
        public int color() {
            return android.R.color.holo_green_light;
        }
    },
    RECLAMACION{
        @Override
        public int color() {
            return R.color.violet_base_1;
        }
    },
    COPC{
        @Override
        public int color() {
            return R.color.red_base_translucent;
        }
    };


    public abstract int color();
}
