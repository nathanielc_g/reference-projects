package com.claro.mobilecfc.dto;

import android.text.TextUtils;
import android.util.Log;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Jansel Valentin on 11/17/2015.
 */
public class Frames {

    private static final String TAG = "Frames";

    private String document;
    private Object jsonPath;

    private boolean loaded = false;

    private static List<String> listNames;

    public Frames() {
    }


    public Frames(String document) {
        this.load( document );
    }


    public Frames( byte[] data ){
        load(new String(data));
    }


    public Frames load( byte[] data ){
        load( new String(data) );
        loaded = true;
        return this;
    }


    public Frames load( String document ){
        this.document = document;
        loaded = true;

        if(!TextUtils.isEmpty(document))
            jsonPath = Configuration.defaultConfiguration(). jsonProvider().parse(document);
        return this;
    }


    public boolean isLoaded() {
        return loaded;
    }

    public List<String> listNames(){
        if( null != listNames && 0 < listNames.size() )
            return listNames;

        try {
            Object doc = JsonPath.read(jsonPath, "$.frames[*].name");
            JSONArray frameNames = (JSONArray)doc;

            List names = Arrays.asList(frameNames.toArray());
            Log.e(TAG, "docs=" + names);
            return  listNames = names;
        }catch ( Exception ex ){
            Log.e( TAG,"error reading frame names",ex );
        }
        return Collections.emptyList();
    }


    public String getCabinaPrefix( int frameIndex ){
        return getFramePath(frameIndex, ".prefixes.cabina");
    }


    public String getPortPrefix( int frameIndex, String port ){
        return getFramePath( frameIndex,".prefixes.ports."+port );
    }


    private String getFramePath( int frame ,String path ) {
        if (null == jsonPath)
            return null;
        try{
            return JsonPath.read(jsonPath, "$.frames[" + frame + "]"+path );
        }catch ( Exception ex ){
            return null;
        }
    }

    public String getTerminalFoPrefix (int frameIndex) {
        return getFramePath(frameIndex, ".prefixes.terminal_fo");
    }

    @Override
    public String toString() {
        return document;
    }
}
