package com.claro.mobilecfc.dto;

import android.text.TextUtils;
import android.util.Log;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by nathaniel calderon on 2/3/2016.
 */
public class FormValues {
    private static final String TAG = "ActionValuesDownloaded";
    private String document;
    private Object jsonPath;
    private boolean isEmpty = true;

    public FormValues( String document ){
        this.document = document;

        if(!TextUtils.isEmpty(document))
            jsonPath = Configuration.defaultConfiguration(). jsonProvider().parse(document);

        this.isEmpty = ((LinkedHashMap)jsonPath).isEmpty();
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public String getPath(String path ) {
        if (null == jsonPath)
            return null;
        try{
            return JsonPath.read(jsonPath, "$." + path);
        }catch ( Exception ex ){
            Log.e("****", ex.getMessage());
            return null;
        }
    }

    public String getPath(String path, String ForNullReturn) {
        if (null == jsonPath)
            return ForNullReturn;
        try{
            return JsonPath.read(jsonPath, "$." + path);
        }catch ( Exception ex ){
            Log.e("****", ex.getMessage());
            return ForNullReturn;
        }
    }

    public String getPathAsString(String path ) {
        if (null == jsonPath)
            return "";
        try{
            return JsonPath.read(jsonPath, "$." + path);
        }catch ( Exception ex ){
            Log.e("****", ex.getMessage());
            return "";
        }
    }

    @Override
    public String toString() {
        return document;
    }
}
