package com.claro.buggy.crashes;


import android.content.Context;


/*
 * Created by Jansel Valentin on 5/20/14.
 */
/*package*/ class DefaultCrashDataReceiver implements CrashDataReceiver<AfterCrashData> {
    private Context context;

    DefaultCrashDataReceiver( Context context ){
        this.context = context;
    }

    @Override
    public void receive(AfterCrashData data) {
        forceSessionClose();
        hook(data);
    }

    @Override
    public void handleUnsentCrash(AfterCrashData data) {
    }


    private void forceSessionClose(){
    }


    private void hook( CrashData data ){
        if( Thread.getDefaultUncaughtExceptionHandler() instanceof UncaughtHandler){
            CrashHook hook = ((UncaughtHandler) Thread.getDefaultUncaughtExceptionHandler()).getCrashHook();
            if( null != hook ){
                hook.hookQuickly( data );
            }
        }
    }
}
