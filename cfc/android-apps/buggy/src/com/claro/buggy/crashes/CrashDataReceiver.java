package com.claro.buggy.crashes;

/*
 * Created by Jansel Valentin on 5/20/14.
 */
/*package*/ interface CrashDataReceiver<T extends com.claro.buggy.crashes.CrashData> {
    public void receive(T data);
    public void handleUnsentCrash(T data);
}
