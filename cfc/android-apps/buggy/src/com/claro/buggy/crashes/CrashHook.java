package com.claro.buggy.crashes;


/*
 * Created by Jansel Valentin on 6/5/2014.
 */
public interface CrashHook {
    void hookQuickly(CrashData data);
}
