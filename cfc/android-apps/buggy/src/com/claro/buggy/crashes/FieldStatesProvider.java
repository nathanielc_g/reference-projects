package com.claro.buggy.crashes;

import android.content.Context;
import android.text.format.Time;
import com.claro.buggy.core.util.Dump;

/*package*/ class FieldStatesProvider implements CrashDataProvider<AfterCrashData> {
    private Context context;

    public FieldStatesProvider(Context cxt) {
        context = cxt;
    }

    public AfterCrashData get(Thread broken, Throwable thr) {
        Time crashTime = new Time();
        crashTime.setToNow();

        final AfterCrashData data = new AfterCrashData();
        data.setCrashTime(crashTime);

        data.setStackTrace(Dump.getStacktraceLastThrowableCause(thr));
        return data;
    }
}
