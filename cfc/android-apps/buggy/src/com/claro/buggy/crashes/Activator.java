package com.claro.buggy.crashes;

import android.content.Context;
import com.claro.buggy.activation.ActivationToken;
import com.claro.buggy.activation.StatesActivator;

/*
 * Created by Jansel Valentin on 5/5/14.
 */
public final class Activator implements com.claro.buggy.activation.Activator {
    @Override
    public void activate(Context app, ActivationToken token) {
        StatesActivator.checkToken(token);

        UncaughtHandler<AfterCrashData> handler = new UncaughtHandler<AfterCrashData>(new FieldStatesProvider(app));
        handler.registerReceiver(new com.claro.buggy.crashes.DefaultCrashDataReceiver(app));
        Thread.setDefaultUncaughtExceptionHandler(handler);
    }
}
