package com.claro.buggy;

import org.json.JSONObject;

/**
 * Created by Jansel Valentin on 2/15/2016.
 **/
public interface ReportInterceptor {
    void intercept(JSONObject crash);
}
