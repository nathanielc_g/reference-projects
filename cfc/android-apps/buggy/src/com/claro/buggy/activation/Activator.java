package com.claro.buggy.activation;

import android.content.Context;

/*
 * Created by Jansel Valentin on 5/4/14.
 */
public interface Activator {
    void activate(Context app, com.claro.buggy.activation.ActivationToken token);
}
