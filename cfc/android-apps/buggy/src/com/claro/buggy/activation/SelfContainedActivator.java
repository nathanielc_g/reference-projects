package com.claro.buggy.activation;

import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by Jansel Valentin on 5/6/14.
 */

final class SelfContainedActivator implements Iterable<com.claro.buggy.activation.Activator> {
    private Class<?>[] activators;

    private SelfContainedActivator() {
        activators = getKnownActivators();
    }

    private Class<?>[] getKnownActivators() {
        return new Class<?>[]{
                com.claro.buggy.crashes.Activator.class
        };
    }

    public static SelfContainedActivator prepare() {
        return new SelfContainedActivator();
    }

    public void clear(){
        activators = null;
    }

    public Iterator<com.claro.buggy.activation.Activator> iterator() {
        return new LazyIterator(this);
    }

    private class LazyIterator implements Iterator<com.claro.buggy.activation.Activator> {
        SelfContainedActivator container;
        int next;

        private LazyIterator(SelfContainedActivator container) {
            this.container = container;
        }

        public boolean hasNext() {
            return null != container.activators && container.activators.length > next;
        }


        public com.claro.buggy.activation.Activator next() {
            if (!hasNext())
                throw new NoSuchElementException();

            Class<?> toLoad = container.activators[next++];
            com.claro.buggy.activation.Activator loaded;
            try {
                loaded = com.claro.buggy.activation.Activator.class.cast(toLoad.newInstance());
            } catch (IllegalAccessException ie) {
                throw new IllegalActivatorException(ie);
            } catch (InstantiationException ie) {
                throw new IllegalActivatorException(ie);
            }
            return loaded;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private static class IllegalActivatorException extends RuntimeException {
        private IllegalActivatorException(Throwable throwable) {
            super(throwable);
        }
    }
}
