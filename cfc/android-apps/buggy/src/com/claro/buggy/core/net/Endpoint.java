package com.claro.buggy.core.net;

/*
 * Created by Jansel Valentin on 5/4/14.
 */
public interface Endpoint {
    String endpoint();
}
