package com.claro.buggy.core.net.connectivity;

/*
 * Created by Jansel Valentin on 5/4/14.
 */
public enum ConnectionState {
    CONNECTED, DISCONNECTED
}
