package com.claro.buggy.core;

import android.content.Context;
import android.database.Cursor;
import android.os.Process;
import com.claro.buggy.Buggy;
import com.claro.buggy.core.net.Endpoint;
import com.claro.buggy.core.net.HttpRequest;
import com.claro.buggy.core.net.NetworkHelper;
import com.claro.buggy.core.util.Dump;
import com.claro.buggy.core.util.log.TP;
import com.claro.buggy.crashes.CrashData;
import org.apache.http.HttpResponse;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

/*
 * Created by Jansel Valentin on 6/4/2014.
 */

final class Dispatcher implements Destroyable {

    static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    private Calendar activityCalendar = GregorianCalendar.getInstance();

    private static Object endSessionSentinel = new Object();
    private static Object crashSessionSentinel = new Object();

    private int reportedState;

    private Context context;
    private DispatchWorker dispatchWorker;
    private CrashData crashData;
    private ExecutorService executor;


    private CountDownLatch crashSyncLock = new CountDownLatch(1);

    private BlockingQueue<Object> sessions = new LinkedBlockingQueue<Object>();
    private ReentrantLock lock = new ReentrantLock();


    private Dispatcher() {
    }

    Dispatcher(Injectable<Context> caller) {
        InlineThread.checkCaller(caller);
        this.context = caller.get();
    }

    public void destroy() {
        if (null != executor)
            executor.shutdown();

        executor = null;
        dispatchWorker = null;
    }

    private S createCrashedSession(){
        String openTime;
        try{
            activityCalendar.setTimeInMillis(System.currentTimeMillis());
            openTime = format.format(activityCalendar.getTime());
        }catch( Exception ex){
            openTime = "";
        }
        S s = new S();
        s.uuid = UUID.randomUUID().toString();
        s.date = openTime;
        s.crashed = true;
        return s;
    }

    private void startWorker() {
        if (null == dispatchWorker || !dispatchWorker.isRunning) {
            dispatchWorker = new DispatchWorker(this);
            dispatchWorker.setPriority(Process.THREAD_PRIORITY_BACKGROUND);
            dispatchWorker.start();
        }
    }


    void dispatch(CrashData data) {
        try {
            crashData = data;
            sessions.put(crashSessionSentinel);
            startWorker();

            crashSyncLock.await(10, TimeUnit.SECONDS);

        } catch (InterruptedException ex) {
            Dump.printStackTraceCause(ex);
        }
    }

    private void sendSession(S session) {
        if (null == executor)
            return;
        if (null == session) {
              session = createCrashedSession();
        }
        executor.execute(new DispatchSessionSender(session));
    }


    private class DispatchSessionSender implements Runnable {
        public S s;
        private final int MAX_RETRY_INTENT = 2;
        private int currentRetry = 1;
        private boolean isDone;


        private DispatchSessionSender(S s) {
            this.s = s;
        }

        public void run() {

            if (!NetworkHelper.isNetworkReady(context)) {
                TP.w("Skip send sessions, device is not connected to network");
                    crashSyncLock.countDown();
                return;
            }
            Charset charset = Charset.forName("UTF-8");
            boolean isSessionCandidateToBeSent = false;
            try {
                JSONObject entity = new JSONObject();

                entity.put("uuid", s.uuid);
                entity.put("date", s.date);


                JSONObject value = new JSONObject();
                entity.put("value", value);

                if (s.crashed && null != crashData) {
                    JSONObject crash;
                    if (null == (crash = crashData.toJson()))
                        crash = new JSONObject();

                    value.put("crash", crash);
                    isSessionCandidateToBeSent = true;
                }
                /*
                    This code avoid orphan sessions were sent to testpoke
                    web, based on issue #17

                    */

                if( null != Buggy.getInterceptor() ){
                    Buggy.getInterceptor().intercept(entity);
                }

                if (!isSessionCandidateToBeSent) {
                    TP.i("Skipping orphan session");
                    return;
                }

                // This is for always have one valid user, after user has logged in


                JSONObject root = new JSONObject();
                root.put("entity", entity);

                String json = root.toString();

                Endpoint endpoint = Buggy.getEndpointProvider().sessions();

                if (NetworkHelper.isNetworkReady(context)) {
                    while (MAX_RETRY_INTENT >= currentRetry && !isDone) {
                        boolean isRetrying = false;
                        try {

                            final byte[] data = json.getBytes(charset);
                            HttpResponse response = HttpRequest.executeHttpPost(endpoint, data);
                            int status = response.getStatusLine().getStatusCode();
                            try {
                                response.getEntity().consumeContent();
                            } catch (Exception e) {
                                Dump.printStackTraceCause(e);
                            }
                            if (200 == status) {
                                /**
                                 * Session was sent, cleaning not active session.
                                 */
                                isDone = true;
                            } else {
                                currentRetry++;
                                TP.w("Error sending session" + s.uuid + ",, attempt #" + currentRetry + ",retrying in 2 seconds");
                            }
                        } catch (Exception ex) {
                            isRetrying = true;
                            currentRetry++;
                            TP.w("Error sending session" + s.uuid + ",, attempt #" + currentRetry + ",retrying in 2 seconds");
                        }
                        if (isRetrying) {
                            try {
                                TimeUnit.SECONDS.sleep(2);
                            } catch (InterruptedException ex) {
                                Dump.printStackTraceCause(ex);
                            }
                        }
                    }
                }
                if (!isDone) {
                    TP.w("Session " + s.uuid + ", could not be sent");
                }
                if (s.crashed)
                    crashSyncLock.countDown();

            } catch (Exception ex) {
                Dump.printStackTraceCause(ex);
            }
        }
    }

    private class DispatchWorker extends Thread {
        private BlockingQueue<Object> sessions;
        private Dispatcher dispatcher;
        private volatile boolean isRunning;

        DispatchWorker(Dispatcher dispatcher) {
            this.sessions = dispatcher.sessions;
            this.dispatcher = dispatcher;
        }


        @Override
        public void run() {
            isRunning = true;

            final int MAX_THREAD_NUMBER = 5;
            if (null == executor)
                executor = Executors.newFixedThreadPool(MAX_THREAD_NUMBER);

            for (; ; ) {
                if (Thread.currentThread().isInterrupted())
                    break;
                try {

                    final Object incoming = this.sessions.take();
                    boolean reportedCrash = this.sessions.contains(dispatcher.crashSessionSentinel);

                    if (incoming == dispatcher.endSessionSentinel && !reportedCrash)
                        break;

                    if (incoming != dispatcher.crashSessionSentinel && reportedCrash) {
                        continue;
                    } else if (incoming == dispatcher.crashSessionSentinel) {
                        dispatcher.sendSession(null);
                        break;
                    }

                    if (incoming instanceof S) {
                        dispatcher.sendSession((S) incoming);
                    }

                } catch (InterruptedException ex) {
                    Dump.printStackTraceCause(ex);
                    break;
                }
            }
            isRunning = false;
        }
    }

    private class S {
        String uuid;
        String date;
        boolean crashed;
    }
}

