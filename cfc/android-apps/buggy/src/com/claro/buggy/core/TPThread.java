package com.claro.buggy.core;

import android.content.Context;
import android.os.Process;
import com.claro.buggy.Buggy;
import com.claro.buggy.core.net.connectivity.ConnectionState;
import com.claro.buggy.core.net.connectivity.ConnectivityListener;
import com.claro.buggy.core.net.connectivity.ConnectivityWatcher;
import com.claro.buggy.core.util.log.TP;

/*
 * Created by Jansel Valentin on 5/6/14.
 */
public final class TPThread extends Thread implements ConnectivityListener {
    private static final String NAME = "Buggy Session Thread";

    private static TPThread tpThread;

    private SimpleSchedulerWrapper scheduler;

    private volatile boolean isServing;
    private volatile boolean trustedBegin;

    private Context context;
    private Object mainLock;

    private TPThread(Context context, Object lock) {
        this.mainLock = lock;
        tryStart(context);
    }

    public static TPThread getDefault(Context context, Object lock) {
        if (null != tpThread)
            return tpThread;

        synchronized (TPThread.class) {
            if (null == tpThread)
                tpThread = new TPThread(context, lock);
        }
        return tpThread;
    }


    private void tryStart(Context context) {
        if (isServing) {
            TP.w("Session Service is already running");
        } else {
            if (!Buggy.isRunning()) {
                TP.d("Not allowed initiate TPThread with Buggy hasn't takeoff");

                stopThread();
            } else {
                this.context = context;
                setName(NAME);
                setPriority(Process.THREAD_PRIORITY_BACKGROUND);

                start();
            }
        }
    }

    public void stopThread() {
        TP.d("Destroying " + NAME);

        if (isServing)
            revoke();

        if (Buggy.isRunning()) {
            Buggy.end();
        }
    }

    @Override
    public void run() {
        isServing = true;
        init();
    }


    public void onChangeDetected(ConnectionState state) {
        if (ConnectionState.CONNECTED == state) {
            //...
        }
    }

    private void init() {
        scheduler = SimpleSchedulerWrapper.shared();
        scheduler.start();

        liftOnActivation(context, mainLock);

        ConnectivityWatcher.registerListener(this);
    }


    private void destroyState() {
        isServing = false;
        if( null != InlineThread.getSynInstance(null) )
            InlineThread.getSynInstance(null).terminate();
    }

    static void liftOnActivation(Context context, Object mainLock) {
        InlineThread thread = InlineThread.getInstance(context, mainLock);
    }

    private void revoke() {
        destroyState();
    }


}
