package com.claro.buggy.core;

/*
 * Created by Jansel Valentin on 6/9/2014.
 */
interface Destroyable {
    void destroy();
}
