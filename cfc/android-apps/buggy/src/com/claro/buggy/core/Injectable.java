package com.claro.buggy.core;

/*
 * Created by Jansel Valentin on 5/3/14.
 */
public abstract interface Injectable<T> {
    public abstract T get();
}
