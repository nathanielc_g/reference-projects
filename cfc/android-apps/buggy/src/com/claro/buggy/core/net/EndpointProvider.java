package com.claro.buggy.core.net;

/**
 * Created by Jansel Valentin on 2/15/2016.
 **/
public interface EndpointProvider {
    Endpoint sessions();
}
