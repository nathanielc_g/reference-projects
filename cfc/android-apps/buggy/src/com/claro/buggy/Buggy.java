package com.claro.buggy;

import android.app.Application;
import com.claro.buggy.core.TPThread;
import com.claro.buggy.core.net.EndpointProvider;
import com.claro.buggy.core.net.connectivity.ConnectivityWatcher;
import com.claro.buggy.core.util.Dump;
import com.claro.buggy.core.util.log.Log;
import com.claro.buggy.core.util.log.TP;

/**
 *
 * Classes used in this project were extracted from a project of Jansel https://github.com/testpoke/testpoke-android
 **/
public final class Buggy {
    private static final String version = "2.1.2";

    private static Application application;
    private static volatile boolean running;
    private static EndpointProvider endpointProvider;
    private static ReportInterceptor interceptor;


    public static void init(Application app, EndpointProvider provider) {

        if (null != application)
            return;

        if(null == app)
            throw new IllegalStateException("Application must not be null");

        if(null == provider)
            throw new IllegalArgumentException("Application must provide a valid endpoint provider");


        TP.i("Configured Buggy SDK Logger Level to " + Log.getLogLevel());

        TP.i("Initiating Buggy SDK...");

        application = app;
        endpointProvider = provider;
        initiate();
    }

    public static ReportInterceptor getInterceptor() {
        return interceptor;
    }

    public static void setInterceptor(ReportInterceptor interceptor) {
        Buggy.interceptor = interceptor;
    }

    public static boolean isRunning() {
        return running;
    }

    public static Application getApplication() {
        return application;
    }

    public static String getVersion() {
        return version;
    }

    public static EndpointProvider getEndpointProvider() {
        return endpointProvider;
    }

    public static void end() {
        if (!running)
            return;

        TP.i("Buggy is ending...");
        running = false;
        TPThread.getDefault(application, null).stopThread();
        ConnectivityWatcher.stop(application);
    }


    private static void initiate() {
        running = true;
        ConnectivityWatcher.start(application);
        final Object mainLock = new Object();
        synchronized (mainLock) {
            TPThread.getDefault(application, mainLock);
            try {
                mainLock.wait();
            } catch (InterruptedException ex) {
                end();
                Dump.printStackTraceCause(ex);
            }
        }
    }
}
