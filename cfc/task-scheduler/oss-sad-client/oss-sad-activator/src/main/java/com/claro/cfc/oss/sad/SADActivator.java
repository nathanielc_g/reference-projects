package com.claro.cfc.oss.sad;

import com.claro.cfc.scheduler.activation.ActivationContext;
import com.claro.cfc.scheduler.activation.Activator;
import com.claro.cfc.scheduler.tasks.TaskHandler;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Created by Jansel R. Abreu on 7/8/2015.
 */
@Remote
@Stateless(name="SADActivatorEJB",mappedName = "SADActivator")
public class SADActivator implements Activator{

    public SADActivator() {
    }

    public TaskHandler activate(ActivationContext context) {
        return new SADTaskHandler(context);
    }
}
