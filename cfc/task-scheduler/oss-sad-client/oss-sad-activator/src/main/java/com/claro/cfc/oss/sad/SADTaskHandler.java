package com.claro.cfc.oss.sad;

import com.claro.cfc.oss.environment.Environment;
import com.claro.cfc.oss.environment.Environments;
import com.claro.cfc.oss.utils.GenericTaskExecutionResult;
import com.claro.cfc.oss.utils.StringUtils;
import com.claro.cfc.scheduler.activation.ActivationContext;
import com.claro.cfc.scheduler.tasks.*;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.util.List;

/**
 * Created by Jansel R. Abreu on 7/8/2015.
 */
public class SADTaskHandler implements TaskHandler {
    private static final String HANDLER_NAME = "SAD OSS Client Service";

    private static final String SECURITY_KEY_QA = "082C-32DF-1AD2-EB2D";
    private static final String SECURITY_KEY_PR = "11CE-86BF-87A0-5AC2";

    private static String DEFAULT_SECURITY_KEY = "";


    private ActivationContext context;
    private Gson gson;

    public SADTaskHandler(ActivationContext context) {
        this.context = context;
    }

    public Configuration getConfiguration() {
        return new Configuration() {
        };
    }

    public String getDisplayName() {
        return HANDLER_NAME;
    }

    public TaskExecutionResult perform(Task task) {
        Logger logger = Logger.getLogger(SADTaskHandler.class);
        logger.info(HANDLER_NAME + "(: perform work for task ) " + task);
        return run(task, logger);
    }


    private TaskExecutionResult run(Task task, Logger logger) {
        Environment env = Environments.getDefault();

        if (DEFAULT_SECURITY_KEY.isEmpty()) {
            DEFAULT_SECURITY_KEY = Environment.PRODUCTION == env ? SECURITY_KEY_PR : SECURITY_KEY_QA;

            logger.info("Using " + env + " services security key " + DEFAULT_SECURITY_KEY);
        }

        final Tuple<String, String> tupleMessage = getMessage(task, logger);
        final String message = tupleMessage.obj2;


        logger.info(HANDLER_NAME + "(:perform:) sending message");
        logger.info(HANDLER_NAME + "(:perform:) " + message);

        if (null == message) {
            logger.info(HANDLER_NAME + "(:perform:) skipping work due on null message");
            return null;
        }

        Tuple<Integer,String> httpResult = null;

        try {
            httpResult = sendMessage(tupleMessage, logger);
        } catch (Exception ex) {
            logger.error("***** Error trying to send update message for " + HANDLER_NAME);
            ex.printStackTrace();
        }

        if (null == httpResult) {
            logger.warn(HANDLER_NAME + "Some error trying to fetch response for service");
            return null;
        }

        GenericTaskExecutionResult taskResult = parseRawResult(httpResult);
        if (null != taskResult) {
            taskResult.setOutgoingXml(message);
            taskResult.setIngoingXml(httpResult.obj2);
        }

        return taskResult;
    }


    private Tuple<String, String> getMessage(Task task, Logger logger) {
        List<Attribute> attrs = task.getAttributes();

        if (null == attrs || 0 == attrs.size()) {
            logger.info(HANDLER_NAME + "(:getMessage:) getting not valid attributes from task ( " + task + " )");
            return null;
        }

        int index = 0;

        String addressId = "";
        String latitud = "";
        String longitud = "";
        String strValid = "";

        if (0 <= (index = attrs.indexOf(Attribute.createAttribute("ID_DIRECCION", "")))) //Frame is reported here
            addressId = StringUtils.asString(attrs.get(index).getValue());
        if (0 <= (index = attrs.indexOf(Attribute.createAttribute("LATITUD", ""))))
            latitud = StringUtils.asString(attrs.get(index).getValue());
        if (0 <= (index = attrs.indexOf(Attribute.createAttribute("LONGITUD", ""))))
            longitud = StringUtils.asString(attrs.get(index).getValue());
        if (0 <= (index = attrs.indexOf(Attribute.createAttribute("COORDVALIDS", ""))))
            strValid = StringUtils.asString(attrs.get(index).getValue());

        boolean validCoordinates = new Boolean(strValid);

        StringBuilder builder = new StringBuilder();
        builder.append("{").append("\"latitud\":\"").append(latitud).append("\",")
                .append("\"longitud\":\"").append(longitud).append("\",")
                .append("\"coordenadasValidas\":"+validCoordinates)
                .append("}");

        return new Tuple<String, String>(addressId, builder.toString());
    }


    private Tuple<Integer,String> sendMessage(Tuple<String, String> tupleMessage, Logger logger) {
        try {

            if (null == tupleMessage) {
                logger.warn(HANDLER_NAME + "(:getMessage:) Error sending update due on null tuple message");
                return null;
            }

            final String addressId = tupleMessage.obj1;
            final String message = tupleMessage.obj2;

            if (null == addressId) {
                if (null == tupleMessage) {
                    logger.warn(HANDLER_NAME + "(:getMessage:) Error sending update due on null address id");
                    return null;
                }
            }

            Environment env = Environments.getDefault();


            final String formattedURL = env.sadHost() + addressId;
            logger.info(HANDLER_NAME + "(:sendMessage:) trying to send message to endpoint on " + formattedURL);

            HttpURLConnection con = (HttpURLConnection) new URL(formattedURL).openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("PUT");
            con.setRequestProperty("Content-type", "application/json; charset=utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Authorization", DEFAULT_SECURITY_KEY);
            con.connect();

            OutputStream out = con.getOutputStream();
            if (null != out) {
                out.write(message.getBytes());
            }
            out.flush();
            out.close();

            InputStream in;

            if( con.getResponseCode() != HttpURLConnection.HTTP_OK )
                in = con.getErrorStream();
            else
                in = con.getInputStream();

            StringBuilder builder = new StringBuilder();

            if( null != in ) {
                ByteBuffer buffer = ByteBuffer.allocate(8192);
                ReadableByteChannel channel = Channels.newChannel(in);

                while (0 < channel.read(buffer)) {
                    buffer.flip();
                    builder.append(Charset.forName("UTF-8").decode(buffer));
                    buffer.clear();
                }

                in.close();
            }

            final int statusCode = con.getResponseCode();

            con.disconnect();
            String result = "".equals( builder.toString().trim() ) ? null : builder.toString().trim();
            logger.info(HANDLER_NAME + "(:sendMessage:) getting response ( " + result + " )\n");
            return new Tuple<Integer, String>(statusCode,result);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private GenericTaskExecutionResult parseRawResult(Tuple<Integer,String> httpResponse) {
        GenericTaskExecutionResult result = new GenericTaskExecutionResult();

        final int httpStatusCode = httpResponse.obj1;
        final String requestBody  = httpResponse.obj2;

        if( null == requestBody )
            return result;

         if( null == gson )
             gson = new Gson();

        if( 200 == httpStatusCode  ) {
            result.setMessage("Direccion actualizada correctamente");
            result.success(true);
            result.setErrorType(TaskResponseStatus.NO_ERROR.toString());
        }else {
            final JsonObject jsonResponse = gson.fromJson(requestBody, JsonObject.class);

            StringBuilder builder = new StringBuilder();

            JsonElement ele;
            /*if( null != (ele = jsonResponse.get( "status")))
                builder.append( "http.status="+ele.getAsString() );*/

            if( null != (ele = jsonResponse.get( "message" ))) {
                final String raw = ele.getAsString();
                final int limit = raw.length() > 700 ? 700 : raw.length()-1;
                String shortMessage = ele.getAsString().substring(0,limit);
                //builder.append(", message=" + shortMessage);
                builder.append(shortMessage);
            }

            result.success( false );
            result.setMessage(builder.toString());
            result.setErrorType( TaskResponseStatus.NO_CHANGE.toString() );
        }

        return result;
    }


    private static class Tuple<T, T2> {
        final T obj1;
        final T2 obj2;

        public Tuple(T obj1, T2 obj2) {
            this.obj1 = obj1;
            this.obj2 = obj2;
        }
    }
}
