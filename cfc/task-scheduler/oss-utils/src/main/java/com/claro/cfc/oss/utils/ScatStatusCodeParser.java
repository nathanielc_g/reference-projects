package com.claro.cfc.oss.utils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.StringReader;

/**
 * Created by Jansel Valentin on 6/1/2016.
 **/
public final class ScatStatusCodeParser {

    public static Status toResult(String message){
        Status status = new Status();

        SAXParserFactory fact = SAXParserFactory.newInstance();
        try {
            fact.setNamespaceAware(true);
            SAXParser parser = fact.newSAXParser();
            parser.parse(new ReaderInputStream(new StringReader(message.trim())),new StatusCodeHandler(status));
        }catch ( Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return status;
    }

    private static class StatusCodeHandler extends DefaultHandler{
        private Status status;
        private String current;
        private String name = "";

        public StatusCodeHandler(Status status) {
            this.status = status;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            name = localName;
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (localName.toLowerCase().contains("scatcfcservicescodesresult")) {
                status.procceed = new Boolean(current);
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (name.toLowerCase().contains("scatcfcservicescodesresult")) {
                current = new String(ch,start,length);
            }
        }
    }

    public static class Status{
        boolean procceed;
        public boolean isOk(){
            return procceed;
        }
    }
}
