package com.claro.cfc.oss.utils;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 9/30/2014.
 */
public final class StringUtils {
    public static final String asString(Object object) {
        return null == object ? "" : object.toString();
    }
}
