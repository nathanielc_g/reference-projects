package com.claro.cfc.oss.environment;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/6/2014.
 */
public enum Environment {
    DEVELOPMENT{
        @Override
        public String m6Host() {
            return "http://nttm6mgd0002:8010/COFEE_MSSWeb/com/verizon/api/services/QuickOrderManager.jws";
        }

        @Override
        public String geffHost() {
            return "http://nttappsweb0003/GEFFSERVICE/CFCService.asmx";
        }

        @Override
        public String domainHost() {
            return "http://172.27.5.88:7005/DOMAINPlusAPI-war/DOMAINPlusAPIWS"; // QA2;
            /*return "http://172.27.5.88:7011/DOMAINPlusAPI-war/DOMAINPlusAPIWS"; QA1 */
        }

        @Override
        public String sadHost() {
            return "http://sadprueba:9026/SADService/v2/Direcciones/";
        }
    },
    STAGING{
        @Override
        public String m6Host() {
            return "http://nttm6mgd0002:8010/COFEE_MSSWeb/com/verizon/api/services/QuickOrderManager.jws";
        }

        @Override
        public String geffHost() {
            return "http://nttappsweb0003/GEFFSERVICE/CFCService.asmx";
        }

        @Override
        public String domainHost() {
            return "http://172.27.5.88:7005/DOMAINPlusAPI-war/DOMAINPlusAPIWS"; // QA2;
            /*return "http://172.27.5.88:7011/DOMAINPlusAPI-war/DOMAINPlusAPIWS"; QA1 */
        }

        @Override
        public String sadHost() {
            return "http://sadprueba:9026/SADService/v2/Direcciones/";
        }
    },
    PRODUCTION{
        @Override
        public String m6Host() {
            return "http://ntpm6int0001:8010/COFEE_MSSWeb/com/verizon/api/services/QuickOrderManager.jws";
        }

        @Override
        public String geffHost() {
            return "http://ntpappsweb0004/GEFFSERVICES/CFCService.asmx";
        }

        @Override
        public String domainHost() {
            return "http://domain/DOMAINPlusAPI-war/DOMAINPlusAPIWS";
        }

        @Override
        public String sadHost() {
            return "http://sad/SADService/v2/Direcciones/";
        }
    };

    public abstract String m6Host();

    public abstract String geffHost();

    public abstract String domainHost();

    public abstract String sadHost();
}

