package com.claro.cfc.scheduler;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 12/30/2014.
 */
public abstract interface SchedulerState extends Serializable{
    public abstract boolean isRunning();

    public abstract Date getWakeupTime();
}
