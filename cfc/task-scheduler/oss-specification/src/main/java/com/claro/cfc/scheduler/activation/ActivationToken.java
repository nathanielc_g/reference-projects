package com.claro.cfc.scheduler.activation;

import java.io.Serializable;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/5/2014.
 */
public abstract interface ActivationToken extends Serializable{
}
