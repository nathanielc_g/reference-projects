package com.claro.cfc.scheduler;

import java.io.Serializable;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 12/30/2014.
 */
public abstract interface SchedulerMonitor extends Serializable{
    public abstract SchedulerState getState();
}
