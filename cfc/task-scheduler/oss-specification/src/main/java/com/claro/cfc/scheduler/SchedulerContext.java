package com.claro.cfc.scheduler;

import java.io.Serializable;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/5/2014.
 */
public abstract interface SchedulerContext extends Serializable{
    public abstract SchedulerMonitor getMonitor();
}
