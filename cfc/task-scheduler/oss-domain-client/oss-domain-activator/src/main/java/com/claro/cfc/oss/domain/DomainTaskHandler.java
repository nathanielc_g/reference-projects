package com.claro.cfc.oss.domain;

import com.claro.cfc.oss.environment.Environment;
import com.claro.cfc.oss.environment.Environments;
import com.claro.cfc.oss.utils.GenericTaskExecutionResult;
import com.claro.cfc.oss.utils.StringUtils;
import com.claro.cfc.oss.utils.TaskResponseParser;
import com.claro.cfc.scheduler.activation.ActivationContext;
import com.claro.cfc.scheduler.tasks.*;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.util.List;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 9/29/2014.
 */
public class DomainTaskHandler implements TaskHandler {

    private static final String HANDLER_NAME = "Domain OSS Client Service";
    private ActivationContext context;

    // E0DC-EFE4-921C-E890
    private static final String SECURITY_KEY_QA = "8902-0246-4577-B154";
    private static final String SECURITY_KEY_PR = "8902-0246-4577-B154";

    private static String DEFAULT_SECURITY_KEY = "";


    public DomainTaskHandler(ActivationContext context) {
        this.context = context;
    }

    public Configuration getConfiguration() {
        return new Configuration() {
        };
    }

    public String getDisplayName() {
        return HANDLER_NAME;
    }


    public TaskExecutionResult perform(Task task) {
        Logger logger = Logger.getLogger(DomainTaskHandler.class);
        logger.info(HANDLER_NAME + "(: perform work for task ) " + task);
        return run(task, logger);
    }

    private TaskExecutionResult run(Task task, Logger logger) {
        Environment env = Environments.getDefault();
        if( DEFAULT_SECURITY_KEY.isEmpty() ) {
            DEFAULT_SECURITY_KEY = Environment.PRODUCTION == env ? SECURITY_KEY_PR : SECURITY_KEY_QA;

            logger.info( "Using "+env+" services security key "+DEFAULT_SECURITY_KEY );
        }

        String message = getMessage(task, logger);
        logger.info(HANDLER_NAME + "(:perform:) sending message");
        logger.info(HANDLER_NAME + "(:perform:) "+message);

        if (null == message) {
            logger.info(HANDLER_NAME + "(:perform:) skipping work due on null message");
            return null;
        }

        String result = null;

        try{
            result = sendMessage(message, logger);
        }catch ( Exception ex ){
           logger.error( "***** Error trying to send update message for "+HANDLER_NAME );
           ex.printStackTrace();
        }

        if (null == result) {
            logger.warn(HANDLER_NAME + "Some error trying to fetch response for service");
            return null;
        }

        GenericTaskExecutionResult taskResult = TaskResponseParser.toResult(result);
        if (null != taskResult) {
            taskResult.setOutgoingXml(message);
            taskResult.setIngoingXml(result);
        }

        return taskResult;
    }

    private String getMessage(Task task, Logger logger) {
        List<Attribute> attrs = task.getAttributes();
        if (null == attrs || 0 == attrs.size()) {
            logger.info(HANDLER_NAME + "(:getMessage:) getting not valid attributes from task ( " + task + " )");
            return null;
        }

        int index = 0;

        String phone = "empty";
        String localidad = "empty";
        String parFeeder = "empty";
        boolean isCuentaFija = false;
        String parLocal = "";
        String terminal = "empty";


        if (0 <= (index = attrs.indexOf(Attribute.createAttribute("LOCALIDAD", "")))) //Frame is reported here
            localidad = StringUtils.asString(attrs.get(index).getValue());
        if (0 <= (index = attrs.indexOf(Attribute.createAttribute("PAR_LOCAL", ""))))
            parLocal = StringUtils.asString(attrs.get(index).getValue());
        if (0 <= (index = attrs.indexOf(Attribute.createAttribute("PAR_FEEDER", ""))))
            parFeeder = StringUtils.asString(attrs.get(index).getValue());
        if (0 <= (index = attrs.indexOf(Attribute.createAttribute("CUENTA_FIJA", "")))) {
            isCuentaFija = StringUtils.asString(attrs.get(index).getValue()).equalsIgnoreCase("SI") ? true : false;
        }if (0 <= (index = attrs.indexOf(Attribute.createAttribute("TERMINAL", ""))))
            terminal = StringUtils.asString(attrs.get(index).getValue());
        if (0 <= (index = attrs.indexOf(Attribute.createAttribute("TELEFONO", "")))) {
            phone = StringUtils.asString(attrs.get(index).getValue());
            if( null != phone ){
                phone = phone.replace( "-","" ).replace("(","").replace(")","").replace(" ","");
            }
        }

        String msg = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.domapi.domainplus.vso.vz.com/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">\n" +
                "   <soapenv:Header>\n" +
                "                <wsse:Security>\n" +
                "                <wsse:key>\n" +
                "                <wsse:value>"+DEFAULT_SECURITY_KEY+"</wsse:value>\n" +
                "                \t </wsse:key>\n" +
                "                </wsse:Security>   \n" +
                "   </soapenv:Header>\n" +
                "   <soapenv:Body>\n" +
                "      <ws:updateSubscriptionCFC>\n" +
                "         <cfcRequest>\n" +
//                "            <centralOffice>"+localidad+"</centralOffice>\n" +
                "            <frame>"+localidad+"</frame>\n" +
                "            <cuentaFija>"+isCuentaFija+"</cuentaFija>\n" +
                "            <distribution>"+parLocal+"</distribution>\n" +
                "            <feeder>"+parFeeder+"</feeder>\n" +
                "            <terminal>"+terminal+"</terminal>\n" +
                "            <tn>"+phone+"</tn>\n" +
                "         </cfcRequest>\n" +
                "      </ws:updateSubscriptionCFC>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>\n";
        return msg;
    }

    private String sendMessage(String message, Logger logger) {
        try {
            Environment env = Environments.getDefault();

            logger.info(HANDLER_NAME + "(:sendMessage:) trying to send message to endpoint on " + env.domainHost());

            HttpURLConnection con = (HttpURLConnection) new URL(env.domainHost()).openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "");
            con.connect();

            OutputStream out = con.getOutputStream();
            if (null != out) {
                out.write(message.getBytes());
            }
            out.flush();
            out.close();

            InputStream in = con.getInputStream();
            ByteBuffer buffer = ByteBuffer.allocate(8192);
            ReadableByteChannel channel = Channels.newChannel(in);

            StringBuilder builder = new StringBuilder();

            while( 0<channel.read(buffer) ){
                buffer.flip();
                builder.append( Charset.forName("UTF-8").decode(buffer) );
                buffer.clear();
            }

            in.close();
            con.disconnect();
            String result = builder.toString().trim();
            logger.info(HANDLER_NAME + "(:sendMessage:) getting response ( " + result + " )\n");
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
