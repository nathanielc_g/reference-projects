package com.claro.cfc.scheduler.util;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * Created by Nathaniel Calderon on 6/20/2017.
 */
public class AppUtilsTest {
    @Test()
    public void testIsTaskFilterEnable() throws Exception {
        assertTrue(AppUtils.isTaskFilterEnable() == false);
    }

    @Test
    public void testGetSchedulerWakeupFrequency() throws Exception {
        assertTrue(AppUtils.getSchedulerWakeupFrequency().equals("05mm"));
    }

}