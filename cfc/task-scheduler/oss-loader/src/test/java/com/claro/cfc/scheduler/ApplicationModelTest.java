package com.claro.cfc.scheduler;

import com.claro.cfc.scheduler.services.model.ApplicationModel;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * Created by Nathaniel Calderon on 5/3/2017.
 */
public class ApplicationModelTest {
    @Test(enabled = false)
    public void testIfTaskFilterIsEnabled () {
        assertTrue(ApplicationModel.isTaskFilterEnable(), "Task filter is enabled.");
    }

    @Test(enabled = false)
    public void testIfTaskFilterIsDisabled () {
        assertFalse(ApplicationModel.isTaskFilterEnable(), "Task filter is disabled.");
    }
}
