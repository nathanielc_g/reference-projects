package com.claro.cfc.scheduler;

import javax.ejb.TimerService;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Jansel R. Abreu  (jrodr) on 6/6/2014.
 */
public final class SchedulerContextImp implements SchedulerContext, Serializable{

    /**
     * `static` property is very important for this class, because clients have unique instance
     * of SchedulerContext and all created timer by Manage Server have the same context that clients have.
     *
     * This is because clients can be lived no matter if timers are detach from server. All timer instance
     * created by server will have same instance as client have, and would be possible for any timer to update
     * the SchedulerContext instance that clients have.
     */
    private static final SchedulerContextImp single = new SchedulerContextImp();

    private SchedulerMonitor monitor;

    private SchedulerContextImp(){
    }

    static SchedulerContextImp getDefault( ){
        return single;
    }


    @Override
    public SchedulerMonitor getMonitor() {
        prepareMonitor();
        return monitor;
    }

    private void prepareMonitor(){
        if(null == monitor )
            monitor = new SchedulerMonitorImp();
    }

    void setMonitorStateRunning(boolean isRunning){
        prepareMonitor();
        ((SchedulerStateImp)monitor.getState()).setRunning(isRunning);
    }

    void setMonitorStateWakeupTime(Date wakeupTime){
        prepareMonitor();
        ((SchedulerStateImp)monitor.getState()).setWakeupTime(wakeupTime);
    }
}
