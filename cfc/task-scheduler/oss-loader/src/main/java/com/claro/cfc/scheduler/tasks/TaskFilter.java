package com.claro.cfc.scheduler.tasks;

/**
 * Created by Jansel Valentin on 6/1/2016.
 **/
public interface TaskFilter<T extends Task> {
    boolean accept(T task);
}
