package com.claro.cfc.scheduler.services;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/6/2014.
 */
@Entity
@Table(name = "TASK_TYPE")
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE,region = "tasks.type")
public class TaskTypeEntity {
    private long taskTypeId;
    private String typeName;
    private List<TaskEntity> tasks;

    @Id
    @Column(name = "TASK_TYPE_ID")
    public long getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(long taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    @Basic
    @Column(name = "TYPE_NAME")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public String toString() {
        return "TaskTypeEntity{" +
                "taskTypeId=" + taskTypeId +
                ", typeName='" + typeName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskTypeEntity)) return false;

        TaskTypeEntity that = (TaskTypeEntity) o;

        return taskTypeId == that.taskTypeId;
    }

    @Override
    public int hashCode() {
        return (int) (taskTypeId ^ (taskTypeId >>> 32));
    }

    @Fetch(FetchMode.SELECT)
    @OneToMany(mappedBy = "type")
    public List<TaskEntity> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskEntity> tasks) {
        this.tasks = tasks;
    }
}
