package com.claro.cfc.scheduler.model.enums;

/**
 * Created by Nathaniel Calderon on 7/12/2017.
 */
public enum NotificationStatusEnum {
    PENDING(0), SENT(1);
    private int code;

    NotificationStatusEnum(int code) {
        this.code = code;
    }

    public int code() {
        return code;
    }
}
