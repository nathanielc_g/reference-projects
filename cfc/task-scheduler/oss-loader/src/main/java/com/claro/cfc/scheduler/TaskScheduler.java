package com.claro.cfc.scheduler;

import com.claro.cfc.scheduler.activation.ServiceActivator;
import com.claro.cfc.scheduler.observer.TaskBroadcaster;
import com.claro.cfc.scheduler.services.TaskServiceEntity;
import com.claro.cfc.scheduler.services.model.ServiceModel;
import com.claro.cfc.scheduler.tasks.TaskHandler;
import com.claro.cfc.scheduler.util.AppUtils;
import com.claro.cfc.scheduler.util.CalendarHelper;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.ejb.Timer;
import java.lang.ref.WeakReference;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/6/2014.
 */
@Remote
@Stateless(name = "Scheduler", mappedName = "TaskScheduler")
public class TaskScheduler implements Scheduler {

    private static final long MAX_ACTIVATION_RETRY_INTENT = 4;
    private static final long STUCK_THREAD_MAX_TIME = 5;
    private static AtomicBoolean isRunning = new AtomicBoolean(false);
    private final Logger log;
    @Resource
    private TimerService ts;
    private int retryIntent = 1;
    private String name;
    private SchedulerContextImp schedulerContext;
    private boolean reloadCache = false;

    public TaskScheduler() {
        this.name = "TaskScheduler[" + Thread.currentThread().getId() + ":" + System.identityHashCode(Thread.currentThread()) + "]";
        log = Logger.getLogger(name);
        log.info("Creating TaskScheduler....");
    }

    public String getName() {
        return name;
    }

    public void start() {
        prepareSchedulerContext();
        setStart();

        if (null != schedulerContext) {
            log.info("Informing schedulerContext: start->setMonitorStateRunning");
            schedulerContext.setMonitorStateRunning(true);
        }
    }


    @Timeout
    public void wakeup(Timer timer) {
        if (isRunning.get()) {
            log.info("Avoiding that another timer perform the work that is currently executed by another timer,total timers " + ts.getTimers().size());
            return;
        }

        isRunning.set(true);

        retryIntent = 1;
        boolean done = false;
        log.info("************* Number of timers " + ts.getTimers().size() + " on thread [" + Thread.currentThread().getId() + ":" + System.identityHashCode(Thread.currentThread()) + "] ******************");
        log.info("TaskScheduler timeout collapsed, Initiating OSS update...");

        prepareSchedulerContext();

        while (!done && MAX_ACTIVATION_RETRY_INTENT >= retryIntent) {
            try {
                List<TaskServiceEntity> services = loadServices();
                WeakReference<Map<Long, TaskHandler>> handlersRef = ServiceActivator.activate(schedulerContext, services);

                if (null != handlersRef && null != handlersRef.get()) {
                    final Map<Long, TaskHandler> handlers = handlersRef.get();
                    if (null != handlers) {
                        scheduleHandlers(handlers);

                        handlers.clear();
                        services.clear();
                        done = true;
                    }
                } else {
                    log.info("Reference to handler report garbage collected trying it again");
                }
            } catch (Exception ex) {
                done = false;
                log.error(ex);
                ex.printStackTrace();
            }

            /**
             * If exists an error during activation, exponential backoff algorithm is used to retry intent
             */
            if (!done) {
                try {
                    int backoff = 0;
                    if (MAX_ACTIVATION_RETRY_INTENT >= retryIntent) {
                        backoff = (int) Math.floor(Math.pow(2, retryIntent) - 1);
                        backoff = new Random(backoff + 1).nextInt(backoff + 1);
                    }
                    log.error("There was an  error trying to activate service retrying  in " + backoff + " minutes  on retryIntent #" + retryIntent);
                    TimeUnit.MINUTES.sleep(backoff);
                    ++retryIntent;
                } catch (InterruptedException ex) {
                    log.error(ex);
                    ex.printStackTrace();
                }
            }
        }
        if (done)
            log.info("OSS Activation task performed successfully!");
        else {
            log.info("OSS Update could not be possible due on activation problem.");
            destroy();
        }

        scheduleTimerForFuture();
        isRunning.set(false);
    }


    public SchedulerContext getContext() {
        prepareSchedulerContext();
        return schedulerContext;
    }

    @Override
    public void reloadCache() {
        reloadCache = true;
    }

    public void stop() {
        if (null != schedulerContext) {
            log.info("Informing schedulerContext: stop->setMonitorStateRunning");
            schedulerContext.setMonitorStateRunning(false);
        }
        cancelAllPendingTimers();
        destroy();
    }


    private void prepareSchedulerContext() {
        if (null == schedulerContext)
            schedulerContext = SchedulerContextImp.getDefault();
    }


    /*private void scheduleTimerForFuture() {
        scheduleTimerForFutureProduction();
    }*/

    private void scheduleTimerForFuture() {
        Calendar now = CalendarHelper.toDay();
        Calendar nextWakeupTime = SchedulerTime.getWakeupTime();
        ts.createTimer(Math.abs(nextWakeupTime.getTimeInMillis() - now.getTimeInMillis()), null);
        log.info("\n\n*********** Re-Scheduled at " + now.getTime() + " to wake up at " + nextWakeupTime.getTime() + " ***********\n");
        setNextWakeupTimeOnContext(nextWakeupTime);
    }

    private void setNextWakeupTimeOnContext(Calendar nextWakeupTime) {
        if (null != schedulerContext) {
            log.info("Informing schedulerContext: scheduleTimerForFuture->setMonitorStateWakeupTime");
            schedulerContext.setMonitorStateWakeupTime(nextWakeupTime.getTime());
        }
    }

    private void setStart() {
        /**
         * Esto es para evitar que existan mas de un timer en ejecucion
         */
        cancelAllPendingTimers();

        Calendar now = CalendarHelper.toDay();
        Calendar nextWakeupTime = SchedulerTime.getWakeupTime();

        log.info("\n\n*********** Scheduled at " + now.getTime() + " to wake up at " + nextWakeupTime.getTime() + "****************\n");
        ts.createTimer(Math.abs(nextWakeupTime.getTimeInMillis() - now.getTimeInMillis()), null);

        if (null != schedulerContext) {
            log.info("Informing schedulerContext: setStart->setMonitorStateWakeupTime");
            schedulerContext.setMonitorStateWakeupTime(nextWakeupTime.getTime());
        }
    }

    private List<TaskServiceEntity> loadServices() {
        return ServiceModel.getServices();
    }

    private void scheduleHandlers(Map<Long, TaskHandler> handlers) {
        if (null == handlers || 0 == handlers.size()) {
            log.info("No Services available, " + handlers);
            destroy();
            return;
        }

        if (reloadCache) {
            AppUtils.reloadConfigValues();
            reloadCache = false;
        }
        TaskBroadcaster broadcaster = new TaskBroadcaster();
        final int sThreadCount = 5;
        try {
            final CountDownLatch sync = new CountDownLatch(handlers.size());
            log.info("Scheduling " + handlers.size() + " handlers");

            final ExecutorService exec = Executors.newFixedThreadPool(sThreadCount);

            for (long context : handlers.keySet()) {
                TaskHandler handler = handlers.get(context);
                if (null == handler) {
                    sync.countDown();
                    continue; // This would never happens
                }

                /**
                 * Here we ensure that AsyncTaskWorkers don't hang TaskScheduler when they hang
                 */
                if (null != handler.getConfiguration() && handler.getConfiguration().taskIsEnabled()) {
                    final AsyncTasksWorker worker = new AsyncTasksWorker(context, handler, sync, this);
                    worker.registerObserver(broadcaster);
                    exec.execute(worker);

                    log.info("AsyncTasksWorker(:context:" + context + ") has been put on execution...");
                } else {
                    sync.countDown();
                }
            }
            // TODO: Temporary workaround to ensure "Async stuck threads" don't stay running endless.
            sync.await(1, TimeUnit.DAYS);
            exec.shutdownNow();
            log.info("All service were performed successfully");
            broadcaster.sendResults();
        } catch (InterruptedException ex) {
            log.error("Error trying to get blocking operation ", ex);
        } finally {
            broadcaster.cleanup();
            broadcaster = null;
        }
    }


    private void cancelAllPendingTimers() {
        Collection<Timer> timers = ts.getTimers();
        for (Timer timer : timers) {
            log.info("Stopping timer wih remaining timeout " + timer.getTimeRemaining());
            timer.cancel();
        }
        log.info("Stopping " + name + "...");
    }

    private void destroy() {
        log.info("Destroying " + name + "...");
        schedulerContext = null;
    }
}

