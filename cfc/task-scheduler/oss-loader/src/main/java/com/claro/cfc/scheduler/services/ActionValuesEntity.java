package com.claro.cfc.scheduler.services;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/6/2014.
 */
@Entity
@Table(name = "ACTION_VALUES")
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE,region = "actions")
public class ActionValuesEntity {
    private long valueId;
    private String akey;
    private String avalue;
    private Timestamp created;
    private long actionId;

    @Id
    @Column(name = "VALUE_ID")
    @GeneratedValue(generator = "values.sec_action_values",strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "values.sec_action_values",sequenceName = "sec_action_values")
    public long getValueId() {
        return valueId;
    }

    public void setValueId(long valueId) {
        this.valueId = valueId;
    }

    @Basic
    @Column(name = "AKEY")
    public String getAkey() {
        return akey;
    }

    public void setAkey(String akey) {
        this.akey = akey;
    }

    @Basic
    @Column(name = "AVALUE")
    public String getAvalue() {
        return avalue;
    }

    public void setAvalue(String avalue) {
        this.avalue = avalue;
    }

    @Basic
    @Column(name = "CREATED")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "ACTION_ID")
    public long getActionId() {
        return actionId;
    }

    public void setActionId(long actionId) {
        this.actionId = actionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ActionValuesEntity)) return false;

        ActionValuesEntity that = (ActionValuesEntity) o;

        return valueId == that.valueId;
    }

    @Override
    public int hashCode() {
        return (int) (valueId ^ (valueId >>> 32));
    }

    @Override
    public String toString() {
        return "ActionValuesEntity{" +
                "valueId=" + valueId +
                ", akey='" + akey + '\'' +
                ", avalue='" + avalue + '\'' +
                ", created=" + created +
                ", actionId=" + actionId +
                '}';
    }
}
