package com.claro.cfc.scheduler.services.model.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/6/2014.
 */
public final class HibernateHelper {

    private static final SessionFactory mSessionFactory = sharedSessionFactory();

    /*private static final ThreadLocal<SessionFactory> threadLocal = new ThreadLocal<SessionFactory>() {
        @Override
        protected SessionFactory initialValue() {
            return new AnnotationConfiguration().configure().buildSessionFactory();
        }
    };*/

    private static final SessionFactory sharedSessionFactory(){
        Configuration config = new Configuration();
        config.configure();
        ServiceRegistry registry = new ServiceRegistryBuilder().applySettings(config.getProperties()).buildServiceRegistry();
        return config.buildSessionFactory(registry);
    }

    public static final SessionFactory getSessionFactory() {
//        return threadLocal.get();
        return mSessionFactory;
    }
}
