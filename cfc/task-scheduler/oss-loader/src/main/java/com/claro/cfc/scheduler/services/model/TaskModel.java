package com.claro.cfc.scheduler.services.model;

import com.claro.cfc.scheduler.services.*;
import com.claro.cfc.scheduler.services.model.util.HibernateHelper;
import com.claro.cfc.scheduler.tasks.Attribute;
import com.claro.cfc.scheduler.tasks.TaskImp;
import com.claro.cfc.scheduler.tasks.TaskResponseStatus;
import com.claro.cfc.scheduler.util.CalendarHelper;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import java.lang.ref.WeakReference;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/6/2014.
 */
public final class TaskModel {

    private static final Logger log = Logger.getLogger(TaskModel.class);

    private static final String mTaskQuery = "SELECT TASK_ID,ACTION_ID FROM TASK WHERE STATUS = :STATUS AND CREATED >= :GT AND CREATED < :LT AND TASK_TYPE_ID = :TCONTEXT ";

    private static final String mActionValues = "SELECT AKEY,AVALUE FROM ACTION_VALUES WHERE ACTION_ID = :ACTION_ID";

    private static final int BATCH_MAX_RESULT = 500;


    public static WeakReference<List<TaskImp>> getTaskByContext(Long context) {
        final StatelessSession session = HibernateHelper.getSessionFactory().openStatelessSession();
        try {
//            Date toDay = CalendarHelper.removeTime(CalendarHelper.toDay().getTime());
            Date yesterday = CalendarHelper.removeTime(CalendarHelper.yesterday().getTime());
            Date tomorrow = CalendarHelper.removeTime(CalendarHelper.tomorrow().getTime());

            log.info("\n\n************* Getting task from " + yesterday + " to " + tomorrow + " with status " + TaskStatusEnum.PENDING.id() + " *************\n");
            SQLQuery query = session.createSQLQuery( mTaskQuery )
             .addScalar("TASK_ID", LongType.INSTANCE)
                    .addScalar("ACTION_ID", LongType.INSTANCE);

            query.setInteger("STATUS", TaskStatusEnum.PENDING.id());
            query.setDate("GT", yesterday);
            query.setDate("LT", tomorrow);
            query.setLong("TCONTEXT", context);
            query.setReadOnly(true).setCacheable(false).setFetchSize(BATCH_MAX_RESULT);
            ScrollableResults results = query.scroll(ScrollMode.FORWARD_ONLY);
            List<TaskImp> tasks = new LinkedList<TaskImp>();

            while (null != results && results.next()) {
                long taskId   = results.getLong(0);
                long actionId = results.getLong(1);
                tasks.add(new TaskImp(taskId, actionId, getAttributes(session, actionId)));
            }
            return new WeakReference<List<TaskImp>>(tasks);

        } catch (HibernateException ex) {
            log.error("Error at getTaskByContext call ", ex);
        } finally {
            if (null != session)
                session.close();
        }
        return new WeakReference<List<TaskImp>>(Collections.EMPTY_LIST);
    }


//     @Deprecated( "replaced bu the methdo below" )
//    public static List<Attribute> getAttributes(Long actionId) {
//        final Session session = HibernateHelper.getSessionFactory().openSession();
//        try {
//            Criteria criteria = session.createCriteria(ActionValuesEntity.class);
//            criteria.setCacheable(true);
//
//            criteria.add(Restrictions.eq("actionId", actionId));
//
//            List<Attribute> attributes = new LinkedList<Attribute>();
//
//            List<ActionValuesEntity> actionValues = criteria.list();
//            for (int i = 0; i < actionValues.size(); ++i) {
//                ActionValuesEntity av = actionValues.get(i);
//
//                attributes.add(Attribute.createAttribute(av.getAkey(), av.getAvalue()));
//            }
//
//            return attributes;
//        } catch (HibernateException ex) {
//            log.error("Error ar getAttributes(" + actionId + ") call ", ex);
//        } finally {
//            if (session.isOpen() || session.isConnected()) {
//                session.flush();
//                session.close();
//            }
//        }
//
//        return Collections.EMPTY_LIST;
//    }
//

    public static List<Attribute> getAttributes(Long actionId) {
        final StatelessSession session = HibernateHelper.getSessionFactory().openStatelessSession();
        try {
            SQLQuery query = session.createSQLQuery(mActionValues);

            query.setInteger("ACTION_ID", actionId.intValue());
            query.setCacheable(false).setReadOnly(true);

            ScrollableResults results = query.scroll(ScrollMode.FORWARD_ONLY);

            List<Attribute> attributes = new LinkedList<Attribute>();

            while (null != results && results.next()) {
                Object[] row = results.get();

                final String key = row[0] + "";
                final String value = row[1] + "";
                attributes.add(Attribute.createAttribute(key, value));
            }

            return attributes;
        } catch (HibernateException ex) {
            log.error("Error ar getAttributes(" + actionId + ") call ", ex);
        } finally {
            if (null != session) {
                session.close();
            }
        }

        return Collections.EMPTY_LIST;
    }

    public static List<Attribute> getAttributes(StatelessSession session, Long actionId) {
        ScrollableResults results = null;
        try {
            SQLQuery query = session.createSQLQuery(mActionValues)
                    .addScalar("AKEY", StringType.INSTANCE)
                    .addScalar("AVALUE", StringType.INSTANCE);
            query.setInteger("ACTION_ID", actionId.intValue());
            query.setCacheable(false).setReadOnly(true);
            results = query.scroll(ScrollMode.FORWARD_ONLY);
            List<Attribute> attributes = new LinkedList<Attribute>();
            while (null != results && results.next()) {
                final String key = results.getString(0);
                final String value = results.getString(1);
                attributes.add(Attribute.createAttribute(key, value));
            }
            results.close();
            return attributes;
        } catch (HibernateException ex) {
            log.error("Error ar getAttributes(" + actionId + ") call ", ex);
        } finally {
            if (results != null)
                results.close();
        }
        return Collections.EMPTY_LIST;
    }


    public static boolean setActionToProcessed(Long actionId) {
        final Session session = HibernateHelper.getSessionFactory().openSession();
        boolean updated = true;

        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            Date current = CalendarHelper.toDay().getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss ");
            String date = formatter.format(current);
            Query query = session.createSQLQuery("UPDATE ACTIONS SET PROCESSED = 1, PROCESSED_DATE='" + date + "' WHERE ACTION_ID="+actionId);
            query.setCacheable(true);

            query.executeUpdate();

            tx.commit();
        } catch (HibernateException ex) {
            updated = false;
            log.error("Error at setActionToProcessed(" + actionId + ") call", ex);
            if (null != tx && !tx.wasCommitted()) {
                tx.rollback();
            }
        } finally {
            if (session.isOpen() || session.isConnected()) {
                session.flush();
                session.close();
            }
        }

        return updated;
    }

    public static TaskStatusEntity getTaskStatus(TaskStatusEnum status) {
        final Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(TaskStatusEntity.class);
            criteria.setCacheable(true);

            criteria.add(Restrictions.eq("status", status.toString()));

            List<TaskStatusEntity> statuses = criteria.list();
            if (null != statuses && 0 != statuses.size())
                return statuses.get(0);

        } catch (HibernateException ex) {
            log.error("Error at getTaskStatus(" + status + ") call", ex);
        } finally {
            if (session.isOpen() || session.isConnected()) {
                session.flush();
                session.close();
            }
        }
        return null;
    }

    public static boolean updateTaskStatus( Long taskId, TaskStatusEnum status ){
        TaskStatusEntity statusEntity = getTaskStatus(status);

        boolean updated = false;
        if( null != statusEntity ){
            final Session session = HibernateHelper.getSessionFactory().openSession();
            Transaction tx = null;
            try{
                tx = session.beginTransaction();

                Timestamp current = new Timestamp( new Date().getTime() );

                Query query = session.getNamedQuery( "tasks.updateStatus");
                query.setCacheable(true);

                query.setParameter( "status",statusEntity );
                query.setParameter( "modified",current );
                query.setParameter( "taskId", taskId );

                query.executeUpdate();

                tx.commit();
                updated = true;
            }catch ( HibernateException ex ){
                log.error( "Error at updateTaskStatus("+taskId+","+status+") call ",ex );
                if( null != tx && !tx.wasCommitted() )
                    tx.rollback();
                updated = false;
            }finally {
                if( session.isOpen() || session.isConnected() ){
                    session.flush();
                    session.close();
                }
            }
        }
        return updated;
    }

    public static boolean insertTaskLog( TaskLogEntity taskLog ){
        boolean saved = false;
        if( null != taskLog ) {
            final Session session = HibernateHelper.getSessionFactory().openSession();
            Transaction tx = null;

            try{
                tx = session.beginTransaction();
                session.saveOrUpdate(taskLog);
                tx.commit();
               saved = true;
            }catch ( HibernateException ex ){
                log.error( "Error at insertTaskLog("+taskLog+") call ",ex );
                if( null == tx && !tx.wasCommitted() )
                    tx.rollback();
            }finally {
                if( session.isOpen() || session.isConnected() ){
                    session.flush();
                    session.close();
                }
            }
        }
        return saved;
    }

    public static TaskResponseTypeEntity getTaskResponseType( TaskResponseStatus type ){
        final Session session = HibernateHelper.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(TaskResponseTypeEntity.class);
            criteria.setCacheable(true);

            type = null == type ? TaskResponseStatus.UNKNOWN : type;
            criteria.add(Restrictions.eq("rtype", type.toString()));

            List<TaskResponseTypeEntity> types = criteria.list();
            if (null != types && 0 != types.size())
                return types.get(0);

        } catch (HibernateException ex) {
            log.error("Error at getTaskResponseType(" + type + ") call", ex);
        } finally {
            if (session.isOpen() || session.isConnected()) {
                session.flush();
                session.close();
            }
        }
        return null;
    }
}

