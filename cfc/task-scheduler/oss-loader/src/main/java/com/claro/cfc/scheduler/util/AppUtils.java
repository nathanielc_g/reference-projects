package com.claro.cfc.scheduler.util;

import com.claro.cfc.scheduler.services.model.ApplicationModel;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Created by Nathaniel Calderon on 6/13/2017.
 */
public class AppUtils {
    private static final Logger log = Logger.getLogger(AppUtils.class.getName());

    private static boolean isTaskFilterActivated = false;
    private static String wakeupFrequency = "01mm";
    private static String SMSAllowedForContext = "21,22,24";
    /*private static final RefreshableProperty<Boolean> taskFilter = new RefreshableProperty<Boolean>(true, 1, TimeUnit.DAYS, new TaskFilterStatus());
    private static final RefreshableProperty<String> wakeupFrequency = new RefreshableProperty<String>(true, 1, TimeUnit.DAYS, new SchedulerWakeupFrequency());*/

    static {
        log.info("Initializing Application Config values on AppUtils.");
        reloadConfigValues();
    }

    // Supresses non-constructor, ensuring non-instantiability.
    private AppUtils () { }

    public static final boolean isTaskFilterEnable () {
        return isTaskFilterActivated;
        //return taskFilter.getValue();
    }

    public static final String getSchedulerWakeupFrequency () {
        return wakeupFrequency;
        //return wakeupFrequency.getValue();
    }

    public static boolean isSendSMSActiveFor(long context) {
        return isSendSMSActiveFor(context + "");
    }
    public static boolean isSendSMSActiveFor(String context) {
        return SMSAllowedForContext.indexOf(context) != -1;
    }

    public static void reloadConfigValues(){
        isTaskFilterActivated = ApplicationModel.isTaskFilterEnable();
        log.info("'isTaskFilterActivated' value was refreshed to " + isTaskFilterActivated+", at "+new Date() );
        wakeupFrequency = ApplicationModel.getSchedulerWakeupFrequency();
        log.info("'WakeupFrequency' value was refreshed to " + wakeupFrequency+", at "+new Date() );
        SMSAllowedForContext = ApplicationModel.getSMSContextAllowed();
    }

/*    private static final class TaskFilterStatus implements RefreshableProperty.ValueAdapter<Boolean>{
        public Boolean get() {
            final boolean taskFilterStatus = ApplicationModel.isTaskFilterEnable();
            log.info("Refreshing 'TaskFilterStatus' value to " + taskFilterStatus+", at "+ new Date() );
            return taskFilterStatus;
        }
    }



    private static final class SchedulerWakeupFrequency implements RefreshableProperty.ValueAdapter<String>{
        public String get() {
            final String frequency = ApplicationModel.getSchedulerWakeupFrequency();
            log.info("Refreshing 'SchedulerWakeupFrequency' value to " + frequency +", at "+ new Date() );
            return frequency;
        }
    }*/

}
