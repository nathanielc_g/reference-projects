package com.claro.cfc.scheduler.observer;

import com.claro.cfc.scheduler.services.TaskStatusEnum;

/**
 * Created by Nathaniel Calderon on 7/18/2017.
 */
public interface TaskObserver {
    public void update(long actionId, TaskStatusEnum status, String message);
}
