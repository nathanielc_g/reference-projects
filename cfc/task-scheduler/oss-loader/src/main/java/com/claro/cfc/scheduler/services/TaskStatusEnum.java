package com.claro.cfc.scheduler.services;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/6/2014.
 */
public enum TaskStatusEnum {
    PENDING{
        @Override
        public int id() {
            return 1;
        }
    }, PROCESSED{
        @Override
        public int id() {
            return 2;
        }
    }, ABORTED{
        @Override
        public int id() {
            return 21;
        }
    };


    /**
     *
     * @return  data base record id, according with the default value inserted in CFC at schema creation time.
     */
    public abstract int id();
}
