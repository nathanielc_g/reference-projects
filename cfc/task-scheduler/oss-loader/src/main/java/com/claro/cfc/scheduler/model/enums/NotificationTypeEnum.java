package com.claro.cfc.scheduler.model.enums;

/**
 * Created by Nathaniel Calderon on 7/12/2017.
 */
public enum NotificationTypeEnum {
    TASK_RESULT(1);
    private int code;

    NotificationTypeEnum(int code) {
        this.code = code;
    }

    public int code() {
        return code;
    }
}
