package com.claro.cfc.scheduler;

import java.util.Date;
import java.util.Set;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 12/30/2014.
 */
public final class SchedulerStateImp implements SchedulerState {

    private boolean isRunning;
    private Date wakeupTime;

    public SchedulerStateImp() {
    }

    @Override
    public boolean isRunning() {
        Set<Thread> threads = Thread.getAllStackTraces().keySet();
        System.out.println( ">>>>>>>>Begin crawl thread stack traces" );
        for( Thread t : threads ){
            System.out.println( "Thread["+t.getId()+":"+System.identityHashCode(t)+"]" );
        }
        return false;
        /*return isRunning;*/
    }

    public void setRunning(boolean isRunning) {
        /*this.isRunning = isRunning;*/
    }

    @Override
    public Date getWakeupTime() {
        return wakeupTime;
    }

    public void setWakeupTime(Date wakeupTime) {
        this.wakeupTime = wakeupTime;
    }
}
