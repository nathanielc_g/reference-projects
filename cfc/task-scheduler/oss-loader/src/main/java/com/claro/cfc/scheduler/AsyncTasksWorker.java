package com.claro.cfc.scheduler;

import com.claro.cfc.scheduler.observer.TaskObservable;
import com.claro.cfc.scheduler.observer.TaskObserver;
import com.claro.cfc.scheduler.services.TaskLogEntity;
import com.claro.cfc.scheduler.services.TaskStatusEnum;
import com.claro.cfc.scheduler.services.model.TaskModel;
import com.claro.cfc.scheduler.tasks.*;
import com.claro.cfc.scheduler.util.AppUtils;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/6/2014.
 */
public final class AsyncTasksWorker implements Runnable, TaskObservable {

    private static final long STUCK_THREAD_MAX_TIME = 1;
    private static final long STUCK_QUERY_THREAD_MAX_TIME = 2;

    private final Logger log;

    private Long context;
    private String name;
    private TaskHandler handler;
    private CountDownLatch sync;
    private TaskScheduler scheduler;
    private TaskObserver observer;
    private boolean isSendSMSActive;

    public AsyncTasksWorker(Long context, TaskHandler handler, CountDownLatch sync, TaskScheduler scheduler) {
        this.context = context;
        this.handler = handler;
        this.sync = sync;
        this.scheduler = scheduler;
        name = "AsyncTasksWorker(:context:" + context + ")[" + scheduler.getName() + "]";
        log = Logger.getLogger(name);
    }


    public void run() {
        long start = System.currentTimeMillis();

        TaskFilter<Task> filter = null;
        if (AppUtils.isTaskFilterEnable())
            filter = new ScatTaskFilter("("+handler.getDisplayName()+")");

        final List<TaskImp> tasks = getTasks(filter);
        if(null == tasks || tasks.isEmpty()) {
            log.info(handler.getDisplayName() + ": 0 Pending tasks");
            if (null != sync)
                sync.countDown();
            return;
        }

        isSendSMSActive = AppUtils.isSendSMSActiveFor(context);
        log.info(Thread.currentThread() + " Running task for context " + context + ", begin, with " + tasks.size() + " tasks");
        ExecutorService executor = Executors.newSingleThreadExecutor();

        try {
            for (final TaskImp task : tasks) {
                try {
                    log.info(".run() method begin");

                    TaskExecutionResult result = null;
                    try {
                        result = executor.submit(new Callable<TaskExecutionResult>() {
                            public TaskExecutionResult call() throws Exception {
                                return handler.perform(task);
                            }
                        }).get(STUCK_THREAD_MAX_TIME, TimeUnit.MINUTES);
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.error("",e);
                        log.error(handler.getDisplayName() + " could not perform (task:" + task.getTaskId() + ",actionId:" + task.getActionId() + ") in 1 minutes, cancelled.!", e);
                    }
                    processTaskResult(handler, task, result);
                    log.info(".run() method end");
                } catch (Exception ex) {
                    log.error("*********** Error processing Task #" + task.getTaskId() + " for handler " + handler.getDisplayName() + " ***********");
                    log.error("",ex);
                    ex.printStackTrace();
                }
            }
        } finally {
            tasks.clear();
            if (null != sync)
                sync.countDown();
        }
        long end = System.currentTimeMillis();
        log.info("############ Task processing spent " + (TimeUnit.MILLISECONDS.toSeconds(end - start)) + " secs to complete. ############");
    }


    private void processTaskResult(TaskHandler handler, Task task, TaskExecutionResult result) {
        if (null == result) {
            log.error("Error: TaskScheduler is getting null TaskExecutionResult for " + handler.getDisplayName() + " handler ");
            return;
        }

        TaskStatusEnum status = !result.success() ? TaskStatusEnum.ABORTED : TaskStatusEnum.PROCESSED;

        log.info("********** Updating Action " + task.getActionId() + " *********** ");
        log.info("********** TaskResultInfo  " + result + " **********");
        TaskModel.setActionToProcessed(task.getActionId());
        TaskModel.updateTaskStatus(task.getTaskId(), status);

        Timestamp current = new Timestamp(new Date().getTime());

        TaskLogEntity taskLog = new TaskLogEntity();
        taskLog.setCreated(current);
        taskLog.setMessage(result.getMessage());
        taskLog.setSuccess(result.success() ? 1 : 0);
        taskLog.setTaskId(task.getTaskId());
        taskLog.setIngoingXml(result.getIngoingXml());
        taskLog.setOutgoingXml(result.getOutgoingXml());
        taskLog.setResponseType(TaskModel.getTaskResponseType(result.getResponseStatus()));
        taskLog.setUpdatedAttrs(result.getUpdatedAttrs());
        TaskModel.insertTaskLog(taskLog);
        // Add pending sms notification for this result
        if(isSendSMSActive)
            notifyObserver(task.getActionId(), status, result.getMessage());
    }

    private List<TaskImp> getTasks(TaskFilter filter) {
        //List<TaskImp> tasks = TaskModel.getTaskByContext(context).get();
        List<TaskImp> tasks  = getPendingTasks();
        if( null == filter )
            return tasks;

        List<TaskImp> filtered = new LinkedList();
        for( Task task : tasks ) {
            if (filter.accept(task)) {
                filtered.add((TaskImp) task);
            }else{
                log.warn("skipping (task="+task.getTaskId()+", action="+task.getActionId()+")");
            }
        }
        return filtered;
    }

    private List<TaskImp> getPendingTasks() {
        ExecutorService service = Executors.newSingleThreadExecutor();
        try {
            return service.submit(new Callable<List<TaskImp>>(){
                public List<TaskImp> call() throws Exception {
                    return TaskModel.getTaskByContext(context).get();
                }
            }).get(STUCK_QUERY_THREAD_MAX_TIME, TimeUnit.MINUTES);
        } catch (Exception ex) {
            log.error("Error: TaskScheduler cannot get tasks from db for " + handler.getDisplayName() + " handler.\n",ex);
            ex.printStackTrace();
        }
        return Collections.emptyList();
    }

    public void registerObserver(TaskObserver observer) {
        this.observer = observer;
    }

    private void notifyObserver(long actionId, TaskStatusEnum status, String message) {
        if (null == observer)
            return;
        observer.update(actionId, status, message);
    }

    @Override
    public String toString() {
        return "AsyncTasksWorker{" +
                "context=" + context +
                ", name='" + name + '\'' +
                ", handler=" + handler.getDisplayName() +
                '}';
    }
}
