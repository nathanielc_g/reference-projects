package com.claro.cfc.scheduler;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 12/30/2014.
 */
public final class SchedulerMonitorImp implements SchedulerMonitor {

    private SchedulerState state;


    public SchedulerMonitorImp() {
    }

    @Override
    public SchedulerState getState() {
        prepareState();
        return state;
    }

    private void prepareState() {
        if (null == state)
            state = new SchedulerStateImp();
    }
}
