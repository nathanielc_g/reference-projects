package com.claro.cfc.scheduler.services;

import javax.persistence.*;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/6/2014.
 */
@Entity
@Table(name = "TASK_SERVICE")
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE,region = "tasks.service")
public class TaskServiceEntity {
    private long tsid;
    private String serviceJndi;
    private String displayName;
    private long tcontext;

    @Id
    @Column(name = "TSID")
    public long getTsid() {
        return tsid;
    }

    public void setTsid(long tsid) {
        this.tsid = tsid;
    }

    @Basic
    @Column(name = "SERVICE_JNDI")
    public String getServiceJndi() {
        return serviceJndi;
    }

    public void setServiceJndi(String serviceJndi) {
        this.serviceJndi = serviceJndi;
    }

    @Basic
    @Column(name = "DISPLAY_NAME")
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Basic
    @Column(name = "TCONTEXT")
    public long getTcontext() {
        return tcontext;
    }

    public void setTcontext(long tcontext) {
        this.tcontext = tcontext;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskServiceEntity)) return false;

        TaskServiceEntity that = (TaskServiceEntity) o;

        return tsid == that.tsid;
    }

    @Override
    public int hashCode() {
        return (int) (tsid ^ (tsid >>> 32));
    }

    @Override
    public String toString() {
        return "TaskServiceEntity{" +
                "tsid=" + tsid +
                ", serviceJndi='" + serviceJndi + '\'' +
                ", displayName='" + displayName + '\'' +
                ", tcontext=" + tcontext +
                '}';
    }
}
