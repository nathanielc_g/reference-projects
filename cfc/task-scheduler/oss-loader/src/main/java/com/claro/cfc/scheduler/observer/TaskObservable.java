package com.claro.cfc.scheduler.observer;

/**
 * Created by Nathaniel Calderon on 7/18/2017.
 */
public interface TaskObservable {
    public void registerObserver(TaskObserver observer);
}
