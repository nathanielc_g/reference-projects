package com.claro.cfc.scheduler;

import com.claro.cfc.scheduler.util.AppUtils;
import com.claro.cfc.scheduler.util.CalendarHelper;
import java.util.Calendar;


/**
 * Created by Nathaniel Calderon on 4/24/2017.
 */
public class SchedulerTime {

    public static final Calendar getWakeupTime () {
        String frequency = AppUtils.getSchedulerWakeupFrequency();
        if (frequency.isEmpty())
            return getDefaultNextWakeupTime();
        else
            return getNextWakeupTime(frequency);
    }

    private static final Calendar getDefaultNextWakeupTime () {
        Calendar nextWakeupTime = CalendarHelper.tomorrow(21);
        Calendar now = CalendarHelper.toDay();
        if (nextWakeupTime.getTime().before(now.getTime()))
            nextWakeupTime.add(Calendar.DATE, 1);

        return nextWakeupTime;
    }

    private static final Calendar getNextWakeupTime (String frequency) {
        int minutes = parseFrequency(frequency);
        Calendar nextWakeupTime = CalendarHelper.nowPlusMinutes(minutes);
        return nextWakeupTime;
    }

    private static final int parseFrequency(String frequency) {
        // PATTERN: 05mm
        String duration = frequency.substring(0, 2);
        return Integer.parseInt(duration);
    }
}
