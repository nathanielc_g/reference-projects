package com.claro.cfc.scheduler;

import com.claro.cfc.oss.utils.ScatStatusCodeParser;
import com.claro.cfc.scheduler.tasks.Attribute;
import com.claro.cfc.scheduler.tasks.Task;
import com.claro.cfc.scheduler.tasks.TaskFilter;
import com.claro.cfc.scheduler.util.Environments;
import com.claro.cfc.scheduler.util.FilterServices;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by Jansel Valentin on 6/1/2016.
 **/
class ScatTaskFilter implements TaskFilter {
    private final Logger log = Logger.getLogger(ScatTaskFilter.class.getSimpleName());

    private static final String ITEM_CODE = "CODE";
    private static final String ITEM_TYPE = "TYPE";
    private static final String USER_CARD = "TARJETA";

    private String name;

    public ScatTaskFilter(String name) {
        this.name = name;
    }

    public boolean accept(Task task) {
        log.info(name + " going to filter task " + task.getTaskId() + " for action " + task.getActionId());

        Attribute itemCode = findAttribute(task.getAttributes(), ITEM_CODE);
        Attribute itemType = findAttribute(task.getAttributes(), ITEM_TYPE);
        Attribute userCard = findAttribute(task.getAttributes(), USER_CARD);

        if( null == itemCode || null == itemType || null == userCard) {
            log.error(name+" can't proceed with scat check due on nullity, code="+(null == itemCode)+",type="+(null==itemType)+",card="+(null == userCard));
            return false;
        }
        return checkScatStatusCode(getMessage(itemCode.getValue(),itemType.getValue(),userCard.getValue()));
    }

    private static Attribute findAttribute(List<Attribute> attributes, String key) {
        if( null == attributes )
            return null;

        int idx;
        if ( 0 > (idx = attributes.indexOf( Attribute.createAttribute(key,null))))
            return null;
        return attributes.get(idx);
    }


    private boolean checkScatStatusCode(String message){
        try {
            // these environments are oss environments
            String hostingService = FilterServices.scat(Environments.getDefault());

            log.info(name+" trying to check SCAT status code on host " + hostingService);

            HttpURLConnection con = (HttpURLConnection) new URL(hostingService).openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", "http://tempuri.org/ScatCfcServicesCodes");
            con.connect();

            OutputStream out = con.getOutputStream();
            if (null != out) {
                out.write(message.getBytes());
            }
            out.flush();
            out.close();

            byte[] buffer = new byte[8192];
            InputStream in = con.getInputStream();
            StringBuilder builder = new StringBuilder();

            if (null != in) {
                int i;
                for (; ; ) {
                    i = in.read(buffer);
                    if (0 >= i)
                        break;
                    builder.append(new String(buffer));
                }
            }
            in.close();
            con.disconnect();
            String result = builder.toString().trim();
            log.info(name + "(:checkScatStatusCode:) getting response ( " + result + " )\n");

            ScatStatusCodeParser.Status status;
            if( null != (status = ScatStatusCodeParser.toResult(result)) )
                return status.isOk();

        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(name+"(:checkScatStatusCode:)",ex);
        }

        log.warn(name+" (:checkScatStatusCode:) had problem while checking against SCAT");
        return false;
    }

    private static String getMessage(String itemCode,String itemType, String userCard){
        final String message = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "  <soap:Body>\n" +
                "    <ScatCfcServicesCodes xmlns=\"http://tempuri.org/\">\n" +
                "      <Orden_Caso>" + itemCode +"</Orden_Caso>\n" +
                "      <tarjeta>" + userCard + "</tarjeta>\n" +
                "      <Tipo>" + itemType + "</Tipo>\n" +
                "    </ScatCfcServicesCodes>\n" +
                "  </soap:Body>\n" +
                "</soap:Envelope>";

        return message;
    }
}
