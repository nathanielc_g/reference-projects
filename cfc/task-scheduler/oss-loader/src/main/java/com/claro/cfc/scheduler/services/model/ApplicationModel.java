package com.claro.cfc.scheduler.services.model;

import com.claro.cfc.scheduler.services.model.util.HibernateHelper;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 7/25/2014.
 */
public final class ApplicationModel {

    private static final Logger log = Logger.getLogger(ApplicationModel.class);

    private static final String SCHEDULER_WAKEUP_FREQUENCY = "SELECT CONFIG_VALUE FROM app_config where state = 1 and config_key = 'SCHEDULER_WAKEUP_FREQUENCY'";
    private static final String ACTIVE_TASK_FILTER_QUERY = "SELECT CONFIG_VALUE FROM app_config where state = 1 and config_key = 'TASK_FILTER'";
    private static final String SMS_ALLOWED_CONTEXT_QUERY = "SELECT CONFIG_VALUE FROM app_config where state = 1 and config_key = 'SMS_ALLOWED_CONTEXT'";

    public static String getSchedulerWakeupFrequency () {
        Session session = HibernateHelper.getSessionFactory().openSession();
        try {

            SQLQuery sqlQuery = session.createSQLQuery(SCHEDULER_WAKEUP_FREQUENCY);
            sqlQuery.addScalar("CONFIG_VALUE");
            Object result = sqlQuery.uniqueResult();

            if (null == result || result.toString().isEmpty())
                return "";

            return result.toString();
        }catch(Exception ex){
            log.error("",ex);
        }finally {
            log.info("AppConfig Model Session Closed in getSchedulerWakeupFrequency!");
            if(session != null && session.isConnected()){
                session.flush();
                session.clear();
                session.close();
            }
        }
        return "";
    }

    public static boolean isTaskFilterEnable () {
        Session session = HibernateHelper.getSessionFactory().openSession();
        try {

            SQLQuery sqlQuery = session.createSQLQuery(ApplicationModel.ACTIVE_TASK_FILTER_QUERY);
            sqlQuery.addScalar("CONFIG_VALUE");
            Object result = sqlQuery.uniqueResult();

            if (null == result || result.toString().isEmpty())
                return false;

            return true;
        }catch(Exception ex){
            log.error("",ex);
        }finally {
            log.info("AppConfig Model Session Closed in isScatFilterEnable!");
            if(session != null && session.isConnected()){
                session.flush();
                session.clear();
                session.close();
            }
        }
        return false;
    }


    public static String getSMSContextAllowed() {
        Session session = HibernateHelper.getSessionFactory().openSession();
        try {

            SQLQuery sqlQuery = session.createSQLQuery(SMS_ALLOWED_CONTEXT_QUERY);
            sqlQuery.addScalar("CONFIG_VALUE");
            Object result = sqlQuery.uniqueResult();

            if (null == result || result.toString().isEmpty())
                return "";

            return result.toString();
        }catch(Exception ex){
            log.error("",ex);
        }finally {
            log.info("AppConfig Model Session Closed in getSMSContextAllowed!");
            if(session != null && session.isConnected()){
                session.flush();
                session.clear();
                session.close();
            }
        }
        return "";
    }
}
