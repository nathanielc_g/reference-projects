package com.claro.cfc.scheduler.services;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/6/2014.
 */
@Entity
@Table(name = "TASK_LOG")
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE,region = "tasks.log")
public class TaskLogEntity {
    private long taskLogId;
    private int success;
    private long taskId;
    private String message;
    private String outgoingXml;
    private String ingoingXml;
    private Timestamp created;
    private TaskResponseTypeEntity responseType;
    private String updatedAttrs;

    @Id
    @SequenceGenerator(name = "logs.sec_task_log",sequenceName = "SEC_TASK_LOG" )
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "logs.sec_task_log" )
    @Column(name = "TASK_LOG_ID")
    public long getTaskLogId() {
        return taskLogId;
    }

    public void setTaskLogId(long taskLogId) {
        this.taskLogId = taskLogId;
    }

    @Basic
    @Column(name = "OUTGOING_XML")
    public String getOutgoingXml() {
        return outgoingXml;
    }

    public void setOutgoingXml(String outgoingXml) {
        this.outgoingXml = outgoingXml;
    }

    @Basic
    @Column(name = "INGOING_XML")
    public String getIngoingXml() {
        return ingoingXml;
    }

    public void setIngoingXml(String ingoingXml) {
        this.ingoingXml = ingoingXml;
    }

    @Basic
    @Column(name = "CREATED")
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }


    @Basic
    @Column( name= "SUCCESS" )
    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    @Basic
    @Column( name= "MESSAGE" )
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Basic
    @Column( name= "TASK_ID" )
    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    @Basic
    @Column(name= "UPDATED_ATTRS", nullable = false)
    public String getUpdatedAttrs() {
        return updatedAttrs;
    }

    public void setUpdatedAttrs(String updatedAttrs) {
        this.updatedAttrs = updatedAttrs;
    }

    @Override
    public String toString() {
        return "TaskLogEntity{" +
                "taskLogId=" + taskLogId +
                ", success=" + success +
                ", taskId=" + taskId +
                ", message='" + message + '\'' +
                ", outgoingXml='" + outgoingXml + '\'' +
                ", ingoingXml='" + ingoingXml + '\'' +
                ", created=" + created +
                ", responseType=" + responseType +
                ", updatedAttrs='" + updatedAttrs + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskLogEntity)) return false;

        TaskLogEntity that = (TaskLogEntity) o;

        return taskLogId == that.taskLogId;
    }

    @Override
    public int hashCode() {
        return (int) (taskLogId ^ (taskLogId >>> 32));
    }

    @Fetch(FetchMode.SELECT)
    @ManyToOne
    @JoinColumn(name = "RESPONSE_TYPE", referencedColumnName = "RID", nullable = false)
    public TaskResponseTypeEntity getResponseType() {
        return responseType;
    }

    public void setResponseType(TaskResponseTypeEntity responseType) {
        this.responseType = responseType;
    }
}
