package com.claro.cfc.scheduler.util;

/**
 * Created by Jansel Valentin on 6/1/2016.
 **/
public final class FilterServices {

    private static final String PRODUCTION_SCAT_FILTER  = "http://ntpappsweb0005/ProceSCAT/WS_SCAT.asmx?op=ScatCfcServicesCodes";
    private static final String DEVELOPMENT_SCAT_FILTER = "http://nttappsweb0003/ProceSCAT/WS_SCAT.asmx?op=ScatCfcServicesCodes";

    public static final String scat(Environment env){
        if(Environment.PRODUCTION == env)
                return PRODUCTION_SCAT_FILTER;
        return DEVELOPMENT_SCAT_FILTER;
    }
}
