package com.claro.cfc.scheduler.observer;

import com.claro.cfc.cema.CEMAClient;
import com.claro.cfc.cema.SMSClient;
import com.claro.cfc.cema.message.SMSMessage;
import com.claro.cfc.scheduler.services.TaskStatusEnum;
import com.claro.cfc.scheduler.services.model.NotificationModel;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Nathaniel Calderon on 7/11/2017.
 */
public class TaskBroadcaster implements TaskObserver {
    private final static Logger log = Logger.getLogger(TaskBroadcaster.class);
    private SMSClient sender;
    private ConcurrentHashMap<Long, String> messages =  new ConcurrentHashMap<Long, String>();
    public TaskBroadcaster() {

    }

    public void sendResults(){
        if(sender == null)
            sender = new CEMAClient();
        if (messages == null || messages.size()<=0) {
            log.info("No messages to be send.");
            return;
        }
        log.info(String.format("Preparing to send %d messages via SMS", messages.size()));
        List<SMSMessage> messagesToSend = NotificationModel.parse(this.messages);
        log.info("Dispatching SMS messages...\n");
        sender.sendSMSBulk(messagesToSend);
        /*NotificationModel.bulkUpdateToSentStatus(messages);*/
    }

    public void update(long actionId, TaskStatusEnum status, String message) {
        try {
            if(TaskStatusEnum.PROCESSED == status)
                messages.putIfAbsent(actionId, "PROCESADO");
            else
                messages.put(actionId, "FALLIDO; " + message);
        }catch (Exception ex){
            log.error(String.format("Error trying to put a message for action:status: %d:%d", actionId, status));
            ex.printStackTrace();
        }
    }

    public void cleanup(){
        sender = null;
        messages.clear();
        messages = null;
    }
}
