package com.claro.cfc.scheduler.services.model;

import com.claro.cfc.cema.message.SMSMessage;
import com.claro.cfc.cema.message.SimpleSMS;
import com.claro.cfc.scheduler.services.model.util.HibernateHelper;
import org.apache.log4j.Logger;
import org.hibernate.*;
import java.util.*;

/**
 * Created by Nathaniel Calderon on 7/17/2017.
 */
public class NotificationModel {
    private static final Logger log = Logger.getLogger(NotificationModel.class);
    private static final String queryPhoneAndActionType = "SELECT a.CODE, a.FORM_TYPE, u.PHONE_NUMBER FROM ACTIONS a\n" +
                                    "inner join USERS u\n" +
                                    "  on u.user_id = a.user_id\n" +
                                    "inner join terminal t\n" +
                                    "  on t.IMEI = u.IMEI\n" +
                                    "where\n" +
                                    "  action_id = :action_id and rownum = 1";
    /*private static final String queryUnsetNotification = "select NOTIF_ID, SEND_TO, MESSAGE from notification where notif_type = 1 and status = 0 and notify_via = 'SMS' and created >= trunc(SYSDATE)";*/
    private static final String bulkUpdate = "update notification set status = 1 where notif_id in (:notif_list)";
    private static final int BATCH_MAX_RESULT = 500;

    /*public static List<SMSMessage> getUnsentMessageResults() {
        final StatelessSession session = HibernateHelper.getSessionFactory().openStatelessSession();
        try {

            Date yesterday = CalendarHelper.removeTime(CalendarHelper.yesterday().getTime());

            log.info("\n\n************* Getting Unsent Messages from " + yesterday + " with status " + TaskStatusEnum.PENDING.id() + " *************\n");
            SQLQuery query = session.createSQLQuery(queryUnsetNotification);


            query.setReadOnly(true).setCacheable(false).setFetchSize(BATCH_MAX_RESULT);

            ScrollableResults results = query.scroll(ScrollMode.FORWARD_ONLY);

            List<SMSMessage> messages = new LinkedList<SMSMessage>();

            while (null != results && results.next()) {
                Object[] row = results.get();

                long notif_id = Long.valueOf(row[0] + "");
                String sendTo   = row[1] + "";
                String message = row[2] + "";

                messages.add(new NotificationMessage(notif_id, sendTo, message));
            }
            return messages;
        } catch (HibernateException ex) {
            log.error("Error at getUnsentMessageResults call ", ex);
        } finally {
            if (null != session)
                session.close();
        }
        return Collections.EMPTY_LIST;
    }*/

    public static List<SMSMessage> parse(Map<Long, String> messageMap) {
        final StatelessSession session = HibernateHelper.getSessionFactory().openStatelessSession();
        try {
            log.info("\n\n************* Getting ActionType and UserPhone for Messages  *************\n");
            Set<Map.Entry<Long, String>> messageSet = messageMap.entrySet();
            List<SMSMessage> messages = new LinkedList<SMSMessage>();
            for(Map.Entry<Long,String> entry: messageSet){
                long actionId = entry.getKey();
                String messageResult = entry.getValue();
                SQLQuery query = session.createSQLQuery(queryPhoneAndActionType);
                query.setReadOnly(true).setCacheable(false).setFetchSize(BATCH_MAX_RESULT);
                query.setLong("action_id", actionId);
                ScrollableResults results = query.scroll(ScrollMode.FORWARD_ONLY);
                while (null != results && results.next()) {
                    Object[] row = results.get();
                    String code = row[0] + "";
                    String actionType = row[1] + "";
                    String phoneNumber = row[2] + "";
                    String smsMessage =  code + " - " + actionType + ": " + messageResult;
                    messages.add(new SimpleSMS(phoneNumber, smsMessage));
                }
                results.close();
                query = null;

            }
            return messages;
        } catch (HibernateException ex) {
            log.error("Error at parse call ", ex);
        } finally {
            if (null != session)
                session.close();
        }
        return Collections.EMPTY_LIST;

    }

    /*public static boolean bulkUpdateToSentStatus(List<SMSMessage> messages) {
        final Session session = HibernateHelper.getSessionFactory().openSession();
        boolean updated = true;

        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            Query query = session.createSQLQuery(bulkUpdate);
            query.setParameterList("notif_list", transformToIdsList(messages), LongType.INSTANCE);
            query.executeUpdate();

            tx.commit();
        } catch (HibernateException ex) {
            updated = false;
            log.error("Error at bulkUpdateToSentStatu call", ex);
            if (null != tx && !tx.wasCommitted()) {
                tx.rollback();
            }
        } finally {
            if (session.isOpen() || session.isConnected()) {
                session.flush();
                session.close();
            }
        }

        return updated;
    }*/

    /*private static List<Long> transformToIdsList(List<SMSMessage> messages){
        List<Long> ids = new ArrayList<Long>(messages.size());
        for(NotificationMessage smsMessage: messages)
            ids.add(smsMessage.getNotificationId());
        return ids;
    }*/
}
