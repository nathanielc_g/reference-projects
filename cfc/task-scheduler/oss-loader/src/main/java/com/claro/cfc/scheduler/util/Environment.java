package com.claro.cfc.scheduler.util;

/**
 * Created by Jansel R. Abreu (jrodr) on 6/6/2014.
 */
public enum Environment {
    DEVELOPMENT{
        @Override
        public String providerHost() {
            return "t3://localhost:7001";
        }
    },
    STAGING{
        @Override
        public String providerHost() {
            return "t3://172.27.17.166:8020";
        }
    },
    PRODUCTION{
        @Override
        public String providerHost() {
            return "t3://172.27.6.201:9020";
        }
    };

    public abstract String providerHost();
}
