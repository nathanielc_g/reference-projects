package com.claro.cfc.scheduler.manager;

import com.claro.cfc.scheduler.Scheduler;
import com.claro.cfc.scheduler.SchedulerMonitor;
import com.claro.cfc.scheduler.SchedulerState;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 12/30/2014.
 */
public class SchedulerMonitorServlet extends HttpServlet {

    private static final Logger log = Logger.getLogger(SchedulerMonitorServlet.class.getSimpleName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if ("/health".equals(req.getPathInfo())) {
            writeHealthResponse(resp);
        }
        if ("/reloadCache".equals(req.getPathInfo())) {
            writeReloadResponse(resp);
        }
    }

    private void writeReloadResponse(HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setStatus(HttpServletResponse.SC_OK);

        final Scheduler scheduler = SchedulerManager.getScheduler(false);
        boolean setToReload = false;
        String message = "Cannot reload config values. Not exist scheduler instance or is running.";
        if (scheduler != null) {
            scheduler.reloadCache();
            message = "SchedulerContext was set to reload cache values on next run.";
            setToReload = true;
        }

        StringBuilder builder = new StringBuilder();
        builder.append("{")
                .append("\"ok\":\"").append(setToReload).append("\"").append(",")
                .append("\"message\":\"").append(message).append("\"");
        builder.append("}");
        log.info(builder.toString());
        resp.getWriter().println(builder.toString());
        builder.setLength(0);
    }

    private void writeHealthResponse(HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setStatus(HttpServletResponse.SC_OK);

        StatusResponse status = StatusResponse.RUNNING;
        String wakeup = "--unknown";
        try {
            final Scheduler scheduler = SchedulerManager.getScheduler(true);

            if (null == scheduler)
                status = StatusResponse.INACTIVE;
            else {
                final SchedulerMonitor monitor = scheduler.getContext().getMonitor();
                final SchedulerState state = monitor.getState();

                if (null != state && state.isRunning()) {
                    if (null != state.getWakeupTime())
                        wakeup = state.getWakeupTime().toString();
                } else {
                    status = StatusResponse.STOPPED;
                }

            }
        } catch (Exception ex) {
            log.error(ex);
            status = StatusResponse.SHUTDOWN;
        }

        log.info(status.message());

        StringBuilder builder = new StringBuilder();
        builder.append("{")
                .append("\"status\":\"").append(status.status()).append("\"").append(",")
                .append("\"code\":\"").append(status.code()).append("\"").append(",")
                .append("\"wakeup-at\":\"").append(wakeup).append("\"").append(",")
                .append("\"message\":\"").append(status.message()).append("\"");
        builder.append("}");

        resp.getWriter().println(builder.toString());
        builder.setLength(0);
    }
}
