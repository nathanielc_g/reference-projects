package com.claro.cfc.scheduler.manager;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 12/30/2014.
 */
public enum StatusResponse {

    RUNNING{
        @Override
        String code() {
            return "SMM001";
        }
        @Override
        String status() {
            return "RUNNING";
        }

        @Override
        String message() {
            return "OSS Job Scheduler is running";
        }
    },
    STOPPED{
        @Override
        String code() {
            return "SSM002";
        }
        @Override
        String status() {
            return "STOPPED";
        }

        @Override
        String message() {
            return "OSS Job Scheduler was stopped";
        }
    },
    SHUTDOWN{
        @Override
        String code() {
            return "SSM004";
        }
        @Override
        String status() {
            return "SHUTDOWN";
        }

        @Override
        String message() {
            return "OSS Job scheduler is in shutdown state";
        }
    },
    INACTIVE{
        @Override
        String code() {
            return "SSM004";
        }
        @Override
        String status() {
            return "INACTIVE";
        }

        @Override
        String message() {
            return "OSS Job Scheduler appear not installed";
        }
    },
    ;
    abstract String status();
    abstract String code();
    abstract String message();
}
