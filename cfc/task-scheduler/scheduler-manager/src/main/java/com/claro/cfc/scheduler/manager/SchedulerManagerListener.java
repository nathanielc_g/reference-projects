
package com.claro.cfc.scheduler.manager;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 9/26/2014.
 */
public class SchedulerManagerListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.err.println("\n\n************ Initializing Scheduler ************\n");
        SchedulerManager.getScheduler(false).start();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.err.println("\n\n************ Destroying Scheduler ************\n");
        SchedulerManager.getScheduler(false).stop();
    }
}
