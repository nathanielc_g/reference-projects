package com.claro.cfc.scheduler.manager;

import com.claro.cfc.scheduler.Scheduler;
import com.claro.cfc.scheduler.manager.util.Environment;
import com.claro.cfc.scheduler.manager.util.Environments;
import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.Properties;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 12/30/2014.
 */
public final class SchedulerManager {

    private static final Logger log = Logger.getLogger( SchedulerManager.class );
    private static final Environment env = Environments.getDefault();

    private static Scheduler scheduler;

    public static Scheduler getScheduler(boolean tryForceCreateNew){

        if( null != scheduler && !tryForceCreateNew )
            return scheduler;

        log.info( "Configured Environment: "+env );
        Properties props = new Properties();

        log.info( "Configuring host: "+env.providerHost() );
        props.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory" );
        props.put(Context.PROVIDER_URL, env.providerHost() );


        InitialContext context;
        try{
            context = new InitialContext(props);
        }catch( Exception ex) {
            log.error( ex );
            throw new IllegalStateException(ex);
        }

        try {
            Object aS = context.lookup("TaskScheduler#com.claro.cfc.scheduler.Scheduler");
            if( aS instanceof Scheduler){
                return scheduler = (Scheduler)aS;
            }else{
                throw new IllegalArgumentException("Registered Scheduler is not instance of com.claro.cfc.scheduler.Scheduler");
            }
        }catch ( Exception ex) {
            throw  new IllegalStateException( ex );
        }
    }
}
