package com.claro.cfc.reports;

import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.OutputStream;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Jansel R. Abreu (Vanwolf) on 10/3/2014.
 */
public class TaskReportServlet extends HttpServlet{
    private static final Logger log = Logger.getLogger( TaskReportServlet.class);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        res.setContentType("application/octet-stream");
        res.setHeader("Content-Disposition", "attachment;filename=tasks-report.csv");

        log.info( "Getting task  reports" );

        List<String> lines = Collections.EMPTY_LIST;//getReportLines();
        OutputStream out = res.getOutputStream();

        for( String line : lines )
             out.write( line.getBytes() );
    }

    private List<String> getReportLines(){
/*
        final String stringConnection = "jdbc:oracle:thin:@//172.27.6.78:1521/appceltec";
        final String user = "APPCAPFAC";
        final String pass = "NRe58TMLdk";

        try{
            Class.forName( "oracle.jdbc.driver.OracleDriver" );
        }catch ( ClassNotFoundException e){
            log.error( e );
			e.printStackTrace();
        }*/

        List<String> result = new ArrayList<String>();
        StringBuilder collect = new StringBuilder();
        injectHeaders(collect);
        result.add(collect.toString());


        try {

            Properties props = new Properties();

            props.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory" );
//            props.put(Context.PROVIDER_URL, "t3://localhost:7001" );
            /**
             *             Production
             */
             //props.put(Context.PROVIDER_URL, "t3://172.27.6.201:9020" );
            /**
             *             Staging
             */
            props.put(Context.PROVIDER_URL, "t3://172.27.17.166:8020" );

            Context initContext = new InitialContext(props);
            DataSource ds = (DataSource) initContext.lookup("cfc_web_datasource");
            Connection con = ds.getConnection();

            Statement stm = con.createStatement();
            ResultSet set = stm.executeQuery( query() );

            if( null != set ){
                while( set.next() ){
                    collect.setLength(0);

                    String di = set.getString( "CODE" );
                    String ft = set.getString( "FORM_TYPE" );
                    String status = set.getString( "STATUS" );
                    String created = set.getString( "CREATED" );
                    String modified = set.getString( "MODIFIED" );
                    String response = set.getString( "RTYPE" );
                    String success = set.getString( "SUCCESS" );
                    String message = set.getString( "MESSAGE" );

                    collect.append( di ).append( "," )
                           .append( ft ).append( "," )
                           .append( status ).append( "," )
                           .append( created ).append( "," )
                           .append( modified ).append( "," )
                           .append( response ).append( "," )
                           .append( success ).append( "," )
                           .append( message ).append("\r\n");

                   result.add( collect.toString() );
                }
            }

            if( null != con && !con.isClosed() ){
                con.close();
            }

        }catch( Exception ex ){
            log.error( ex );
			ex.printStackTrace();
			throw  new RuntimeException( ex );
        }
        return result;
    }



    private void injectHeaders(StringBuilder builder) {
        if (null == builder)
            return;
        builder.append("DISPATCH_ITEM").append(",").append("FORM_TYPE").append(",").append("STATUS")
                .append(",").append("TASK_CREATED").append(",").append("TASK_MODIFIED").append(",").append("RESPONSE_TYPE")
                .append(",").append("SUCCESS").append(",").append("RESPONSE_MESSAGE").append("\r\n");
    }


    private String query(){
//        return "SELECT AC.CODE, AC.FORM_TYPE, TS.STATUS, T.CREATED, T.MODIFIED, TRT.RTYPE, TL.SUCCESS, TL.MESSAGE FROM TASK T JOIN ACTIONS AC ON T.ACTION_ID = AC.ACTION_ID LEFT JOIN TASK_LOG TL ON T.TASK_ID = TL.TASK_ID JOIN TASK_STATUS TS ON T.STATUS = TS.STATUS_ID LEFT JOIN TASK_RESPONSE_TYPE TRT ON TL.RESPONSE_TYPE = TRT.RID";
        return queryFor3DaysBefore();
    }

    private String queryFor3DaysBefore(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - 5);

        return "SELECT AC.CODE, AC.FORM_TYPE, TS.STATUS, T.CREATED, T.MODIFIED, TRT.RTYPE, TL.SUCCESS, TL.MESSAGE FROM TASK T JOIN ACTIONS AC ON T.ACTION_ID = AC.ACTION_ID LEFT JOIN TASK_LOG TL ON T.TASK_ID = TL.TASK_ID JOIN TASK_STATUS TS ON T.STATUS = TS.STATUS_ID LEFT JOIN TASK_RESPONSE_TYPE TRT ON TL.RESPONSE_TYPE = TRT.RID WHERE T.CREATED >= TO_TIMESTAMP('"+formatter.format(cal.getTime())+"')";
    }
}
